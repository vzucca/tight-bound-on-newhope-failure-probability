This repository contains the code used to derive the results of: Tight bound on NewHope failure probability (https://eprint.iacr.org/2019/1451.pdf) by Thomas Plantard, Arnaud Sipasseuth, Willy Susilo and Vincent Zucca. It is composed of two folders containing:

- the C++ script used to compute the decapsulation failure probability of NewHope which requires to have the gmp (https://gmplib.org/) and mpfr (https://www.mpfr.org/) libraries installed. (folder code_probability).

- the C code used to compute the polynomial products on alternative NewHope parameters with both the reference and optimized implementations (folder code_product)

If you use any of these scripts for your work, please cite the aforementionned paper.

Licence: public domain
