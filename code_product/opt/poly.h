#ifndef POLY
#define POLY

#include <stdint.h>

#include "params.h"

typedef struct{
  int16_t __attribute__((aligned(32))) coeffs[N];
} poly;

/* Generate random poly modulo Q*/
void poly_rand(poly* a, unsigned int Q){

  for(int i = 0 ; i < N ; i++)
    a->coeffs[i] = rand()%Q;
}

void poly_print(poly* a, char* name, unsigned int n){

  unsigned int m = n>>4;

  printf("%s = [",name);
  for(unsigned int i = 0 ; i < m-1 ; i++){
    for(unsigned int j = 0 ; j < 16 ; j++)
      printf("%*d, ",5, a->coeffs[i*16+j]);
    printf("\\\n     ");
  }

  for(unsigned int j = 0 ; j < 15 ; j++)
    printf("%*d, ", 5, a->coeffs[n-16+j]);

  printf("%*d ];\n\n", 5, a->coeffs[n-1]);
}

/* Arithmetic functions */
void poly_add(int16_t *c, const int16_t *a, const int16_t* b, unsigned int n){

  for(unsigned int i = 0 ; i < n ; i++)
    c[i] = a[i] + b[i];
}

void poly_sub(int16_t *c, const int16_t *a, const int16_t* b, unsigned int n){

  for(unsigned int i = 0 ; i < n ; i++)
    c[i] = a[i] - b[i];
}

void poly_sub_cst(int16_t *c, const int16_t *a, const int16_t b, unsigned int n){

  for(unsigned int i = 0 ; i < n ; i++)
    c[i] = a[i] - b;
}


void poly_subq(int16_t *c, const int16_t *a, const int16_t* b, unsigned int n, unsigned int Q){

  for(unsigned int i = 0 ; i < n ; i++){
    c[i] = a[i] - b[i];
    c[i] += (c[i]>>15) & Q;
  }
}

uint16_t poly_max(int16_t *a, unsigned int n){
  uint16_t res = abs(a[0]);
  for(unsigned int i = 1 ; i < n ; i++){
    if(abs(a[i]) > res)
      res = abs(a[i]);
  }
  return res;
}

int poly_are_equals(poly *a, poly *b){
  for(unsigned int i = 0 ; i < N ; i++)
    if(a->coeffs[i] != b->coeffs[i])
      return 0;
  return 1;
}

void poly_copy(poly *acpy, poly *a){
  for(int i = 0 ; i < N ; i++)
    acpy->coeffs[i] = a->coeffs[i];
}

#endif
