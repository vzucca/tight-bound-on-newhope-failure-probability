.include "asm_macros.h"

.global ntt_32_512_opt
ntt_32_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
vmovdqa		_low_mask_32(%rip),%ymm2

#level 0

#zetas
vmovdqa		(%rsi),%ymm3

#first round
#load

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#second round

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#third round

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#fourth round

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
save_64_coeffs 4,5,6,7

add		$256,%rdi

# level 0 tested: all good!

#level 1

#zetas
# vmovdqa		32(%rsi),%ymm3
vmovdqa		64(%rsi),%ymm3

#first round

#load

load_64_coeffs 4,5,6,7
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#second round

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

sub		$384,%rdi

#third round
#zetas
vmovdqa		32(%rsi),%ymm3

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#fourth round

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 8,9,10,11 rdi,256,288,320,352

# level 1 tested: all good!

#first round 128-coeffs (level 2-4)

#level 2
#zetas
vmovdqa		96(%rsi),%ymm3

# load_128_coeffs 4,5,6,7,8,9,10,11
load_64_coeffs 8,9,10,11 rdi,128,160,192,224
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3
#zetas
load_32_coeffs 15,3 rsi,224,256

ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 4
#zetas
load_64_coeffs 13,14,15,3 rsi,480,512,544,576

ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

add		$256,%rdi

#second round 128-coeffs (level 2-4)

#level 2
#zetas
vmovdqa		128(%rsi),%ymm3

load_128_coeffs 4,5,6,7,8,9,10,11
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3
#zetas
load_32_coeffs 15,3 rsi,288,320

ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 4
#zetas
load_64_coeffs 13,14,15,3 rsi,608,640,672,704

ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

add		$256,%rdi

#third round 128-coeffs (level 2-3)

#level 2
#zetas
vmovdqa		160(%rsi),%ymm3

load_128_coeffs 4,5,6,7,8,9,10,11
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3
#zetas
load_32_coeffs 15,3 rsi,352,384

ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 4
#zetas
load_64_coeffs 13,14,15,3 rsi,736,768,800,832

ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

add		$256,%rdi

#fourth round 128-coeffs (level 2-3)

#level 2
#zetas
vmovdqa		192(%rsi),%ymm3

load_128_coeffs 4,5,6,7,8,9,10,11
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3
#zetas
load_32_coeffs 15,3 rsi,416,448

ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 4
#zetas
load_64_coeffs 13,14,15,3 rsi,864,896,928,960

ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

add 	%r11,%rsp

ret
