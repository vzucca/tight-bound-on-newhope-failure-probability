.include "asm_macros.h"
.include "asm_shuffle.h"
.include "shift_macros.h"

################################################################################

.global poly_shift_2_modq32_64_opt
poly_shift_2_modq32_64_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_lowdword(%rip),%ymm2

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1

shift_multiply_2_64_q32

add 	%r11,%rsp
ret

################################################################################

.global poly_shift_4_modq32_128_opt
poly_shift_4_modq32_128_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1

/*do it 8*64=512 times*/
shift_multiply_4_64
add   $128,%rdi
add   $32,%rsi
shift_multiply_4_64


add 	%r11,%rsp
ret

################################################################################

.global poly_shift_8_modq32_256_opt
poly_shift_8_modq32_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1

/*do it 2*128=256 times*/
shift_multiply_8_128
add   $256,%rdi
add   $32,%rsi
shift_multiply_8_128


add 	%r11,%rsp
ret

################################################################################

.global poly_shift_16_modq32_512_opt
poly_shift_16_modq32_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1

# load the values of ntt_Y
vmovdqa  (%rsi),%ymm15

shift_multiply_16_256
add   $512,%rdi
add   $32,%rsi
vmovdqa  (%rsi),%ymm15
shift_multiply_16_256

add 	%r11,%rsp
ret

################################################################################

.global sub_cst1q_q32_512
sub_cst1q_q32_512:
mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32(%rip),%ymm0

load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8
add  $256,%rdi
load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8
add  $256,%rdi

load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8
add  $256,%rdi
load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8

add 	%r11,%rsp
ret

################################################################################

.global sub_cst4q_q32_128
sub_cst4q_q32_128:
mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16x4Q32(%rip),%ymm0

load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8

add 	%r11,%rsp
ret

################################################################################

.global sub_cst4q_q32_64
sub_cst4q_q32_64:
mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16x4Q32(%rip),%ymm0

load_64_coeffs 1,2,3,4
sub_64_cst 1,2,3,4
save_64_coeffs 1,2,3,4

add 	%r11,%rsp
ret
