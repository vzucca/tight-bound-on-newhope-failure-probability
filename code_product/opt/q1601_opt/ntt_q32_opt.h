#ifndef NTT32_OPT
#define NTT32_OPT

#include <stdio.h>

/* ################ NTT ################ */
/*WARNING CHECK BARRETT REDUCTIONS*/
#include "const_32_opt.h"

void ntt_32_128_opt(int16_t *inout, const int16_t *zetas) asm("ntt_32_128_opt");
void invntt_32_128_opt(int16_t *inout, const int16_t *zetas) asm("invntt_32_128_opt");

void ntt_32_256_opt(int16_t *inout, const int16_t *zetas) asm("ntt_32_256_opt");
void invntt_32_256_opt(int16_t *inout, const int16_t *zetas) asm("invntt_32_256_opt");

void ntt_32_512_opt(int16_t *inout, const int16_t *zetas) asm("ntt_32_512_opt");
void invntt_32_512_opt(int16_t *inout, const int16_t *zetas) asm("invntt_32_512_opt");

void ntt_32_1024_opt(int16_t *inout, const int16_t *zetas) asm("ntt_32_1024_opt");
void invntt_32_1024_opt(int16_t *inout, const int16_t *zetas) asm("invntt_32_1024_opt");
void invntt_32_16x64_opt(int16_t *inout, const int16_t *zetas) asm("invntt_32_16x64_opt");

#endif
