.include "asm_macros.h"

.macro block_64
load_64_coeffs 1,2,3,4
signed_barrett_64_coeffs_2 1,2,3,4 0,11,9,6
save_64_coeffs 1,2,3,4
.endm

.global poly_mers_q32_128
poly_mers_q32_128:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_low_mask_32(%rip),%ymm0

/*repeat 2 times*/
block_64
add  $128,%rdi
block_64

add 	%r11,%rsp

ret

.global poly_mers_q32_64
poly_mers_q32_64:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_low_mask_32(%rip),%ymm0

block_64

add 	%r11,%rsp

ret
