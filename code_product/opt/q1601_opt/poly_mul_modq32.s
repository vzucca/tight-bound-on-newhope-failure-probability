.include "asm_macros.h"
.include "asm_shuffle.h"
.include "product_macros.h"

################################################################################

.global poly_mul_modq32_1024_opt
poly_mul_modq32_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
# TODO shift everything in product deg_16_16 to put ymm2 and not ymm6
vmovdqa		_lowdword(%rip),%ymm6

prod_deg_32_512
add  $64,%rsi
add  $64,%rdi
add  $32,%rdx
prod_deg_32_512

add 	%r11,%rsp
ret

################################################################################

.global poly_mul_modq32_512_opt
poly_mul_modq32_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
# TODO shift everything in product deg_16_16 to put ymm2 and not ymm6
vmovdqa		_lowdword(%rip),%ymm6

prod_deg_16_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
prod_deg_16_256

add 	%r11,%rsp
ret

################################################################################

.global poly_mul_modq32_256_opt
poly_mul_modq32_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_8_256

add 	%r11,%rsp
ret

################################################################################

.global poly_mul_modq32_128_opt
poly_mul_modq32_128_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_4_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_64

add 	%r11,%rsp
ret

################################################################################

.global poly_mul_modq32_64_opt
poly_mul_modq32_64_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_1_64

add 	%r11,%rsp
ret
