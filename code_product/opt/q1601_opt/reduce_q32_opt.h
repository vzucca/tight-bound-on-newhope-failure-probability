#ifndef REDUCE32_OPT
#define REDUCE32_OPT

// #include <stdio.h>
#include <stdint.h>
#include "params.h"

/* ############################################################################################## */
/* ############################### Functions for Q256 = 12289 ################################## */
/* ############################################################################################## */

/* Barrett */
void double_poly_barrettq32_64_opt(int16_t *a,int16_t *b) asm("poly_barrett_q32_2x64");
void poly_barrettq32_128_opt(int16_t *a) asm("poly_barrett_q32_128");
void poly_barrettq32_256_opt(int16_t *a) asm("poly_barrett_q32_256");
void poly_barrettq32_512_opt(int16_t *a) asm("poly_barrett_q32_512");
void poly_barrettq32_1024_opt(int16_t *a) asm("poly_barrett_q32_1024");

/* Mersenne */
void poly_mersq32_64_opt(int16_t* a) asm("poly_mers_q32_64");
void poly_mersq32_128_opt(int16_t* a) asm("poly_mers_q32_128");

/* classic multiplication a <- a * b */
void poly_mul_2_modq32_64_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq32_64_opt");
void poly_mul_4_modq32_128_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq32_128_opt");
void poly_mul_8_modq32_256_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq32_256_opt");
void poly_mul_16_modq32_512_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq32_512_opt");
void poly_mul_32_modq32_1024_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq32_1024_opt");

/* classic shift and multiply by ntt_Y*/
void poly_shift_2_modq32_64_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_2_modq32_64_opt");
void poly_shift_4_modq32_128_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_4_modq32_128_opt");
void poly_shift_8_modq32_256_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_8_modq32_256_opt");
void poly_shift_16_modq32_512_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_16_modq32_512_opt");

/* sub cst */
void poly_sub1q_cst_q32_512(int16_t*a) asm ("sub_cst1q_q32_512");
void poly_sub4q_cst_q32_128(int16_t*a) asm ("sub_cst4q_q32_128");
void poly_sub4q_cst_q32_64(int16_t*a) asm ("sub_cst4q_q32_64");

#endif
