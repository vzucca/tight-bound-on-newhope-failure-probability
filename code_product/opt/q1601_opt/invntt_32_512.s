.include "asm_macros.h"
#WARNING please check Barretts
.global invntt_32_512_opt
invntt_32_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
vmovdqa		_16xv_32(%rip),%ymm2

#level 0-2

#first round block(1-2)

load_128_coeffs 3,4,5,6,7,8,9,10
#level 0-1
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,0,32,64,96
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,512,544

#level 2
vmovdqa		768(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

#second round block(3-4)

load_128_coeffs 3,4,5,6,7,8,9,10
#level 0-1
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,128,160,192,224
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,576,608

#level 2
vmovdqa		800(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#first pack of 256 (1-4, currently block 2 with block 4)

#level 3

#zetas
vmovdqa		896(%rsi),%ymm11

load_64_coeffs 3,4,5,6
# preloaded from previous step
# vmovdqa		256(%rdi),%ymm7
# vmovdqa		288(%rdi),%ymm8
# vmovdqa		320(%rdi),%ymm9
# vmovdqa		352(%rdi),%ymm10
invntt_update 3,4,5,6,7,8,9,10
mul_lh_64 11,11,11,11,12,13,14,15,7,8,9,10,12,13,14,15
montgomery_reduce_64 7,8,9,10,12,13,14,15,7,8,9,10

# signed_montgomery_64x64 11,11,11,11,7,8,9,10

#store
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#second part of first pack of 256 (block 1 with block 3)

#load
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_update 3,4,5,6,7,8,9,10
# barrett at level 3 after update on the add sum
gen_barrett_32_coeffs 3,4 2,1,9,7,8
mul_lh_64 11,11,11,11,12,13,14,15,7,8,9,10,12,13,14,15
montgomery_reduce_64 7,8,9,10,12,13,14,15,7,8,9,10

#store
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

add		$512,%rdi

#second block of 256 coeffs (5-8)

#level 0-2

#first round block(5-6)
load_128_coeffs 3,4,5,6,7,8,9,10
#level 0-1
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,256,288,320,352
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,640,672
#level 2
#zetas
vmovdqa		832(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

#second round block(7-8)
#load
load_128_coeffs 3,4,5,6,7,8,9,10
#level 0-1
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,384,416,448,480
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,704,736
#level 2
vmovdqa		864(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#second pack of 256 (5-8, currently block 6 with block 8)

#level 3

#zetas
vmovdqa		928(%rsi),%ymm11

#load
load_64_coeffs 3,4,5,6
# preloaded from previous step
# vmovdqa		256(%rdi),%ymm7
# vmovdqa		288(%rdi),%ymm8
# vmovdqa		320(%rdi),%ymm9
# vmovdqa		352(%rdi),%ymm10
invntt_update 3,4,5,6,7,8,9,10
mul_lh_64 11,11,11,11,12,13,14,15,7,8,9,10,12,13,14,15
montgomery_reduce_64 7,8,9,10,12,13,14,15,7,8,9,10

#store
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#second part of second pack of 256 (block 5 with block 7)

#load
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_update 3,4,5,6,7,8,9,10
mul_lh_64 11,11,11,11,12,13,14,15,7,8,9,10,12,13,14,15
montgomery_reduce_64 7,8,9,10,12,13,14,15,7,8,9,10

#store
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

# for simplicity goes back to 0
sub		$512,%rdi

#level 4, final level

#f
vmovdqa		_F32(%rip),%ymm2
vmovdqa		960(%rsi),%ymm3

#first block

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_butterfly_1 4,5,6,7,8,9,10,11,3

#correction +q if neg ou rien
add_q_ifneg 8,9,10,11

signed_montgomery_64x64 2,2,2,2,4,5,6,7

#correction +q if neg ou rien
add_q_ifneg 4,5,6,7

save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#second block

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_butterfly_1 4,5,6,7,8,9,10,11,3

#correction +q if neg ou rien
add_q_ifneg 8,9,10,11

signed_montgomery_64x64 2,2,2,2,4,5,6,7

#correction +q if neg ou rien
add_q_ifneg 4,5,6,7

save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#third block

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_butterfly_1 4,5,6,7,8,9,10,11,3

#correction +q if neg ou rien
add_q_ifneg 8,9,10,11

signed_montgomery_64x64 2,2,2,2,4,5,6,7

#correction +q if neg ou rien
add_q_ifneg 4,5,6,7

save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#fourth block

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_butterfly_1 4,5,6,7,8,9,10,11,3

#correction +q if neg ou rien
add_q_ifneg 8,9,10,11

signed_montgomery_64x64 2,2,2,2,4,5,6,7

#correction +q if neg ou rien
add_q_ifneg 4,5,6,7

save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add 	%r11,%rsp

ret
