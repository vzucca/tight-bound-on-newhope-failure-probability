.include "asm_macros.h"
#WARNING please check Barretts
.global invntt_32_16x64_opt
invntt_32_16x64_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ32INV(%rip),%ymm0
vmovdqa		_16xQ32(%rip),%ymm1
vmovdqa		_16xv_32(%rip),%ymm2

#first half (1-8/16 packs of 128)

#first quarter (1-4)
#level (0-1)

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,0,32
vmovdqa		512(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,64,96
vmovdqa		544(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 2

vmovdqa		768(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

add		$512,%rdi

#second quarter (5-8)

#level (0-1)

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,128,160
vmovdqa		576(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,192,224
vmovdqa		608(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 2

vmovdqa		800(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 7,8,9,10 rdi,256,288,320,352

sub		$512,%rdi

#level 3

vmovdqa		896(%rsi),%ymm11
# load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
load_64_coeffs 7,8,9,10
invntt_butterfly_1 7,8,9,10,3,4,5,6,11
gen_barrett_64_coeffs 7,8,9,10 2,1,9
save_128_coeffs 7,8,9,10,3,4,5,6 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608

add		$640,%rdi

#second half (9-16/16 packs of 128)

#third quarter (9-12)
#level (0-1)

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,256,288
vmovdqa		640(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,320,352
vmovdqa		672(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 2

vmovdqa		832(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

add		$512,%rdi

#fourth quarter (13-16)

#level (0-1)

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,384,416
vmovdqa		704(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,448,480
vmovdqa		736(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 2

vmovdqa		864(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 7,8,9,10 rdi,256,288,320,352

sub		$512,%rdi

#level 3

vmovdqa		928(%rsi),%ymm11
# load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
load_64_coeffs 7,8,9,10
invntt_butterfly_1 7,8,9,10,3,4,5,6,11
save_128_coeffs 7,8,9,10,3,4,5,6 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
save_64_coeffs 3,4,5,6

sub		$512,%rdi

#level 4

#f/zeta
vmovdqa		_F32(%rip),%ymm2
vmovdqa		960(%rsi),%ymm11

load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120


add 	%r11,%rsp

ret
