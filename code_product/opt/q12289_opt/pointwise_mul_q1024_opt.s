.include "asm_macros.h"

.macro block_64
load_64_coeffs 2,3,4,5 rdx
load_64_coeffs 6,7,8,9 rsi
signed_montgomery_64x64 2,3,4,5,6,7,8,9
save_64_coeffs 6,7,8,9
.endm

.macro repeat
block_64
add $128,%rdx
add $128,%rsi
add $128,%rdi
.endm

.macro repeat_2
repeat
repeat
repeat
repeat
.endm

.global pointwise_mul_q1024_1024_opt
pointwise_mul_q1024_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ1024INV(%rip),%ymm0
vmovdqa		_16xQ1024(%rip),%ymm1

/*repeat 16 times*/
repeat_2
repeat_2
repeat_2

repeat
repeat
repeat
block_64

add 	%r11,%rsp

ret

.global pointwise_mul_q1024_512_opt
pointwise_mul_q1024_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ1024INV(%rip),%ymm0
vmovdqa		_16xQ1024(%rip),%ymm1

/*repeat 8 times*/
repeat_2

repeat
repeat
repeat
block_64

add 	%r11,%rsp

ret
