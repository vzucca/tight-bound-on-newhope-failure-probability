#ifndef KARATSUBA1024_OPT
#define KARATSUBA1024_OPT

#include <stdint.h>

#include "reduce_q1024_opt.h"
#include "ntt_q1024_opt.h"

/*########################################### n = 512 ############################################*/
/*########################################## q = 12289 ############################################*/

void poly_product_512_K0_P0_opt(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t a_tmp[N] __attribute__((aligned(32))), b_tmp[N] __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  ntt_512_opt(a_tmp,zetas_512_opt);
  ntt_512_opt(b_tmp,zetas_512_opt);

  pointwise_mul_q1024_512_opt(c,a_tmp,b_tmp);

  invntt_512_opt(c,inv_zetas_512_opt);
}


/*########################################### n = 1024 ############################################*/
/*########################################## q = 12289 ############################################*/

void poly_product_1024_K0_P0_opt(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t a_tmp[N] __attribute__((aligned(32))), b_tmp[N] __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  ntt_1024_opt(a_tmp,zetas_1024_opt);
  ntt_1024_opt(b_tmp,zetas_1024_opt);

  pointwise_mul_q1024_1024_opt(c,a_tmp,b_tmp);

  invntt_1024_opt(c,inv_zetas_1024_opt);
}

#endif
