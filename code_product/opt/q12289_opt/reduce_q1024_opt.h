#ifndef REDUCE1024_OPT
#define REDUCE1024_OPT

// #include <stdio.h>
#include <stdint.h>
#include "params.h"

/* ############################################################################################## */
/* ############################### Functions for Q1024 = 12289 ################################## */
/* ############################################################################################## */

/* coefficient-wise multiplication */
void pointwise_mul_q1024_1024_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q1024_1024_opt");
void pointwise_mul_q1024_512_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q1024_512_opt");

#endif
