#ifndef NTT1024_OPT
#define NTT1024_OPT

#include <stdio.h>

/* ################ NTT ################ */
/* ntt functions nttq16__(size of input) */
#include "const_1024_opt.h"

void ntt_1024_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_1024_opt");
void invntt_1024_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_1024_opt");

void ntt_512_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_512_opt");
void invntt_512_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_512_opt");

#endif
