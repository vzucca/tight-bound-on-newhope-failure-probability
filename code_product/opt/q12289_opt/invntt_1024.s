.global invntt_1024_opt
invntt_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ1024INV(%rip),%ymm0 # ymm0 is array of Q_inv
vmovdqa		_16xQ1024(%rip),%ymm1 # ymm1 is array of Q
vmovdqa		_16xv_1024(%rip),%ymm2 # ymm2 is a constant for barrett

mov		%rsi,%r8

################################################################################
##
##
##                           BEGIN TREE A
##
##
################################################################################

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A1
#
# ---------------------------------------------------------------------------

#load block 1 and block 2
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

vmovdqa		_lowdword(%rip),%ymm3
#reorder
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		(%r8),%ymm7
vmovdqa		32(%r8),%ymm9
vmovdqa		64(%r8),%ymm11
vmovdqa		96(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1024(%r8),%ymm7
vmovdqa		1056(%r8),%ymm9
vmovdqa		1088(%r8),%ymm11
vmovdqa		1120(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2048(%r8),%ymm7
vmovdqa		2080(%r8),%ymm9
vmovdqa		2112(%r8),%ymm11
vmovdqa		2144(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------
#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3072(%r8),%ymm6
vmovdqa		3104(%r8),%ymm8
vmovdqa		3136(%r8),%ymm10
vmovdqa		3168(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4096(%r8),%ymm12
vmovdqa		4128(%r8),%ymm13
vmovdqa		4160(%r8),%ymm14
vmovdqa		4192(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5120(%rsi),%ymm14
vmovdqa		5152(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5632(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,128(%rdi)
vmovdqa		%ymm7,160(%rdi)
vmovdqa		%ymm8,192(%rdi)
vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A1
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A2
#
# ---------------------------------------------------------------------------

#load block 3 and block 4
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder
vmovdqa		_lowdword(%rip),%ymm3
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		128(%r8),%ymm7
vmovdqa		160(%r8),%ymm9
vmovdqa		192(%r8),%ymm11
vmovdqa		224(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1152(%r8),%ymm7
vmovdqa		1184(%r8),%ymm9
vmovdqa		1216(%r8),%ymm11
vmovdqa		1248(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2176(%r8),%ymm7
vmovdqa		2208(%r8),%ymm9
vmovdqa		2240(%r8),%ymm11
vmovdqa		2272(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3200(%r8),%ymm6
vmovdqa		3232(%r8),%ymm8
vmovdqa		3264(%r8),%ymm10
vmovdqa		3296(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4224(%r8),%ymm12
vmovdqa		4256(%r8),%ymm13
vmovdqa		4288(%r8),%ymm14
vmovdqa		4320(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5184(%rsi),%ymm14
vmovdqa		5216(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5664(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
# vmovdqa		%ymm6,128(%rdi)
# vmovdqa		%ymm7,160(%rdi)
# vmovdqa		%ymm8,192(%rdi)
# vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A2
#
# ---------------------------------------------------------------------------

# shift to take part of A1 tree
sub		$128,%rdi

#level 7 with block 2 and block 4 (4 already loaded)
vmovdqa		(%rdi),%ymm10
vmovdqa		32(%rdi),%ymm3
vmovdqa		64(%rdi),%ymm4
vmovdqa		96(%rdi),%ymm5

#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5888(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm5,%ymm14
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,256(%rdi)
vmovdqa		%ymm7,288(%rdi)
vmovdqa		%ymm8,320(%rdi)
vmovdqa		%ymm9,352(%rdi)

sub		$128,%rdi

#load block 1 and 3
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

# moves to complete tree A3 and A4
add		$512,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A3
#
# ---------------------------------------------------------------------------

#load block 5 and block 6
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

vmovdqa		_lowdword(%rip),%ymm3
#reorder
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		256(%r8),%ymm7
vmovdqa		288(%r8),%ymm9
vmovdqa		320(%r8),%ymm11
vmovdqa		352(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1280(%r8),%ymm7
vmovdqa		1312(%r8),%ymm9
vmovdqa		1344(%r8),%ymm11
vmovdqa		1376(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2304(%r8),%ymm7
vmovdqa		2336(%r8),%ymm9
vmovdqa		2368(%r8),%ymm11
vmovdqa		2400(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------
#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3328(%r8),%ymm6
vmovdqa		3360(%r8),%ymm8
vmovdqa		3392(%r8),%ymm10
vmovdqa		3424(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4352(%r8),%ymm12
vmovdqa		4384(%r8),%ymm13
vmovdqa		4416(%r8),%ymm14
vmovdqa		4448(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5248(%rsi),%ymm14
vmovdqa		5280(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5696(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,128(%rdi)
vmovdqa		%ymm7,160(%rdi)
vmovdqa		%ymm8,192(%rdi)
vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A3
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A4
#
# ---------------------------------------------------------------------------

#load block 7 and block 8
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder
vmovdqa		_lowdword(%rip),%ymm3
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		384(%r8),%ymm7
vmovdqa		416(%r8),%ymm9
vmovdqa		448(%r8),%ymm11
vmovdqa		480(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1408(%r8),%ymm7
vmovdqa		1440(%r8),%ymm9
vmovdqa		1472(%r8),%ymm11
vmovdqa		1504(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2432(%r8),%ymm7
vmovdqa		2464(%r8),%ymm9
vmovdqa		2496(%r8),%ymm11
vmovdqa		2528(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3456(%r8),%ymm6
vmovdqa		3488(%r8),%ymm8
vmovdqa		3520(%r8),%ymm10
vmovdqa		3552(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4480(%r8),%ymm12
vmovdqa		4512(%r8),%ymm13
vmovdqa		4544(%r8),%ymm14
vmovdqa		4576(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5312(%rsi),%ymm14
vmovdqa		5344(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5728(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
# vmovdqa		%ymm6,128(%rdi)
# vmovdqa		%ymm7,160(%rdi)
# vmovdqa		%ymm8,192(%rdi)
# vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A4
#
# ---------------------------------------------------------------------------

# shift to take part of A3 tree
sub		$128,%rdi

#level 7 with block 6 and block 8 (8 already loaded)
vmovdqa		(%rdi),%ymm10
vmovdqa		32(%rdi),%ymm3
vmovdqa		64(%rdi),%ymm4
vmovdqa		96(%rdi),%ymm5

#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5920(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm5,%ymm14
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,256(%rdi)
vmovdqa		%ymm7,288(%rdi)
vmovdqa		%ymm8,320(%rdi)
vmovdqa		%ymm9,352(%rdi)

sub		$128,%rdi

#load block 5 and 7
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
# vmovdqa		%ymm4,(%rdi)
# vmovdqa		%ymm5,32(%rdi)
# vmovdqa		%ymm6,64(%rdi)
# vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

# now ready to complete level 8 of part A
# moves to get block 1 to combine with block 5
sub		$512,%rdi


#level 8

#zetas
vmovdqa		6016(%rsi),%ymm15

#load block 1 and block 5 (already loaded in 4,5,6,7)
vmovdqa		(%rdi),%ymm8
vmovdqa		32(%rdi),%ymm9
vmovdqa		64(%rdi),%ymm10
vmovdqa		96(%rdi),%ymm11
# vmovdqa		512(%rdi),%ymm4
# vmovdqa		544(%rdi),%ymm5
# vmovdqa		576(%rdi),%ymm6
# vmovdqa		608(%rdi),%ymm7

#update
vmovdqa		%ymm8,%ymm12
vmovdqa		%ymm9,%ymm13
vmovdqa		%ymm10,%ymm14
vmovdqa		%ymm11,%ymm3
vpaddw		%ymm8,%ymm4,%ymm8
vpaddw		%ymm9,%ymm5,%ymm9
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm11,%ymm7,%ymm11
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm6,%ymm14,%ymm6
vpsubw		%ymm7,%ymm3,%ymm7

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm4,%ymm12
vpmullw		%ymm15,%ymm5,%ymm13
vpmullw		%ymm15,%ymm6,%ymm14
vpmullw		%ymm15,%ymm7,%ymm3
vpmulhw		%ymm15,%ymm4,%ymm4
vpmulhw		%ymm15,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm8,%ymm12
vpmulhw		%ymm2,%ymm9,%ymm13
vpmulhw		%ymm2,%ymm10,%ymm14
vpmulhw		%ymm2,%ymm11,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm8,(%rdi)
vmovdqa		%ymm9,32(%rdi)
vmovdqa		%ymm10,64(%rdi)
vmovdqa		%ymm11,96(%rdi)
vmovdqa		%ymm4,512(%rdi)
vmovdqa		%ymm5,544(%rdi)
vmovdqa		%ymm6,576(%rdi)
vmovdqa		%ymm7,608(%rdi)

add		$128,%rdi

#load block 2 and block 6
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#load block 3 and block 7
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#load block 4 and block 8
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

################################################################################
##
##
##                           END TREE A
##
##
################################################################################

# moves to complete tree B1 and B2
add		$640,%rdi

################################################################################
##
##
##                           BEGIN TREE B
##
##
################################################################################

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B1
#
# ---------------------------------------------------------------------------

#load block 9 and block 10
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

vmovdqa		_lowdword(%rip),%ymm3
#reorder
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		512(%r8),%ymm7
vmovdqa		544(%r8),%ymm9
vmovdqa		576(%r8),%ymm11
vmovdqa		608(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1536(%r8),%ymm7
vmovdqa		1568(%r8),%ymm9
vmovdqa		1600(%r8),%ymm11
vmovdqa		1632(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2560(%r8),%ymm7
vmovdqa		2592(%r8),%ymm9
vmovdqa		2624(%r8),%ymm11
vmovdqa		2656(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------
#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3584(%r8),%ymm6
vmovdqa		3616(%r8),%ymm8
vmovdqa		3648(%r8),%ymm10
vmovdqa		3680(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4608(%r8),%ymm12
vmovdqa		4640(%r8),%ymm13
vmovdqa		4672(%r8),%ymm14
vmovdqa		4704(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5376(%rsi),%ymm14
vmovdqa		5408(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5760(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,128(%rdi)
vmovdqa		%ymm7,160(%rdi)
vmovdqa		%ymm8,192(%rdi)
vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B1
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B2
#
# ---------------------------------------------------------------------------

#load block 11 and block 12
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder
vmovdqa		_lowdword(%rip),%ymm3
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		640(%r8),%ymm7
vmovdqa		672(%r8),%ymm9
vmovdqa		704(%r8),%ymm11
vmovdqa		736(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1664(%r8),%ymm7
vmovdqa		1696(%r8),%ymm9
vmovdqa		1728(%r8),%ymm11
vmovdqa		1760(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2688(%r8),%ymm7
vmovdqa		2720(%r8),%ymm9
vmovdqa		2752(%r8),%ymm11
vmovdqa		2784(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3712(%r8),%ymm6
vmovdqa		3744(%r8),%ymm8
vmovdqa		3776(%r8),%ymm10
vmovdqa		3808(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4736(%r8),%ymm12
vmovdqa		4768(%r8),%ymm13
vmovdqa		4800(%r8),%ymm14
vmovdqa		4832(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5440(%rsi),%ymm14
vmovdqa		5472(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5792(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
# vmovdqa		%ymm6,128(%rdi)
# vmovdqa		%ymm7,160(%rdi)
# vmovdqa		%ymm8,192(%rdi)
# vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B2
#
# ---------------------------------------------------------------------------

# shift to take part of A1 tree
sub		$128,%rdi

#level 7 with block 10 and block 12 (12 already loaded)
vmovdqa		(%rdi),%ymm10
vmovdqa		32(%rdi),%ymm3
vmovdqa		64(%rdi),%ymm4
vmovdqa		96(%rdi),%ymm5

#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5952(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm5,%ymm14
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,256(%rdi)
vmovdqa		%ymm7,288(%rdi)
vmovdqa		%ymm8,320(%rdi)
vmovdqa		%ymm9,352(%rdi)

sub		$128,%rdi

#load block 9 and 11
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

# moves to complete tree B3 and B4
add		$512,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B3
#
# ---------------------------------------------------------------------------

#load block 13 and block 14
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

vmovdqa		_lowdword(%rip),%ymm3
#reorder
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		768(%r8),%ymm7
vmovdqa		800(%r8),%ymm9
vmovdqa		832(%r8),%ymm11
vmovdqa		864(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1792(%r8),%ymm7
vmovdqa		1824(%r8),%ymm9
vmovdqa		1856(%r8),%ymm11
vmovdqa		1888(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2816(%r8),%ymm7
vmovdqa		2848(%r8),%ymm9
vmovdqa		2880(%r8),%ymm11
vmovdqa		2912(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------
#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3840(%r8),%ymm6
vmovdqa		3872(%r8),%ymm8
vmovdqa		3904(%r8),%ymm10
vmovdqa		3936(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4864(%r8),%ymm12
vmovdqa		4896(%r8),%ymm13
vmovdqa		4928(%r8),%ymm14
vmovdqa		4960(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5504(%rsi),%ymm14
vmovdqa		5536(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5824(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,128(%rdi)
vmovdqa		%ymm7,160(%rdi)
vmovdqa		%ymm8,192(%rdi)
vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B3
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B4
#
# ---------------------------------------------------------------------------

#load block 15 and block 16
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder
vmovdqa		_lowdword(%rip),%ymm3
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#level 0
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		896(%r8),%ymm7
vmovdqa		928(%r8),%ymm9
vmovdqa		960(%r8),%ymm11
vmovdqa		992(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 1
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		1920(%r8),%ymm7
vmovdqa		1952(%r8),%ymm9
vmovdqa		1984(%r8),%ymm11
vmovdqa		2016(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		2944(%r8),%ymm7
vmovdqa		2976(%r8),%ymm9
vmovdqa		3008(%r8),%ymm11
vmovdqa		3040(%r8),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
vpmulhw		%ymm2,%ymm10,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8
vpsubw		%ymm15,%ymm10,%ymm10

# ----------- SPECIAL REDUCTION ENDS -----------

#level 3
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		3968(%r8),%ymm6
vmovdqa		4000(%r8),%ymm8
vmovdqa		4032(%r8),%ymm10
vmovdqa		4064(%r8),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm7,%ymm14
vpmulhw		%ymm2,%ymm9,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm7,%ymm7
vpsubw		%ymm15,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#level 4
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		4992(%r8),%ymm12
vmovdqa		5024(%r8),%ymm13
vmovdqa		5056(%r8),%ymm14
vmovdqa		5088(%r8),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm8,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#level 5
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5568(%rsi),%ymm14
vmovdqa		5600(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#level 6
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5856(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm12
vpmulhw		%ymm2,%ymm3,%ymm13
vpmulhw		%ymm2,%ymm4,%ymm14
vpmulhw		%ymm2,%ymm5,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm10,%ymm10
vpsubw		%ymm13,%ymm3,%ymm3
vpsubw		%ymm14,%ymm4,%ymm4
vpsubw		%ymm15,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
# vmovdqa		%ymm6,128(%rdi)
# vmovdqa		%ymm7,160(%rdi)
# vmovdqa		%ymm8,192(%rdi)
# vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B4
#
# ---------------------------------------------------------------------------

# shift to take part of B3 tree
sub		$128,%rdi

#level 7 with block 14 and block 16 (16 already loaded)
vmovdqa		(%rdi),%ymm10
vmovdqa		32(%rdi),%ymm3
vmovdqa		64(%rdi),%ymm4
vmovdqa		96(%rdi),%ymm5

#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		5984(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm3,%ymm12
vpmulhw		%ymm2,%ymm4,%ymm13
vpmulhw		%ymm2,%ymm5,%ymm14
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm3,%ymm3
vpsubw		%ymm13,%ymm4,%ymm4
vpsubw		%ymm14,%ymm5,%ymm5

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,256(%rdi)
vmovdqa		%ymm7,288(%rdi)
vmovdqa		%ymm8,320(%rdi)
vmovdqa		%ymm9,352(%rdi)

sub		$128,%rdi

#load block 13 and 15
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
# vmovdqa		%ymm4,(%rdi)
# vmovdqa		%ymm5,32(%rdi)
# vmovdqa		%ymm6,64(%rdi)
# vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

# now ready to complete level 8 of part B
# moves to get block 9 to combine with block 13
sub		$512,%rdi

#level 8

#zetas
vmovdqa		6048(%rsi),%ymm15

#load block 9 and block 13 (already loaded in 4,5,6,7)
vmovdqa		(%rdi),%ymm8
vmovdqa		32(%rdi),%ymm9
vmovdqa		64(%rdi),%ymm10
vmovdqa		96(%rdi),%ymm11
# vmovdqa		512(%rdi),%ymm4
# vmovdqa		544(%rdi),%ymm5
# vmovdqa		576(%rdi),%ymm6
# vmovdqa		608(%rdi),%ymm7

#update
vmovdqa		%ymm8,%ymm12
vmovdqa		%ymm9,%ymm13
vmovdqa		%ymm10,%ymm14
vmovdqa		%ymm11,%ymm3
vpaddw		%ymm8,%ymm4,%ymm8
vpaddw		%ymm9,%ymm5,%ymm9
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm11,%ymm7,%ymm11
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm6,%ymm14,%ymm6
vpsubw		%ymm7,%ymm3,%ymm7

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm4,%ymm12
vpmullw		%ymm15,%ymm5,%ymm13
vpmullw		%ymm15,%ymm6,%ymm14
vpmullw		%ymm15,%ymm7,%ymm3
vpmulhw		%ymm15,%ymm4,%ymm4
vpmulhw		%ymm15,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm8,%ymm12
vpmulhw		%ymm2,%ymm9,%ymm13
vpmulhw		%ymm2,%ymm10,%ymm14
vpmulhw		%ymm2,%ymm11,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm8,(%rdi)
vmovdqa		%ymm9,32(%rdi)
vmovdqa		%ymm10,64(%rdi)
vmovdqa		%ymm11,96(%rdi)
vmovdqa		%ymm4,512(%rdi)
vmovdqa		%ymm5,544(%rdi)
vmovdqa		%ymm6,576(%rdi)
vmovdqa		%ymm7,608(%rdi)

add		$128,%rdi

#load block 10 and block 14
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#load block 11 and block 15
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#load block 12 and block 16
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SPECIAL REDUCTION STARTS on v -----------

#reduce 2
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm3
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm3,%ymm3
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
# vmovdqa		%ymm8,512(%rdi)
# vmovdqa		%ymm9,544(%rdi)
# vmovdqa		%ymm10,576(%rdi)
# vmovdqa		%ymm11,608(%rdi)

################################################################################
##
##
##                           END TREE B
##
##
################################################################################

#level 9

#zetas
vmovdqa		6080(%rsi),%ymm15
vmovdqa		_F1024(%rip),%ymm2

# shift to get block 8
sub		$512,%rdi

#load block 8 and block 16 (already loaded)
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
# vmovdqa		1024(%rdi),%ymm8
# vmovdqa		1056(%rdi),%ymm9
# vmovdqa		1088(%rdi),%ymm10
# vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

sub		$128,%rdi

#load block 7 and block 15
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

sub		$128,%rdi

#load block 6 and block 14
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

sub		$128,%rdi

#load block 5 and block 13
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

sub		$128,%rdi

#load block 4 and block 12
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

sub		$128,%rdi

#load block 3 and block 11
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

sub		$128,%rdi

#load block 2 and block 10
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

sub		$128,%rdi

#load block 1 and block 16
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

add 	%r11,%rsp

ret
