.global ntt_1024_opt
ntt_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ1024INV(%rip),%ymm0
vmovdqa		_16xQ1024(%rip),%ymm1
vmovdqa		_16xv_1024(%rip),%ymm2

#level 0

#zetas
vmovdqa		(%rsi),%ymm3

#Block 1 (0-3) with block 9 (32 - 35)
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 2 with block 10
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 3 with block 11
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 4 with block 12
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 5 with block 13
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 6 with block 14
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 7 with block 15
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 8 with block 16
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		1024(%rdi),%ymm8
vmovdqa		1056(%rdi),%ymm9
vmovdqa		1088(%rdi),%ymm10
vmovdqa		1120(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store (ymm4,5,6,7 will be reused in the immediate next level)
# vmovdqa		%ymm4,(%rdi)
# vmovdqa		%ymm5,32(%rdi)
# vmovdqa		%ymm6,64(%rdi)
# vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,1024(%rdi)
vmovdqa		%ymm9,1056(%rdi)
vmovdqa		%ymm10,1088(%rdi)
vmovdqa		%ymm11,1120(%rdi)

#tree A and B ready to be started. Starts with A
sub		$512,%rdi

################################################################################
##
##
##                           BEGIN TREE A
##
##
################################################################################

#level 1

#zetas
vmovdqa		32(%rsi),%ymm3

#Block 4 with 8 (already loaded with ymm4,5,6,7)
#load
# vmovdqa		(%rdi),%ymm4
# vmovdqa		32(%rdi),%ymm5
# vmovdqa		64(%rdi),%ymm6
# vmovdqa		96(%rdi),%ymm7
# vmovdqa		512(%rdi),%ymm8
# vmovdqa		544(%rdi),%ymm9
# vmovdqa		576(%rdi),%ymm10
# vmovdqa		608(%rdi),%ymm11
vmovdqa		(%rdi),%ymm8
vmovdqa		32(%rdi),%ymm9
vmovdqa		64(%rdi),%ymm10
vmovdqa		96(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm4,%ymm12
vpmulhw		%ymm3,%ymm4,%ymm4
vpmullw		%ymm3,%ymm5,%ymm13
vpmulhw		%ymm3,%ymm5,%ymm5
vpmullw		%ymm3,%ymm6,%ymm14
vpmulhw		%ymm3,%ymm6,%ymm6
vpmullw		%ymm3,%ymm7,%ymm15
vpmulhw		%ymm3,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm12
vpsubw		%ymm13,%ymm5,%ymm13
vpsubw		%ymm14,%ymm6,%ymm14
vpsubw		%ymm15,%ymm7,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm4
vpsraw		$15,%ymm13,%ymm5
vpsraw		$15,%ymm14,%ymm6
vpsraw		$15,%ymm15,%ymm7
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpaddw		%ymm4,%ymm12,%ymm12
vpaddw		%ymm5,%ymm13,%ymm13
vpaddw		%ymm6,%ymm14,%ymm14
vpaddw		%ymm7,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm4
vpsubw		%ymm1,%ymm13,%ymm5
vpsubw		%ymm1,%ymm14,%ymm6
vpsubw		%ymm1,%ymm15,%ymm7
# start the update normally
vpsubw		%ymm4,%ymm8,%ymm4
vpsubw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm10,%ymm6
vpsubw		%ymm7,%ymm11,%ymm7
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm15,%ymm11,%ymm11

#store
vmovdqa		%ymm8,(%rdi)
vmovdqa		%ymm9,32(%rdi)
vmovdqa		%ymm10,64(%rdi)
vmovdqa		%ymm11,96(%rdi)
vmovdqa		%ymm4,512(%rdi)
vmovdqa		%ymm5,544(%rdi)
vmovdqa		%ymm6,576(%rdi)
vmovdqa		%ymm7,608(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
sub		$128,%rdi

#Block 3 with 7
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm8
vpsubw		%ymm1,%ymm13,%ymm9
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm8,%ymm4,%ymm8
vpsubw		%ymm9,%ymm5,%ymm9
vpsubw		%ymm10,%ymm6,%ymm10
vpsubw		%ymm11,%ymm7,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

sub		$128,%rdi

#Block 2 with 6
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm8
vpsubw		%ymm1,%ymm13,%ymm9
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm8,%ymm4,%ymm8
vpsubw		%ymm9,%ymm5,%ymm9
vpsubw		%ymm10,%ymm6,%ymm10
vpsubw		%ymm11,%ymm7,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

sub		$128,%rdi

#Block 1 with 5
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm8
vpsubw		%ymm1,%ymm13,%ymm9
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm8,%ymm4,%ymm8
vpsubw		%ymm9,%ymm5,%ymm9
vpsubw		%ymm10,%ymm6,%ymm10
vpsubw		%ymm11,%ymm7,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store (block 1 will be reused straight away)
# vmovdqa		%ymm4,(%rdi)
# vmovdqa		%ymm5,32(%rdi)
# vmovdqa		%ymm6,64(%rdi)
# vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)


#level 2 for A1 and A2

#zetas
vmovdqa		96(%rsi),%ymm3

#Block 1 (already loaded) and Block 3
#load
# vmovdqa		(%rdi),%ymm4
# vmovdqa		32(%rdi),%ymm5
# vmovdqa		64(%rdi),%ymm6
# vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

add		$128,%rdi

#Block 2 and Block 4
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store (Block 4 is goind to be used immediately)
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
# vmovdqa		%ymm8,256(%rdi)
# vmovdqa		%ymm9,288(%rdi)
# vmovdqa		%ymm10,320(%rdi)
# vmovdqa		%ymm11,352(%rdi)

# Trees A1,A2 are ready to go. Start with tree A2
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A2
#
# ---------------------------------------------------------------------------

#Block 3 and 4 (4 already loaded in ymm8,9,10,11)
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
# vmovdqa		128(%rdi),%ymm8
# vmovdqa		160(%rdi),%ymm9
# vmovdqa		192(%rdi),%ymm10
# vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		256(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		544(%rsi),%ymm15
vmovdqa		576(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		1120(%rsi),%ymm13
vmovdqa		1152(%rsi),%ymm14
vmovdqa		1184(%rsi),%ymm15
vmovdqa		1216(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2144(%rsi),%ymm12
vmovdqa		2176(%rsi),%ymm13
vmovdqa		2208(%rsi),%ymm14
vmovdqa		2240(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3168(%rsi),%ymm12
vmovdqa		3200(%rsi),%ymm13
vmovdqa		3232(%rsi),%ymm14
vmovdqa		3264(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4192(%rsi),%ymm12
vmovdqa	  4224(%rsi),%ymm13
vmovdqa		4256(%rsi),%ymm14
vmovdqa		4288(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5216(%rsi),%ymm12
vmovdqa		5248(%rsi),%ymm13
vmovdqa		5280(%rsi),%ymm14
vmovdqa		5312(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A2
#
# ---------------------------------------------------------------------------

# tree A1 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A1
#
# ---------------------------------------------------------------------------


#Block 1 and 2
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		224(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		480(%rsi),%ymm15
vmovdqa		512(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		992(%rsi),%ymm13
vmovdqa		1024(%rsi),%ymm14
vmovdqa		1056(%rsi),%ymm15
vmovdqa		1088(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2016(%rsi),%ymm12
vmovdqa		2048(%rsi),%ymm13
vmovdqa		2080(%rsi),%ymm14
vmovdqa		2112(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3040(%rsi),%ymm12
vmovdqa		3072(%rsi),%ymm13
vmovdqa		3104(%rsi),%ymm14
vmovdqa		3136(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4064(%rsi),%ymm12
vmovdqa	  4096(%rsi),%ymm13
vmovdqa		4128(%rsi),%ymm14
vmovdqa		4160(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5088(%rsi),%ymm12
vmovdqa		5120(%rsi),%ymm13
vmovdqa		5152(%rsi),%ymm14
vmovdqa		5184(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A1
#
# ---------------------------------------------------------------------------


add		$512,%rdi

#level 2 for A3 and A4

#zetas
vmovdqa		128(%rsi),%ymm3

#Block 5 and Block 7
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

add		$128,%rdi

#Block 6 and Block 8
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store (Block 8 is goind to be used immediately)
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
# vmovdqa		%ymm8,256(%rdi)
# vmovdqa		%ymm9,288(%rdi)
# vmovdqa		%ymm10,320(%rdi)
# vmovdqa		%ymm11,352(%rdi)

# Trees A3,A4 are ready to go. Start with tree A4
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A4
#
# ---------------------------------------------------------------------------

#Block 7 and 8 (4 already loaded in ymm8,9,10,11)
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
# vmovdqa		128(%rdi),%ymm8
# vmovdqa		160(%rdi),%ymm9
# vmovdqa		192(%rdi),%ymm10
# vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		320(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		672(%rsi),%ymm15
vmovdqa		704(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		1376(%rsi),%ymm13
vmovdqa		1408(%rsi),%ymm14
vmovdqa		1440(%rsi),%ymm15
vmovdqa		1472(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2400(%rsi),%ymm12
vmovdqa		2432(%rsi),%ymm13
vmovdqa		2464(%rsi),%ymm14
vmovdqa		2496(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3424(%rsi),%ymm12
vmovdqa		3456(%rsi),%ymm13
vmovdqa		3488(%rsi),%ymm14
vmovdqa		3520(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4448(%rsi),%ymm12
vmovdqa	  4480(%rsi),%ymm13
vmovdqa		4512(%rsi),%ymm14
vmovdqa		4544(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5472(%rsi),%ymm12
vmovdqa		5504(%rsi),%ymm13
vmovdqa		5536(%rsi),%ymm14
vmovdqa		5568(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A4
#
# ---------------------------------------------------------------------------

# tree A3 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A3
#
# ---------------------------------------------------------------------------


#Block 5 and 6
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		288(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		608(%rsi),%ymm15
vmovdqa		640(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		1248(%rsi),%ymm13
vmovdqa		1280(%rsi),%ymm14
vmovdqa		1312(%rsi),%ymm15
vmovdqa		1344(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2272(%rsi),%ymm12
vmovdqa		2304(%rsi),%ymm13
vmovdqa		2336(%rsi),%ymm14
vmovdqa		2368(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3296(%rsi),%ymm12
vmovdqa		3328(%rsi),%ymm13
vmovdqa		3360(%rsi),%ymm14
vmovdqa		3392(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4320(%rsi),%ymm12
vmovdqa	  4352(%rsi),%ymm13
vmovdqa		4384(%rsi),%ymm14
vmovdqa		4416(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5344(%rsi),%ymm12
vmovdqa		5376(%rsi),%ymm13
vmovdqa		5408(%rsi),%ymm14
vmovdqa		5440(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE A3
#
# ---------------------------------------------------------------------------

# moves to tree B, from block 5 to block 12
add		$896,%rdi

###############################################################################
##
##
##                           BEGIN TREE B
##
##
################################################################################

#level 1

#zetas
vmovdqa		64(%rsi),%ymm3

#Block 12 with 16
#load
vmovdqa		(%rdi),%ymm8
vmovdqa		32(%rdi),%ymm9
vmovdqa		64(%rdi),%ymm10
vmovdqa		96(%rdi),%ymm11
vmovdqa		512(%rdi),%ymm4
vmovdqa		544(%rdi),%ymm5
vmovdqa		576(%rdi),%ymm6
vmovdqa		608(%rdi),%ymm7

#mul
vpmullw		%ymm3,%ymm4,%ymm12
vpmulhw		%ymm3,%ymm4,%ymm4
vpmullw		%ymm3,%ymm5,%ymm13
vpmulhw		%ymm3,%ymm5,%ymm5
vpmullw		%ymm3,%ymm6,%ymm14
vpmulhw		%ymm3,%ymm6,%ymm6
vpmullw		%ymm3,%ymm7,%ymm15
vpmulhw		%ymm3,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm12
vpsubw		%ymm13,%ymm5,%ymm13
vpsubw		%ymm14,%ymm6,%ymm14
vpsubw		%ymm15,%ymm7,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm4
vpsraw		$15,%ymm13,%ymm5
vpsraw		$15,%ymm14,%ymm6
vpsraw		$15,%ymm15,%ymm7
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpaddw		%ymm4,%ymm12,%ymm12
vpaddw		%ymm5,%ymm13,%ymm13
vpaddw		%ymm6,%ymm14,%ymm14
vpaddw		%ymm7,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm4
vpsubw		%ymm1,%ymm13,%ymm5
vpsubw		%ymm1,%ymm14,%ymm6
vpsubw		%ymm1,%ymm15,%ymm7
# start the update normally
vpsubw		%ymm4,%ymm8,%ymm4
vpsubw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm10,%ymm6
vpsubw		%ymm7,%ymm11,%ymm7
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm15,%ymm11,%ymm11

#store
vmovdqa		%ymm8,(%rdi)
vmovdqa		%ymm9,32(%rdi)
vmovdqa		%ymm10,64(%rdi)
vmovdqa		%ymm11,96(%rdi)
vmovdqa		%ymm4,512(%rdi)
vmovdqa		%ymm5,544(%rdi)
vmovdqa		%ymm6,576(%rdi)
vmovdqa		%ymm7,608(%rdi)

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
sub		$128,%rdi

#Block 11 with 15
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm8
vpsubw		%ymm1,%ymm13,%ymm9
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm8,%ymm4,%ymm8
vpsubw		%ymm9,%ymm5,%ymm9
vpsubw		%ymm10,%ymm6,%ymm10
vpsubw		%ymm11,%ymm7,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

sub		$128,%rdi

#Block 10 with 14
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm8
vpsubw		%ymm1,%ymm13,%ymm9
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm8,%ymm4,%ymm8
vpsubw		%ymm9,%ymm5,%ymm9
vpsubw		%ymm10,%ymm6,%ymm10
vpsubw		%ymm11,%ymm7,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

sub		$128,%rdi

#Block 9 with 13
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm8
vpsubw		%ymm1,%ymm13,%ymm9
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm8,%ymm4,%ymm8
vpsubw		%ymm9,%ymm5,%ymm9
vpsubw		%ymm10,%ymm6,%ymm10
vpsubw		%ymm11,%ymm7,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store (block 9 will be reused straight away)
# vmovdqa		%ymm4,(%rdi)
# vmovdqa		%ymm5,32(%rdi)
# vmovdqa		%ymm6,64(%rdi)
# vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)


#level 2 for B1 and B2

#zetas
vmovdqa		160(%rsi),%ymm3

#Block 9 (already loaded) and Block 11
#load
# vmovdqa		(%rdi),%ymm4
# vmovdqa		32(%rdi),%ymm5
# vmovdqa		64(%rdi),%ymm6
# vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

add		$128,%rdi

#Block 10 and Block 12
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store (Block 12 is goind to be used immediately)
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
# vmovdqa		%ymm8,256(%rdi)
# vmovdqa		%ymm9,288(%rdi)
# vmovdqa		%ymm10,320(%rdi)
# vmovdqa		%ymm11,352(%rdi)

# Trees B1,B2 are ready to go. Start with tree A2
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B2
#
# ---------------------------------------------------------------------------

#Block 11 and 12 (12 already loaded in ymm8,9,10,11)
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
# vmovdqa		128(%rdi),%ymm8
# vmovdqa		160(%rdi),%ymm9
# vmovdqa		192(%rdi),%ymm10
# vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		384(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		800(%rsi),%ymm15
vmovdqa		832(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		1632(%rsi),%ymm13
vmovdqa		1664(%rsi),%ymm14
vmovdqa		1696(%rsi),%ymm15
vmovdqa		1728(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2656(%rsi),%ymm12
vmovdqa		2688(%rsi),%ymm13
vmovdqa		2720(%rsi),%ymm14
vmovdqa		2752(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3680(%rsi),%ymm12
vmovdqa		3712(%rsi),%ymm13
vmovdqa		3744(%rsi),%ymm14
vmovdqa		3776(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4704(%rsi),%ymm12
vmovdqa	  4736(%rsi),%ymm13
vmovdqa		4768(%rsi),%ymm14
vmovdqa		4800(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5728(%rsi),%ymm12
vmovdqa		5760(%rsi),%ymm13
vmovdqa		5792(%rsi),%ymm14
vmovdqa		5824(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B2
#
# ---------------------------------------------------------------------------

# tree A1 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B1
#
# ---------------------------------------------------------------------------


#Block 9 and 10
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		352(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		736(%rsi),%ymm15
vmovdqa		768(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		1504(%rsi),%ymm13
vmovdqa		1536(%rsi),%ymm14
vmovdqa		1568(%rsi),%ymm15
vmovdqa		1600(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2528(%rsi),%ymm12
vmovdqa		2560(%rsi),%ymm13
vmovdqa		2592(%rsi),%ymm14
vmovdqa		2624(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3552(%rsi),%ymm12
vmovdqa		3584(%rsi),%ymm13
vmovdqa		3616(%rsi),%ymm14
vmovdqa		3648(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4576(%rsi),%ymm12
vmovdqa	  4608(%rsi),%ymm13
vmovdqa		4640(%rsi),%ymm14
vmovdqa		4672(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5600(%rsi),%ymm12
vmovdqa		5632(%rsi),%ymm13
vmovdqa		5664(%rsi),%ymm14
vmovdqa		5696(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B1
#
# ---------------------------------------------------------------------------


add		$512,%rdi

#level 2 for B3 and B4

#zetas
vmovdqa		192(%rsi),%ymm3

#Block 13 and Block 15
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

add		$128,%rdi

#Block 15 and Block 16
#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store (Block 16 is goind to be used immediately)
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
# vmovdqa		%ymm8,256(%rdi)
# vmovdqa		%ymm9,288(%rdi)
# vmovdqa		%ymm10,320(%rdi)
# vmovdqa		%ymm11,352(%rdi)

# Trees A3,A4 are ready to go. Start with tree A4
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B4
#
# ---------------------------------------------------------------------------

#Block 15 and 16 (16 already loaded in ymm8,9,10,11)
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
# vmovdqa		128(%rdi),%ymm8
# vmovdqa		160(%rdi),%ymm9
# vmovdqa		192(%rdi),%ymm10
# vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		448(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		928(%rsi),%ymm15
vmovdqa		960(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		1888(%rsi),%ymm13
vmovdqa		1920(%rsi),%ymm14
vmovdqa		1952(%rsi),%ymm15
vmovdqa		1984(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2912(%rsi),%ymm12
vmovdqa		2944(%rsi),%ymm13
vmovdqa		2976(%rsi),%ymm14
vmovdqa		3008(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3936(%rsi),%ymm12
vmovdqa		3968(%rsi),%ymm13
vmovdqa		4000(%rsi),%ymm14
vmovdqa		4032(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4960(%rsi),%ymm12
vmovdqa	  4992(%rsi),%ymm13
vmovdqa		5024(%rsi),%ymm14
vmovdqa		5056(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5984(%rsi),%ymm12
vmovdqa		6016(%rsi),%ymm13
vmovdqa		6048(%rsi),%ymm14
vmovdqa		6080(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B4
#
# ---------------------------------------------------------------------------

# tree B3 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B3
#
# ---------------------------------------------------------------------------


#Block 13 and 14
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#level 3

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm5,%ymm13
vpmulhw		%ymm2,%ymm6,%ymm14
vpmulhw		%ymm2,%ymm7,%ymm15
# TMP = TMP >>i 12
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
vpsraw		$12,%ymm15,%ymm15
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
vpmullw		%ymm1,%ymm15,%ymm15
# v = v - TMP
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		416(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm8
vpsraw		$15,%ymm13,%ymm9
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm8,%ymm12,%ymm12
vpaddw		%ymm9,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
#  do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#level 4

#zetas
vmovdqa		864(%rsi),%ymm15
vmovdqa		896(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm10
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm10,%ymm10
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm12,%ymm6
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm10
vpsubw		%ymm1,%ymm15,%ymm11
# start the update normally
vpsubw		%ymm6,%ymm4,%ymm6
vpsubw		%ymm7,%ymm5,%ymm7
vpsubw		%ymm10,%ymm8,%ymm10
vpsubw		%ymm11,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 5
#zetas
vmovdqa		1760(%rsi),%ymm13
vmovdqa		1792(%rsi),%ymm14
vmovdqa		1824(%rsi),%ymm15
vmovdqa		1856(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpsraw		$15,%ymm15,%ymm11
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpand		%ymm1,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14
vpaddw		%ymm11,%ymm15,%ymm15

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
vpsubw    %ymm1,%ymm15,%ymm15
# do the update add part
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,128(%rdi)
vmovdqa		%ymm9,160(%rdi)
vmovdqa		%ymm10,192(%rdi)
vmovdqa		%ymm11,224(%rdi)

#level 6

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm3,%ymm11
vpmulhw		%ymm2,%ymm5,%ymm12
vpmulhw		%ymm2,%ymm7,%ymm13
vpmulhw		%ymm2,%ymm9,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		2784(%rsi),%ymm12
vmovdqa		2816(%rsi),%ymm13
vmovdqa		2848(%rsi),%ymm14
vmovdqa		2880(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within packs of 8 -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS  -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm4
vpsraw		$15,%ymm12,%ymm6
vpsraw		$15,%ymm13,%ymm8
vpsraw		$15,%ymm14,%ymm10
vpand		%ymm1,%ymm4,%ymm4
vpand		%ymm1,%ymm6,%ymm6
vpand		%ymm1,%ymm8,%ymm8
vpand		%ymm1,%ymm10,%ymm10
vpaddw		%ymm4,%ymm11,%ymm11
vpaddw		%ymm6,%ymm12,%ymm12
vpaddw		%ymm8,%ymm13,%ymm13
vpaddw		%ymm10,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
vpsubw		%ymm14,%ymm9,%ymm10
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
#  do the update add part
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#level 7

#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#zetas
vmovdqa		3808(%rsi),%ymm12
vmovdqa		3840(%rsi),%ymm13
vmovdqa		3872(%rsi),%ymm14
vmovdqa		3904(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 4s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm10
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm10,%ymm3,%ymm10
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 4s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm10,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm10,%ymm10
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
# add -q to the montgomery part we want to substract
vpsubw		%ymm1,%ymm10,%ymm3
vpsubw		%ymm1,%ymm12,%ymm5
vpsubw		%ymm1,%ymm13,%ymm7
vpsubw		%ymm1,%ymm14,%ymm9
# start the update normally
vpsubw		%ymm3,%ymm11,%ymm3
vpsubw		%ymm5,%ymm4,%ymm5
vpsubw		%ymm7,%ymm6,%ymm7
vpsubw		%ymm9,%ymm8,%ymm9
vpaddw		%ymm10,%ymm11,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 8
#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#zetas
vmovdqa		4832(%rsi),%ymm12
vmovdqa	  4864(%rsi),%ymm13
vmovdqa		4896(%rsi),%ymm14
vmovdqa		4928(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within pack of 2s -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within pack of 2s -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14


#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#level 9
#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

# ----------- SPECIAL REDUCTION STARTS -----------

#reduce 2
# V = 2^28/Q + 1
# TMP = V * v >> 16
vpmulhw		%ymm2,%ymm10,%ymm11
vpmulhw		%ymm2,%ymm4,%ymm12
vpmulhw		%ymm2,%ymm6,%ymm13
vpmulhw		%ymm2,%ymm8,%ymm14
# TMP = TMP >>i 12
vpsraw		$12,%ymm11,%ymm11
vpsraw		$12,%ymm12,%ymm12
vpsraw		$12,%ymm13,%ymm13
vpsraw		$12,%ymm14,%ymm14
# TMP = TMP * Q mod 2^16
vpmullw		%ymm1,%ymm11,%ymm11
vpmullw		%ymm1,%ymm12,%ymm12
vpmullw		%ymm1,%ymm13,%ymm13
vpmullw		%ymm1,%ymm14,%ymm14
# v = v - TMP
vpsubw		%ymm11,%ymm10,%ymm10
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm6,%ymm6
vpsubw		%ymm14,%ymm8,%ymm8

# ----------- SPECIAL REDUCTION ENDS -----------

#zetas
vmovdqa		5856(%rsi),%ymm12
vmovdqa		5888(%rsi),%ymm13
vmovdqa		5920(%rsi),%ymm14
vmovdqa		5952(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS within singletons -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm11
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14

# ----------- SIGNED MONTGOMERY ENDS within singletons -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm11,%ymm3
vpsraw		$15,%ymm12,%ymm5
vpsraw		$15,%ymm13,%ymm7
vpsraw		$15,%ymm14,%ymm9
vpand		%ymm1,%ymm3,%ymm3
vpand		%ymm1,%ymm5,%ymm5
vpand		%ymm1,%ymm7,%ymm7
vpand		%ymm1,%ymm9,%ymm9
vpaddw		%ymm3,%ymm11,%ymm11
vpaddw		%ymm5,%ymm12,%ymm12
vpaddw		%ymm7,%ymm13,%ymm13
vpaddw		%ymm9,%ymm14,%ymm14

#update
vpsubw		%ymm11,%ymm10,%ymm3
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
# update the output of Montgomery with -q
vpsubw    %ymm1,%ymm11,%ymm11
vpsubw    %ymm1,%ymm12,%ymm12
vpsubw    %ymm1,%ymm13,%ymm13
vpsubw    %ymm1,%ymm14,%ymm14
# do the update add part
vpaddw		%ymm11,%ymm10,%ymm10
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE B3
#
# ---------------------------------------------------------------------------
add 	%r11,%rsp

ret
