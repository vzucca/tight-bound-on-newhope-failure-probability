#ifndef NTT32
#define NTT32

#include <stdio.h>

#include "reduce_q32.h"

/* static const uint16_t tree[32] = {
   0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30,
   1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31 }; */


/* zetas[i] = M*g^tree[i] with g = 227 */
const int16_t zetas_32[32] ={
  1496, 998, 387, 530, 1369, 1275, 1404, 1476, 1467, 557, 1363, 1515, 817, 941, 328, 1289,
  236, 166, 228, 486, 186, 565, 641, 1577, 1460, 837, 108, 483, 1015, 1026, 1062, 747 };

/* inv_zetas[i] = M*g^(-tree[i]-1)%q */
const int16_t inv_zetas_32[32] = {
  854, 539, 575, 586, 1118, 1493, 764, 141, 24, 960, 1036, 1415, 1115, 1373, 1435, 1365,
  312, 1273, 660, 784, 86, 238, 1044, 134, 125, 197, 326, 232, 1071, 1214, 603, 105 };

/* ntt_Y stored in Montgonery form (useful for Karatsuba reconstruction)*/
const int16_t ntt_Y_32[32] __attribute__((aligned(32))) = {
  382, 1219, 1134, 467, 79, 1522, 964, 637, 903, 698, 679, 922, 182, 1419, 1309, 292,
  1245, 356, 514, 1087, 720, 881, 644, 957, 235, 1366, 108, 1493, 63, 1538, 841, 760 };

/* ################ NTT ################ */
/* ntt functions nttq32__(size of input) */

/* input array of size 64 */
void nttq32_64(int16_t *c){
  // #ifdef TEST_MODE_ON
  // printf("call nttq32_64\n");
  // unsigned int nb_print = 0;
  // uint16_t last_zetas[16];
  // uint16_t ass_pack_id = 0;
  // #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 64/2;
  for(i = 0 ; i < 5 ; i++){

    // #ifdef TEST_MODE_ON
    // if(nb_print == 16){
    //   printf("\n");nb_print = 0;
    //   if (last_zetas[15]==last_zetas[0]) {
    //     printf("_VAL_16x_(%5u),", last_zetas[0]);
    //   } else if (last_zetas[7]==last_zetas[0]) {
    //     printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
    //   } else if (last_zetas[3]==last_zetas[0]) {
    //     printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
    //     last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    //   } else if (last_zetas[1]==last_zetas[0]) {
    //     printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
    //     last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
    //     last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    //   } else {
    //     printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
    //     last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
    //     last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
    //     last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
    //     last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    //   }
    //   printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    // }
    // printf("\n// LEVEL %u\n", i);
    // #endif

    for(s = 0 ; s < 64 ; s = j+l){

      // #ifdef TEST_MODE_ON
      // if(nb_print == 16){
      //   printf("\n");nb_print = 0;
      //   if (last_zetas[15]==last_zetas[0]) {
      //     printf("_VAL_16x_(%5u),", last_zetas[0]);
      //   } else if (last_zetas[7]==last_zetas[0]) {
      //     printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      //   } else if (last_zetas[3]==last_zetas[0]) {
      //     printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
      //     last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      //   } else if (last_zetas[1]==last_zetas[0]) {
      //     printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
      //     last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      //     last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      //   } else {
      //     printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
      //     last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      //     last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      //     last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      //     last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      //   }
      //   printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      // }
      // #endif

      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q32((int32_t)c[j+l]*zetas_32[k]);
  // #ifdef TEST_MODE_ON
  // if (nb_print < 16) {last_zetas[nb_print]=zetas_32[k];nb_print++;}
  // #endif

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  // #ifdef TEST_MODE_ON
  // if(nb_print == 16){
  //   printf("\n");nb_print = 0;
  //   if (last_zetas[15]==last_zetas[0]) {
  //     printf("_VAL_16x_(%5u)", last_zetas[0]);
  //   } else if (last_zetas[7]==last_zetas[0]) {
  //     printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
  //   } else if (last_zetas[3]==last_zetas[0]) {
  //     printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
  //     last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
  //   } else if (last_zetas[1]==last_zetas[0]) {
  //     printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
  //     last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
  //     last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
  //   } else {
  //     printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
  //     last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
  //     last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
  //     last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
  //     last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
  //   }
  //   printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  // }
  // #endif

  for(s = 0 ; s < 64 ; s++)
    c[s] = barrett_q32(c[s]);
}


/* input array of size 128 */
void nttq32_128(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 128/2;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 128 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q32((int32_t)c[j+l]*zetas_32[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 128 ; s++)
    c[s] = barrett_q32(c[s]);
}


/* input array of size 256 */
void nttq32_256(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 256/2;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 256 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q32((int32_t)c[j+l]*zetas_32[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 256 ; s++)
    c[s] = barrett_q32(c[s]);
}

/* input array of size 512 */
void nttq32_512(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 512/2;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 512 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q32((int32_t)c[j+l]*zetas_32[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 512 ; s++)
    c[s] = barrett_q32(c[s]);
}

/* input array of size 1024 */
void nttq32_1024(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 1024/2;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 1024 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q32((int32_t)c[j+l]*zetas_32[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 1024 ; s++)
    c[s] = barrett_q32(c[s]);
}

/* ################ INVNTT ################ */

void invnttq32_64(int16_t *c){
  // #ifdef TEST_MODE_ON
  // printf("call invnttq32_64\n");
  // unsigned int nb_print = 0;
  // uint16_t last_zetas[16];
  // uint16_t ass_pack_id = 0;
  // #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<1;
  for(i = 0 ; i < 5 ; i++){

    // #ifdef TEST_MODE_ON
    // if(nb_print == 16){
    //   printf("\n");nb_print = 0;
    //   if (last_zetas[15]==last_zetas[0]) {
    //     printf("_VAL_16x_(%5u),", last_zetas[0]);
    //   } else if (last_zetas[7]==last_zetas[0]) {
    //     printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
    //   } else if (last_zetas[3]==last_zetas[0]) {
    //     printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
    //     last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    //   } else if (last_zetas[1]==last_zetas[0]) {
    //     printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
    //     last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
    //     last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    //   } else {
    //     printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
    //     last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
    //     last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
    //     last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
    //     last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    //   }
    //   printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    // }
    // printf("\n// LEVEL %u\n", i);
    // #endif

    for(s = 0 ; s < 64 ; s = j+l){

      // #ifdef TEST_MODE_ON
      // if(nb_print == 16){
      //   printf("\n");nb_print = 0;
      //   if (last_zetas[15]==last_zetas[0]) {
      //     printf("_VAL_16x_(%5u),", last_zetas[0]);
      //   } else if (last_zetas[7]==last_zetas[0]) {
      //     printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      //   } else if (last_zetas[3]==last_zetas[0]) {
      //     printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
      //     last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      //   } else if (last_zetas[1]==last_zetas[0]) {
      //     printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
      //     last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      //     last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      //   } else {
      //     printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
      //     last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      //     last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      //     last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      //     last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      //   }
      //   printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      // }
      // #endif

      for(j = s ; j < s+l ; j++){
	t = c[j];
	if( (i==3) && (j<4) )
	  c[j] = barrett_q32(t + c[j+l]);
	else
	  c[j] = t + c[j+l];

	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q32((int32_t)c[j+l]*inv_zetas_32[k]);

  // #ifdef TEST_MODE_ON
  // if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_32[k];nb_print++;}
  // #endif
      }
      k++;
    }
    l<<=1;
  }

  // #ifdef TEST_MODE_ON
  // if(nb_print == 16){
  //   printf("\n");nb_print = 0;
  //   if (last_zetas[15]==last_zetas[0]) {
  //     printf("_VAL_16x_(%5u)", last_zetas[0]);
  //   } else if (last_zetas[7]==last_zetas[0]) {
  //     printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
  //   } else if (last_zetas[3]==last_zetas[0]) {
  //     printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
  //     last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
  //   } else if (last_zetas[1]==last_zetas[0]) {
  //     printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
  //     last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
  //     last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
  //   } else {
  //     printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
  //     last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
  //     last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
  //     last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
  //     last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
  //   }
  //   printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  // }
  // #endif

  for(j = 0 ; j < 64 ; j++){
    c[j] = montgomery_reduce_q32((int32_t) c[j]*F32); /* F32 = (mont^2/n)%q and 447 = (mont/n)%q */
    c[j] += (c[j]>>15) & Q32;
  }
}

void invnttq32_128(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<2;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 128 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];
	if( (i==3) && (j<4) )
	  c[j] = barrett_q32(t + c[j+l]);
	else
	  c[j] = t + c[j+l];

	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q32((int32_t)c[j+l]*inv_zetas_32[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 128 ; j++){
    c[j] = montgomery_reduce_q32((int32_t) c[j]*F32); /* F32 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q32;
  }
}

void invnttq32_256(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<3;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 256 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];
	if( (i==3) && (j<8) )
	  c[j] = barrett_q32(t + c[j+l]);
	else
	  c[j] = t + c[j+l];

	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q32((int32_t)c[j+l]*inv_zetas_32[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 256 ; j++){
    c[j] = montgomery_reduce_q32((int32_t) c[j]*F32); /* F32 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q32;
  }
}

void invnttq32_512(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<4;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 512 ; s = j+l){
      for(j = s ; j < s+l ; j++){
  	t = c[j];
  	if( (i==3) && (j<16) )
  	  c[j] = barrett_q32(t + c[j+l]);
  	else
	  c[j] = t + c[j+l];

  	c[j+l] = t - c[j+l];
  	c[j+l] = montgomery_reduce_q32((int32_t)c[j+l]*inv_zetas_32[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 512 ; j++){
    c[j] = montgomery_reduce_q32((int32_t) c[j]*F32); /* F32 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q32;
  }
}

void invnttq32_1024(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<5;
  for(i = 0 ; i < 5 ; i++){
    for(s = 0 ; s < 1024 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];

	if( (i==3) && (j<32) )
          c[j] = barrett_q32(t + c[j+l]);
	else
          c[j] = t + c[j+l];
	
	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q32((int32_t)c[j+l]*inv_zetas_32[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 1024 ; j++){
    c[j] = montgomery_reduce_q32((int32_t) c[j]*F32); /* F32 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q32;
  }
}

#endif
