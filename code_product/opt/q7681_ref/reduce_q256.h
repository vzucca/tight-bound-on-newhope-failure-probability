#ifndef REDUCE256
#define REDUCE256

#include <stdio.h>

/* ############################################################################################## */
/* ################################ Functions for Q256 = 7681 ################################### */
/* ################################# ]-4q,4q[ C ]-2^15,2^15[ ################################### */
/* ############################################################################################## */
/* Works only for 7681 = 2^13 - 2^9 + 1 */
/* Input in [-2^15, 2^15), output in [-2^15+4*q,2^15-3*q)*/
int16_t signed_mers_q256(int16_t a){

  int16_t t, u;

  t = a>>13;

  u = a & ((1<<13)-1);

  u -= t;

  return u+ (t<<9);
}


/* V256 = 2^27/q256 + 1*/
// const int16_t V256 = 17474;

/* Input in [-2^15, 2^15), output in [0,q) */
int16_t barrett_q256(int16_t a){

  int16_t t;

  t = ((int32_t) V256*a)>>27;

  t *= Q256;

  return a - t;
}

/* Q^(-1) mod M where M = 2^16 */
// const int16_t Q256INV = -7679;

int16_t montgomery_reduce_q256(int32_t a)
{
  int32_t t;
  int16_t u;

  u = a * Q256INV;

  t = (int32_t)u * Q256;
  t = a - t;
  t >>= 16;

  return t;
}

void poly_barrettq256(int16_t *r, int16_t *a, unsigned int n){
  for(unsigned int i = 0 ; i < n ; i++)
    r[i] = barrett_q256(a[i]);
}

void poly_mersq256(int16_t *r, int16_t *a, unsigned int n){
  for(unsigned int i = 0 ; i < n ; i++)
    r[i] = signed_mers_q256(a[i]);
}

/* coefficient-wise multiplication */
void pointwise_mul_q256(int16_t *c, const int16_t *a, const int16_t* b, unsigned int n){

  for(unsigned int i = 0 ; i < n ; i++)
    c[i] = montgomery_reduce_q256((int32_t)a[i]*b[i]); /* Output ab*M^(-1) */

}

/* mutiplication of size_input coefficients as if they were polynomials of size size_poly modulo X^deg_poly-1 - zeta*/
void poly_mul_modq256(int16_t *c, const int16_t *a, const int16_t* b, const unsigned int size_input, const unsigned int size_poly, const int16_t *zeta){

  int32_t tmp;
  int16_t c_tmp[size_poly];

  for(unsigned int i = 0 ; i < size_input/size_poly ; i++){
    for(unsigned int j = 0 ; j < size_poly ; j++){
      tmp = 0;
      for(unsigned int k = j+1 ; k < size_poly ; k++)
	tmp += a[i*size_poly + k]*b[i*size_poly + size_poly - k + j];

      c_tmp[j] = montgomery_reduce_q256(tmp);
      tmp = c_tmp[j]*zeta[i];

      for(unsigned int k = 0 ; k <= j ; k++)
	tmp += a[i*size_poly + k]*b[i*size_poly + j-k];

      c_tmp[j] = montgomery_reduce_q256(tmp);
    }
    for(unsigned int j = 0 ; j < size_poly ; j++)
      c[i*size_poly + j] = c_tmp[j];
  }
}

/* Compute a*X mod X^size_poly - zeta^i on a block of size_input coefficients */
void poly_shift_modq256(int16_t *c, const int16_t *a, const unsigned int size_input, const unsigned int size_poly, const int16_t *zeta){

  int16_t tmp;

  for(unsigned int i = 0 ; i < size_input/size_poly ; i++){
    tmp = montgomery_reduce_q256( (int32_t)a[i*size_poly + size_poly-1]*zeta[i]);
    for(unsigned int j = size_poly-1 ; j >=1 ; j--)
      c[i*size_poly + j] = a[i*size_poly + j-1];
    c[i*size_poly] = tmp;
  }
}

#endif
