#ifndef KARATSUBA256
#define KARATSUBA256

#include <stdint.h>

#include "reduce_q256.h"
#include "ntt_q256.h"

/*########################################### n = 512 ############################################*/
/*########################################## q = 7681 ############################################*/

void poly_product_512_K0_P1(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t a_tmp[N], b_tmp[N];

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  nttq256_512(a_tmp);
  nttq256_512(b_tmp);

  poly_mul_modq256(c,a_tmp,b_tmp,N,2,ntt_Y_256);

  invnttq256_512(c);
}

void poly_product_512_K1_P0(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2], a1[N/2], a01[N/2];
  int16_t b0[N/2], b1[N/2], b01[N/2];
  int16_t c0[N/2], c1[N/2], c2[N/2];

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  nttq256_256(a0);
  nttq256_256(a1);
  nttq256_256(b0);
  nttq256_256(b1);

  /* Output of the ntt is in [0, q) */
  poly_add(a01,a0,a1,N/2);
  poly_add(b01,b0,b1,N/2);

  /* Polynomial products */
  pointwise_mul_q256(c0,a0,b0,N/2);
  pointwise_mul_q256(c1,a01,b01,N/2);
  pointwise_mul_q256(c2,a1,b1,N/2);

  /* Reconstruction c1 <- c1 - c0 - c2 in ]-3q,3q[ */
  poly_sub(c1,c1,c0,N/2); poly_sub(c1,c1,c2,N/2);
  /* Reconstruction c0 <- c0 + c2*ntt_Y  ]-2q,2q[*/
  pointwise_mul_q256(c2,c2,ntt_Y_256,N/2); poly_add(c0,c0,c2,N/2);

  /* Reduction before invntt */
  poly_barrettq256(c0,c0,N/2);
  poly_barrettq256(c1,c1,N/2);

  invnttq256_256(c0);
  invnttq256_256(c1);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = c0[i];
    c[2*i+1] = c1[i];
  }
}

/* /\*########################################## n = 1024 ############################################*\/ */
/* /\*########################################## q = 7681 ############################################*\/ */

void poly_product_1024_K0_P2(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t a_tmp[N], b_tmp[N];

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  nttq256_1024(a_tmp);
  nttq256_1024(b_tmp);

  poly_mul_modq256(c,a_tmp,b_tmp,N,4,ntt_Y_256);

  invnttq256_1024(c);
}

void poly_product_1024_K1_P1(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2], a1[N/2], a01[N/2];
  int16_t b0[N/2], b1[N/2], b01[N/2];
  int16_t c0[N/2], c1[N/2], c2[N/2];

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  nttq256_512(a0); nttq256_512(a1);
  nttq256_512(b0); nttq256_512(b1);

  /* Output of the ntt is in [0, q) */
  poly_add(a01,a0,a1,N/2);
  poly_add(b01,b0,b1,N/2);

  /* Polynomial products */
  poly_mul_modq256(c0,a0,b0,N/2,2,ntt_Y_256);
  poly_mul_modq256(c1,a01,b01,N/2,2,ntt_Y_256);
  poly_mul_modq256(c2,a1,b1,N/2,2,ntt_Y_256);

  /* Reconstruction c1 <- c1 - c0 - c2 in ]-3q,3q[ */
  poly_sub(c1,c1,c0,N/2); poly_sub(c1,c1,c2,N/2);
  /* Reconstruction c0 <- c0 + c2*ntt_Y  ]-2q,2q[*/
  poly_shift_modq256(c2,c2,N/2,2,ntt_Y_256); poly_add(c0,c0,c2,N/2);

  /* Reduction before invntt */
  poly_barrettq256(c0,c0,N/2);
  poly_barrettq256(c1,c1,N/2);

  invnttq256_512(c0);
  invnttq256_512(c1);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = c0[i];
    c[2*i+1] = c1[i];
  }
}


void poly_product_1024_K2_P0(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4], a1[N/4], a2[N/4], a3[N/4], a01[N/4], a23[N/4], a02[N/4], a13[N/4], a0123[N/4];

  int16_t b0[N/4], b1[N/4], b2[N/4], b3[N/4], b01[N/4], b23[N/4], b02[N/4], b13[N/4], b0123[N/4];

  int16_t c0[N/4], c1[N/4], c2[N/4], c3[N/4], c01[N/4], c23[N/4], c02[N/4], c13[N/4], c0123[N/4];

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  nttq256_256(a0); nttq256_256(a1); nttq256_256(a2); nttq256_256(a3);
  nttq256_256(b0); nttq256_256(b1); nttq256_256(b2); nttq256_256(b3);
  /* Output of ntt256 in [0,q) */

  /* Start computing the sums needed for Karatsuba */
  poly_add(a01,a0,a1,N/4);
  poly_add(a23,a2,a3,N/4);

  poly_add(a02,a0,a2,N/4);
  poly_add(a13,a1,a3,N/4);
  poly_add(a0123,a02,a13,N/4);

  poly_add(b01,b0,b1,N/4);
  poly_add(b23,b2,b3,N/4);

  poly_add(b02,b0,b2,N/4);
  poly_add(b13,b1,b3,N/4);
  poly_add(b0123,b02,b13,N/4);
  /* Output of the sums in [0,2q) or [0,4q)*/

  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  pointwise_mul_q256(c0,a0,b0,N/4);
  pointwise_mul_q256(c1,a1,b1,N/4);
  pointwise_mul_q256(c01,a01,b01,N/4);

  pointwise_mul_q256(c2,a2,b2,N/4);
  pointwise_mul_q256(c3,a3,b3,N/4);
  pointwise_mul_q256(c23,a23,b23,N/4);

  pointwise_mul_q256(c02,a02,b02,N/4);
  pointwise_mul_q256(c13,a13,b13,N/4);
  /* Need to reduce [0,4q) to ]-2q,2q[ otherwise Montgomery won't handle the product */
  poly_sub_cst(a0123,a0123,2*Q256,N/4); poly_sub_cst(b0123,b0123,2*Q256,N/4);
  pointwise_mul_q256(c0123,a0123,b0123,N/4);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* c02 <- c02 - c0 - c2 ==> c02 in ]-3q,3q[ = ]-9987, 9987[ */
  poly_sub(c02,c02,c0,N/4); poly_sub(c02,c02,c2,N/4);
  /* c13 <- c13 - c1 - c3 ==> c13 in ]-3q, 3q[ */
  poly_sub(c13,c13,c1,N/4); poly_sub(c13,c13,c3,N/4);
  /* c0123 <- c0123 - c01 - c23 ==> c0123 in ]-3q, 3q[ */
  poly_sub(c0123,c0123,c01,N/4); poly_sub(c0123,c0123,c23,N/4);
  /* c0 <- c0 + c2*ntt_Y ==> c0 in ]-2q,2q[ = ]-2q,2q[ */
  pointwise_mul_q256(c2,c2,ntt_Y_256,N/4); poly_add(c0,c0,c2,N/4);
  /* c1 <- c1 + c3*ntt_Y ==> c1 in ]-2q,2q[ */
  pointwise_mul_q256(c3,c3,ntt_Y_256,N/4); poly_add(c1,c1,c3,N/4);
  /* c01 <- c01 + c23*ntt_Y ==> c01 in ]-2q,2q[ */
  pointwise_mul_q256(c23,c23,ntt_Y_256,N/4); poly_add(c01,c01,c23,N/4);

  /* Reconstruct second level of Karatsuba */
  /* c01 <- c01 - c0 - c1 ==> in ]-6q, 6q[ thus reduce c01-c01 */
  poly_sub(c01,c01,c0,N/4); poly_mersq256(c01,c01,N/4); poly_sub(c01,c01,c1,N/4);
  /* c0123 <- c0123 - c02 - c13 ==> in ]-9q, 9q[ thus reduce each term */
  poly_mersq256(c0123,c0123,N/4); poly_mersq256(c02,c02,N/4); poly_mersq256(c13,c13,N/4);
  poly_sub(c0123,c0123,c02,N/4); poly_sub(c0123,c0123,c13,N/4);
  /* c0 <- c0 + c13*ntt_Y ==> in ]-2q,2q[ */
  pointwise_mul_q256(c13,c13,ntt_Y_256,N/4); poly_add(c0,c0,c13,N/4);
  /* c1 <- c1 + c02 ==> in ]-4q,4q[ */
  poly_add(c1,c1,c02,N/4);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq256(c0,c0,N/4);
  poly_barrettq256(c1,c1,N/4);
  poly_barrettq256(c01,c01,N/4);
  poly_barrettq256(c0123,c0123,N/4);

  invnttq256_256(c0);
  invnttq256_256(c1);
  invnttq256_256(c01);
  invnttq256_256(c0123);

  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = c0[i];
    c[4*i+1] = c01[i];
    c[4*i+2] = c1[i];
    c[4*i+3] = c0123[i];
  }
}

#endif
