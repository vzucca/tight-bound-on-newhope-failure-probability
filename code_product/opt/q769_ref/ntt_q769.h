#ifndef NTT769
#define NTT769

#include <stdio.h>

#include "reduce_q769.h"

/* static const uint16_t tree[128] = { */
/*   0, 64, 32, 96, 16, 80, 48, 112, 8, 72, 40, 104, 24, 88, 56, 120,  */
/*   4, 68, 36, 100, 20, 84, 52, 116, 12, 76, 44, 108, 28, 92, 60, 124,  */
/*   2, 66, 34, 98, 18, 82, 50, 114, 10, 74, 42, 106, 26, 90, 58, 122,  */
/*   6, 70, 38, 102, 22, 86, 54, 118, 14, 78, 46, 110, 30, 94, 62, 126,  */
/*   1, 65, 33, 97, 17, 81, 49, 113, 9, 73, 41, 105, 25, 89, 57, 121,  */
/*   5, 69, 37, 101, 21, 85, 53, 117, 13, 77, 45, 109, 29, 93, 61, 125,  */
/*   3, 67, 35, 99, 19, 83, 51, 115, 11, 75, 43, 107, 27, 91, 59, 123,  */
/*   7, 71, 39, 103, 23, 87, 55, 119, 15, 79, 47, 111, 31, 95, 63, 127}; */


/* zetas[i] = M*g^tree[i] with g = 7 */
const int16_t zetas_769[128] = {
  171, 605, 688, 361, 583, 3, 250, 120, 640, 461, 223, 753, 626, 362, 432, 638,
  694, 733, 76, 98, 203, 282, 430, 514, 178, 270, 199, 34, 400, 192, 620, 759,
  689, 423, 645, 2, 114, 147, 715, 497, 600, 288, 161, 754, 683, 51, 405, 502,
  170, 543, 648, 188, 719, 745, 307, 578, 263, 157, 523, 128, 375, 180, 389, 279,
  428, 390, 202, 220, 236, 21, 212, 71, 635, 151, 23, 657, 537, 227, 717, 621,
  244, 517, 532, 686, 652, 436, 703, 522, 477, 352, 624, 238, 493, 575, 495, 699,
  209, 654, 670, 14, 29, 260, 391, 403, 355, 478, 358, 664, 167, 357, 528, 438,
  421, 725, 691, 547, 419, 601, 611, 201, 303, 330, 585, 127, 318, 491, 416, 415
};

/* inv_zetas[i] = M*g^(-tree[i]-1)%q */
const int16_t inv_zetas_769[128] = {
  354, 353, 278, 451, 642, 184, 439, 466, 568, 158, 168, 350, 222, 78, 44, 348,
  331, 241, 412, 602, 105, 411, 291, 414, 366, 378, 509, 740, 755, 99, 115, 560,
  70, 274, 194, 276, 531, 145, 417, 292, 247, 66, 333, 117, 83, 237, 252, 525,
  148, 52, 542, 232, 112, 746, 618, 134, 698, 557, 748, 533, 549, 567, 379, 341,
  490, 380, 589, 394, 641, 246, 612, 506, 191, 462, 24, 50, 581, 121, 226, 599,
  267, 364, 718, 86, 15, 608, 481, 169, 272, 54, 622, 655, 767, 124, 346, 80,
  10, 149, 577, 369, 735, 570, 499, 591, 255, 339, 487, 566, 671, 693, 36, 75,
  131, 337, 407, 143, 16, 546, 308, 129, 649, 519, 766, 186, 408, 81, 164, 598
};

/* ntt_Y stored in Montgonery form (useful for Karatsuba reconstruction)*/
const int16_t ntt_Y_769[128] = {
  428, 341, 390, 379, 202, 567, 220, 549, 236, 533, 21, 748, 212, 557, 71, 698,
  635, 134, 151, 618, 23, 746, 657, 112, 537, 232, 227, 542, 717, 52, 621, 148,
  244, 525, 517, 252, 532, 237, 686, 83, 652, 117, 436, 333, 703, 66, 522, 247,
  477, 292, 352, 417, 624, 145, 238, 531, 493, 276, 575, 194, 495, 274, 699, 70,
  209, 560, 654, 115, 670, 99, 14, 755, 29, 740, 260, 509, 391, 378, 403, 366,
  355, 414, 478, 291, 358, 411, 664, 105, 167, 602, 357, 412, 528, 241, 438, 331,
  421, 348, 725, 44, 691, 78, 547, 222, 419, 350, 601, 168, 611, 158, 201, 568,
  303, 466, 330, 439, 585, 184, 127, 642, 318, 451, 491, 278, 416, 353, 415, 354
};

/* ################ NTT ################ */
/* ntt functions ntt_(degree of input)_(size of ntt) */

/* input array of size 128 */
void nttq769_128(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq769_128\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 128/2;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 128 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q769((int32_t)c[j+l]*zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_769[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 128 ; s++)
  c[s] = barrett_q769(c[s]);
}

/* input array of size 256 */
void nttq769_256(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq769_256\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 256/2;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 256 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u)", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q769((int32_t)c[j+l]*zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_769[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 256 ; s++)
  c[s] = barrett_q769(c[s]);
}

/* input array of size 512 */
void nttq769_512(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq769_512\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 512/2;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 512 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u)", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q769((int32_t)c[j+l]*zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_769[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 512 ; s++)
  c[s] = barrett_q769(c[s]);
}

/* input array of size 1024 */
void nttq769_1024(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq769_1024\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 1024/2;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 1024 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u)", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q769((int32_t)c[j+l]*zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_769[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 1024 ; s++)
  c[s] = barrett_q769(c[s]);
}

/* ################ INVNTT ################ */

void invnttq769_128(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq769_128\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 1;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 128 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u)", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];
        if( (i==4) && (j%32 < 2) )
        c[j] = barrett_q769(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q769((int32_t)c[j+l]*inv_zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_769[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 128 ; j++){
    c[j] = montgomery_reduce_q769((int32_t) c[j]*655); /* 655 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q769;
  }
}

void invnttq769_256(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq769_256\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 2;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 256 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u)", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];
        if( (i==4) && (j%64 < 4) )
        c[j] = barrett_q769(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q769((int32_t)c[j+l]*inv_zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_769[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 256 ; j++){
    c[j] = montgomery_reduce_q769((int32_t) c[j]*655); /* 655 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q769;
  }
}

void invnttq769_512(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq769_512\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 4;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 512 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u)", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];

        if( (i==4) && (j%128 < 8) )
        c[j] = barrett_q769(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q769((int32_t)c[j+l]*inv_zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_769[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 512 ; j++){
    c[j] = montgomery_reduce_q769((int32_t) c[j]*655); /* 655 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q769;
  }
}

void invnttq769_1024(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq769_1024\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 8;
  for(i = 0 ; i < 7 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 1024 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u)", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];

        if( (i==4) && (j%256 < 16) )
        c[j] = barrett_q769(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q769((int32_t)c[j+l]*inv_zetas_769[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_769[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 1024 ; j++){
    c[j] = montgomery_reduce_q769((int32_t) c[j]*655); /* 655 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q769;
  }
}

#endif
