#ifndef NTT64
#define NTT64

#include <stdio.h>

#include "reduce_q64.h"

/* static const uint16_t tree[32] = {
0, 32, 16, 48, 8, 40, 24, 56, 4, 36, 20, 52, 12, 44, 28, 60,
2, 34, 18, 50, 10, 42, 26, 58, 6, 38, 22, 54, 14, 46, 30, 62,
1, 33, 17, 49, 9, 41, 25, 57, 5, 37, 21, 53, 13, 45, 29, 61,
3, 35, 19, 51, 11, 43, 27, 59, 7, 39, 23, 55, 15, 47, 31, 63 }; */


/* zetas[i] = M*g^tree[i] with g = 21 */
const int16_t zetas_64[64] = {
  722, 865, 1125, 1260, 551, 1068, 599, 220, 1387, 1328, 196, 1234, 354, 791, 817, 126,
  1377, 1035, 157, 514, 643, 382, 676, 1208, 161, 913, 487, 320, 1124, 808, 1002, 615,
  1072, 1257, 1081, 1098, 299, 1293, 1307, 393, 947, 1117, 1298, 552, 389, 1112, 249, 1237,
  737, 600, 479, 931, 822, 977, 106, 6, 563, 856, 364, 1084, 1060, 60, 1316, 234
};

/* inv_zetas[i] = M*g^(-tree[i]-1)%q */
const int16_t inv_zetas_64[64] = {
  1175, 93, 1349, 349, 325, 1045, 553, 846, 1403, 1303, 432, 587, 478, 930, 809, 672,
  172, 1160, 297, 1020, 857, 111, 292, 462, 1016, 102, 116, 1110, 311, 328, 152, 337,
  794, 407, 601, 285, 1089, 922, 496, 1248, 201, 733, 1027, 766, 895, 1252, 374, 32,
  1283, 592, 618, 1055, 175, 1213, 81, 22, 1189, 810, 341, 858, 149, 284, 544, 687
};

/* ntt_Y stored in Montgomery form (useful for Karatsuba reconstruction)*/
const int16_t ntt_Y_64[64] __attribute__((aligned(32))) = {
  1072, 337, 1257, 152, 1081, 328, 1098, 311, 299, 1110, 1293, 116, 1307, 102, 393, 1016,
  947, 462, 1117, 292, 1298, 111, 552, 857, 389, 1020, 1112, 297, 249, 1160, 1237, 172,
  737, 672, 600, 809, 479, 930, 931, 478, 822, 587, 977, 432, 106, 1303, 6, 1403,
  563, 846, 856, 553, 364, 1045, 1084, 325, 1060, 349, 60, 1349, 1316, 93, 234, 1175
};

/* ################ NTT ################ */
/* ntt functions nttq64__(size of input) */

/* input array of size 64 */
void nttq64_64(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq64_64\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 64/2;
  for(i = 0 ; i < 6 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 64 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q64((int32_t)c[j+l]*zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_64[k];nb_print++;}
        #endif
        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 64 ; s++)
  c[s] = barrett_q64(c[s]);
}

/* input array of size 128 */
void nttq64_128(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq64_128\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 128/2;
  for(i = 0 ; i < 6 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 128 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q64((int32_t)c[j+l]*zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_64[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 128 ; s++)
  c[s] = barrett_q64(c[s]);
}

/* input array of size 256 */
void nttq64_256(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq64_256\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 256/2;
  for(i = 0 ; i < 6 ; i++){
    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif
    for(s = 0 ; s < 256 ; s = j+l){
      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif
      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q64((int32_t)c[j+l]*zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_64[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 256 ; s++)
  c[s] = barrett_q64(c[s]);
}

/* input array of size 512 */
void nttq64_512(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq64_512\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 512/2;
  for(i = 0 ; i < 6 ; i++){
    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif
    for(s = 0 ; s < 512 ; s = j+l){
      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif
      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q64((int32_t)c[j+l]*zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_64[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 512 ; s++)
  c[s] = barrett_q64(c[s]);
}

/* input array of size 1024 */
void nttq64_1024(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call nttq64_1024\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 1024/2;
  for(i = 0 ; i < 6 ; i++){
    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif
    for(s = 0 ; s < 1024 ; s = j+l){
      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif
      for(j = s ; j < s+l ; j++){
        t = montgomery_reduce_q64((int32_t)c[j+l]*zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=zetas_64[k];nb_print++;}
        #endif

        /* No reductions -> be lazy */
        u = c[j];
        c[j+l] = u - t;
        c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(s = 0 ; s < 1024 ; s++)
  c[s] = barrett_q64(c[s]);
}

/* ################ INVNTT ################ */
void invnttq64_64(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq64_64\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 1;
  for(i = 0 ; i < 6 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 64 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];
        if( (i==3) && (j%16 < 2) )
        c[j] = barrett_q64(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q64((int32_t)c[j+l]*inv_zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_64[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 64 ; j++){
    c[j] = montgomery_reduce_q64((int32_t) c[j]*1012); /* 1012 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q64;
  }
}

void invnttq64_128(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq64_128\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<1;
  for(i = 0 ; i < 6 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 128 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];
        if( (i==3) && (j%32<4) )
        c[j] = barrett_q64(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q64((int32_t)c[j+l]*inv_zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_64[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 128 ; j++){
    c[j] = montgomery_reduce_q64((int32_t) c[j]*1012); /* 1012 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q64;
  }
}

void invnttq64_256(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq64_256\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<2;
  for(i = 0 ; i < 6 ; i++){
  // for(i = 0 ; i < 5 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 256 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];
        if( (i==3) && (j%64 < 8) )
        c[j] = barrett_q64(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q64((int32_t)c[j+l]*inv_zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_64[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 256 ; j++){
    c[j] = montgomery_reduce_q64((int32_t) c[j]*1012); /* 1012 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q64;
  }
}

void invnttq64_512(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq64_512\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<3;
  for(i = 0 ; i < 6 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 512 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];
        if( (i==3) && (j%128 < 16) )
        c[j] = barrett_q64(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q64((int32_t)c[j+l]*inv_zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_64[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 512 ; j++){
    c[j] = montgomery_reduce_q64((int32_t) c[j]*1012); /* 1012 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q64;
  }
}

void invnttq64_1024(int16_t *c){
  #ifdef TEST_MODE_ON
  printf("call invnttq64_1024\n");
  unsigned int nb_print = 0;
  uint16_t last_zetas[16];
  uint16_t ass_pack_id = 0;
  #endif

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<4;
  for(i = 0 ; i < 6 ; i++){

    #ifdef TEST_MODE_ON
    if(nb_print == 16){
      printf("\n");nb_print = 0;
      if (last_zetas[15]==last_zetas[0]) {
        printf("_VAL_16x_(%5u),", last_zetas[0]);
      } else if (last_zetas[7]==last_zetas[0]) {
        printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
      } else if (last_zetas[3]==last_zetas[0]) {
        printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
      } else if (last_zetas[1]==last_zetas[0]) {
        printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
        last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
        last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
      } else {
        printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
        last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
        last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
        last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
        last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
      }
      printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
    }
    printf("\n// LEVEL %u\n", i);
    #endif

    for(s = 0 ; s < 1024 ; s = j+l){

      #ifdef TEST_MODE_ON
      if(nb_print == 16){
        printf("\n");nb_print = 0;
        if (last_zetas[15]==last_zetas[0]) {
          printf("_VAL_16x_(%5u),", last_zetas[0]);
        } else if (last_zetas[7]==last_zetas[0]) {
          printf("_VAL_2x8_(%5u,%5u),", last_zetas[0],last_zetas[8]);
        } else if (last_zetas[3]==last_zetas[0]) {
          printf("_VAL_4x4_(%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
        } else if (last_zetas[1]==last_zetas[0]) {
          printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u),",\
          last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
          last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
        } else {
          printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,",\
          last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
          last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
          last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
          last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
        }
        printf(" // (%u)", ass_pack_id);ass_pack_id+=32;
      }
      #endif

      for(j = s ; j < s+l ; j++){
        t = c[j];
        if( ((i==3) && (j%256 < 32))  )
        c[j] = barrett_q64(t + c[j+l]);
        else
        c[j] = t + c[j+l];

        c[j+l] = t - c[j+l];
        c[j+l] = montgomery_reduce_q64((int32_t)c[j+l]*inv_zetas_64[k]);
        #ifdef TEST_MODE_ON
        if (nb_print < 16) {last_zetas[nb_print]=inv_zetas_64[k];nb_print++;}
        #endif
      }
      k++;
    }
    l<<=1;
  }

  #ifdef TEST_MODE_ON
  if(nb_print == 16){
    printf("\n");nb_print = 0;
    if (last_zetas[15]==last_zetas[0]) {
      printf("_VAL_16x_(%5u)", last_zetas[0]);
    } else if (last_zetas[7]==last_zetas[0]) {
      printf("_VAL_2x8_(%5u,%5u)", last_zetas[0],last_zetas[8]);
    } else if (last_zetas[3]==last_zetas[0]) {
      printf("_VAL_4x4_(%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[4],last_zetas[8],last_zetas[12]);
    } else if (last_zetas[1]==last_zetas[0]) {
      printf("_VAL_8x2_(%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u)",\
      last_zetas[0],last_zetas[2],last_zetas[4],last_zetas[6],\
      last_zetas[8],last_zetas[10],last_zetas[12],last_zetas[14]);
    } else {
      printf("%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u,%5u",\
      last_zetas[0],last_zetas[1],last_zetas[2],last_zetas[3],\
      last_zetas[4],last_zetas[5],last_zetas[6],last_zetas[7],\
      last_zetas[8],last_zetas[9],last_zetas[10],last_zetas[11],\
      last_zetas[12],last_zetas[13],last_zetas[14],last_zetas[15]);
    }
    printf("  // (%u)\n", ass_pack_id);ass_pack_id+=32;
  }
  #endif

  for(j = 0 ; j < 1024 ; j++){
    c[j] = montgomery_reduce_q64((int32_t) c[j]*1012); /* 1012 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q64;
  }
}

#endif
