.include "asm_macros.h"

.macro block_128
load_128_coeffs 2,3,4,5,6,7,8,9
signed_barrett_64_coeffs 2,3,4,5 0,11,5
signed_barrett_64_coeffs 6,7,8,9 0,11,5
save_128_coeffs 2,3,4,5,6,7,8,9
.endm

.global poly_mers_q16_128
poly_mers_q16_128:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_low_mask_16(%rip),%ymm0

/*repeat 2 times*/
block_128

add 	%r11,%rsp

ret

.global poly_mers_q16_64
poly_mers_q16_64:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_low_mask_16(%rip),%ymm0

load_64_coeffs 2,3,4,5
signed_barrett_64_coeffs 2,3,4,5 0,11,5
save_64_coeffs 2,3,4,5

add 	%r11,%rsp

ret
