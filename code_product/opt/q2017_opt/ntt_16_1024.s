.include "asm_macros.h"

.global ntt_16_1024_opt
ntt_16_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
vmovdqa		_low_mask_16(%rip),%ymm2

#level 0

#zetas
vmovdqa		(%rsi),%ymm3

#8 rounds to go

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
add		$128,%rdi

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
save_64_coeffs 4,5,6,7

add		$512,%rdi

# level 0 tested: all good!

#deal with second half (9-16/16 packs of 128)

#level 1

#zetas
# vmovdqa		32(%rsi),%ymm3
vmovdqa		64(%rsi),%ymm3

#4 rounds to go

load_64_coeffs 4,5,6,7
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
save_64_coeffs 8,9,10,11 rdi,512,544,576,608

#deal with third quarter (9-12 out of 16)

#level 2

#zetas
vmovdqa		160(%rsi),%ymm3

load_64_coeffs 8,9,10,11 rdi,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

add		$128,%rdi

#deal with pack 11-12

# level 3-4
#level 3

load_64_coeffs 4,5,6,7
vmovdqa		384(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,800,832
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

sub		$256,%rdi

#deal with pack 9-10
load_128_coeffs 4,5,6,7,8,9,10,11
vmovdqa		352(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,736,768
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

add		$512,%rdi

#deal with fourth quarter (13-16 out of 16)

#level 2

#zetas
vmovdqa		192(%rsi),%ymm3

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

add		$128,%rdi

#deal with pack 15-16

# level 3-4
#level 3

load_64_coeffs 4,5,6,7
vmovdqa		448(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,928,960
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

sub		$256,%rdi

#deal with pack 13-14
load_128_coeffs 4,5,6,7,8,9,10,11
vmovdqa		416(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,864,896
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

sub		$1152,%rdi

#deal with first half (1-8/16 packs of 128)

#level 1

#zetas
vmovdqa		32(%rsi),%ymm3

#4 rounds to go

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
save_64_coeffs 8,9,10,11 rdi,512,544,576,608

#deal with first quarter (1-4 out of 16)

#level 2

#zetas
vmovdqa		96(%rsi),%ymm3

load_64_coeffs 8,9,10,11 rdi,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

add		$128,%rdi


# level 3-4
#level 3

#deal with pack 3-4
load_64_coeffs 4,5,6,7
vmovdqa		256(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,544,576
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

sub		$256,%rdi

#deal with pack 1-2
load_128_coeffs 4,5,6,7,8,9,10,11
vmovdqa		224(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,480,512
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

add		$512,%rdi

#deal with second quarter (5-8 out of 16)

#level 2

#zetas
vmovdqa		128(%rsi),%ymm3

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

add		$128,%rdi

# level 3-4
#level 3

#deal with pack 7-8
load_64_coeffs 4,5,6,7
vmovdqa		320(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,672,704
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

sub		$256,%rdi

#deal with pack 5-6
load_128_coeffs 4,5,6,7,8,9,10,11
vmovdqa		288(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# load_32_coeffs 15,3 rsi,608,640
# ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

add 	%r11,%rsp

ret
