.include "asm_macros.h"
#WARNING please check Barretts
.global invntt_16_1024_opt
invntt_16_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
vmovdqa		_16xv_16(%rip),%ymm2

#first half (1-8/16 packs of 128)

#first quarter (1-4)
#level (0-1)

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		0(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		32(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 1

vmovdqa		256(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

add		$512,%rdi

#second quarter (5-8)

#level 0

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		64(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		96(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 1

vmovdqa		288(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 7,8,9,10 rdi,256,288,320,352

sub		$512,%rdi

#level 2

vmovdqa		384(%rsi),%ymm11
# load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
load_64_coeffs 7,8,9,10
invntt_butterfly_1 7,8,9,10,3,4,5,6,11
save_128_coeffs 7,8,9,10,3,4,5,6 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608

add		$640,%rdi

#second half (9-16/16 packs of 128)

#third quarter (9-12)
#level 0

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		128(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		160(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 1

vmovdqa		320(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

add		$512,%rdi

#fourth quarter (13-16)

#level (0-1)

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		192(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10

add		$256,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10
vmovdqa		224(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6

sub		$128,%rdi

#level 1

vmovdqa		352(%rsi),%ymm11
load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 7,8,9,10 rdi,256,288,320,352

sub		$512,%rdi

#level 2

vmovdqa		416(%rsi),%ymm11
# load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
load_64_coeffs 7,8,9,10
invntt_butterfly_1 7,8,9,10,3,4,5,6,11
save_128_coeffs 7,8,9,10,3,4,5,6 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
add		$128,%rdi
load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,512,544,576,608
save_64_coeffs 3,4,5,6

sub		$512,%rdi

#level 4

#f/zeta
vmovdqa		_F16(%rip),%ymm2
vmovdqa		448(%rsi),%ymm11

load_64_coeffs 3,4,5,6
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
sub		$128,%rdi

load_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
add_q_ifneg 7,8,9,10
signed_montgomery_64x64 2,2,2,2,3,4,5,6
add_q_ifneg 3,4,5,6
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,1024,1056,1088,1120


add 	%r11,%rsp

ret
