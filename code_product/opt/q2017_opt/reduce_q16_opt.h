#ifndef REDUCE16_OPT
#define REDUCE16_OPT

// #include <stdio.h>
#include <stdint.h>
#include "params.h"

/* ############################################################################################## */
/* ############################### Functions for Q16 = 2017 ################################## */
/* ############################################################################################## */

/* Barrett */
void poly_barrettq16_64_opt(int16_t *a) asm("poly_barrett_q16_64");
void poly_barrettq16_128_opt(int16_t *a) asm("poly_barrett_q16_128");
void poly_barrettq16_256_opt(int16_t *a) asm("poly_barrett_q16_256");
void poly_barrettq16_512_opt(int16_t *a) asm("poly_barrett_q16_512");
void poly_barrettq16_1024_opt(int16_t *a) asm("poly_barrett_q16_1024");

/* Mersenne */
void poly_mersq16_64_opt(int16_t* a) asm("poly_mers_q16_64");
void poly_mersq16_128_opt(int16_t* a) asm("poly_mers_q16_128");

/* classic multiplication a <- a * b */
void poly_mul_32_modq16_512_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq16_512_opt");
void poly_mul_16_modq16_256_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq16_256_opt");
void poly_mul_8_modq16_128_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq16_128_opt");
void poly_mul_4_modq16_64_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq16_64_opt");

/* classic shift and multiply by ntt_Y*/
void poly_shift_4_modq16_64_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_4_modq16_64_opt");
void poly_shift_8_modq16_128_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_8_modq16_128_opt");
void poly_shift_16_modq16_256_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_16_modq16_256_opt");

/* sub cst */
void poly_sub1q_cst_q16_512(int16_t*a) asm ("sub_cst1q_q16_512");
void poly_sub4q_cst_q16_128(int16_t*a) asm ("sub_cst4q_q16_128");
void poly_sub4q_cst_q16_64(int16_t*a) asm ("sub_cst4q_q16_64");

#endif
