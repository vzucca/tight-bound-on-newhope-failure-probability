#ifndef KARATSUBA16_OPT
#define KARATSUBA16_OPT

#include <stdint.h>

#include "ntt_q16.h"
#include "ntt_q16_opt.h"
#include "const_16_opt.h"
#include "reduce_q16_opt.h"

/*########################################### n = 512 ############################################*/
/*########################################## q = 2017 ############################################*/

void poly_product_512_K0_P5_opt(int16_t *c, const int16_t *a, const int16_t *b){
  // int16_t a_tmp[N] __attribute__((aligned(32)));
  int16_t b_tmp[N] __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    // a_tmp[i] = a[i];
    c[i] = a[i];
    b_tmp[i] = b[i];
  }

  // ntt_16_512_opt(a_tmp,zetas_q16_512_opt); poly_barrettq16_512_opt(a_tmp);
  ntt_16_512_opt(c,zetas_q16_512_opt); poly_barrettq16_512_opt(c);
  ntt_16_512_opt(b_tmp,zetas_q16_512_opt);
  poly_barrettq16_512_opt(b_tmp);

  // poly_mul_mod_q16(c,a_tmp,b_tmp,N,32,ntt_Y_16);
  poly_mul_32_modq16_512_opt(c,b_tmp,ntt_Y_16_prod_pack32);

  invntt_16_512_opt(c,inv_zetas_q16_512_opt);
}

void poly_product_512_K1_P4_opt(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2] __attribute__((aligned(32))), a1[N/2] __attribute__((aligned(32))), a01[N/2] __attribute__((aligned(32)));
  int16_t b0[N/2] __attribute__((aligned(32))), b1[N/2] __attribute__((aligned(32))), b01[N/2] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  ntt_16_256_opt(a0,zetas_q16_256_opt);  ntt_16_256_opt(a1,zetas_q16_256_opt);
  ntt_16_256_opt(b0,zetas_q16_256_opt);  ntt_16_256_opt(b1,zetas_q16_256_opt);

  /* Output of the ntt is in [0, q) */
  poly_add(a01,a0,a1,N/2);
  poly_add(b01,b0,b1,N/2);

  /* Polynomial products */
  poly_mul_16_modq16_256_opt(a0,b0,ntt_Y_16_prod_pack16);
  poly_mul_16_modq16_256_opt(a01,b01,ntt_Y_16_prod_pack16);
  poly_mul_16_modq16_256_opt(a1,b1,ntt_Y_16_prod_pack16);

  /* Reconstruction a01 <- a01 - a0 - a1 in ]-3q,3q[ */
  poly_sub_256_short(a01,a0); poly_sub_256_short(a01,a1);
  /* Reconstruction a0 <- a0 + a1*ntt_Y  ]-2q,2q[*/
  poly_shift_16_modq16_256_opt(a1,ntt_Y_16); poly_add_256_short(a0,a1);

  /* Reduction before invntt */
  poly_barrettq16_256_opt(a0);
  poly_barrettq16_256_opt(a01);

  invntt_16_256_opt(a0,inv_zetas_q16_256_opt);  invntt_16_256_opt(a01,inv_zetas_q16_256_opt);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = a0[i];
    c[2*i+1] = a01[i];
  }
}

void poly_product_512_K2_P3_opt(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4] __attribute__((aligned(32))), a1[N/4] __attribute__((aligned(32))), a2[N/4] __attribute__((aligned(32))), a3[N/4] __attribute__((aligned(32))), a01[N/4] __attribute__((aligned(32))), a23[N/4] __attribute__((aligned(32))), a02[N/4] __attribute__((aligned(32))), a13[N/4] __attribute__((aligned(32))), a0123[N/4] __attribute__((aligned(32)));

  int16_t b0[N/4] __attribute__((aligned(32))), b1[N/4] __attribute__((aligned(32))), b2[N/4] __attribute__((aligned(32))), b3[N/4] __attribute__((aligned(32))), b01[N/4] __attribute__((aligned(32))), b23[N/4] __attribute__((aligned(32))), b02[N/4] __attribute__((aligned(32))), b13[N/4] __attribute__((aligned(32))), b0123[N/4] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  ntt_16_128_opt(a0,zetas_q16_128_opt); ntt_16_128_opt(a1,zetas_q16_128_opt); ntt_16_128_opt(a2,zetas_q16_128_opt); ntt_16_128_opt(a3,zetas_q16_128_opt);
  ntt_16_128_opt(b0,zetas_q16_128_opt); ntt_16_128_opt(b1,zetas_q16_128_opt); ntt_16_128_opt(b2,zetas_q16_128_opt); ntt_16_128_opt(b3,zetas_q16_128_opt);
  /* Output of ntt128 in [0,q) */

  /* Start computing the sums needed for Karatsuba */
  poly_add_128(a01,a0,a1);
  poly_add_128(a23,a2,a3);

  poly_add_128(a02,a0,a2);
  poly_add_128(a13,a1,a3);
  poly_add_128(a0123,a02,a13);

  poly_add_128(b01,b0,b1);
  poly_add_128(b23,b2,b3);

  poly_add_128(b02,b0,b2);
  poly_add_128(b13,b1,b3);
  poly_add_128(b0123,b02,b13);
  /* Output of the sums in [0,2q) or [0,4q)*/

  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  poly_mul_8_modq16_128_opt(a0,b0,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a1,b1,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a01,b01,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a2,b2,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a3,b3,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a23,b23,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a02,b02,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a13,b13,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a0123,b0123,ntt_Y_16_prod_pack8);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* a02 <- a02 - a0 - a2 ==> a02 in ]-3q,3q[ */
  poly_sub_128_short(a02,a0); poly_sub_128_short(a02,a2);
  /* a13 <- a13 - a1 - a3 ==> a13 in ]-3q,3q[ */
  poly_sub_128_short(a13,a1); poly_sub_128_short(a13,a3);
  /* a0123 <- a0123 - a01 - a23 ==> a0123 in ]-3q,3q[ */
  poly_sub_128_short(a0123,a01); poly_sub_128_short(a0123,a23);
  /* a0 <- a0 + a2*ntt_Y ==> a0 in ]-2q,2q[ = ]-3q,3q[ */
  poly_shift_8_modq16_128_opt(a2,ntt_Y_16_pack8); poly_add_128_short(a0,a2);
  /* a1 <- a1 + a3*ntt_Y ==> a1 in ]-3q,3q[ */
  poly_shift_8_modq16_128_opt(a3,ntt_Y_16_pack8); poly_add_128_short(a1,a3);
  /* a01 <- a01 + a23*ntt_Y ==> a01 in ]-3q,3q[ */
  poly_shift_8_modq16_128_opt(a23,ntt_Y_16_pack8); poly_add_128_short(a01,a23);

  /* Reconstruct second level of Karatsuba */
  /* a01 <- a01 - a0 - a1 ==> in ]-6q, 6q[ */
  poly_sub_128_short(a01,a0); poly_sub_128_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 ==> in ]-9q, 9q[ */
  poly_sub_128_short(a0123,a02); poly_sub_128_short(a0123,a13);
  /* a0 <- a0 + a13*ntt_Y ==> in ]-3q,3q[ */
  poly_shift_8_modq16_128_opt(a13,ntt_Y_16_pack8); poly_add_128_short(a0,a13);
  /* a1 <- a1 + a02 ==> in ]-5q,5q[ */
  poly_add_128_short(a1,a02);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq16_128_opt(a01);
  poly_barrettq16_128_opt(a0123);
  poly_barrettq16_128_opt(a0);
  poly_barrettq16_128_opt(a1);

  invntt_16_128_opt(a0,inv_zetas_q16_128_opt); invntt_16_128_opt(a1,inv_zetas_q16_128_opt); invntt_16_128_opt(a01,inv_zetas_q16_128_opt); invntt_16_128_opt(a0123,inv_zetas_q16_128_opt);

  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = a0[i];
    c[4*i+1] = a01[i];
    c[4*i+2] = a1[i];
    c[4*i+3] = a0123[i];
  }
}

/*########################################### n = 1024 ############################################*/
/*########################################## q = 2017 ############################################*/

void poly_product_1024_K0_P6_opt(int16_t *c, const int16_t *a, const int16_t *b){
 int16_t a_tmp[N] __attribute__((aligned(32))), b_tmp[N] __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  // ntt_q16_1024(a_tmp);
  // ntt_q16_1024(b_tmp);
  ntt_16_1024_opt(a_tmp,zetas_q16_1024_opt);
  ntt_16_1024_opt(b_tmp,zetas_q16_1024_opt);
  poly_barrettq16_1024_opt(a_tmp);
  poly_barrettq16_1024_opt(b_tmp);

  poly_mul_mod_q16(c,a_tmp,b_tmp,N,64,ntt_Y_16);

  // invntt_q16_1024(c);
  invntt_16_1024_opt(c,inv_zetas_q16_1024_opt);
}

void poly_product_1024_K1_P5_opt(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2] __attribute__((aligned(32))), a1[N/2] __attribute__((aligned(32))), a01[N/2] __attribute__((aligned(32)));
  int16_t b0[N/2] __attribute__((aligned(32))), b1[N/2] __attribute__((aligned(32))), b01[N/2] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  // ntt_q16_512(a0); ntt_q16_512(a1);
  // ntt_q16_512(b0); ntt_q16_512(b1);

  ntt_16_512_opt(a0,zetas_q16_512_opt);poly_barrettq16_512_opt(a0);
  ntt_16_512_opt(a1,zetas_q16_512_opt);poly_barrettq16_512_opt(a1);
  ntt_16_512_opt(b0,zetas_q16_512_opt);poly_barrettq16_512_opt(b0);
  ntt_16_512_opt(b1,zetas_q16_512_opt);poly_barrettq16_512_opt(b1);

  /* Output of the ntt is in [0, q) */
  poly_add_512(a01,a0,a1);
  poly_add_512(b01,b0,b1);

  /* Polynomial products */
  // poly_mul_mod_q16(a0,a0,b0,N/2,32,ntt_Y_16);
  // poly_mul_mod_q16(a01,a01,b01,N/2,32,ntt_Y_16);
  // poly_mul_mod_q16(a1,a1,b1,N/2,32,ntt_Y_16);
  poly_mul_32_modq16_512_opt(a0,b0,ntt_Y_16_prod_pack32);
  poly_mul_32_modq16_512_opt(a01,b01,ntt_Y_16_prod_pack32);
  poly_mul_32_modq16_512_opt(a1,b1,ntt_Y_16_prod_pack32);

  /* Reconstruction a01 <- a01 - a0 - a1 in ]-3q,3q[ */
  poly_sub_512_short(a01,a0); poly_sub_512_short(a01,a1);
  /* Reconstruction a0 <- a0 + a1*ntt_Y  ]-2q,2q[*/
  poly_shift_mod_q16(a1,a1,N/2,32,ntt_Y_16); poly_add_512_short(a0,a1);

  /* Reduction before invntt */
  poly_barrettq16_512_opt(a0);
  poly_barrettq16_512_opt(a01);

  // invntt_q16_512(a0);
  // invntt_q16_512(a01);
  invntt_16_512_opt(a0,inv_zetas_q16_512_opt);
  invntt_16_512_opt(a01,inv_zetas_q16_512_opt);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = a0[i];
    c[2*i+1] = a01[i];
  }
}

void poly_product_1024_K2_P4_opt(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4] __attribute__((aligned(32))), a1[N/4] __attribute__((aligned(32))), a2[N/4] __attribute__((aligned(32))), a3[N/4] __attribute__((aligned(32))), a01[N/4] __attribute__((aligned(32))), a23[N/4] __attribute__((aligned(32))), a02[N/4] __attribute__((aligned(32))), a13[N/4] __attribute__((aligned(32))), a0123[N/4] __attribute__((aligned(32)));

  int16_t b0[N/4] __attribute__((aligned(32))), b1[N/4] __attribute__((aligned(32))), b2[N/4] __attribute__((aligned(32))), b3[N/4] __attribute__((aligned(32))), b01[N/4] __attribute__((aligned(32))), b23[N/4] __attribute__((aligned(32))), b02[N/4] __attribute__((aligned(32))), b13[N/4] __attribute__((aligned(32))), b0123[N/4] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  ntt_16_256_opt(a0,zetas_q16_256_opt);  ntt_16_256_opt(a1,zetas_q16_256_opt);   ntt_16_256_opt(a2,zetas_q16_256_opt);  ntt_16_256_opt(a3,zetas_q16_256_opt);
  ntt_16_256_opt(b0,zetas_q16_256_opt);  ntt_16_256_opt(b1,zetas_q16_256_opt);   ntt_16_256_opt(b2,zetas_q16_256_opt);  ntt_16_256_opt(b3,zetas_q16_256_opt);
  /* Output of ntt16 in [0,q) */

  /* Start computing the sums needed for Karatsuba */
  poly_add_256(a01,a0,a1);
  poly_add_256(a23,a2,a3);

  poly_add_256(a02,a0,a2);
  poly_add_256(a13,a1,a3);
  poly_add_256(a0123,a02,a13);

  poly_add_256(b01,b0,b1);
  poly_add_256(b23,b2,b3);

  poly_add_256(b02,b0,b2);
  poly_add_256(b13,b1,b3);
  poly_add_256(b0123,b02,b13);
  /* Output of the sums in [0,2q) or [0,4q)*/

  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  poly_mul_16_modq16_256_opt(a0,b0,ntt_Y_16_prod_pack16);
  poly_mul_16_modq16_256_opt(a1,b1,ntt_Y_16_prod_pack16);
  poly_mul_16_modq16_256_opt(a01,b01,ntt_Y_16_prod_pack16);

  poly_mul_16_modq16_256_opt(a2,b2,ntt_Y_16_prod_pack16);
  poly_mul_16_modq16_256_opt(a3,b3,ntt_Y_16_prod_pack16);
  poly_mul_16_modq16_256_opt(a23,b23,ntt_Y_16_prod_pack16);

  poly_mul_16_modq16_256_opt(a02,b02,ntt_Y_16_prod_pack16);
  poly_mul_16_modq16_256_opt(a13,b13,ntt_Y_16_prod_pack16);

  poly_mul_16_modq16_256_opt(a0123,b0123,ntt_Y_16_prod_pack16);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* a02 <- a02 - a0 - a2 ==> a02 in ]-3q,3q[ */
  poly_sub_256_short(a02,a0); poly_sub_256_short(a02,a2);
  /* a13 <- a13 - a1 - a3 ==> a13 in ]-3q, 3q[ */
  poly_sub_256_short(a13,a1); poly_sub_256_short(a13,a3);
  /* a0123 <- a0123 - a01 - a23 ==> a0123 in ]-3q, 3q[ */
  poly_sub_256_short(a0123,a01); poly_sub_256_short(a0123,a23);
  /* a0 <- a0 + a2*ntt_Y ==> a0 in ]-2q,2q[ = ]-2q,2q[ */
   poly_shift_16_modq16_256_opt(a2,ntt_Y_16); poly_add_256_short(a0,a2);
  /* a1 <- a1 + a3*ntt_Y ==> a1 in ]-2q,2q[ */
  poly_shift_16_modq16_256_opt(a3,ntt_Y_16); poly_add_256_short(a1,a3);
  /* a01 <- a01 + a23*ntt_Y ==> a01 in ]-2q,2q[ */
   poly_shift_16_modq16_256_opt(a23,ntt_Y_16); poly_add_256_short(a01,a23);

  /* Reconstruct second level of Karatsuba */
  /* a01 <- a01 - a0 - a1 ==> in ]-6q, 6q[ */
  poly_sub_256_short(a01,a0); poly_sub_256_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 ==> in ]-9q, 9q[ */
  poly_sub_256_short(a0123,a02); poly_sub_256_short(a0123,a13);
  /* a0 <- a0 + a13*ntt_Y ==> in ]-5q,5q[ */
  poly_shift_16_modq16_256_opt(a13,ntt_Y_16); poly_add_256_short(a0,a13);
  /* a1 <- a1 + a02 ==> in ]-5q,5q[ */
  poly_add_256_short(a1,a02);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq16_256_opt(a0);
  poly_barrettq16_256_opt(a1);
  poly_barrettq16_256_opt(a01);
  poly_barrettq16_256_opt(a0123);

  invntt_16_256_opt(a0,inv_zetas_q16_256_opt);  invntt_16_256_opt(a1,inv_zetas_q16_256_opt);   invntt_16_256_opt(a01,inv_zetas_q16_256_opt);  invntt_16_256_opt(a0123,inv_zetas_q16_256_opt);

  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = a0[i];
    c[4*i+1] = a01[i];
    c[4*i+2] = a1[i];
    c[4*i+3] = a0123[i];
  }
}

void poly_product_1024_K3_P3_opt(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N>>3] __attribute__((aligned(32))), a1[N>>3] __attribute__((aligned(32))), a2[N>>3] __attribute__((aligned(32))), a3[N>>3] __attribute__((aligned(32))), a4[N>>3] __attribute__((aligned(32))), a5[N>>3] __attribute__((aligned(32))), a6[N>>3] __attribute__((aligned(32))), a7[N>>3] __attribute__((aligned(32))), a04[N>>3] __attribute__((aligned(32))), a26[N>>3] __attribute__((aligned(32))), a02[N>>3] __attribute__((aligned(32))), a46[N>>3] __attribute__((aligned(32))), a0246[N>>3] __attribute__((aligned(32))), a15[N>>3] __attribute__((aligned(32))), a37[N>>3] __attribute__((aligned(32))), a13[N>>3] __attribute__((aligned(32))), a57[N>>3] __attribute__((aligned(32))), a1357[N>>3] __attribute__((aligned(32))), a01[N>>3] __attribute__((aligned(32))), a45[N>>3] __attribute__((aligned(32))), a0145[N>>3] __attribute__((aligned(32))), a23[N>>3] __attribute__((aligned(32))), a67[N>>3] __attribute__((aligned(32))), a2367[N>>3] __attribute__((aligned(32))), a0123[N>>3] __attribute__((aligned(32))), a4567[N>>3] __attribute__((aligned(32))), a01234567[N>>3] __attribute__((aligned(32)));

  int16_t b0[N>>3] __attribute__((aligned(32))), b1[N>>3] __attribute__((aligned(32))), b2[N>>3] __attribute__((aligned(32))), b3[N>>3] __attribute__((aligned(32))), b4[N>>3] __attribute__((aligned(32))), b5[N>>3] __attribute__((aligned(32))), b6[N>>3] __attribute__((aligned(32))), b7[N>>3] __attribute__((aligned(32))), b04[N>>3] __attribute__((aligned(32))), b26[N>>3] __attribute__((aligned(32))), b02[N>>3] __attribute__((aligned(32))), b46[N>>3] __attribute__((aligned(32))), b0246[N>>3] __attribute__((aligned(32))), b15[N>>3] __attribute__((aligned(32))), b37[N>>3] __attribute__((aligned(32))), b13[N>>3] __attribute__((aligned(32))), b57[N>>3] __attribute__((aligned(32))), b1357[N>>3] __attribute__((aligned(32))), b01[N>>3] __attribute__((aligned(32))), b45[N>>3] __attribute__((aligned(32))), b0145[N>>3] __attribute__((aligned(32))), b23[N>>3] __attribute__((aligned(32))), b67[N>>3] __attribute__((aligned(32))), b2367[N>>3] __attribute__((aligned(32))), b0123[N>>3] __attribute__((aligned(32))), b4567[N>>3] __attribute__((aligned(32))), b01234567[N>>3] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/8 ; i++){
    a0[i] = a[8*i]; a1[i] = a[8*i+1]; a2[i] = a[8*i+2]; a3[i] = a[8*i+3];
    a4[i] = a[8*i+4]; a5[i] = a[8*i+5]; a6[i] = a[8*i+6]; a7[i] = a[8*i+7];

    b0[i] = b[8*i]; b1[i] = b[8*i+1]; b2[i] = b[8*i+2]; b3[i] = b[8*i+3];
    b4[i] = b[8*i+4]; b5[i] = b[8*i+5]; b6[i] = b[8*i+6]; b7[i] = b[8*i+7];
  }

  ntt_16_128_opt(a0,zetas_q16_128_opt); ntt_16_128_opt(a1,zetas_q16_128_opt); ntt_16_128_opt(a2,zetas_q16_128_opt); ntt_16_128_opt(a3,zetas_q16_128_opt);
  ntt_16_128_opt(a4,zetas_q16_128_opt); ntt_16_128_opt(a5,zetas_q16_128_opt); ntt_16_128_opt(a6,zetas_q16_128_opt); ntt_16_128_opt(a7,zetas_q16_128_opt);

  ntt_16_128_opt(b0,zetas_q16_128_opt); ntt_16_128_opt(b1,zetas_q16_128_opt); ntt_16_128_opt(b2,zetas_q16_128_opt); ntt_16_128_opt(b3,zetas_q16_128_opt);
  ntt_16_128_opt(b4,zetas_q16_128_opt); ntt_16_128_opt(b5,zetas_q16_128_opt); ntt_16_128_opt(b6,zetas_q16_128_opt); ntt_16_128_opt(b7,zetas_q16_128_opt);
  /* Output of ntt_q16 in ]-q,q[ */

  /* Compute the different sums and reduce them for the Montgomery multiplications to come */
  poly_add_128(a04,a0,a4); poly_add_128(a26,a2,a6);
  poly_add_128(a02,a0,a2); poly_add_128(a46,a4,a6);

  poly_add_128(a15,a1,a5); poly_add_128(a37,a3,a7);
  poly_add_128(a13,a1,a3); poly_add_128(a57,a5,a7);

  poly_add_128(a01,a0,a1); poly_add_128(a45,a4,a5);
  poly_add_128(a23,a2,a3); poly_add_128(a67,a6,a7);

  poly_add_128(a0246,a02,a46); poly_add_128(a1357,a13,a57);
  poly_add_128(a0145,a01,a45); poly_add_128(a2367,a23,a67);
  poly_add_128(a0123,a01,a23); poly_add_128(a4567,a45,a67);

  poly_add_128(a01234567,a0123,a4567);

  poly_add_128(b04,b0,b4); poly_add_128(b26,b2,b6);
  poly_add_128(b02,b0,b2); poly_add_128(b46,b4,b6);

  poly_add_128(b15,b1,b5); poly_add_128(b37,b3,b7);
  poly_add_128(b13,b1,b3); poly_add_128(b57,b5,b7);

  poly_add_128(b01,b0,b1); poly_add_128(b45,b4,b5);
  poly_add_128(b23,b2,b3); poly_add_128(b67,b6,b7);

  poly_add_128(b0246,b02,b46); poly_add_128(b1357,b13,b57);
  poly_add_128(b0145,b01,b45); poly_add_128(b2367,b23,b67);
  poly_add_128(b0123,b01,b23); poly_add_128(b4567,b45,b67);

  poly_add_128(b01234567,b0123,b4567);
  /* Output in [0,2q), [0,4q) or  [0,8q) need to reduce [0,8q) to [-4q,4q) otherwise the product won't fit in ]-q*2^15,q*2^15[*/

  /* Compute the products with Montgomery (Montgomery factor will be removed with invntt)*/
  poly_mul_8_modq16_128_opt(a0,b0,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a1,b1,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a2,b2,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a3,b3,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a4,b4,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a5,b5,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a6,b6,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a7,b7,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a04,b04,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a26,b26,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a02,b02,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a46,b46,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a0246,b0246,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a15,b15,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a37,b37,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a13,b13,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a57,b57,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a1357,b1357,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a01,b01,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a45,b45,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a0145,b0145,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a23,b23,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a67,b67,ntt_Y_16_prod_pack8);
  poly_mul_8_modq16_128_opt(a2367,b2367,ntt_Y_16_prod_pack8);

  poly_mul_8_modq16_128_opt(a0123,b0123,ntt_Y_16_prod_pack8);  poly_mul_8_modq16_128_opt(a4567,b4567,ntt_Y_16_prod_pack8);

  /* [0,8q) to [-4q,4q) */
  poly_sub4q_cst_q16_128(a01234567); poly_sub4q_cst_q16_128(b01234567);
  poly_mul_8_modq16_128_opt(a01234567,b01234567,ntt_Y_16_prod_pack8);
  /*############################## Output in ]-q,q[ = ]-2017,2017[ ###############################*/

  /* Start the reconstruction of Karatsuba... */
  /* Begin 1st level */

  /* a04 <- a04 - a0 - a4 in ]-3q,3q[*/
  poly_sub_128_short(a04,a0); poly_sub_128_short(a04,a4);
  /* a26 <- a26 - a2 - a6 in ]-3q,3q[ */
  poly_sub_128_short(a26,a2); poly_sub_128_short(a26,a6);
  /* a0246 <- a0246 - a02 - a46 in ]-3q,3q[ */
  poly_sub_128_short(a0246,a02); poly_sub_128_short(a0246,a46);

  /* a0 <- a0 + a4*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a4,ntt_Y_16_pack8); poly_add_128_short(a0,a4);
  /* a2 <- a2 + a6*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a6,ntt_Y_16_pack8); poly_add_128_short(a2,a6);
  /* a02 <- a02 + a46*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a46,ntt_Y_16_pack8); poly_add_128_short(a02,a46);

  /* a15 <- a15 - a1 - a5 in ]-3q,3q[*/
  poly_sub_128_short(a15,a1); poly_sub_128_short(a15,a5);
  /* a37 <- a37 - a3 - a7 in ]-3q,3q[ */
  poly_sub_128_short(a37,a3); poly_sub_128_short(a37,a7);
  /* a1357 <- a1357 - a13 - a57 in ]-3q,3q[ */
  poly_sub_128_short(a1357,a13); poly_sub_128_short(a1357,a57);

  /* a1 <- a1 + a5*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a5,ntt_Y_16_pack8); poly_add_128_short(a1,a5);
  /* a3 <- a3 + a7*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a7,ntt_Y_16_pack8); poly_add_128_short(a3,a7);
  /* a13 <- a13 + a57*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a57,ntt_Y_16_pack8); poly_add_128_short(a13,a57);

  /* a0145 <- a0145 - a01 - a45 in ]-3q,3q[*/
  poly_sub_128_short(a0145,a01); poly_sub_128_short(a0145,a45);
  /* a2367 <- a2367 - a23 - a67 in ]-3q,3q[ */
  poly_sub_128_short(a2367,a23); poly_sub_128_short(a2367,a67);
  /* a01234567 <- a01234567 - a0123 - a4567 in ]-3q,3q[ */
  poly_sub_128_short(a01234567,a0123); poly_sub_128_short(a01234567,a4567);

  /* a01 <- a01 + a45*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a45,ntt_Y_16_pack8); poly_add_128_short(a01,a45);
  /* a23 <- a23 + a67*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a67,ntt_Y_16_pack8); poly_add_128_short(a23,a67);
  /* a0123 <- a0123 + a4567*ntt_Y in ]-2q, 2q[ */
  poly_shift_8_modq16_128_opt(a4567,ntt_Y_16_pack8); poly_add_128_short(a0123,a4567);

  /* End 1st level */
  /*##############################################################################################*/
  /* Begin 2nd level */

  /* a02 <- a02 - a0 - a2 in ]-6q,6q[*/
  poly_sub_128_short(a02,a0); poly_sub_128_short(a02,a2);
  /* a0246 <- a0246 - a04 - a26 in ]-9q,9q[ */
  poly_sub_128_short(a0246,a04); poly_sub_128_short(a0246,a26);

  /* a13 <- a13 - a1 - a3 in ]-6q,6q[*/
  poly_sub_128_short(a13,a1); poly_sub_128_short(a13,a3);
  /* a1357 <- a1357 - a15 - a37 in ]-9q,9q[ */
  poly_sub_128_short(a1357,a15); poly_sub_128_short(a1357,a37);

  /* a0123 <- a0123 - a01 - a23 in ]-6q,6q[*/
  poly_sub_128_short(a0123,a01); poly_sub_128_short(a0123,a23);
  /* a01234567 <- a01234567 - a0145 - a2367 in ]-9q,9q[ */
  poly_sub_128_short(a01234567,a0145); poly_sub_128_short(a01234567,a2367);

  /* a0 <- a0 + a26*ntt_Y in ]-3q, 3q[ */
  poly_shift_8_modq16_128_opt(a26,ntt_Y_16_pack8); poly_add_128_short(a0,a26);
  /* a1 <- a1 + a37*ntt_Y in ]-3q, 3q[ */
  poly_shift_8_modq16_128_opt(a37,ntt_Y_16_pack8); poly_add_128_short(a1,a37);
  /* a01 <- a01 + a2367*ntt_Y in ]-3q, 3q[ */
  poly_shift_8_modq16_128_opt(a2367,ntt_Y_16_pack8); poly_add_128_short(a01,a2367);

  /* a04 <- a04 + a2 in ]-5q,5q[ */
  poly_add_128_short(a04,a2);
  /* a15 <- a15 + a3 in ]-5q,5q[ */
  poly_add_128_short(a15,a3);
  /* a0145 <- a0145 + a23 in ]-5q,5q[ */
  poly_add_128_short(a0145,a23);

  /* a01 <- a01 - a0 - a1 in ]-9q,9q[ */
  poly_sub_128_short(a01,a0); poly_sub_128_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 in ]-18q,18q[ --> Reduce a0123 - a01 to [-2^15+16q,2^15-15q) --> in [-2^15+10q,2^15-9q) */
  poly_sub_128_short(a0123,a02); poly_mersq16_128_opt(a0123);
  poly_sub_128_short(a0123,a13);
  /* a0145 <- a0145 - a04 - a15 in ]-15q,15q[*/
  poly_sub_128_short(a0145,a04); poly_sub_128_short(a0145,a15);
  /* a01234567 <- a01234567 - a1357 - a0246 in ]-27q,27q[ --> Reduce a01234567 - a1357 to [-2^15+16q,2^15-15q) --> in  [-2^15+7q,2^15-6q) */
  poly_sub_128_short(a01234567,a1357); poly_mersq16_128_opt(a01234567);
  poly_sub_128_short(a01234567,a0246);

  /* End 2nd level */
  /*##############################################################################################*/
  /* Begin 3rd level */

  /* a02 <- a02 + a1 in ]-8q,8q[ */
  poly_add_128_short(a02,a1);
  /* a0246 <- a0246 + a15 in ]-14q,14q[ */
  poly_add_128_short(a0246,a15);
  /* a04 <- a04 + a13 in ]-11q,11q[ */
  poly_add_128_short(a04,a13);
  /* a0 <- a0 + a1357*ntt_Y in ]-4q,4q[ */
  poly_shift_8_modq16_128_opt(a1357,ntt_Y_16_pack8); poly_add_128_short(a0,a1357);

  /* End 3rd and last level */
  /*##############################################################################################*/

  /* Reduce everyone before the inverse NTT ==> coeffs in ]-q,q[ */
  poly_barrettq16_128_opt(a0); poly_barrettq16_128_opt(a01);
  poly_barrettq16_128_opt(a02); poly_barrettq16_128_opt(a0123);
  poly_barrettq16_128_opt(a04); poly_barrettq16_128_opt(a0145);
  poly_barrettq16_128_opt(a0246); poly_barrettq16_128_opt(a01234567);

  invntt_16_128_opt(a0,inv_zetas_q16_128_opt); invntt_16_128_opt(a01,inv_zetas_q16_128_opt); invntt_16_128_opt(a02,inv_zetas_q16_128_opt); invntt_16_128_opt(a0123,inv_zetas_q16_128_opt);
  invntt_16_128_opt(a04,inv_zetas_q16_128_opt); invntt_16_128_opt(a0145,inv_zetas_q16_128_opt); invntt_16_128_opt(a0246,inv_zetas_q16_128_opt); invntt_16_128_opt(a01234567,inv_zetas_q16_128_opt);


  for(unsigned int i = 0 ; i < N/8 ; i++){
    c[8*i] = a0[i]; c[8*i+1] = a01[i];
    c[8*i+2] = a02[i]; c[8*i+3] = a0123[i];
    c[8*i+4] = a04[i]; c[8*i+5] = a0145[i];
    c[8*i+6] = a0246[i]; c[8*i+7] = a01234567[i];
  }
}

/*########################################### NEW STUFF ############################################*/
/*########################################## q = 2017 ############################################*/

void poly_product_512_K3_P2_bis(int16_t* c, const int16_t* a, const int16_t* b, unsigned int n){

  int16_t a0[n>>3] __attribute__((aligned(32))), a1[n>>3] __attribute__((aligned(32))), a2[n>>3] __attribute__((aligned(32))), a3[n>>3] __attribute__((aligned(32))), a4[n>>3] __attribute__((aligned(32))), a5[n>>3] __attribute__((aligned(32))), a6[n>>3] __attribute__((aligned(32))), a7[n>>3] __attribute__((aligned(32))), a04[n>>3] __attribute__((aligned(32))), a26[n>>3] __attribute__((aligned(32))), a02[n>>3] __attribute__((aligned(32))), a46[n>>3] __attribute__((aligned(32))), a0246[n>>3] __attribute__((aligned(32))), a15[n>>3] __attribute__((aligned(32))), a37[n>>3] __attribute__((aligned(32))), a13[n>>3] __attribute__((aligned(32))), a57[n>>3] __attribute__((aligned(32))), a1357[n>>3] __attribute__((aligned(32))), a01[n>>3] __attribute__((aligned(32))), a45[n>>3] __attribute__((aligned(32))), a0145[n>>3] __attribute__((aligned(32))), a23[n>>3] __attribute__((aligned(32))), a67[n>>3] __attribute__((aligned(32))), a2367[n>>3] __attribute__((aligned(32))), a0123[n>>3] __attribute__((aligned(32))), a4567[n>>3] __attribute__((aligned(32))), a01234567[n>>3] __attribute__((aligned(32)));

  int16_t b0[n>>3] __attribute__((aligned(32))), b1[n>>3] __attribute__((aligned(32))), b2[n>>3] __attribute__((aligned(32))), b3[n>>3] __attribute__((aligned(32))), b4[n>>3] __attribute__((aligned(32))), b5[n>>3] __attribute__((aligned(32))), b6[n>>3] __attribute__((aligned(32))), b7[n>>3] __attribute__((aligned(32))), b04[n>>3] __attribute__((aligned(32))), b26[n>>3] __attribute__((aligned(32))), b02[n>>3] __attribute__((aligned(32))), b46[n>>3] __attribute__((aligned(32))), b0246[n>>3] __attribute__((aligned(32))), b15[n>>3] __attribute__((aligned(32))), b37[n>>3] __attribute__((aligned(32))), b13[n>>3] __attribute__((aligned(32))), b57[n>>3] __attribute__((aligned(32))), b1357[n>>3] __attribute__((aligned(32))), b01[n>>3] __attribute__((aligned(32))), b45[n>>3] __attribute__((aligned(32))), b0145[n>>3] __attribute__((aligned(32))), b23[n>>3] __attribute__((aligned(32))), b67[n>>3] __attribute__((aligned(32))), b2367[n>>3] __attribute__((aligned(32))), b0123[n>>3] __attribute__((aligned(32))), b4567[n>>3] __attribute__((aligned(32))), b01234567[n>>3] __attribute__((aligned(32)));

  for(unsigned int j = 0 ; j < n/8 ; j++){
    a0[j] = a[j]; a1[j] = a[64+j]; a2[j] = a[2*64+j]; a3[j] = a[3*64+j];
    a4[j] = a[4*64+j]; a5[j] = a[5*64+j]; a6[j] = a[6*64+j]; a7[j] = a[7*64+j];

    b0[j] = b[j]; b1[j] = b[64+j]; b2[j] = b[2*64+j]; b3[j] = b[3*64+j];
    b4[j] = b[4*64+j]; b5[j] = b[5*64+j]; b6[j] = b[6*64+j]; b7[j] = b[7*64+j];
  }

  /* Compute the different sums and reduce them for the Montgomery multiplications to come */
  poly_add_64(a04,a0,a4); poly_add_64(a26,a2,a6);
  poly_add_64(a02,a0,a2); poly_add_64(a46,a4,a6);

  poly_add_64(a15,a1,a5); poly_add_64(a37,a3,a7);
  poly_add_64(a13,a1,a3); poly_add_64(a57,a5,a7);

  poly_add_64(a01,a0,a1); poly_add_64(a45,a4,a5);
  poly_add_64(a23,a2,a3); poly_add_64(a67,a6,a7);

  poly_add_64(a0246,a02,a46); poly_add_64(a1357,a13,a57);
  poly_add_64(a0145,a01,a45); poly_add_64(a2367,a23,a67);
  poly_add_64(a0123,a01,a23); poly_add_64(a4567,a45,a67);

  poly_add_64(a01234567,a0123,a4567);

  poly_add_64(b04,b0,b4); poly_add_64(b26,b2,b6);
  poly_add_64(b02,b0,b2); poly_add_64(b46,b4,b6);

  poly_add_64(b15,b1,b5); poly_add_64(b37,b3,b7);
  poly_add_64(b13,b1,b3); poly_add_64(b57,b5,b7);

  poly_add_64(b01,b0,b1); poly_add_64(b45,b4,b5);
  poly_add_64(b23,b2,b3); poly_add_64(b67,b6,b7);

  poly_add_64(b0246,b02,b46); poly_add_64(b1357,b13,b57);
  poly_add_64(b0145,b01,b45); poly_add_64(b2367,b23,b67);
  poly_add_64(b0123,b01,b23); poly_add_64(b4567,b45,b67);

  poly_add_64(b01234567,b0123,b4567);
  /* Output in [0,2q), [0,4q) or  [0,8q) need to reduce [0,8q) to [-4q,4q) otherwise the product won't fit in ]-q*2^15,q*2^15[*/

  /* Compute the products with Montgomery (Montgomery factor will be removed with invntt)*/
  poly_mul_4_modq16_64_opt(a0,b0,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a1,b1,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a2,b2,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a3,b3, ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a4,b4,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a5,b5,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a6,b6,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a7,b7,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a04,b04,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a26,b26,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a02,b02,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a46,b46,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a0246,b0246,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a15,b15,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a37,b37,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a13,b13,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a57,b57,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a1357,b1357,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a01,b01,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a45,b45,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a0145,b0145,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a23,b23,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a67,b67,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a2367,b2367,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a0123,b0123,ntt_Y_16_prod_pack4); poly_mul_4_modq16_64_opt(a4567,b4567,ntt_Y_16_prod_pack4);

  /* [0,8q) to [-4q,4q) */
  poly_sub4q_cst_q16_128(a01234567); poly_sub4q_cst_q16_128(b01234567);
  poly_mul_4_modq16_64_opt(a01234567,b01234567,ntt_Y_16_prod_pack4);

  /*############################## Output in ]-q,q[ = ]-3329,3329[ ###############################*/

  /* Start the reconstruction of Karatsuba... */
  /* Begin 1st level */

    /* a04 <- a04 - a0 - a4 in ]-3q,3q[*/
  poly_sub_64_short(a04,a0); poly_sub_64_short(a04,a4);
  /* a26 <- a26 - a2 - a6 in ]-3q,3q[ */
  poly_sub_64_short(a26,a2); poly_sub_64_short(a26,a6);
  /* a0246 <- a0246 - a02 - a46 in ]-3q,3q[ */
  poly_sub_64_short(a0246,a02); poly_sub_64_short(a0246,a46);

  /* a0 <- a0 + a4*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a4,ntt_Y_16_pack4); poly_add_64_short(a0,a4);
  /* a2 <- a2 + a6*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a6,ntt_Y_16_pack4); poly_add_64_short(a2,a6);
  /* a02 <- a02 + a46*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a46,ntt_Y_16_pack4); poly_add_64_short(a02,a46);

  /* a15 <- a15 - a1 - a5 in ]-3q,3q[*/
  poly_sub_64_short(a15,a1); poly_sub_64_short(a15,a5);
  /* a37 <- a37 - a3 - a7 in ]-3q,3q[ */
  poly_sub_64_short(a37,a3); poly_sub_64_short(a37,a7);
  /* a1357 <- a1357 - a13 - a57 in ]-3q,3q[ */
  poly_sub_64_short(a1357,a13); poly_sub_64_short(a1357,a57);

  /* a1 <- a1 + a5*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a5,ntt_Y_16_pack4); poly_add_64_short(a1,a5);
  /* a3 <- a3 + a7*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a7,ntt_Y_16_pack4); poly_add_64_short(a3,a7);
  /* a13 <- a13 + a57*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a57,ntt_Y_16_pack4); poly_add_64_short(a13,a57);

  /* a0145 <- a0145 - a01 - a45 in ]-3q,3q[*/
  poly_sub_64_short(a0145,a01); poly_sub_64_short(a0145,a45);
  /* a2367 <- a2367 - a23 - a67 in ]-3q,3q[ */
  poly_sub_64_short(a2367,a23); poly_sub_64_short(a2367,a67);
  /* a01234567 <- a01234567 - a0123 - a4567 in ]-3q,3q[ */
  poly_sub_64_short(a01234567,a0123); poly_sub_64_short(a01234567,a4567);

  /* a01 <- a01 + a45*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a45,ntt_Y_16_pack4); poly_add_64_short(a01,a45);
  /* a23 <- a23 + a67*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a67,ntt_Y_16_pack4); poly_add_64_short(a23,a67);
  /* a0123 <- a0123 + a4567*ntt_Y in ]-2q, 2q[ */
  poly_shift_4_modq16_64_opt(a4567,ntt_Y_16_pack4); poly_add_64_short(a0123,a4567);

  /* End 1st level */
  /*##############################################################################################*/
  /* Begin 2nd level */

  /* a02 <- a02 - a0 - a2 in ]-6q,6q[*/
  poly_sub_64_short(a02,a0); poly_sub_64_short(a02,a2);
  /* a0246 <- a0246 - a04 - a26 in ]-9q,9q[ */
  poly_sub_64_short(a0246,a04); poly_sub_64_short(a0246,a26);

  /* a13 <- a13 - a1 - a3 in ]-6q,6q[*/
  poly_sub_64_short(a13,a1); poly_sub_64_short(a13,a3);
  /* a1357 <- a1357 - a15 - a37 in ]-9q,9q[ */
  poly_sub_64_short(a1357,a15); poly_sub_64_short(a1357,a37);

  /* a0123 <- a0123 - a01 - a23 in ]-6q,6q[*/
  poly_sub_64_short(a0123,a01); poly_sub_64_short(a0123,a23);
  /* a01234567 <- a01234567 - a0145 - a2367 in ]-9q,9q[ */
  poly_sub_64_short(a01234567,a0145); poly_sub_64_short(a01234567,a2367);

  /* a0 <- a0 + a26*ntt_Y in ]-3q, 3q[ */
  poly_shift_4_modq16_64_opt(a26,ntt_Y_16_pack4); poly_add_64_short(a0,a26);
  /* a1 <- a1 + a37*ntt_Y in ]-3q, 3q[ */
  poly_shift_4_modq16_64_opt(a37,ntt_Y_16_pack4); poly_add_64_short(a1,a37);
  /* a01 <- a01 + a2367*ntt_Y in ]-3q, 3q[ */
  poly_shift_4_modq16_64_opt(a2367,ntt_Y_16_pack4); poly_add_64_short(a01,a2367);

  /* a04 <- a04 + a2 in ]-5q,5q[ */
  poly_add_64_short(a04,a2);
  /* a15 <- a15 + a3 in ]-5q,5q[ */
  poly_add_64_short(a15,a3);
  /* a0145 <- a0145 + a23 in ]-5q,5q[ */
  poly_add_64_short(a0145,a23);

  /* a01 <- a01 - a0 - a1 in ]-9q,9q[ */
  poly_sub_64_short(a01,a0); poly_sub_64_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 in ]-18q,18q[ --> Reduce a0123 to [-2^15+8q,2^15-7q) and a02 to [0,q) --> in ]-29439,29439[ */
  poly_mersq16_64_opt(a0123); poly_barrettq16_64_opt(a02);
  poly_sub_64_short(a0123,a02); poly_sub_64_short(a0123,a13);
  /* a0145 <- a0145 - a04 - a15 in ]-15q,15q[ --> Reduce a0145 and a04 to [-2^15+8q,2^15-7q) --> in ]-32246,32246[ A ]-2^15,2^15[ */
  poly_mersq16_64_opt(a0145); poly_mersq16_64_opt(a04);
  poly_sub_64_short(a0145,a04); poly_sub_64_short(a0145,a15);
  /* a01234567 <- a01234567 - a1357 - a0246 in ]-27q,27q[ --> Reduce all to [-2^15+8q,2^15-7q) --> in ]-25066,21737[ */
  poly_mersq16_64_opt(a01234567); poly_mersq16_64_opt(a1357); poly_mersq16_64_opt(a0246);
  poly_sub_64_short(a01234567,a1357); poly_sub_64_short(a01234567,a0246);

  /* End 2nd level */
  /*##############################################################################################*/
  /* Begin 3rd level */

  /* a02 <- a02 + a1 in ]-3q,4q[ */
  poly_add_64_short(a02,a1);
  /* a0246 <- a0246 + a15 in ]-22781,26110[ */
  poly_add_64_short(a0246,a15);
  /* a04 <- a04 + a13 in ]-26110,29439[ */
  poly_add_64_short(a04,a13);
  /* a0 <- a0 + a1357*ntt_Y in ]-4q,4q[ */
  poly_shift_4_modq16_64_opt(a1357,ntt_Y_16_pack4); poly_add_64_short(a0,a1357);

  /* End 3rd and last level */
  /*##############################################################################################*/

  /* Reduce everyone before the inverse nTT ==> coeffs in ]-q,q[ */
  poly_barrettq16_64_opt(a0); poly_barrettq16_64_opt(a01);
  poly_barrettq16_64_opt(a02); poly_barrettq16_64_opt(a0123);
  poly_barrettq16_64_opt(a04); poly_barrettq16_64_opt(a0145);
  poly_barrettq16_64_opt(a0246); poly_barrettq16_64_opt(a01234567);

  /* invntt_q16_64(a0); invntt_q16_64(a01); */
  /* invntt_q16_64(a02); invntt_q16_64(a0123); */
  /* invntt_q16_64(a04); invntt_q16_64(a0145); */
  /* invntt_q16_64(a0246); invntt_q16_64(a01234567); */

  for(unsigned int i = 0 ; i < n/8 ; i++){
    c[i] = a0[i]; c[64+i] = a01[i];
    c[2*64+i] = a02[i]; c[3*64+i] = a0123[i];
    c[4*64+i] = a04[i]; c[5*64+i] = a0145[i];
    c[6*64+i] = a0246[i]; c[7*64+i] = a01234567[i];

    /* c[8*i] = c0[i]; c[8*i+1] = c01[i]; */
    /* c[8*i+2] = c02[i]; c[8*i+3] = c0123[i]; */
    /* c[8*i+4] = c04[i]; c[8*i+5] = c0145[i]; */
    /* c[8*i+6] = c0246[i]; c[8*i+7] = c01234567[i];  */
  }
}


void poly_product_512_K3_P2_NTT(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[64] __attribute__((aligned(32))), a1[64] __attribute__((aligned(32))), a2[64] __attribute__((aligned(32))), a3[64] __attribute__((aligned(32))), a4[64] __attribute__((aligned(32))), a5[64] __attribute__((aligned(32))), a6[64] __attribute__((aligned(32))), a7[64] __attribute__((aligned(32))), a04[64] __attribute__((aligned(32))), a26[64] __attribute__((aligned(32))), a02[64] __attribute__((aligned(32))), a46[64] __attribute__((aligned(32))), a0246[64] __attribute__((aligned(32))), a15[64] __attribute__((aligned(32))), a37[64] __attribute__((aligned(32))), a13[64] __attribute__((aligned(32))), a57[64] __attribute__((aligned(32))), a1357[64] __attribute__((aligned(32))), a01[64] __attribute__((aligned(32))), a45[64] __attribute__((aligned(32))), a0145[64] __attribute__((aligned(32))), a23[64] __attribute__((aligned(32))), a67[64] __attribute__((aligned(32))), a2367[64] __attribute__((aligned(32))), a0123[64] __attribute__((aligned(32))), a4567[64] __attribute__((aligned(32))), a01234567[64] __attribute__((aligned(32)));

  for(unsigned int j = 0 ; j < 64 ; j++){
    a0[j] = a[j]; a1[j] = a[64+j]; a2[j] = a[2*64+j]; a3[j] = a[3*64+j];
    a4[j] = a[4*64+j]; a5[j] = a[5*64+j]; a6[j] = a[6*64+j]; a7[j] = a[7*64+j];
  }

  /* Compute the different sums and reduce them for the Montgomery multiplications to come */
  poly_add_64(a04,a0,a4); poly_add_64(a26,a2,a6);
  poly_add_64(a02,a0,a2); poly_add_64(a46,a4,a6);

  poly_add_64(a15,a1,a5); poly_add_64(a37,a3,a7);
  poly_add_64(a13,a1,a3); poly_add_64(a57,a5,a7);

  poly_add_64(a01,a0,a1); poly_add_64(a45,a4,a5);
  poly_add_64(a23,a2,a3); poly_add_64(a67,a6,a7);

  poly_add_64(a0246,a02,a46); poly_add_64(a1357,a13,a57);
  poly_add_64(a0145,a01,a45); poly_add_64(a2367,a23,a67);
  poly_add_64(a0123,a01,a23); poly_add_64(a4567,a45,a67);

  poly_add_64(a01234567,a0123,a4567);

  /* Compute the products with Montgomery (Montgomery factor will be removed with invntt)*/
  poly_mul_4_modq16_64_opt(a1,b,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a15,b,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a13,b,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a1357,b,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a01,b,ntt_Y_16_prod_pack4);
  poly_mul_4_modq16_64_opt(a0145,b,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a0123,b,ntt_Y_16_prod_pack4);

  poly_mul_4_modq16_64_opt(a01234567,b,ntt_Y_16_prod_pack4);
  /*############################## Output in ]-q,q[ = ]-3329,3329[ ###############################*/

  /* Start the reconstruction of Karatsuba... */
  /* Begin 1st level */

  /* a15 <- a15 - a1 in ]-2q,2q[*/
  poly_sub_64_short(a15,a1);
  /* a1357 <- a1357 - a13 in ]-2q,2q[ */
  poly_sub_64_short(a1357,a13);
  /* a0145 <- a0145 - a01 in ]-2q,2q[*/
  poly_sub_64_short(a0145,a01);
  /* a01234567 <- a01234567 - a0123 in ]-2q,2q[ */
  poly_sub_64_short(a01234567,a0123);

  /* End 1st level */
  /*##############################################################################################*/
  /* Begin 2nd level */

  /* a13 <- a13 - a1 in ]-2q,2q[*/
  poly_sub_64_short(a13,a1);
  /* a1357 <- a1357 - a15 in ]-4q,4q[ */
  poly_sub_64_short(a1357,a15);
  /* a0123 <- a0123 - a01 in ]-2q,2q[*/
  poly_sub_64_short(a0123,a01);
  /* a01234567 <- a01234567 - a0145 in ]-3q,3q[ */
  poly_sub_64_short(a01234567,a0145);
  /* a01 <- a01 - a1 in ]2q,2q[ */
  poly_sub_64_short(a01,a1);
  /* a0123 <- a0123 - a13 in ]-4q,4q[ */
  poly_sub_64_short(a0123,a13);
  /* a0145 <- a0145 - a15 in ]-4q,4q[ */
  poly_sub_64_short(a0145,a15);
  /* a01234567 <- a01234567 - a1357 in ]-6q,6q[ */
  poly_sub_64_short(a01234567,a1357);

  /* End 2nd level */
  /*##############################################################################################*/
  /* Begin 3rd level */

  /* a1357 <- a1357*ntt_Y in ]-q,q[ */
  poly_shift_4_modq16_64_opt(a1357,ntt_Y_16_pack4);

  /* End 3rd and last level */
  /*##############################################################################################*/


  /* Reduce everyone before the inverse nTT ==> coeffs in ]-q,q[ */
  poly_barrettq16_64_opt(a01); poly_barrettq16_64_opt(a0123);
  poly_barrettq16_64_opt(a13); poly_barrettq16_64_opt(a0145);
  poly_barrettq16_64_opt(a15); poly_barrettq16_64_opt(a01234567);

  for(unsigned int i = 0 ; i < 64 ; i++){
    c[i] = a1357[i]; c[64+i] = a01[i];
    c[2*64+i] = a1[i]; c[3*64+i] = a0123[i];
    c[4*64+i] = a13[i]; c[5*64+i] = a0145[i];
    c[6*64+i] = a15[i]; c[7*64+i] = a01234567[i];
  }
}


const int16_t NTT_Y_K4_P2[64] __attribute__((aligned(32))) = {
  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0,
  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0,
  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0,
  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0,  992,     0,  0,     0 };

void poly_product_1024_K4_P2_opt(int16_t* c, const int16_t* a, const int16_t* b){

  // int16_t a0[N>>1] __attribute__((aligned(32))), a1[N>>1] __attribute__((aligned(32))), a01[N>>1] __attribute__((aligned(32)));
  // int16_t b0[N>>1] __attribute__((aligned(32))), b1[N>>1] __attribute__((aligned(32))), b01[N>>1] __attribute__((aligned(32)));
  // int16_t c0[N>>1] __attribute__((aligned(32))), c1[N>>1] __attribute__((aligned(32))), c01[N>>1] __attribute__((aligned(32)));
  //TODO : cut memory size by switching cX to aX, and use c as temporary variable for the ntt
  int16_t buf[9*(N>>1)] __attribute__((aligned(32)));
  int16_t *a0 =buf       ; int16_t *a1 = a0+(N>>1);
  int16_t *b0 =a1+(N>>1) ; int16_t *b1 = b0+(N>>1);
  int16_t *a01=b1+(N>>1) ; int16_t *b01=a01+(N>>1);

  int16_t *c0 =b01+(N>>1); int16_t *c1 = c0+(N>>1);
  int16_t *c01=c1+(N>>1);

  /* Pack the coefficients in block of 64 for the Karatsuba*/
  // for(unsigned int i = 0 ; i < 8 ; i++)
  //   for(unsigned int j = 0 ; j < 64 ; j++){
  //     a0[i*64+j] = a[16*j+2*i], a1[i*64+j] = a[16*j+2*i+1];
  //     b0[i*64+j] = b[16*j+2*i], b1[i*64+j] = b[16*j+2*i+1];
  //   }
  //
  // ntt_q16_64(a0), ntt_q16_64(a0+64), ntt_q16_64(a0+2*64), ntt_q16_64(a0+3*64), ntt_q16_64(a0+4*64), ntt_q16_64(a0+5*64), ntt_q16_64(a0+6*64), ntt_q16_64(a0+7*64);
  // ntt_q16_64(a1), ntt_q16_64(a1+64), ntt_q16_64(a1+2*64), ntt_q16_64(a1+3*64), ntt_q16_64(a1+4*64), ntt_q16_64(a1+5*64), ntt_q16_64(a1+6*64), ntt_q16_64(a1+7*64);
  // ntt_q16_64(b0), ntt_q16_64(b0+64), ntt_q16_64(b0+2*64), ntt_q16_64(b0+3*64), ntt_q16_64(b0+4*64), ntt_q16_64(b0+5*64), ntt_q16_64(b0+6*64), ntt_q16_64(b0+7*64);
  // ntt_q16_64(b1), ntt_q16_64(b1+64), ntt_q16_64(b1+2*64), ntt_q16_64(b1+3*64), ntt_q16_64(b1+4*64), ntt_q16_64(b1+5*64), ntt_q16_64(b1+6*64), ntt_q16_64(b1+7*64);

  for (size_t i = 0; i < N; i++) {
    b0[i]=a[i];
    a01[i]=b[i];
  }
  ntt_16_1024_opt(b0,zetas_q16_1024_opt);poly_barrettq16_1024_opt(b0);
  ntt_16_1024_opt(a01,zetas_q16_1024_opt);poly_barrettq16_1024_opt(a01);
  for(unsigned int i = 0 ; i < 8 ; i++)
    for(unsigned int j = 0 ; j < 64 ; j++){
      a0[i*64+j] = b0[16*j+2*i], a1[i*64+j] = b0[16*j+2*i+1];
    }

  for(unsigned int i = 0 ; i < 8 ; i++)
    for(unsigned int j = 0 ; j < 64 ; j++){
      b0[i*64+j] = a01[16*j+2*i], b1[i*64+j] = a01[16*j+2*i+1];
    }

  /* Output of the ntt in [0,q) */

  poly_add_512(a01,a0,a1); poly_add_512(b01,b0,b1);

  /* /\* Output in [0,2q), substract q to get it in [-q,q) *\/ */
  poly_sub1q_cst_q16_512(a01);poly_sub1q_cst_q16_512(b01);

  poly_product_512_K3_P2_bis(c0,a0,b0,N/2);
  poly_product_512_K3_P2_bis(c01,a01,b01,N/2);
  poly_product_512_K3_P2_bis(c1,a1,b1,N/2);

  poly_sub_512_short(c01,c0); poly_sub_512_short(c01,c1);
  poly_product_512_K3_P2_NTT(c1,c1,NTT_Y_K4_P2); poly_add_512(c0,c0,c1);

  /* Reduce before invntt */
  poly_barrettq16_512_opt(c0), poly_barrettq16_512_opt(c01);

  /* Unpack the coefficients */
  for(unsigned int i = 0 ; i < 8 ; i++)
    for(unsigned int j = 0 ; j < 64 ; j++){
      c[16*j+2*i] = c0[i*64+j];
      c[16*j+2*i+1] = c01[i*64+j];
    }

  invntt_16_1024_opt(c,inv_zetas_q16_1024_opt);
}


#endif
