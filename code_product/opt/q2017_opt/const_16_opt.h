#ifndef _CONST_16_OPT_H_
#define _CONST_16_OPT_H_

#include <stdint.h>
// clang-format off

#include "params.h"
#include "tab_macros.h"

const uint16_t _16xQ16INV[16] asm("_16xQ16INV") _DEF_TAB16_(Q16INV);
const uint16_t _16xQ16[16] asm("_16xQ16") _DEF_TAB16_(Q16);
const uint16_t _F16[16] asm("_F16") _DEF_TAB16_(F16);
const uint16_t _16xv_16[16] asm("_16xv_16") _DEF_TAB16_(V16);

const uint16_t _16x4Q16[16] asm("_16x4Q16") _DEF_TAB16_(4*Q16);

#define LOW ((1U << 11) - 1)
const uint16_t _low_mask_16[16] asm("_low_mask_16") __attribute__((aligned(32))) = {
  LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW
};
#undef LOW

const int16_t zetas_q16_128_opt[176] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_( 753),
  // LEVEL 1
  _VAL_16x_(1043),
  _VAL_16x_(1176),
  // LEVEL 2
  _VAL_16x_(1954),
  _VAL_16x_( 308),
  _VAL_16x_(1782),
  _VAL_16x_(1373),
  // LEVEL 3
  _VAL_2x8_(1297,1503),
  _VAL_2x8_( 772, 708),
  _VAL_2x8_(1835,1338),
  _VAL_2x8_(1114,1053)
};

const int16_t inv_zetas_q16_128_opt[176] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_2x8_( 964,  903),
  _VAL_2x8_( 679,  182),
  _VAL_2x8_(1309, 1245),
  _VAL_2x8_( 514,  720),
  // LEVEL 1
  _VAL_16x_( 644),
  _VAL_16x_( 235),
  _VAL_16x_(1709),
  _VAL_16x_(  63),
  // LEVEL 2
  _VAL_16x_( 841),
  _VAL_16x_( 974),
  // LEVEL 3
  _VAL_16x_(1722)
};

const int16_t zetas_q16_256_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_(  753), // (0)
  // LEVEL 1
  _VAL_16x_( 1043), // (32)
  _VAL_16x_( 1176), // (64)
  // LEVEL 2
  _VAL_16x_( 1954), // (96)
  _VAL_16x_(  308), // (128)
  _VAL_16x_( 1782), // (160)
  _VAL_16x_( 1373), // (192)
  // LEVEL 3
  _VAL_16x_( 1297), // (224)
  _VAL_16x_( 1503), // (256)
  _VAL_16x_(  772), // (288)
  _VAL_16x_(  708), // (320)
  _VAL_16x_( 1835), // (352)
  _VAL_16x_( 1338), // (384)
  _VAL_16x_( 1114), // (416)
  _VAL_16x_( 1053)  // (448)
};

const int16_t inv_zetas_q16_256_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_(  964), // (0)
  _VAL_16x_(  903), // (32)
  _VAL_16x_(  679), // (64)
  _VAL_16x_(  182), // (96)
  _VAL_16x_( 1309), // (128)
  _VAL_16x_( 1245), // (160)
  _VAL_16x_(  514), // (192)
  _VAL_16x_(  720), // (224)
  // LEVEL 1
  _VAL_16x_(  644), // (256)
  _VAL_16x_(  235), // (288)
  _VAL_16x_( 1709), // (320)
  _VAL_16x_(   63), // (352)
  // LEVEL 2
  _VAL_16x_(  841), // (384)
  _VAL_16x_(  974), // (416)
  // LEVEL 3
  _VAL_16x_( 1722)   // (448)
};

// 1024,512 uses same zetas as 256
#define zetas_q16_512_opt zetas_q16_256_opt
#define inv_zetas_q16_512_opt inv_zetas_q16_256_opt
#define zetas_q16_1024_opt zetas_q16_256_opt
#define inv_zetas_q16_1024_opt inv_zetas_q16_256_opt

const int16_t ntt_Y_16_pack4[16] __attribute__((aligned(32))) = {
  1809, 1632, 644, 929, 1608, 1692, 1983, 426, 284, 465, 1134, 147, 749,1630,79,925
};

const int16_t ntt_Y_16_pack8[16] __attribute__((aligned(32))) = {
  1809, 284,1632, 465, 929, 147, 644,1134,1608, 749,1692,1630, 426, 925,1983,  79
};

#define ntt_Y_16_prod_pack32 ntt_Y_16_prod_pack16

const int16_t ntt_Y_16_prod_pack16[256] __attribute__((aligned(32))) = {
  _VAL_16x_(1809), _VAL_16x_(1608), _VAL_16x_( 284), _VAL_16x_( 749),
  _VAL_16x_(1632), _VAL_16x_(1692), _VAL_16x_( 465), _VAL_16x_(1630),
  _VAL_16x_( 929), _VAL_16x_( 426), _VAL_16x_( 147), _VAL_16x_( 925),
  _VAL_16x_( 644), _VAL_16x_(1983), _VAL_16x_(1134), _VAL_16x_(  79)
};

const int16_t ntt_Y_16_prod_pack8[128] __attribute__((aligned(32))) = {
  _VAL_2x8_(1809,1608), _VAL_2x8_( 284, 749),
  _VAL_2x8_(1632,1692), _VAL_2x8_( 465,1630),
  _VAL_2x8_( 929, 426), _VAL_2x8_( 147, 925),
  _VAL_2x8_( 644,1983), _VAL_2x8_(1134,  79)
};

const int16_t ntt_Y_16_prod_pack4[64] __attribute__((aligned(32))) = {
  _VAL_4x4_(1809,1608, 284, 749),
  _VAL_4x4_(1632,1692, 465,1630),
  _VAL_4x4_( 929, 426, 147, 925),
  _VAL_4x4_( 644,1983,1134,  79)
};


#endif
// clang-format on
