.include "asm_macros.h"

.macro block_128
load_128_coeffs 2,3,4,5,6,7,8,9
gen_barrett_64_coeffs 2,3,4,5 0,1,9
gen_barrett_64_coeffs 6,7,8,9 0,1,9
save_128_coeffs 2,3,4,5,6,7,8,9
.endm

.global poly_barrett_q16_1024
poly_barrett_q16_1024:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_16(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1

/*repeat 8 times*/
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128

add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128

add 	%r11,%rsp

ret

.global poly_barrett_q16_512
poly_barrett_q16_512:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_16(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1

/*repeat 4 times*/
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128

add 	%r11,%rsp

ret

.global poly_barrett_q16_256
poly_barrett_q16_256:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_16(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1

/*repeat 2 times*/
block_128
add $256,%rdi
block_128

add 	%r11,%rsp

ret

.global poly_barrett_q16_128
poly_barrett_q16_128:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_16(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1

block_128

add 	%r11,%rsp

ret

.global poly_barrett_q16_64
poly_barrett_q16_64:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_16(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1

load_64_coeffs 2,3,4,5
gen_barrett_64_coeffs 2,3,4,5 0,1,9
save_64_coeffs 2,3,4,5

add 	%r11,%rsp

ret
