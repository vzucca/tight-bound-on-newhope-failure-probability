.include "asm_macros.h"

.global invntt_16_512_opt
invntt_16_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
vmovdqa		_16xv_16(%rip),%ymm2

#level 0-1

#first round
#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
vmovdqa		128(%rdi),%ymm7
vmovdqa		160(%rdi),%ymm8
vmovdqa		192(%rdi),%ymm9
vmovdqa		224(%rdi),%ymm10

#level 0

#update
vpsubw		%ymm5,%ymm3,%ymm12
vpsubw		%ymm6,%ymm4,%ymm13
vpsubw		%ymm9,%ymm7,%ymm14
vpsubw		%ymm10,%ymm8,%ymm15
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm4,%ymm6,%ymm4
vpaddw		%ymm9,%ymm7,%ymm7
vpaddw		%ymm10,%ymm8,%ymm8

#zetas
vmovdqa		(%rsi),%ymm10
vmovdqa		32(%rsi),%ymm11
# vmovdqa		64(%rsi),%ymm10
# vmovdqa		96(%rsi),%ymm11

#mul
vpmullw		%ymm10,%ymm12,%ymm5
vpmulhw		%ymm10,%ymm12,%ymm12
vpmullw		%ymm10,%ymm13,%ymm6
vpmulhw		%ymm10,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#level 1

#zetas
vmovdqa		256(%rsi),%ymm11
# vmovdqa		288(%rsi),%ymm11

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
vmovdqa		%ymm7,128(%rdi)
vmovdqa		%ymm8,160(%rdi)
vmovdqa		%ymm9,192(%rdi)
vmovdqa		%ymm10,224(%rdi)

add		$256,%rdi

#second round
#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
vmovdqa		128(%rdi),%ymm7
vmovdqa		160(%rdi),%ymm8
vmovdqa		192(%rdi),%ymm9
vmovdqa		224(%rdi),%ymm10

#level 0

#update
vpsubw		%ymm5,%ymm3,%ymm12
vpsubw		%ymm6,%ymm4,%ymm13
vpsubw		%ymm9,%ymm7,%ymm14
vpsubw		%ymm10,%ymm8,%ymm15
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm4,%ymm6,%ymm4
vpaddw		%ymm9,%ymm7,%ymm7
vpaddw		%ymm10,%ymm8,%ymm8

#zetas
# vmovdqa		(%rsi),%ymm10
# vmovdqa		32(%rsi),%ymm11
vmovdqa		64(%rsi),%ymm10
vmovdqa		96(%rsi),%ymm11

#mul
vpmullw		%ymm10,%ymm12,%ymm5
vpmulhw		%ymm10,%ymm12,%ymm12
vpmullw		%ymm10,%ymm13,%ymm6
vpmulhw		%ymm10,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#level 1

#zetas
# vmovdqa		256(%rsi),%ymm10
vmovdqa		288(%rsi),%ymm11

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
# presaved for next step
# vmovdqa		%ymm7,128(%rdi)
# vmovdqa		%ymm8,160(%rdi)
# vmovdqa		%ymm9,192(%rdi)
# vmovdqa		%ymm10,224(%rdi)

sub		$128,%rdi

#first pack of 256

#level 2

#zetas
vmovdqa		384(%rsi),%ymm11

#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
# preloaded from previous step
# vmovdqa		256(%rdi),%ymm7
# vmovdqa		288(%rdi),%ymm8
# vmovdqa		320(%rdi),%ymm9
# vmovdqa		352(%rdi),%ymm10

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
vmovdqa		%ymm7,256(%rdi)
vmovdqa		%ymm8,288(%rdi)
vmovdqa		%ymm9,320(%rdi)
vmovdqa		%ymm10,352(%rdi)

sub		$128,%rdi

#second part of first pack of 256

#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
vmovdqa		256(%rdi),%ymm7
vmovdqa		288(%rdi),%ymm8
vmovdqa		320(%rdi),%ymm9
vmovdqa		352(%rdi),%ymm10

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
vmovdqa		%ymm7,256(%rdi)
vmovdqa		%ymm8,288(%rdi)
vmovdqa		%ymm9,320(%rdi)
vmovdqa		%ymm10,352(%rdi)

add		$512,%rdi

#second block of 256 coeffs

#level 0-1

#first round block 1
#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
vmovdqa		128(%rdi),%ymm7
vmovdqa		160(%rdi),%ymm8
vmovdqa		192(%rdi),%ymm9
vmovdqa		224(%rdi),%ymm10

#level 0

#update
vpsubw		%ymm5,%ymm3,%ymm12
vpsubw		%ymm6,%ymm4,%ymm13
vpsubw		%ymm9,%ymm7,%ymm14
vpsubw		%ymm10,%ymm8,%ymm15
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm4,%ymm6,%ymm4
vpaddw		%ymm9,%ymm7,%ymm7
vpaddw		%ymm10,%ymm8,%ymm8

#zetas
vmovdqa		128(%rsi),%ymm10
vmovdqa		160(%rsi),%ymm11

#mul
vpmullw		%ymm10,%ymm12,%ymm5
vpmulhw		%ymm10,%ymm12,%ymm12
vpmullw		%ymm10,%ymm13,%ymm6
vpmulhw		%ymm10,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#level 1

#zetas
vmovdqa		320(%rsi),%ymm11

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
vmovdqa		%ymm7,128(%rdi)
vmovdqa		%ymm8,160(%rdi)
vmovdqa		%ymm9,192(%rdi)
vmovdqa		%ymm10,224(%rdi)

add		$256,%rdi

#first round block 1
#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
vmovdqa		128(%rdi),%ymm7
vmovdqa		160(%rdi),%ymm8
vmovdqa		192(%rdi),%ymm9
vmovdqa		224(%rdi),%ymm10

#level 0

#update
vpsubw		%ymm5,%ymm3,%ymm12
vpsubw		%ymm6,%ymm4,%ymm13
vpsubw		%ymm9,%ymm7,%ymm14
vpsubw		%ymm10,%ymm8,%ymm15
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm4,%ymm6,%ymm4
vpaddw		%ymm9,%ymm7,%ymm7
vpaddw		%ymm10,%ymm8,%ymm8

#zetas
vmovdqa		192(%rsi),%ymm10
vmovdqa		224(%rsi),%ymm11

#mul
vpmullw		%ymm10,%ymm12,%ymm5
vpmulhw		%ymm10,%ymm12,%ymm12
vpmullw		%ymm10,%ymm13,%ymm6
vpmulhw		%ymm10,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#level 1

#zetas
vmovdqa		352(%rsi),%ymm11

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
# presaved for next step
# vmovdqa		%ymm7,128(%rdi)
# vmovdqa		%ymm8,160(%rdi)
# vmovdqa		%ymm9,192(%rdi)
# vmovdqa		%ymm10,224(%rdi)

sub		$128,%rdi

#second pack of 256

#level 2

#zetas
vmovdqa		416(%rsi),%ymm11

#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
# preloaded from previous step
# vmovdqa		256(%rdi),%ymm7
# vmovdqa		288(%rdi),%ymm8
# vmovdqa		320(%rdi),%ymm9
# vmovdqa		352(%rdi),%ymm10

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
vmovdqa		%ymm7,256(%rdi)
vmovdqa		%ymm8,288(%rdi)
vmovdqa		%ymm9,320(%rdi)
vmovdqa		%ymm10,352(%rdi)

sub		$128,%rdi

#second part of second pack of 256

#load
vmovdqa		(%rdi),%ymm3
vmovdqa		32(%rdi),%ymm4
vmovdqa		64(%rdi),%ymm5
vmovdqa		96(%rdi),%ymm6
vmovdqa		256(%rdi),%ymm7
vmovdqa		288(%rdi),%ymm8
vmovdqa		320(%rdi),%ymm9
vmovdqa		352(%rdi),%ymm10

#update
vpsubw		%ymm7,%ymm3,%ymm12
vpsubw		%ymm8,%ymm4,%ymm13
vpsubw		%ymm9,%ymm5,%ymm14
vpsubw		%ymm10,%ymm6,%ymm15
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6

#mul
vpmullw		%ymm11,%ymm12,%ymm7
vpmulhw		%ymm11,%ymm12,%ymm12
vpmullw		%ymm11,%ymm13,%ymm8
vpmulhw		%ymm11,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm7,%ymm12,%ymm7
vpsubw		%ymm8,%ymm13,%ymm8
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm10,%ymm15,%ymm10

#store
vmovdqa		%ymm3,(%rdi)
vmovdqa		%ymm4,32(%rdi)
vmovdqa		%ymm5,64(%rdi)
vmovdqa		%ymm6,96(%rdi)
vmovdqa		%ymm7,256(%rdi)
vmovdqa		%ymm8,288(%rdi)
vmovdqa		%ymm9,320(%rdi)
vmovdqa		%ymm10,352(%rdi)

# for simplicity goes back to 0
sub		$512,%rdi

#level 3, final level

#f
vmovdqa		_F16(%rip),%ymm2
vmovdqa		448(%rsi),%ymm3

#first block

#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vpsubw		%ymm8,%ymm4,%ymm12
vpsubw		%ymm9,%ymm5,%ymm13
vpsubw		%ymm10,%ymm6,%ymm14
vpsubw		%ymm11,%ymm7,%ymm15
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7

#mul
vpmullw		%ymm3,%ymm12,%ymm8
vpmulhw		%ymm3,%ymm12,%ymm12
vpmullw		%ymm3,%ymm13,%ymm9
vpmulhw		%ymm3,%ymm13,%ymm13
vpmullw		%ymm3,%ymm14,%ymm10
vpmulhw		%ymm3,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm15,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm15,%ymm11,%ymm11

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm15
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#second block

#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vpsubw		%ymm8,%ymm4,%ymm12
vpsubw		%ymm9,%ymm5,%ymm13
vpsubw		%ymm10,%ymm6,%ymm14
vpsubw		%ymm11,%ymm7,%ymm15
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7

#mul
vpmullw		%ymm3,%ymm12,%ymm8
vpmulhw		%ymm3,%ymm12,%ymm12
vpmullw		%ymm3,%ymm13,%ymm9
vpmulhw		%ymm3,%ymm13,%ymm13
vpmullw		%ymm3,%ymm14,%ymm10
vpmulhw		%ymm3,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm15,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm15,%ymm11,%ymm11

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm15
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#third block

#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vpsubw		%ymm8,%ymm4,%ymm12
vpsubw		%ymm9,%ymm5,%ymm13
vpsubw		%ymm10,%ymm6,%ymm14
vpsubw		%ymm11,%ymm7,%ymm15
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7

#mul
vpmullw		%ymm3,%ymm12,%ymm8
vpmulhw		%ymm3,%ymm12,%ymm12
vpmullw		%ymm3,%ymm13,%ymm9
vpmulhw		%ymm3,%ymm13,%ymm13
vpmullw		%ymm3,%ymm14,%ymm10
vpmulhw		%ymm3,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm15,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm15,%ymm11,%ymm11

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm15
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#fourth block

#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vpsubw		%ymm8,%ymm4,%ymm12
vpsubw		%ymm9,%ymm5,%ymm13
vpsubw		%ymm10,%ymm6,%ymm14
vpsubw		%ymm11,%ymm7,%ymm15
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7

#mul
vpmullw		%ymm3,%ymm12,%ymm8
vpmulhw		%ymm3,%ymm12,%ymm12
vpmullw		%ymm3,%ymm13,%ymm9
vpmulhw		%ymm3,%ymm13,%ymm13
vpmullw		%ymm3,%ymm14,%ymm10
vpmulhw		%ymm3,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm10,%ymm10
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm15,%ymm11

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm15,%ymm11,%ymm11

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm15
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm15,%ymm7,%ymm7

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm15
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm15,%ymm15
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add 	%r11,%rsp

ret
