.include "asm_macros.h"
.include "asm_shuffle.h"
.include "product_macros.h"

################################################################################

.global poly_mul_modq16_512_opt
poly_mul_modq16_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
# TODO shift everything in product deg_16_16 to put ymm2 and not ymm6
vmovdqa		_lowdword(%rip),%ymm6

# do it 16 times : 16*16 = 256
prod_deg_32_512

add 	%r11,%rsp
ret

################################################################################

.global poly_mul_modq16_256_opt
poly_mul_modq16_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
# TODO shift everything in product deg_16_16 to put ymm2 and not ymm6
vmovdqa		_lowdword(%rip),%ymm6

# do it 16 times : 16*16 = 256
prod_deg_16_256

add 	%r11,%rsp
ret

################################################################################

.global poly_mul_modq16_128_opt
poly_mul_modq16_128_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

# do it 8 times : 8*16 = 128
product_deg_8_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_64

add 	%r11,%rsp
ret

################################################################################

.global poly_mul_modq16_64_opt
poly_mul_modq16_64_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_4_64

add 	%r11,%rsp
ret
