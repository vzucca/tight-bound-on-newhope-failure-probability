.global ntt_16_128_opt
ntt_16_128_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ16INV(%rip),%ymm0
vmovdqa		_16xQ16(%rip),%ymm1
# vmovdqa		_low_mask_16(%rip),%ymm2 #used for Barrett. But we removed it.

#zetas
vmovdqa		(%rsi),%ymm3

#load
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#level 0

# ----------- SIGNED MONTGOMERY START with A and B -----------

#mul
vpmullw		%ymm3,%ymm8,%ymm12
vpmulhw		%ymm3,%ymm8,%ymm8
vpmullw		%ymm3,%ymm9,%ymm13
vpmulhw		%ymm3,%ymm9,%ymm9
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm8,%ymm12
vpsubw		%ymm13,%ymm9,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS with A and B -----------

#update
vpsubw		%ymm12,%ymm4,%ymm8
vpsubw		%ymm13,%ymm5,%ymm9
vpsubw		%ymm14,%ymm6,%ymm10
vpsubw		%ymm15,%ymm7,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm15,%ymm7,%ymm7


#level 1
#zetas
vmovdqa		32(%rsi),%ymm15
vmovdqa		64(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY START -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm12
vpmulhw		%ymm15,%ymm6,%ymm6
vpmullw		%ymm15,%ymm7,%ymm13
vpmulhw		%ymm15,%ymm7,%ymm7
vpmullw		%ymm3,%ymm10,%ymm14
vpmulhw		%ymm3,%ymm10,%ymm10
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#update
vpsubw		%ymm12,%ymm4,%ymm6
vpsubw		%ymm13,%ymm5,%ymm7
vpsubw		%ymm14,%ymm8,%ymm10
vpsubw		%ymm15,%ymm9,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm9,%ymm9


#level 2
#zetas
vmovdqa		96(%rsi),%ymm13
vmovdqa		128(%rsi),%ymm14
vmovdqa		160(%rsi),%ymm15
vmovdqa		192(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9
vpmullw		%ymm3,%ymm11,%ymm15
vpmulhw		%ymm3,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm15,%ymm15
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm15,%ymm15
vpsubw		%ymm12,%ymm5,%ymm12
vpsubw		%ymm13,%ymm7,%ymm13
vpsubw		%ymm14,%ymm9,%ymm14
vpsubw		%ymm15,%ymm11,%ymm15

# ----------- SIGNED MONTGOMERY ENDS -----------

#update
vpsubw		%ymm12,%ymm4,%ymm5
vpsubw		%ymm13,%ymm6,%ymm7
vpsubw		%ymm14,%ymm8,%ymm9
vpsubw		%ymm15,%ymm10,%ymm11
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm6,%ymm6
vpaddw		%ymm14,%ymm8,%ymm8
vpaddw		%ymm15,%ymm10,%ymm10


#level 3
#zetas
vmovdqa		224(%rsi),%ymm12
vmovdqa		256(%rsi),%ymm13
vmovdqa		288(%rsi),%ymm14
vmovdqa		320(%rsi),%ymm15

#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------
vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10


# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm12,%ymm4,%ymm11
vpmulhw		%ymm12,%ymm4,%ymm4
vpmullw		%ymm13,%ymm6,%ymm12
vpmulhw		%ymm13,%ymm6,%ymm6
vpmullw		%ymm14,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm8,%ymm8
vpmullw		%ymm15,%ymm10,%ymm14
vpmulhw		%ymm15,%ymm10,%ymm10

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm11
vpsubw		%ymm12,%ymm6,%ymm12
vpsubw		%ymm13,%ymm8,%ymm13
vpsubw		%ymm14,%ymm10,%ymm14

# ----------- SIGNED MONTGOMERY ENDS -----------

#update
vpsubw		%ymm11,%ymm3,%ymm4
vpsubw		%ymm12,%ymm5,%ymm6
vpsubw		%ymm13,%ymm7,%ymm8
# vpsubw		%ymm14,%ymm9,%ymm10
vpsubw		%ymm14,%ymm9,%ymm15
vpaddw		%ymm11,%ymm3,%ymm3
vpaddw		%ymm12,%ymm5,%ymm5
vpaddw		%ymm13,%ymm7,%ymm7
vpaddw		%ymm14,%ymm9,%ymm9

#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------
# vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x00,%ymm4,%ymm3,%ymm10
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
# vshufpd		$0x00,%ymm10,%ymm9,%ymm8
# vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
vshufpd		$0x00,%ymm15,%ymm9,%ymm8
vshufpd		$0x0F,%ymm15,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5

#store
vmovdqa		%ymm11,(%rdi)
vmovdqa		%ymm12,32(%rdi)
vmovdqa		%ymm13,64(%rdi)
vmovdqa		%ymm14,96(%rdi)
vmovdqa		%ymm15,128(%rdi)
vmovdqa		%ymm3,160(%rdi)
vmovdqa		%ymm4,192(%rdi)
vmovdqa		%ymm5,224(%rdi)

add 	%r11,%rsp

ret
