#ifndef NTT16_OPT
#define NTT16_OPT

#include <stdio.h>

/* ################ NTT ################ */
/* ntt functions nttq16__(size of input) */
#include "const_16_opt.h"

void ntt_16_128_opt(int16_t *inout, const int16_t *zetas) asm("ntt_16_128_opt");
void invntt_16_128_opt(int16_t *inout, const int16_t *zetas) asm("invntt_16_128_opt");

void ntt_16_256_opt(int16_t *inout, const int16_t *zetas) asm("ntt_16_256_opt");
void invntt_16_256_opt(int16_t *inout, const int16_t *zetas) asm("invntt_16_256_opt");

void ntt_16_512_opt(int16_t *inout, const int16_t *zetas) asm("ntt_16_512_opt");
void invntt_16_512_opt(int16_t *inout, const int16_t *zetas) asm("invntt_16_512_opt");

void ntt_16_1024_opt(int16_t *inout, const int16_t *zetas) asm("ntt_16_1024_opt");
void invntt_16_1024_opt(int16_t *inout, const int16_t *zetas) asm("invntt_16_1024_opt");

#endif
