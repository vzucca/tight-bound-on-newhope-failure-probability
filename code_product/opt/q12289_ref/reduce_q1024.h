#ifndef REDUCE1024
#define REDUCE1024

// #include <stdio.h>
#include <stdint.h>
#include "params.h"

/* ############################################################################################## */
/* ############################### Functions for Q1024 = 12289 ################################## */
/* ############################################################################################## */

/* v128 = 2^25/q16 + 1*/
// const int16_t V1024 = 21844;

int16_t barrett_q1024_ref(int16_t a){

  int16_t t;

  t = ((int32_t) V1024*a)>>28;

  t *= Q1024;

  return a - t;
}

/* Q^(-1) mod M where M = 2^16 */
// const int16_t Q1024INV = -12287;

int16_t montgomery_reduce_q1024_ref(int32_t a)
{
  int32_t t;
  int16_t m;

  m = (a & UINT16_MAX) * Q1024INV;

  t = (int32_t)m * Q1024;
  t >>= 16;

  return (a>>16)-t;
}

void poly_barrettq1024_ref(int16_t *r, int16_t *a, unsigned int n){
  for(unsigned int i = 0 ; i < n ; i++)
    r[i] = barrett_q1024_ref(a[i]);
}

/* coefficient-wise multiplication */
void pointwise_mul_q1024_ref(int16_t *c, const int16_t *a, const int16_t* b, unsigned int n){

  for(unsigned int i = 0 ; i < n ; i++)
    c[i] = montgomery_reduce_q1024_ref((int32_t)a[i]*b[i]); /* Output ab*M^(-1) */

}

/* Input in (-q,q) output in [0,q) */
uint16_t positive_modulo_q1024_ref(int16_t a) {
  int16_t t;

  t = a >> 15;

  return a + (t & Q1024);
}


#endif
