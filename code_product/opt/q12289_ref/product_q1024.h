#ifndef KARATSUBA1024
#define KARATSUBA1024

#include <stdint.h>

#include "reduce_q1024.h"
#include "ntt_q1024.h"
#include "ntt_q512.h"

/*########################################### n = 512 ############################################*/
/*########################################## q = 2017 ############################################*/

void poly_product_512_K0_P0(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t a_tmp[N], b_tmp[N];

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  ntt_512(a_tmp);
  ntt_512(b_tmp);

  pointwise_mul_q1024_ref(c,a_tmp,b_tmp,N);

  invntt_512(c);
}


/*########################################### n = 1024 ############################################*/
/*########################################## q = 2017 ############################################*/

void poly_product_1024_K0_P0(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t a_tmp[N], b_tmp[N];

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  ntt_1024(a_tmp);
  ntt_1024(b_tmp);

  pointwise_mul_q1024_ref(c,a_tmp,b_tmp,N);

  invntt_1024(c);
}

#endif
