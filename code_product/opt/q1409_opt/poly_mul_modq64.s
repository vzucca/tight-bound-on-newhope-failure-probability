.include "asm_macros.h"
.include "asm_shuffle.h"
.include "product_macros.h"

.global poly_mul_1_modq64_128_opt
poly_mul_1_modq64_128_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_1_128

add 	%r11,%rsp
ret

.global poly_mul_8_modq64_512_opt
poly_mul_8_modq64_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_8_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_256

add 	%r11,%rsp
ret

.global poly_mul_4_modq64_256_opt
poly_mul_4_modq64_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_4_256

add 	%r11,%rsp
ret

.global poly_mul_16_modq64_1024_opt
poly_mul_16_modq64_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1
# TODO shift everything in product deg_16_16 to put ymm2 and not ymm6
vmovdqa		_lowdword(%rip),%ymm6

# do it 4 times : 4*256
prod_deg_16_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
prod_deg_16_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
prod_deg_16_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
prod_deg_16_256

add 	%r11,%rsp
ret
