#ifndef REDUCE64_OPT
#define REDUCE64_OPT

// #include <stdio.h>
#include <stdint.h>
#include "params.h"

/* ############################################################################################## */
/* ############################### Functions for Q128 = 3329 ################################## */
/* ############################################################################################## */

/* coefficient-wise multiplication */
void pointwise_mul_q64_256_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q64_256_opt");
void pointwise_mul_q64_512_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q64_512_opt");

/* short version a <- a*b */
void pointwise_mul_q64_128_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q64_128_opt_short");
void pointwise_mul_q64_256_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q64_256_opt_short");
void pointwise_mul_q64_512_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q64_512_opt_short");

/* Barrett */
void poly_barrettq64_128_opt(int16_t *a) asm("poly_barrett_q64_128");
void poly_barrettq64_256_opt(int16_t *a) asm("poly_barrett_q64_256");
void poly_barrettq64_512_opt(int16_t *a) asm("poly_barrett_q64_512");
void poly_barrettq64_1024_opt(int16_t *a) asm("poly_barrett_q64_1024");

/* Mersenne */
void poly_mersq64_128_opt(int16_t* a) asm("poly_mers_q64_128");

/* classic multiplication a <- a * b */
void poly_mul_1_modq64_128_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_1_modq64_128_opt");
void poly_mul_4_modq64_256_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_4_modq64_256_opt");
void poly_mul_8_modq64_512_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_8_modq64_512_opt");
void poly_mul_16_modq64_1024_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_16_modq64_1024_opt");

/* classic shift and multiply by ntt_Y*/
void poly_shift_2_modq64_128_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_2_modq64_128_opt");
void poly_shift_4_modq64_256_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_4_modq64_256_opt");
void poly_shift_8_modq64_512_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_8_modq64_512_opt");

/* sub cst */
void poly_sub4q_cst_q64_128(int16_t*a) asm ("sub_cst4q_q64_128");

#endif
