.include "asm_macros.h"

.macro shuffle_pre4
#shuffle
vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10
.endm

.macro shuffle_pre5
#shuffle
vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10
.endm

.macro final_shuffle
#shuffle
vpshufd		$0xB1,%ymm11,%ymm12
# vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm11,%ymm13,%ymm10
# vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5
.endm

.global ntt_64_256_opt
ntt_64_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1

#zetas
vmovdqa		(%rsi),%ymm3

#level 0
#first round
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
add		$128,%rdi
#second round
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#first round
#level 1
#zetas
vmovdqa		32(%rsi),%ymm3
load_128_coeffs 4,5,6,7,8,9,10,11
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 2
load_32_coeffs 15,3 rsi,96,128
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 3
load_64_coeffs 13,14,15,3 rsi,224,256,288,320
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 4
load_64_coeffs 12,13,14,15 rsi,480,512,544,576
shuffle_pre4
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14

#level 5
load_64_coeffs 12,13,14,15 rsi,736,768,800,832
shuffle_pre5
ntt_butterfly_4 11,4,6,8,3,5,7,9,12,13,14,15 0,1,10,12,13,14
final_shuffle

save_128_coeffs 11,12,13,14,15,3,4,5

add		$256,%rdi

#second round
#zetas
vmovdqa		64(%rsi),%ymm3

#load
load_128_coeffs 4,5,6,7,8,9,10,11
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 2
load_32_coeffs 15,3 rsi,160,192
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 3
load_64_coeffs 13,14,15,3 rsi,352,384,416,448
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 4
load_64_coeffs 12,13,14,15 rsi,608,640,672,704
shuffle_pre4
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14

#level 5
load_64_coeffs 12,13,14,15 rsi,864,896,928,960
shuffle_pre5
ntt_butterfly_4 11,4,6,8,3,5,7,9,12,13,14,15 0,1,10,12,13,14
final_shuffle

save_128_coeffs 11,12,13,14,15,3,4,5

add 	%r11,%rsp

ret
