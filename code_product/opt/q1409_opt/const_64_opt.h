#ifndef _CONST_64_OPT_H_
#define _CONST_64_OPT_H_

#include <stdint.h>
// clang-format off

#include "params.h"
#include "tab_macros.h"

const uint16_t _16xQ64INV[16] asm("_16xQ64INV") _DEF_TAB16_(Q64INV);
const uint16_t _16xQ64[16] asm("_16xQ64") _DEF_TAB16_(Q64);
const uint16_t _F64[16] asm("_F64") _DEF_TAB16_(F64);
const uint16_t _16xv_64[16] asm("_16xv_64") _DEF_TAB16_(V64);

const uint16_t _16x4Q64[16] asm("_16x4Q64") _DEF_TAB16_(4*Q64);

#define LOW ((1U << 11) - 1)
const uint16_t _low_mask_64[16] asm("_low_mask_64") __attribute__((aligned(32))) = {
  LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW
};
#undef LOW

/* mapping from packs of 16 4*(1,2,4,3)*/
const int16_t ntt_Y_64_pack4[128] __attribute__((aligned(32))) = {
  // 1072,  337, 1257,  152,
  // 1081,  328, 1098,  311,
  //  299, 1110, 1293,  116,
  // 1307,  102,  393, 1016,
  1072, 1081, 1307,  299,
   337,  328,  102, 1110,
  1257, 1098,  393, 1293,
   152,  311, 1016,  116,
  // 947,  462, 1117,  292,
  //1298,  111,  552,  857,
  // 389, 1020, 1112,  297,
  // 249, 1160, 1237,  172,
   947, 1298,  249,  389,
   462,  111, 1160, 1020,
  1117,  552, 1237, 1112,
   292,  857,  172,  297,
  // 737,  672,  600,  809,
  // 479,  930,  931,  478,
  // 822,  587,  977,  432,
  // 106, 1303,    6, 1403,
   737,  479,  106,  822,
   672,  930, 1303,  587,
   600,  931,    6,  977,
   809,  478, 1403,  432,
  //  563,  846,  856,  553,
  //  364, 1045, 1084,  325,
  // 1060,  349,   60, 1349,
  // 1316,   93, 234,  1175
   563,  364, 1316, 1060,
   846, 1045,   93,  349,
   856, 1084,  234,   60,
   553,  325, 1175, 1349
};

/* mapping from pack of 16 1*(1,3,5,7,9,11,13,15,2,4,6,8,10,12,14,16)*/
const int16_t ntt_Y_64_pack8[128] __attribute__((aligned(32))) = {
  // 1072,  337, 1257,  152, 1081,  328, 1098,  311,  299, 1110, 1293,  116, 1307,  102,  393, 1016,
  1072, 1257, 1081, 1098,  299, 1293, 1307,  393,
   337,  152,  328,  311, 1110,  116,  102, 1016,
  // 947,  462, 1117,  292, 1298,  111,  552,  857,  389, 1020, 1112,  297,  249, 1160, 1237,  172,
   947, 1117, 1298,  552,  389, 1112,  249, 1237,
   462,  292,  111,  857, 1020,  297, 1160,  172,
  // 737,  672,  600,  809,  479,  930,  931,  478,  822,  587,  977,  432,  106, 1303,    6, 1403,
   737,  600,  479,  931,  822,  977,  106,    6,
   672,  809,  930,  478,  587,  432, 1303, 1403,
  // 563,  846,  856,  553,  364, 1045, 1084,  325, 1060,  349,   60, 1349, 1316,  93,  234,  1175
   563,  856,  364, 1084, 1060,   60, 1316,  234,
   846,  553, 1045,  325,  349, 1349,   93, 1175
};

const int16_t ntt_Y_64_prod_pack8[64*8] __attribute__((aligned(32))) = {
  _VAL_2x8_(1072, 337), _VAL_2x8_(1257, 152), _VAL_2x8_(1081, 328), _VAL_2x8_(1098, 311),
  _VAL_2x8_(299, 1110), _VAL_2x8_(1293, 116), _VAL_2x8_(1307, 102), _VAL_2x8_(393, 1016),
  _VAL_2x8_(947, 462), _VAL_2x8_(1117, 292), _VAL_2x8_(1298, 111), _VAL_2x8_(552, 857),
  _VAL_2x8_(389, 1020), _VAL_2x8_(1112, 297), _VAL_2x8_(249, 1160), _VAL_2x8_(1237, 172),
  _VAL_2x8_(737, 672), _VAL_2x8_(600, 809), _VAL_2x8_(479, 930), _VAL_2x8_(931, 478),
  _VAL_2x8_(822, 587), _VAL_2x8_(977, 432), _VAL_2x8_(106, 1303), _VAL_2x8_(6, 1403),
  _VAL_2x8_(563, 846), _VAL_2x8_(856, 553), _VAL_2x8_(364, 1045), _VAL_2x8_(1084, 325),
  _VAL_2x8_(1060, 349), _VAL_2x8_(60, 1349), _VAL_2x8_(1316, 93), _VAL_2x8_(234, 1175)
};

const int16_t ntt_Y_64_prod_pack4[64*4] __attribute__((aligned(32))) = {
  _VAL_4x4_(1072, 337,1257, 152), _VAL_4x4_(1081, 328,1098, 311),
  _VAL_4x4_(299, 1110,1293, 116), _VAL_4x4_(1307, 102,393, 1016),
  _VAL_4x4_(947, 462,1117, 292), _VAL_4x4_(1298, 111,552, 857),
  _VAL_4x4_(389, 1020,1112, 297), _VAL_4x4_(249, 1160,1237, 172),
  _VAL_4x4_(737, 672,600, 809), _VAL_4x4_(479, 930,931, 478),
  _VAL_4x4_(822, 587,977, 432), _VAL_4x4_(106, 1303,6, 1403),
  _VAL_4x4_(563, 846,856, 553), _VAL_4x4_(364, 1045,1084, 325),
  _VAL_4x4_(1060, 349,60, 1349), _VAL_4x4_(1316, 93,234, 1175)
};

const int16_t ntt_Y_64_prod_pack16[64*16] __attribute__((aligned(32))) = {
  _VAL_16x_(1072), _VAL_16x_(337), _VAL_16x_(1257), _VAL_16x_(152),
  _VAL_16x_(1081), _VAL_16x_(328), _VAL_16x_(1098), _VAL_16x_(311),
  _VAL_16x_(299), _VAL_16x_(1110), _VAL_16x_(1293), _VAL_16x_(116),
  _VAL_16x_(1307), _VAL_16x_(102), _VAL_16x_(393), _VAL_16x_(1016),
  _VAL_16x_(947), _VAL_16x_(462), _VAL_16x_(1117), _VAL_16x_(292),
  _VAL_16x_(1298), _VAL_16x_(111), _VAL_16x_(552), _VAL_16x_(857),
  _VAL_16x_(389), _VAL_16x_(1020), _VAL_16x_(1112), _VAL_16x_(297),
  _VAL_16x_(249), _VAL_16x_(1160), _VAL_16x_(1237), _VAL_16x_(172),
  _VAL_16x_(737), _VAL_16x_(672), _VAL_16x_(600), _VAL_16x_(809),
  _VAL_16x_(479), _VAL_16x_(930), _VAL_16x_(931), _VAL_16x_(478),
  _VAL_16x_(822), _VAL_16x_(587), _VAL_16x_(977), _VAL_16x_(432),
  _VAL_16x_(106), _VAL_16x_(1303), _VAL_16x_(6), _VAL_16x_(1403),
  _VAL_16x_(563), _VAL_16x_(846), _VAL_16x_(856), _VAL_16x_(553),
  _VAL_16x_(364), _VAL_16x_(1045), _VAL_16x_(1084), _VAL_16x_(325),
  _VAL_16x_(1060), _VAL_16x_(349), _VAL_16x_(60), _VAL_16x_(1349),
  _VAL_16x_(1316), _VAL_16x_(93), _VAL_16x_(234), _VAL_16x_(1175)
};

const uint16_t zetas_64_64_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_(  865), // (0)
  // LEVEL 1
  _VAL_16x_( 1125), // (32)
  _VAL_16x_( 1260), // (64)
  // LEVEL 2
  _VAL_2x8_(  551, 1068), // (96)
  _VAL_2x8_(  599,  220), // (128)
  // LEVEL 3
  _VAL_4x4_( 1387, 1328,  196, 1234), // (160)
  _VAL_4x4_(  354,  791,  817,  126), // (192)
  // LEVEL 4
  _VAL_8x2_( 1377, 1035,  157,  514,  643,  382,  676, 1208), // (224)
  _VAL_8x2_(  161,  913,  487,  320, 1124,  808, 1002,  615), // (256)
  // LEVEL 5
  1072, 1257, 1081, 1098,  299, 1293, 1307,  393,  947, 1117, 1298,  552,  389, 1112,  249, 1237, // (288)
  737,  600,  479,  931,  822,  977,  106,    6,  563,  856,  364, 1084, 1060,   60, 1316,  234  // (320)
};

const uint16_t zetas_64_128_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_(  865), // (0)
  // LEVEL 1
  _VAL_16x_( 1125), // (32)
  _VAL_16x_( 1260), // (64)
  // LEVEL 2
  _VAL_16x_(  551), // (96)
  _VAL_16x_( 1068), // (128)
  _VAL_16x_(  599), // (160)
  _VAL_16x_(  220), // (192)
  // LEVEL 3
  _VAL_2x8_( 1387, 1328), // (224)
  _VAL_2x8_(  196, 1234), // (256)
  _VAL_2x8_(  354,  791), // (288)
  _VAL_2x8_(  817,  126), // (320)
  // LEVEL 4
  _VAL_4x4_( 1377, 1035,  157,  514), // (352)
  _VAL_4x4_(  643,  382,  676, 1208), // (384)
  _VAL_4x4_(  161,  913,  487,  320), // (416)
  _VAL_4x4_( 1124,  808, 1002,  615), // (448)
  // LEVEL 5
  _VAL_8x2_( 1072, 1257, 1081, 1098,  299, 1293, 1307,  393), // (480)
  _VAL_8x2_(  947, 1117, 1298,  552,  389, 1112,  249, 1237), // (512)
  _VAL_8x2_(  737,  600,  479,  931,  822,  977,  106,    6), // (544)
  _VAL_8x2_(  563,  856,  364, 1084, 1060,   60, 1316,  234)  // (576)
};

const uint16_t zetas_64_256_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_(  865), // (0)
  // LEVEL 1
  _VAL_16x_( 1125), // (32)
  _VAL_16x_( 1260), // (64)
  // LEVEL 2
  _VAL_16x_(  551), // (96)
  _VAL_16x_( 1068), // (128)
  _VAL_16x_(  599), // (160)
  _VAL_16x_(  220), // (192)
  // LEVEL 3
  _VAL_16x_( 1387), // (224)
  _VAL_16x_( 1328), // (256)
  _VAL_16x_(  196), // (288)
  _VAL_16x_( 1234), // (320)
  _VAL_16x_(  354), // (352)
  _VAL_16x_(  791), // (384)
  _VAL_16x_(  817), // (416)
  _VAL_16x_(  126), // (448)
  // LEVEL 4
  _VAL_2x8_( 1377, 1035), // (480)
  _VAL_2x8_(  157,  514), // (512)
  _VAL_2x8_(  643,  382), // (544)
  _VAL_2x8_(  676, 1208), // (576)
  _VAL_2x8_(  161,  913), // (608)
  _VAL_2x8_(  487,  320), // (640)
  _VAL_2x8_( 1124,  808), // (672)
  _VAL_2x8_( 1002,  615), // (704)
  // LEVEL 5
  _VAL_4x4_( 1072, 1257, 1081, 1098), // (736)
  _VAL_4x4_(  299, 1293, 1307,  393), // (768)
  _VAL_4x4_(  947, 1117, 1298,  552), // (800)
  _VAL_4x4_(  389, 1112,  249, 1237), // (832)
  _VAL_4x4_(  737,  600,  479,  931), // (864)
  _VAL_4x4_(  822,  977,  106,    6), // (896)
  _VAL_4x4_(  563,  856,  364, 1084), // (928)
  _VAL_4x4_( 1060,   60, 1316,  234)  // (960)
};

const uint16_t zetas_64_512_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_(  865), // (0)
  // LEVEL 1
  _VAL_16x_( 1125), // (32)
  _VAL_16x_( 1260), // (64)
  // LEVEL 2
  _VAL_16x_(  551), // (96)
  _VAL_16x_( 1068), // (128)
  _VAL_16x_(  599), // (160)
  _VAL_16x_(  220), // (192)
  // LEVEL 3
  _VAL_16x_( 1387), // (224)
  _VAL_16x_( 1328), // (256)
  _VAL_16x_(  196), // (288)
  _VAL_16x_( 1234), // (320)
  _VAL_16x_(  354), // (352)
  _VAL_16x_(  791), // (384)
  _VAL_16x_(  817), // (416)
  _VAL_16x_(  126), // (448)
  // LEVEL 4
  _VAL_16x_( 1377), // (480)
  _VAL_16x_( 1035), // (512)
  _VAL_16x_(  157), // (544)
  _VAL_16x_(  514), // (576)
  _VAL_16x_(  643), // (608)
  _VAL_16x_(  382), // (640)
  _VAL_16x_(  676), // (672)
  _VAL_16x_( 1208), // (704)
  _VAL_16x_(  161), // (736)
  _VAL_16x_(  913), // (768)
  _VAL_16x_(  487), // (800)
  _VAL_16x_(  320), // (832)
  _VAL_16x_( 1124), // (864)
  _VAL_16x_(  808), // (896)
  _VAL_16x_( 1002), // (928)
  _VAL_16x_(  615), // (960)
  // LEVEL 5
  _VAL_2x8_( 1072, 1257), // (992)
  _VAL_2x8_( 1081, 1098), // (1024)
  _VAL_2x8_(  299, 1293), // (1056)
  _VAL_2x8_( 1307,  393), // (1088)
  _VAL_2x8_(  947, 1117), // (1120)
  _VAL_2x8_( 1298,  552), // (1152)
  _VAL_2x8_(  389, 1112), // (1184)
  _VAL_2x8_(  249, 1237), // (1216)
  _VAL_2x8_(  737,  600), // (1248)
  _VAL_2x8_(  479,  931), // (1280)
  _VAL_2x8_(  822,  977), // (1312)
  _VAL_2x8_(  106,    6), // (1344)
  _VAL_2x8_(  563,  856), // (1376)
  _VAL_2x8_(  364, 1084), // (1408)
  _VAL_2x8_( 1060,   60), // (1440)
  _VAL_2x8_( 1316,  234)  // (1472)
};

const uint16_t zetas_64_1024_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_(  865), // (0)
  // LEVEL 1
  _VAL_16x_( 1125), // (32)
  _VAL_16x_( 1260), // (64)
  // LEVEL 2
  _VAL_16x_(  551), // (96)
  _VAL_16x_( 1068), // (128)
  _VAL_16x_(  599), // (160)
  _VAL_16x_(  220), // (192)
  // LEVEL 3
  _VAL_16x_( 1387), // (224)
  _VAL_16x_( 1328), // (256)
  _VAL_16x_(  196), // (288)
  _VAL_16x_( 1234), // (320)
  _VAL_16x_(  354), // (352)
  _VAL_16x_(  791), // (384)
  _VAL_16x_(  817), // (416)
  _VAL_16x_(  126), // (448)
  // LEVEL 4
  _VAL_16x_( 1377), // (480)
  _VAL_16x_( 1035), // (512)
  _VAL_16x_(  157), // (544)
  _VAL_16x_(  514), // (576)
  _VAL_16x_(  643), // (608)
  _VAL_16x_(  382), // (640)
  _VAL_16x_(  676), // (672)
  _VAL_16x_( 1208), // (704)
  _VAL_16x_(  161), // (736)
  _VAL_16x_(  913), // (768)
  _VAL_16x_(  487), // (800)
  _VAL_16x_(  320), // (832)
  _VAL_16x_( 1124), // (864)
  _VAL_16x_(  808), // (896)
  _VAL_16x_( 1002), // (928)
  _VAL_16x_(  615), // (960)
  // LEVEL 5
  _VAL_16x_( 1072), // (992)
  _VAL_16x_( 1257), // (1024)
  _VAL_16x_( 1081), // (1056)
  _VAL_16x_( 1098), // (1088)
  _VAL_16x_(  299), // (1120)
  _VAL_16x_( 1293), // (1152)
  _VAL_16x_( 1307), // (1184)
  _VAL_16x_(  393), // (1216)
  _VAL_16x_(  947), // (1248)
  _VAL_16x_( 1117), // (1280)
  _VAL_16x_( 1298), // (1312)
  _VAL_16x_(  552), // (1344)
  _VAL_16x_(  389), // (1376)
  _VAL_16x_( 1112), // (1408)
  _VAL_16x_(  249), // (1440)
  _VAL_16x_( 1237), // (1472)
  _VAL_16x_(  737), // (1504)
  _VAL_16x_(  600), // (1536)
  _VAL_16x_(  479), // (1568)
  _VAL_16x_(  931), // (1600)
  _VAL_16x_(  822), // (1632)
  _VAL_16x_(  977), // (1664)
  _VAL_16x_(  106), // (1696)
  _VAL_16x_(    6), // (1728)
  _VAL_16x_(  563), // (1760)
  _VAL_16x_(  856), // (1792)
  _VAL_16x_(  364), // (1824)
  _VAL_16x_( 1084), // (1856)
  _VAL_16x_( 1060), // (1888)
  _VAL_16x_(   60), // (1920)
  _VAL_16x_( 1316), // (1952)
  _VAL_16x_(  234)  // (1984)
};

const uint16_t inv_zetas_64_64_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0

  1175,   93, 1349,  349,  325, 1045,  553,  846, 1403, 1303,  432,  587,  478,  930,  809,  672, // (0)
  172, 1160,  297, 1020,  857,  111,  292,  462, 1016,  102,  116, 1110,  311,  328,  152,  337, // (32)
  // LEVEL 1

  _VAL_8x2_(  794,  407,  601,  285, 1089,  922,  496, 1248), // (64)
  _VAL_8x2_(  201,  733, 1027,  766,  895, 1252,  374,   32), // (96)
  // LEVEL 2

  _VAL_4x4_( 1283,  592,  618, 1055), // (128)
  _VAL_4x4_(  175, 1213,   81,   22), // (160)
  // LEVEL 3

  _VAL_2x8_( 1189,  810), // (192)
  _VAL_2x8_(  341,  858), // (224)
  // LEVEL 4

  _VAL_16x_(  149), // (256)
  _VAL_16x_(  284), // (288)
  // LEVEL 5

  _VAL_16x_(  544)  // (320)

};

const uint16_t inv_zetas_64_128_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0

  _VAL_8x2_( 1175,   93, 1349,  349,  325, 1045,  553,  846), // (0)
  _VAL_8x2_( 1403, 1303,  432,  587,  478,  930,  809,  672), // (32)
  _VAL_8x2_(  172, 1160,  297, 1020,  857,  111,  292,  462), // (64)
  _VAL_8x2_( 1016,  102,  116, 1110,  311,  328,  152,  337), // (96)
  // LEVEL 1

  _VAL_4x4_(  794,  407,  601,  285), // (128)
  _VAL_4x4_( 1089,  922,  496, 1248), // (160)
  _VAL_4x4_(  201,  733, 1027,  766), // (192)
  _VAL_4x4_(  895, 1252,  374,   32), // (224)
  // LEVEL 2

  _VAL_2x8_( 1283,  592), // (256)
  _VAL_2x8_(  618, 1055), // (288)
  _VAL_2x8_(  175, 1213), // (320)
  _VAL_2x8_(   81,   22), // (352)
  // LEVEL 3

  _VAL_16x_( 1189), // (384)
  _VAL_16x_(  810), // (416)
  _VAL_16x_(  341), // (448)
  _VAL_16x_(  858), // (480)
  // LEVEL 4

  _VAL_16x_(  149), // (512)
  _VAL_16x_(  284), // (544)
  // LEVEL 5
  _VAL_16x_(  501)  // (576)
  // _VAL_16x_(  544)  // (576)

};

const uint16_t inv_zetas_64_256_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0

  _VAL_4x4_( 1175,   93, 1349,  349), // (0)
  _VAL_4x4_(  325, 1045,  553,  846), // (32)
  _VAL_4x4_( 1403, 1303,  432,  587), // (64)
  _VAL_4x4_(  478,  930,  809,  672), // (96)
  _VAL_4x4_(  172, 1160,  297, 1020), // (128)
  _VAL_4x4_(  857,  111,  292,  462), // (160)
  _VAL_4x4_( 1016,  102,  116, 1110), // (192)
  _VAL_4x4_(  311,  328,  152,  337), // (224)
  // LEVEL 1

  _VAL_2x8_(  794,  407), // (256)
  _VAL_2x8_(  601,  285), // (288)
  _VAL_2x8_( 1089,  922), // (320)
  _VAL_2x8_(  496, 1248), // (352)
  _VAL_2x8_(  201,  733), // (384)
  _VAL_2x8_( 1027,  766), // (416)
  _VAL_2x8_(  895, 1252), // (448)
  _VAL_2x8_(  374,   32), // (480)
  // LEVEL 2

  _VAL_16x_( 1283), // (512)
  _VAL_16x_(  592), // (544)
  _VAL_16x_(  618), // (576)
  _VAL_16x_( 1055), // (608)
  _VAL_16x_(  175), // (640)
  _VAL_16x_( 1213), // (672)
  _VAL_16x_(   81), // (704)
  _VAL_16x_(   22), // (736)
  // LEVEL 3

  _VAL_16x_( 1189), // (768)
  _VAL_16x_(  810), // (800)
  _VAL_16x_(  341), // (832)
  _VAL_16x_(  858), // (864)
  // LEVEL 4

  _VAL_16x_(  149), // (896)
  _VAL_16x_(  284), // (928)
  // LEVEL 5
  _VAL_16x_(  501)  // (960)
  // _VAL_16x_(  544)  // (960)
};

const uint16_t inv_zetas_64_512_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0

  _VAL_2x8_( 1175,   93), // (0)
  _VAL_2x8_( 1349,  349), // (32)
  _VAL_2x8_(  325, 1045), // (64)
  _VAL_2x8_(  553,  846), // (96)
  _VAL_2x8_( 1403, 1303), // (128)
  _VAL_2x8_(  432,  587), // (160)
  _VAL_2x8_(  478,  930), // (192)
  _VAL_2x8_(  809,  672), // (224)
  _VAL_2x8_(  172, 1160), // (256)
  _VAL_2x8_(  297, 1020), // (288)
  _VAL_2x8_(  857,  111), // (320)
  _VAL_2x8_(  292,  462), // (352)
  _VAL_2x8_( 1016,  102), // (384)
  _VAL_2x8_(  116, 1110), // (416)
  _VAL_2x8_(  311,  328), // (448)
  _VAL_2x8_(  152,  337), // (480)
  // LEVEL 1

  _VAL_16x_(  794), // (512)
  _VAL_16x_(  407), // (544)
  _VAL_16x_(  601), // (576)
  _VAL_16x_(  285), // (608)
  _VAL_16x_( 1089), // (640)
  _VAL_16x_(  922), // (672)
  _VAL_16x_(  496), // (704)
  _VAL_16x_( 1248), // (736)
  _VAL_16x_(  201), // (768)
  _VAL_16x_(  733), // (800)
  _VAL_16x_( 1027), // (832)
  _VAL_16x_(  766), // (864)
  _VAL_16x_(  895), // (896)
  _VAL_16x_( 1252), // (928)
  _VAL_16x_(  374), // (960)
  _VAL_16x_(   32), // (992)
  // LEVEL 2

  _VAL_16x_( 1283), // (1024)
  _VAL_16x_(  592), // (1056)
  _VAL_16x_(  618), // (1088)
  _VAL_16x_( 1055), // (1120)
  _VAL_16x_(  175), // (1152)
  _VAL_16x_( 1213), // (1184)
  _VAL_16x_(   81), // (1216)
  _VAL_16x_(   22), // (1248)
  // LEVEL 3

  _VAL_16x_( 1189), // (1280)
  _VAL_16x_(  810), // (1312)
  _VAL_16x_(  341), // (1344)
  _VAL_16x_(  858), // (1376)
  // LEVEL 4

  _VAL_16x_(  149), // (1408)
  _VAL_16x_(  284), // (1440)
  // LEVEL 5
  _VAL_16x_(  501)  // (1472)
  // _VAL_16x_(  544)  // (1472)
};

const uint16_t inv_zetas_64_1024_opt[] __attribute__((aligned(32))) = {
  // LEVEL 0
  _VAL_16x_( 1175), // (0)
  _VAL_16x_(   93), // (32)
  _VAL_16x_( 1349), // (64)
  _VAL_16x_(  349), // (96)
  _VAL_16x_(  325), // (128)
  _VAL_16x_( 1045), // (160)
  _VAL_16x_(  553), // (192)
  _VAL_16x_(  846), // (224)
  _VAL_16x_( 1403), // (256)
  _VAL_16x_( 1303), // (288)
  _VAL_16x_(  432), // (320)
  _VAL_16x_(  587), // (352)
  _VAL_16x_(  478), // (384)
  _VAL_16x_(  930), // (416)
  _VAL_16x_(  809), // (448)
  _VAL_16x_(  672), // (480)
  _VAL_16x_(  172), // (512)
  _VAL_16x_( 1160), // (544)
  _VAL_16x_(  297), // (576)
  _VAL_16x_( 1020), // (608)
  _VAL_16x_(  857), // (640)
  _VAL_16x_(  111), // (672)
  _VAL_16x_(  292), // (704)
  _VAL_16x_(  462), // (736)
  _VAL_16x_( 1016), // (768)
  _VAL_16x_(  102), // (800)
  _VAL_16x_(  116), // (832)
  _VAL_16x_( 1110), // (864)
  _VAL_16x_(  311), // (896)
  _VAL_16x_(  328), // (928)
  _VAL_16x_(  152), // (960)
  _VAL_16x_(  337), // (992)
  // LEVEL 1
  _VAL_16x_(  794), // (1024)
  _VAL_16x_(  407), // (1056)
  _VAL_16x_(  601), // (1088)
  _VAL_16x_(  285), // (1120)
  _VAL_16x_( 1089), // (1152)
  _VAL_16x_(  922), // (1184)
  _VAL_16x_(  496), // (1216)
  _VAL_16x_( 1248), // (1248)
  _VAL_16x_(  201), // (1280)
  _VAL_16x_(  733), // (1312)
  _VAL_16x_( 1027), // (1344)
  _VAL_16x_(  766), // (1376)
  _VAL_16x_(  895), // (1408)
  _VAL_16x_( 1252), // (1440)
  _VAL_16x_(  374), // (1472)
  _VAL_16x_(   32), // (1504)
  // LEVEL 2
  _VAL_16x_( 1283), // (1536)
  _VAL_16x_(  592), // (1568)
  _VAL_16x_(  618), // (1600)
  _VAL_16x_( 1055), // (1632)
  _VAL_16x_(  175), // (1664)
  _VAL_16x_( 1213), // (1696)
  _VAL_16x_(   81), // (1728)
  _VAL_16x_(   22), // (1760)
  // LEVEL 3
  _VAL_16x_( 1189), // (1792)
  _VAL_16x_(  810), // (1824)
  _VAL_16x_(  341), // (1856)
  _VAL_16x_(  858), // (1888)
  // LEVEL 4
  _VAL_16x_(  149), // (1920)
  _VAL_16x_(  284), // (1952)
  // LEVEL 5
  _VAL_16x_(  501)  // (1984)
  // _VAL_16x_(  544)  // (1984)
};

#endif
// clang-format on
