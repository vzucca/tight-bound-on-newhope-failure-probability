.include "asm_macros.h"
.include "asm_shuffle.h"
.include "shift_macros.h"

.global poly_shift_2_modq64_128_opt
poly_shift_2_modq64_128_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_lowdword(%rip),%ymm2

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1

/*do it 4*64=256 times*/
shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi
shift_multiply_2_64

add 	%r11,%rsp
ret

.global poly_shift_4_modq64_256_opt
poly_shift_4_modq64_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1

/*do it 4*64=256 times*/
shift_multiply_4_64
add   $128,%rdi
add   $32,%rsi
shift_multiply_4_64
add   $128,%rdi
add   $32,%rsi
shift_multiply_4_64
add   $128,%rdi
add   $32,%rsi
shift_multiply_4_64

add 	%r11,%rsp
ret

.global poly_shift_8_modq64_512_opt
poly_shift_8_modq64_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1

/*do it 2*128=256 times*/
shift_multiply_8_128
add   $256,%rdi
add   $32,%rsi
shift_multiply_8_128
add   $256,%rdi
add   $32,%rsi

shift_multiply_8_128
add   $256,%rdi
add   $32,%rsi
shift_multiply_8_128

add 	%r11,%rsp
ret

.global sub_cst4q_q64_128
sub_cst4q_q64_128:
mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16x4Q64(%rip),%ymm0

load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8

add 	%r11,%rsp
ret
