.include "asm_macros.h"

.macro first_shuffle
#reorder
# REORDER A
vmovdqa		_lowdword(%rip),%ymm3
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
# REORDER B (same pattern as A)
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10
.endm

.macro shuffle_2
# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9
.endm

.global invntt_64_512_opt
invntt_64_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1
vmovdqa		_16xv_64(%rip),%ymm2

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 1
#
# ---------------------------------------------------------------------------
#level 0

#load block 1 and 2
load_128_coeffs 4,5,6,7,8,9,10,11

first_shuffle

invntt_butterfly_4_z 3,5,7,9,4,6,8,10,0,32,64,96 rsi,0,1,12,13,14,15,11

#level 1
shuffle_2

invntt_butterfly_4_z 11,4,6,8,3,5,7,9,512,544,576,608 rsi,0,1,12,13,14,15,10

#level 2
#update
invntt_update_2 11,3,6,7,4,5,8,9
load_32_coeffs 14,15 rsi,1024,1056
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,10,12,13,14

#level 3
invntt_update_2 11,3,4,5,6,7,8,9
# gen_barrett_32_coeffs 11,3 2,1,9
# gen_barrett_64_coeffs 11,3,4,5 2,1,9
vmovdqa		1280(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,10,12,13,14

save_128_coeffs 11,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE 1
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 2
#
# ---------------------------------------------------------------------------
#level 0

#load block 1 and 2
load_128_coeffs 4,5,6,7,8,9,10,11

first_shuffle

invntt_butterfly_4_z 3,5,7,9,4,6,8,10,128,160,192,224 rsi,0,1,12,13,14,15,11

#level 1
shuffle_2

invntt_butterfly_4_z 11,4,6,8,3,5,7,9,640,672,704,736 rsi,0,1,12,13,14,15,10

#level 2
#update
invntt_update_2 11,3,6,7,4,5,8,9
load_32_coeffs 14,15 rsi,1088,1120
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,10,12,13,14

#level 3
invntt_update_2 11,3,4,5,6,7,8,9
# gen_barrett_32_coeffs 11,3 2,1,9
# gen_barrett_64_coeffs 11,3,4,5 2,1,9
vmovdqa		1312(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,10,12,13,14

save_64_coeffs 11,3,4,5
# save_128_coeffs 11,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE 2
#
# ---------------------------------------------------------------------------

# level 5 for TREE 1 and 2 can be done now

# shift to get the part (20-23), (28-31) is already loaded in ymm6,7,8,9
sub		$128,%rdi

#level 4

#load block 2, block 4 already loaded
load_64_coeffs 10,3,4,5
# load_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		1408(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#load block 1 and 3
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

gen_barrett_32_coeffs 4,8 2,1,9
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

# shift to next trees 3,4
add		$512,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 3
#
# ---------------------------------------------------------------------------
#level 0
#load block 5 and 7
load_128_coeffs 4,5,6,7,8,9,10,11

first_shuffle

invntt_butterfly_4_z 3,5,7,9,4,6,8,10,256,288,320,352 rsi,0,1,12,13,14,15,11

#level 1
shuffle_2

invntt_butterfly_4_z 11,4,6,8,3,5,7,9,768,800,832,864 rsi,0,1,12,13,14,15,10

#level 2
#update
invntt_update_2 11,3,6,7,4,5,8,9
load_32_coeffs 14,15 rsi,1152,1184
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,10,12,13,14

#level 3
invntt_update_2 11,3,4,5,6,7,8,9
# gen_barrett_32_coeffs 11,3 2,1,9
# gen_barrett_64_coeffs 11,3,4,5 2,1,9
vmovdqa		1344(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,10,12,13,14

save_128_coeffs 11,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE 3
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 4
#
# ---------------------------------------------------------------------------
#level 0
#load block 6 and 8
load_128_coeffs 4,5,6,7,8,9,10,11

first_shuffle

invntt_butterfly_4_z 3,5,7,9,4,6,8,10,384,416,448,480 rsi,0,1,12,13,14,15,11

#level 1
shuffle_2

invntt_butterfly_4_z 11,4,6,8,3,5,7,9,896,928,960,992 rsi,0,1,12,13,14,15,10

#level 2
#update
invntt_update_2 11,3,6,7,4,5,8,9
load_32_coeffs 14,15 rsi,1216,1248
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,10,12,13,14

#level 3
invntt_update_2 11,3,4,5,6,7,8,9
# gen_barrett_32_coeffs 11,3 2,1,9
# gen_barrett_64_coeffs 11,3,4,5 2,1,9
vmovdqa		1376(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,10,12,13,14

save_64_coeffs 11,3,4,5
# save_128_coeffs 11,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE 4
#
# ---------------------------------------------------------------------------

# level 5 for TREE 3,4 can now be done

# shift to get the part (20-23), (28-31) is already loaded in ymm6,7,8,9
sub		$128,%rdi

#level 4

#load 6 (20-23), 8 (28-31) already loaded
load_64_coeffs 10,3,4,5
# load_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

invntt_update_2 10,3,4,5,6,7,8,9
# invntt_update_2 10,4,6,8,3,5,7,9
vmovdqa		1440(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#load block 5, 7
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

gen_barrett_32_coeffs 4,8 2,1,9
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_64_coeffs 4,5,6,7
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

sub		$256,%rdi

#level 5

#zetas
vmovdqa		1472(%rsi),%ymm3
vmovdqa		_F64(%rip),%ymm2

#load 3, 7 already loaded
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_update_2 4,5,6,7,8,9,10,11
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,15
add_q_ifneg 4,5,6,7
signed_montgomery_64x64 3,3,3,3,8,9,10,11 1,0,12,13,14,15
add_q_ifneg 8,9,10,11
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#load 4, 8
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_update_2 4,5,6,7,8,9,10,11
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,15
add_q_ifneg 4,5,6,7
signed_montgomery_64x64 3,3,3,3,8,9,10,11 1,0,12,13,14,15
add_q_ifneg 8,9,10,11
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$256,%rdi

#load 2, 6
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_update_2 4,5,6,7,8,9,10,11
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,15
add_q_ifneg 4,5,6,7
signed_montgomery_64x64 3,3,3,3,8,9,10,11 1,0,12,13,14,15
add_q_ifneg 8,9,10,11
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#load 1, 5
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

invntt_update_2 4,5,6,7,8,9,10,11
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,15
add_q_ifneg 4,5,6,7
signed_montgomery_64x64 3,3,3,3,8,9,10,11 1,0,12,13,14,15
add_q_ifneg 8,9,10,11
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add 	%r11,%rsp

ret
