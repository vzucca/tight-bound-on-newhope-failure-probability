.include "asm_macros.h"

.global ntt_64_1024_opt
ntt_64_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1

#level 0

#zetas
vmovdqa		(%rsi),%ymm3

#Block 1 (0-3) with block 9 (32 - 35)
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 2 with block 10
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 3 with block 11
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 4 with block 12
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 5 with block 13
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 6 with block 14
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 7 with block 15
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 8 with block 16
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
# store (ymm4,5,6,7 will be reused in the immediate next level)
save_64_coeffs 8,9,10,11 rdi,1024,1056,1088,1120

#tree A and B ready to be started. Starts with A
sub		$512,%rdi

################################################################################
##
##
##                           BEGIN TREE A
##
##
################################################################################

#level 1

#zetas
vmovdqa		32(%rsi),%ymm3

#Block 4 with 8 (already loaded with ymm4,5,6,7)
#load
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
load_64_coeffs 8,9,10,11
ntt_butterfly_1 8,9,10,11,4,5,6,7,3
save_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
sub		$128,%rdi

#Block 3 with 7
#load

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 2 with 6
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 1 with 5
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
#store (block 1 will be reused straight away)
save_64_coeffs 8,9,10,11 rdi,512,544,576,608

#level 2 for A1 and A2

#zetas
vmovdqa		96(%rsi),%ymm3

#Block 1 (already loaded) and Block 3
#load
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
load_64_coeffs 8,9,10,11 rdi,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 2 and Block 4
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees A1,A2 are ready to go. Start with tree A2
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A2
#
# ---------------------------------------------------------------------------

#Block 3 and 4 (4 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11

#level 3

vmovdqa		256(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4

load_32_coeffs 15,3 rsi,544,576
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
# save_128_coeffs 4,5,6,7,8,9,10,11

load_64_coeffs 13,14,15,3 rsi,1120,1152,1184,1216
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
# save_128_coeffs 4,5,6,7,8,9,10,11
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE A2
#
# ---------------------------------------------------------------------------

# tree A1 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A1
#
# ---------------------------------------------------------------------------


#Block 1 and 2
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3

vmovdqa		224(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,480,512
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,992,1024,1056,1088
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE A1
#
# ---------------------------------------------------------------------------


add		$512,%rdi

#level 2 for A3 and A4

#zetas
vmovdqa		128(%rsi),%ymm3

#Block 5 and Block 7
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 6 and Block 8
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store (Block 8 is goind to be used immediately)
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees A3,A4 are ready to go. Start with tree A4
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A4
#
# ---------------------------------------------------------------------------

#Block 7 and 8 (4 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		320(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,672,704
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,1376,1408,1440,1472
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE A4
#
# ---------------------------------------------------------------------------

# tree A3 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A3
#
# ---------------------------------------------------------------------------


#Block 5 and 6
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		288(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,608,640
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3


#level 5
load_64_coeffs 13,14,15,3 rsi,1248,1280,1312,1344
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE A3
#
# ---------------------------------------------------------------------------

# moves to tree B, from block 5 to block 12
add		$896,%rdi

###############################################################################
##
##
##                           BEGIN TREE B
##
##
################################################################################

#level 1

#zetas
vmovdqa		64(%rsi),%ymm3

#Block 12 with 16

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
sub		$128,%rdi

#Block 11 with 15

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 10 with 14

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 9 with 13

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
#store (block 9 will be reused straight away)
save_64_coeffs 8,9,10,11 rdi,512,544,576,608

#level 2 for B1 and B2

#zetas
vmovdqa		160(%rsi),%ymm3

#Block 9 (already loaded) and Block 11
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
load_64_coeffs 8,9,10,11 rdi,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 10 and Block 12
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store (Block 12 is goind to be used immediately)
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees B1,B2 are ready to go. Start with tree A2
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B2
#
# ---------------------------------------------------------------------------

#Block 11 and 12 (12 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11
#level 3

#zetas
vmovdqa		384(%rsi),%ymm3

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,800,832
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3


#level 5
load_64_coeffs 13,14,15,3 rsi,1632,1664,1696,1728
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
# save_128_coeffs 4,5,6,7,8,9,10,11
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE B2
#
# ---------------------------------------------------------------------------

# tree B1 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B1
#
# ---------------------------------------------------------------------------


#Block 9 and 10
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		352(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,736,768
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,1504,1536,1568,1600
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE B1
#
# ---------------------------------------------------------------------------


add		$512,%rdi

#level 2 for B3 and B4

#zetas
vmovdqa		192(%rsi),%ymm3

#Block 13 and Block 15
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 15 and Block 16
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
#store (Block 16 is goind to be used immediately)
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees A3,A4 are ready to go. Start with tree A4
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B4
#
# ---------------------------------------------------------------------------

#Block 15 and 16 (16 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11
#level 3

#zetas
vmovdqa		448(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,928,960
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3


#level 5
load_64_coeffs 13,14,15,3 rsi,1888,1920,1952,1984
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE B4
#
# ---------------------------------------------------------------------------

# tree B3 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B3
#
# ---------------------------------------------------------------------------


#Block 13 and 14
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		416(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,864,896
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,1760,1792,1824,1856
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
save_128_coeffs 4,5,6,7,8,9,10,11

# ---------------------------------------------------------------------------
#
#                              END TREE B3
#
# ---------------------------------------------------------------------------

add 	%r11,%rsp

ret
