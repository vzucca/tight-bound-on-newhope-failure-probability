.include "asm_macros.h"

#level 0-2

.macro tree_1 z01,z02,z03,z04,z11,z12,z21
load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,\z01,\z02,\z03,\z04
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,\z11,\z12
vmovdqa		\z21(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
save_128_coeffs 3,4,5,6,7,8,9,10
.endm

.macro tree_2 z01,z02,z03,z04,z11,z12,z21
load_128_coeffs 3,4,5,6,7,8,9,10
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,\z01,\z02,\z03,\z04
invntt_butterfly_2_z 3,4,7,8,5,6,9,10,\z11,\z12
vmovdqa		\z21(%rsi),%ymm11
invntt_butterfly_1 3,4,5,6,7,8,9,10,11
# save_128_coeffs 3,4,5,6,7,8,9,10
save_64_coeffs 3,4,5,6
.endm

#level 3

.macro level3_1 z3
load_64_coeffs 3,4,5,6
invntt_update_2 3,4,5,6,7,8,9,10
vmovdqa		\z3(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,7,8,9,10 1,0,11,12,13,14
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
.endm

.macro level3_2 z3
load_64_coeffs 3,4,5,6
invntt_update_2 3,4,5,6,7,8,9,10
vmovdqa		\z3(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,7,8,9,10 1,0,11,12,13,14
save_128_coeffs 3,4,5,6,7,8,9,10 rdi,0,32,64,96,256,288,320,352
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 8,9,10,11 rdi,256,288,320,352
.endm

#level 4

.macro lvl4_block
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
.endm

.macro level4_1 z4
# (already loaded 4,5,6,7)
load_64_coeffs 8,9,10,11
gen_barrett_64_coeffs 8,9,4,5 2,1,9
# load_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608
vmovdqa		\z4(%rsi),%ymm15
invntt_update_2 8,9,10,11,4,5,6,7 12,13,14,3
signed_montgomery_64x64 15,15,15,15,4,5,6,7 1,0,12,13,14,3
save_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608

lvl4_block
lvl4_block
lvl4_block
.endm

.macro level4_2 z4
# (already loaded 4,5,6,7)
load_64_coeffs 8,9,10,11
gen_barrett_64_coeffs 8,9,4,5 2,1,9
# gen_barrett_64_coeffs 6,7,10,11 2,1,9
# load_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608
vmovdqa		\z4(%rsi),%ymm15
invntt_update_2 8,9,10,11,4,5,6,7 12,13,14,3
signed_montgomery_64x64 15,15,15,15,4,5,6,7 1,0,12,13,14,3
save_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608
lvl4_block
lvl4_block
add		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
save_64_coeffs 4,5,6,7
.endm

.macro lvl5_block
sub		$128,%rdi
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
.endm

.global invntt_64_1024_opt
invntt_64_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1
vmovdqa		_16xv_64(%rip),%ymm2

################################################################################
##
##
##                           BEGIN TREE A
##
##
################################################################################

#level 0-2
tree_1 0,32,64,96,1024,1056,1536
add		$256,%rdi
tree_2 128,160,192,224,1088,1120,1568
# tree_1 128,160,192,224,1088,1120,1568

sub		$128,%rdi

level3_1 1792
# sub		$128,%rdi

add		$512,%rdi

tree_1 256,288,320,352,1152,1184,1600
add		$256,%rdi
tree_2 384,416,448,480,1216,1248,1632
# tree_1 384,416,448,480,1216,1248,1632

sub		$128,%rdi

level3_2 1824
# level3_1 1824
# sub		$128,%rdi

# moves to get block 1 to combine with block 5
sub		$512,%rdi

#level 4
level4_1 1920
# add		$128,%rdi
# add		$128,%rdi
# add		$128,%rdi

################################################################################
##
##
##                           END TREE A
##
##
################################################################################

# moves to complete tree B1 and B2
add		$640,%rdi

################################################################################
##
##
##                           BEGIN TREE B
##
##
################################################################################

tree_1 512,544,576,608,1280,1312,1664
add		$256,%rdi
# tree_1 640,672,704,736,1344,1376,1696
tree_2 640,672,704,736,1344,1376,1696

# shift to take part of A1 tree
sub		$128,%rdi

level3_1 1856
# sub		$128,%rdi

# moves to complete tree B3 and B4
add		$512,%rdi

tree_1 768,800,832,864,1408,1440,1728
add		$256,%rdi
# tree_1 896,928,960,992,1472,1504,1760
tree_2 896,928,960,992,1472,1504,1760

# shift to take part of B3 tree
sub		$128,%rdi

# level3_1 1888
level3_2 1888
# sub		$128,%rdi

# now ready to complete level 8 of part B
# moves to get block 9 to combine with block 13
sub		$512,%rdi

# level4_1 1952
level4_2 1952
# add		$128,%rdi
# add		$128,%rdi
# add		$128,%rdi
# add		$128,%rdi

################################################################################
##
##
##                           END TREE B
##
##
################################################################################

#level 5

#zetas
vmovdqa		1984(%rsi),%ymm15
vmovdqa		_F64(%rip),%ymm2

# shift to get block 8
sub		$512,%rdi

#load block 8 and block 16 (already loaded)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

lvl5_block
lvl5_block
lvl5_block
lvl5_block
lvl5_block
lvl5_block
lvl5_block

add 	%r11,%rsp

ret
