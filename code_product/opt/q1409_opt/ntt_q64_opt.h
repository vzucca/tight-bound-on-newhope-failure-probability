#ifndef NTT64_OPT
#define NTT64_OPT

#include <stdio.h>

/* ################ NTT ################ */
/* ntt functions nttq16__(size of input) */
#include "const_64_opt.h"

// void ntt_64_64_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_64_64_opt");
// void invntt_64_64_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_64_64_opt");

void ntt_64_128_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_64_128_opt");
void invntt_64_128_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_64_128_opt");

void ntt_64_256_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_64_256_opt");
void invntt_64_256_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_64_256_opt");

void ntt_64_512_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_64_512_opt");
void invntt_64_512_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_64_512_opt");

void ntt_64_1024_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_64_1024_opt");
void invntt_64_1024_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_64_1024_opt");

#endif
