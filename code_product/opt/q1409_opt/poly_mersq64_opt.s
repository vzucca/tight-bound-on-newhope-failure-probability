.include "asm_macros.h"

.macro block_64
load_64_coeffs 1,2,3,4
signed_barrett_64_coeffs_3 1,2,3,4 0,11,9,7
save_64_coeffs 1,2,3,4
.endm

.global poly_mers_q64_128
poly_mers_q64_128:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_low_mask_64(%rip),%ymm0

block_64
add  $128,%rdi
block_64

add 	%r11,%rsp

ret
