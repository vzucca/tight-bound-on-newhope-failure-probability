.include "asm_macros.h"

.macro first_shuffle
#reorder
vmovdqa		_lowdword(%rip),%ymm3
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11
#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#shuffle
vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11
.endm

.macro second_shuffle
#shuffle
vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10
.endm

.macro third_shuffle
#shuffle
vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9
.endm

.global invntt_64_256_opt
invntt_64_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1
vmovdqa		_16xv_64(%rip),%ymm2

#first round
load_128_coeffs 4,5,6,7,8,9,10,11

#level 0
first_shuffle
invntt_butterfly_4_z 4,6,8,10,5,7,9,11,0,32,64,96 rsi,0,1,12,13,14,15,3

#level 1
second_shuffle
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,256,288,320,352 rsi,0,1,12,13,14,15,11

#level 2
third_shuffle
invntt_butterfly_4_z 11,4,6,8,3,5,7,9,512,544,576,608 rsi,0,1,12,13,14,15,10

#level 3
invntt_update_2 11,3,6,7,4,5,8,9
gen_barrett_32_coeffs 11,6 2,1,9
load_32_coeffs 14,15 rsi,768,800
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,10,12,13,14

#level 4
invntt_update_2 11,3,4,5,6,7,8,9
vmovdqa		896(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,10,12,13,14

save_128_coeffs 11,3,4,5,6,7,8,9

add		$256,%rdi

#second round
load_128_coeffs 4,5,6,7,8,9,10,11

#level 0
first_shuffle
invntt_butterfly_4_z 4,6,8,10,5,7,9,11,128,160,192,224 rsi,0,1,12,13,14,15,3

#level 1
second_shuffle
invntt_butterfly_4_z 3,5,7,9,4,6,8,10,384,416,448,480 rsi,0,1,12,13,14,15,11

#level 2
third_shuffle
invntt_butterfly_4_z 11,4,6,8,3,5,7,9,640,672,704,736 rsi,0,1,12,13,14,15,10

#level 3
invntt_update_2 11,3,6,7,4,5,8,9
gen_barrett_32_coeffs 11,6 2,1,9
load_32_coeffs 14,15 rsi,832,864
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,10,12,13,14

#level 4
invntt_update_2 11,3,4,5,6,7,8,9
vmovdqa		928(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,10,12,13,14

save_128_coeffs 11,3,4,5,6,7,8,9

sub		$256,%rdi

#level 5
#f
vmovdqa		_F64(%rip),%ymm2
#zeta
vmovdqa		960(%rsi),%ymm3

#first round
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11

signed_montgomery_64x64 3,3,3,3,8,9,10,11 1,0,12,13,14,15
add_q_ifneg 8,9,10,11
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,15
add_q_ifneg 4,5,6,7

save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#second round
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11

signed_montgomery_64x64 3,3,3,3,8,9,10,11 1,0,12,13,14,15
add_q_ifneg 8,9,10,11
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,15
add_q_ifneg 4,5,6,7
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add 	%r11,%rsp

ret
