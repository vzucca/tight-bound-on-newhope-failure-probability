.include "asm_macros.h"

.macro shuffle_pre5
vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10
.endm

.macro final_shuffle
vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

vpshufd		$0xB1,%ymm11,%ymm12
# vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm11,%ymm13,%ymm10
# vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5
.endm

.global ntt_64_512_opt
ntt_64_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ64INV(%rip),%ymm0
vmovdqa		_16xQ64(%rip),%ymm1

#level 0

#zetas
vmovdqa		(%rsi),%ymm3

#first round with block 1 (0 - 3) with 5 (16 - 19)
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#second round with block 2 (4 - 7) and 6 (20 - 23)
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#third round with block 3 (8 - 11) and 7 (24 - 27)
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#fourth round with block 4 (12 - 15) and 8 (28 - 31)
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
save_64_coeffs 4,5,6,7

#level 1

add		$256,%rdi

#zetas
vmovdqa		64(%rsi),%ymm3

# 8 (28-31) already loaded in ymm8,9,10,11
#level 1 with 6 (20-23) and 8 (28-31)
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
load_64_coeffs 4,5,6,7
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#level 1 with 5 (16-19) and 7 (24-27)
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 8,9,10,11 rdi,256,288,320,352

# we can now start trees 3 and 4

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 3
#
# ---------------------------------------------------------------------------

#load
# block 5 already loaded
# load_128_coeffs 4,5,6,7,8,9,10,11
load_64_coeffs 8,9,10,11 rdi,128,160,192,224

#level 2 with 5 (16-19) to 6 (20-23)

#zetas
vmovdqa		160(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3

#zetas

vmovdqa		352(%rsi),%ymm15
vmovdqa		384(%rsi),%ymm3
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 4
#zetas
vmovdqa		736(%rsi),%ymm13
vmovdqa		768(%rsi),%ymm14
vmovdqa		800(%rsi),%ymm15
vmovdqa		832(%rsi),%ymm3
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 5
#zetas
vmovdqa		1248(%rsi),%ymm12
vmovdqa		1280(%rsi),%ymm13
vmovdqa		1312(%rsi),%ymm14
vmovdqa		1344(%rsi),%ymm15

shuffle_pre5
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14

final_shuffle
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE 3
#
# ---------------------------------------------------------------------------

# move to tree 4
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 4
#
# ---------------------------------------------------------------------------

load_128_coeffs 4,5,6,7,8,9,10,11

#level 2 with 7 (24-27) and 8 (28-31)

#zetas
vmovdqa		192(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3

#zetas
vmovdqa		416(%rsi),%ymm15
vmovdqa		448(%rsi),%ymm3
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 4
#zetas
vmovdqa		864(%rsi),%ymm13
vmovdqa		896(%rsi),%ymm14
vmovdqa		928(%rsi),%ymm15
vmovdqa		960(%rsi),%ymm3

ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 5
#zetas
vmovdqa		1376(%rsi),%ymm12
vmovdqa		1408(%rsi),%ymm13
vmovdqa		1440(%rsi),%ymm14
vmovdqa		1472(%rsi),%ymm15

shuffle_pre5
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14

final_shuffle
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE 4
#
# ---------------------------------------------------------------------------

sub		$640,%rdi

#level 1 with 2 (4-7) and 4 (12-15)

#zetas
vmovdqa		32(%rsi),%ymm3

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#level 1 with (0-3) and (8-11)

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 8,9,10,11 rdi,256,288,320,352

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 1
#
# ---------------------------------------------------------------------------

#level 2 with (0-3) to (4-7)

#zetas
vmovdqa		96(%rsi),%ymm3

#load
# charge A (les 1-64e entiers de 16-bits) et B (les 65-128emes entiers)
# load_128_coeffs 4,5,6,7,8,9,10,11
load_64_coeffs 8,9,10,11 rdi,128,160,192,224
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3

#zetas
vmovdqa		224(%rsi),%ymm15
vmovdqa		256(%rsi),%ymm3
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 4
#zetas
vmovdqa		480(%rsi),%ymm13
vmovdqa		512(%rsi),%ymm14
vmovdqa		544(%rsi),%ymm15
vmovdqa		576(%rsi),%ymm3
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 5
#zetas
vmovdqa		992(%rsi),%ymm12
vmovdqa		1024(%rsi),%ymm13
vmovdqa		1056(%rsi),%ymm14
vmovdqa		1088(%rsi),%ymm15

shuffle_pre5
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14

final_shuffle
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE 1
#
# ---------------------------------------------------------------------------

# move to tree 2
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 2
#
# ---------------------------------------------------------------------------

#load
# charge A (les 1-64e entiers de 16-bits) et B (les 65-128emes entiers)
load_128_coeffs 4,5,6,7,8,9,10,11

#level 2 with (8-11) to (12-15)

#zetas
vmovdqa		128(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 3

#zetas
vmovdqa		288(%rsi),%ymm15
vmovdqa		320(%rsi),%ymm3
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

vmovdqa		%ymm6,(%rdi)
vmovdqa		%ymm7,32(%rdi)
vmovdqa		%ymm10,64(%rdi)
vmovdqa		%ymm11,96(%rdi)
vmovdqa		%ymm4,128(%rdi)
vmovdqa		%ymm5,160(%rdi)
vmovdqa		%ymm8,192(%rdi)
vmovdqa		%ymm9,224(%rdi)

#level 4
#zetas
vmovdqa		608(%rsi),%ymm13
vmovdqa		640(%rsi),%ymm14
vmovdqa		672(%rsi),%ymm15
vmovdqa		704(%rsi),%ymm3
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 5
#zetas
vmovdqa		1120(%rsi),%ymm12
vmovdqa		1152(%rsi),%ymm13
vmovdqa		1184(%rsi),%ymm14
vmovdqa		1216(%rsi),%ymm15

shuffle_pre5
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14

final_shuffle
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE 2
#
# ---------------------------------------------------------------------------

add 	%r11,%rsp

ret
