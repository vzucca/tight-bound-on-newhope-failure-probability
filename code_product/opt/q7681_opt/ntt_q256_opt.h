#ifndef NTT256_OPT
#define NTT256_OPT

#include <stdio.h>

/* ################ NTT ################ */
/* ntt functions nttq16__(size of input) */
#include "const_256_opt.h"

void ntt_256_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_256_opt");
void invntt_256_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_256_opt");

void ntt_256_512_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_256_512_opt");
void invntt_256_512_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_256_512_opt");

void ntt_256_1024_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_256_1024_opt");
void invntt_256_1024_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_256_1024_opt");

#endif
