.include "asm_macros.h"
.include "asm_shuffle.h"
.include "shift_macros.h"

.global poly_shift_2_modq256_512_opt
poly_shift_2_modq256_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_lowdword(%rip),%ymm2

vmovdqa		_16xQ256INV(%rip),%ymm0
vmovdqa		_16xQ256(%rip),%ymm1

/*do it 8*64=512 times*/
shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi
shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi
shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi
shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi

shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi
shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi
shift_multiply_2_64
add   $128,%rdi
add   $64,%rsi
shift_multiply_2_64

add 	%r11,%rsp
ret

.global sub_cst2q_q256_256
sub_cst2q_q256_256:
mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16x2Q256(%rip),%ymm0

load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8
add   $256,%rdi
load_128_coeffs 1,2,3,4,5,6,7,8
sub_128_cst 1,2,3,4,5,6,7,8
save_128_coeffs 1,2,3,4,5,6,7,8

add 	%r11,%rsp
ret
