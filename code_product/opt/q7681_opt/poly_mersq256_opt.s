.include "asm_macros.h"

.macro block_128
load_128_coeffs 2,3,4,5,6,7,8,9
signed_barrett_64_coeffs 2,3,4,5 0,13,9
signed_barrett_64_coeffs 6,7,8,9 0,13,9
save_128_coeffs 2,3,4,5,6,7,8,9
.endm

.global poly_mers_q256_256
poly_mers_q256_256:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_low_mask_256(%rip),%ymm0

/*repeat 2 times*/
block_128
add $256,%rdi
block_128

add 	%r11,%rsp

ret
