.include "asm_macros.h"

.macro block_64
load_64_coeffs 2,3,4,5 rdx
load_64_coeffs 6,7,8,9 rsi
signed_montgomery_64x64 2,3,4,5,6,7,8,9
save_64_coeffs 6,7,8,9
.endm

.macro block_64_short
load_64_coeffs 2,3,4,5 rsi
load_64_coeffs 6,7,8,9 rdi
signed_montgomery_64x64 2,3,4,5,6,7,8,9
save_64_coeffs 6,7,8,9
.endm

.macro repeat
block_64
add $128,%rdx
add $128,%rsi
add $128,%rdi
.endm

.macro repeat_2
block_64_short
add $128,%rdx
add $128,%rsi
add $128,%rdi
.endm

.global pointwise_mul_q256_256_opt
pointwise_mul_q256_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ256INV(%rip),%ymm0
vmovdqa		_16xQ256(%rip),%ymm1

/*repeat 4 times*/
repeat
repeat
repeat
block_64

add 	%r11,%rsp

ret

.global pointwise_mul_q256_512_opt
pointwise_mul_q256_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ256INV(%rip),%ymm0
vmovdqa		_16xQ256(%rip),%ymm1

/*repeat 8 times*/
repeat
repeat
repeat
repeat

repeat
repeat
repeat
block_64

add 	%r11,%rsp

ret

.global pointwise_mul_q256_256_opt_short
pointwise_mul_q256_256_opt_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ256INV(%rip),%ymm0
vmovdqa		_16xQ256(%rip),%ymm1

/*repeat 4 times*/
repeat_2
repeat_2
repeat_2
block_64_short

add 	%r11,%rsp

ret

.global pointwise_mul_q256_512_opt_short
pointwise_mul_q256_512_opt_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ256INV(%rip),%ymm0
vmovdqa		_16xQ256(%rip),%ymm1

/*repeat 8 times*/
repeat_2
repeat_2
repeat_2
repeat_2

repeat_2
repeat_2
repeat_2
block_64_short

add 	%r11,%rsp

ret
