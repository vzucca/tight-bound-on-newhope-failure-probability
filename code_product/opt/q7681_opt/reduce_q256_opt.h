#ifndef REDUCE256_OPT
#define REDUCE256_OPT

// #include <stdio.h>
#include <stdint.h>
#include "params.h"

/* ############################################################################################## */
/* ############################### Functions for Q256 = 12289 ################################## */
/* ############################################################################################## */

/* coefficient-wise multiplication */
void pointwise_mul_q256_256_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q256_256_opt");
void pointwise_mul_q256_512_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q256_512_opt");

/* short version a <- a*b */
void pointwise_mul_q256_256_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q256_256_opt_short");
void pointwise_mul_q256_512_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q256_512_opt_short");

/* Barrett */
void poly_barrettq256_128_opt(int16_t *a) asm("poly_barrett_q256_128");
void poly_barrettq256_256_opt(int16_t *a) asm("poly_barrett_q256_256");
void poly_barrettq256_512_opt(int16_t *a) asm("poly_barrett_q256_512");
void poly_barrettq256_1024_opt(int16_t *a) asm("poly_barrett_q256_1024");

/* Mersenne */
void poly_mersq256_256_opt(int16_t* a) asm("poly_mers_q256_256");

/* classic multiplication a <- a * b */
void poly_mul_modq256_512_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq256_512_opt");
void poly_mul_4_modq256_1024_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq256_1024_opt");

/* classic shift and multiply by ntt_Y*/
void poly_shift_2_modq256_512_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_2_modq256_512_opt");

/* sub cst */
void poly_sub2q_cst_q256_256(int16_t*a) asm ("sub_cst2q_q256_256");

#endif
