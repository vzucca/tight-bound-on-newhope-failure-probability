.include "asm_macros.h"

.macro permut_one
vmovdqa		_lowdword(%rip),%ymm3
#reorder
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11
.endm

.macro permut_two
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10
.endm

.macro end_permut
# ----------- SWAP UPPER 8 AND LOW 8 -----------
vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9
.endm

.global invntt_256_1024_opt
invntt_256_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ256INV(%rip),%ymm0
vmovdqa		_16xQ256(%rip),%ymm1
vmovdqa		_16xv_256(%rip),%ymm2

################################################################################
##
##
##                           BEGIN TREE A
##
##
################################################################################

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A1
#
# ---------------------------------------------------------------------------

#load block 1 and block 2
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,0,32,64,96 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1024,1056,1088,1120
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2048,2080,2112,2144
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3072,3104
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4

invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3584(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE A1
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A2
#
# ---------------------------------------------------------------------------

#load block 3 and block 4
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,128,160,192,224 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1152,1184,1216,1248
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2176,2208,2240,2272
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3136,3168
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4

invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3616(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14
save_64_coeffs 10,3,4,5
# save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE A2
#
# ---------------------------------------------------------------------------

# shift to take part of A1 tree
sub		$128,%rdi

#level 5 with block 2 and block 4 (4 already loaded)
load_64_coeffs 10,3,4,5
invntt_update_2 10,3,4,5,6,7,8,9
gen_barrett_64_coeffs 10,3,4,5 2,1,11
vmovdqa		3840(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14
save_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#load block 1 and 3
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
gen_barrett_64_coeffs 4,5,6,7 2,1,11,12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3

save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

# moves to complete tree A3 and A4
add		$512,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A3
#
# ---------------------------------------------------------------------------

#load block 5 and block 6
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,256,288,320,352 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1280,1312,1344,1376
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2304,2336,2368,2400
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3200,3232
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4
invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3648(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE A3
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A4
#
# ---------------------------------------------------------------------------

#load block 7 and block 8
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,384,416,448,480 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1408,1440,1472,1504
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2432,2464,2496,2528
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3264,3296
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4
invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3680(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_64_coeffs 10,3,4,5
# save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE A4
#
# ---------------------------------------------------------------------------

# shift to take part of A3 tree
sub		$128,%rdi

#level 5 with block 6 and block 8 (8 already loaded)
load_64_coeffs 10,3,4,5
invntt_update_2 10,3,4,5,6,7,8,9
gen_barrett_64_coeffs 10,3,4,5 2,1,11
vmovdqa		3872(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14
save_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#load block 5 and 7
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
gen_barrett_64_coeffs 4,5,6,7 2,1,11,12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3

save_64_coeffs 8,9,10,11 rdi,256,288,320,352
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

# now ready to complete level 8 of part A
# moves to get block 1 to combine with block 5
sub		$512,%rdi
#level 6

#zetas
vmovdqa		3968(%rsi),%ymm15

#load block 1 and block 5 (already loaded in 4,5,6,7)
load_64_coeffs 8,9,10,11
# load_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608
invntt_update_2 8,9,10,11,4,5,6,7 12,13,14,3
signed_montgomery_64x64 15,15,15,15,4,5,6,7 1,0,12,13,14,3
save_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#load block 2 and block 6
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#load block 3 and block 7
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#load block 4 and block 8
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

################################################################################
##
##
##                           END TREE A
##
##
################################################################################

# moves to complete tree B1 and B2
add		$640,%rdi

################################################################################
##
##
##                           BEGIN TREE B
##
##
################################################################################

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B1
#
# ---------------------------------------------------------------------------

#load block 9 and block 10
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,512,544,576,608 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1536,1568,1600,1632
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2560,2592,2624,2656
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3328,3360
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4
invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3712(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE B1
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B2
#
# ---------------------------------------------------------------------------

#load block 11 and block 12
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,640,672,704,736 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1664,1696,1728,1760
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2688,2720,2752,2784
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3392,3424
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4
invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3744(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

# save_64_coeffs 10,3,4,5
save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE B2
#
# ---------------------------------------------------------------------------

# shift to take part of A1 tree
sub		$128,%rdi

#level 5 with block 10 and block 12 (12 already loaded)
load_64_coeffs 10,3,4,5
invntt_update_2 10,3,4,5,6,7,8,9
gen_barrett_64_coeffs 10,3,4,5 2,1,11
vmovdqa		3904(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14
save_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#load block 9 and 11
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
gen_barrett_64_coeffs 4,5,6,7 2,1,11,12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3

save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

# moves to complete tree B3 and B4
add		$512,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B3
#
# ---------------------------------------------------------------------------

#load block 13 and block 14
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,768,800,832,864 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1792,1824,1856,1888
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2816,2848,2880,2912
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3456,3488
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4
invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3776(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE B3
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B4
#
# ---------------------------------------------------------------------------

#load block 15 and block 16
load_128_coeffs 4,5,6,7,8,9,10,11

permut_one
#level 0

invntt_butterfly_4_z 4,6,8,10,5,7,9,11,896,928,960,992 rsi,0,1,12,13,14,15,3

permut_two
#level 1

invntt_update 3,5,7,9,4,6,8,10
gen_barrett_64_coeffs 3,5,7,9 2,1,11,4,6,8,10
load_64_coeffs 6,8,10,11 rsi,1920,1952,1984,2016
mul_lh_64 6,8,10,11,12,13,14,15,4,6,8,10,12,13,14,15
montgomery_reduce_64 4,6,8,10,12,13,14,15,4,6,8,10

#level 2
end_permut

invntt_update_2b 10,4,6,8,3,5,7,9
load_64_coeffs 12,13,14,15 rsi,2944,2976,3008,3040
signed_montgomery_64x64 12,13,14,15,3,5,7,9 1,0,11,12,13,14
# save_128_coeffs 10,3,4,5,6,7,8,9

#level 3

invntt_update_2 10,3,6,7,4,5,8,9
gen_barrett_64_coeffs 10,3,6,7 2,1,11
load_32_coeffs 14,15 rsi,3520,3552
signed_montgomery_64x64 14,14,15,15,4,5,8,9 1,0,11,12,13,14

#level 4
invntt_update_2 10,3,4,5,6,7,8,9
vmovdqa		3808(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14

save_64_coeffs 10,3,4,5
# save_128_coeffs 10,3,4,5,6,7,8,9

# ---------------------------------------------------------------------------
#
#                              END TREE B4
#
# ---------------------------------------------------------------------------

# shift to take part of B3 tree
sub		$128,%rdi

#level 5 with block 14 and block 16 (16 already loaded)
load_64_coeffs 10,3,4,5
invntt_update_2 10,3,4,5,6,7,8,9
gen_barrett_64_coeffs 10,3,4,5 2,1,11
vmovdqa		3936(%rsi),%ymm15
signed_montgomery_64x64 15,15,15,15,6,7,8,9 1,0,11,12,13,14
save_128_coeffs 10,3,4,5,6,7,8,9 rdi,0,32,64,96,256,288,320,352

sub		$128,%rdi

#load block 13 and 15
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
gen_barrett_64_coeffs 4,5,6,7 2,1,11,12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3

save_64_coeffs 8,9,10,11 rdi,256,288,320,352
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

# now ready to complete level 8 of part B
# moves to get block 9 to combine with block 13
sub		$512,%rdi

#level 6

#zetas
vmovdqa		4000(%rsi),%ymm15

#load block 9 and block 13 (already loaded in 4,5,6,7)
load_64_coeffs 8,9,10,11
# load_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608

invntt_update_2 8,9,10,11,4,5,6,7 12,13,14,3
signed_montgomery_64x64 15,15,15,15,4,5,6,7 1,0,12,13,14,3
save_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#load block 10 and block 14
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#load block 11 and block 15
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

add		$128,%rdi

#load block 12 and block 16
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3

save_64_coeffs 4,5,6,7
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

################################################################################
##
##
##                           END TREE B
##
##
################################################################################

#level 7

#zetas
vmovdqa		4032(%rsi),%ymm15
vmovdqa		_F256(%rip),%ymm2

# shift to get block 8
sub		$512,%rdi

#load block 8 and block 16 (already loaded)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

sub		$128,%rdi

#load block 7 and block 15
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

sub		$128,%rdi

#load block 6 and block 14
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

sub		$128,%rdi

#load block 5 and block 13
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

sub		$128,%rdi

#load block 4 and block 12
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

sub		$128,%rdi

#load block 3 and block 11
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

sub		$128,%rdi

#load block 2 and block 10
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

sub		$128,%rdi

#load block 1 and block 16
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
invntt_update_2 4,5,6,7,8,9,10,11 12,13,14,3
signed_montgomery_64x64 15,15,15,15,8,9,10,11 1,0,12,13,14,3
add_q_ifneg 8,9,10,11 1,12,13,14,3
signed_montgomery_64x64 2,2,2,2,4,5,6,7 1,0,12,13,14,3
add_q_ifneg 4,5,6,7 1,12,13,14,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

add 	%r11,%rsp

ret
