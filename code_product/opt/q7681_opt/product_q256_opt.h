#ifndef KARATSUBA256_OPT
#define KARATSUBA256_OPT

#include <stdint.h>

#include "const_256_opt.h"
#include "ntt_q256_opt.h"
#include "reduce_q256_opt.h"

#include "reduce_q256.h"
#include "ntt_q256.h"

/*########################################### n = 512 ############################################*/
/*########################################## q = 7681 ############################################*/

void poly_product_512_K0_P1_opt(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t b_tmp[N] __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    c[i] = a[i];
    b_tmp[i] = b[i];
  }

  ntt_256_512_opt(c,zetas_256_512_opt);
  ntt_256_512_opt(b_tmp,zetas_256_512_opt);
  poly_barrettq256_512_opt(c);
  poly_barrettq256_512_opt(b_tmp);

  poly_mul_modq256_512_opt(c,b_tmp,ntt_Y_256);

  invntt_256_512_opt(c,inv_zetas_256_512_opt);
}

void poly_product_512_K1_P0_opt(int16_t *c, const int16_t *a, const int16_t *b){

  /* START OLD PART */
  int16_t a0[N/2] __attribute__((aligned(32))), a1[N/2] __attribute__((aligned(32))), a01[N/2] __attribute__((aligned(32)));
  int16_t b0[N/2] __attribute__((aligned(32))), b1[N/2] __attribute__((aligned(32))), b01[N/2] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  ntt_256_opt(a0,zetas_256_opt);
  ntt_256_opt(a1,zetas_256_opt);
  ntt_256_opt(b0,zetas_256_opt);
  ntt_256_opt(b1,zetas_256_opt);
  //since we removed the final reduction in the NTT we do it here
  poly_barrettq256_256_opt(a0);
  poly_barrettq256_256_opt(a1);
  poly_barrettq256_256_opt(b0);
  poly_barrettq256_256_opt(b1);
  /* END OLD PART*/

  /* START NEW PART*/
  // TODO: Combine ntt([a]) and ntt([b]) in a single ntt([a][b])
  // TODO: Find another alignment to combine functions better
  /* int16_t ab[6*N/2] __attribute__((aligned(32))); */
  /* int16_t* a0  = ab       ; int16_t* a1  = a0 +(N/2); */
  /* int16_t* b0  = a1 +(N/2); int16_t* b1  = b0 +(N/2); */
  /* int16_t* a01 = b1 +(N/2); int16_t* b01 = a01+(N/2); */
  /* for (size_t i = 0; i < N; i++) { */
  /*   b0[i] = a[i]; */
  /*   a01[i] = b[i]; */
  /* } */
  /* ntt_256_512_opt(b0,zetas_256_512_opt); */
  /* ntt_256_512_opt(a01,zetas_256_512_opt); */
  /* for (size_t i = 0; i < N/2; i++) { */
  /*   a0[i]     = b0 [2*i  ]; */
  /*   a1[i]     = b0 [2*i+1]; */
  /* } */
  /* for (size_t i = 0; i < N/2; i++) { */
  /*   b0[i]     = a01[2*i  ]; */
  /*   b1[i]     = a01[2*i+1]; */
  /* } */
  /* poly_barrettq256_1024_opt(ab); */
  /* END NEW PART*/

  /* Output of the ntt is in [0, q) */
  poly_add_256(a01,a0,a1);
  poly_add_256(b01,b0,b1);

  /* Polynomial products */
  pointwise_mul_q256_256_opt_short(a0,b0);
  pointwise_mul_q256_256_opt_short(a1,b1);
  // pointwise_mul_q256_512_opt_short(ab,ab+N);
  pointwise_mul_q256_256_opt_short(a01,b01);

  /* Reconstruction a01 <- a01 - a0 - a1 in ]-3q,3q[ */
  poly_sub_256_short(a01,a0); poly_sub_256_short(a01,a1);
  /* Reconstruction a0 <- a0 + a1*ntt_Y  ]-2q,2q[*/
  pointwise_mul_q256_256_opt_short(a1,ntt_Y_256);
  poly_add_256_short(a0,a1);

  /* Reduction before invntt */
  poly_barrettq256_256_opt(a0);
  poly_barrettq256_256_opt(a01);

  /* START OLD PART */
  invntt_256_opt(a0,inv_zetas_256_opt);
  invntt_256_opt(a01,inv_zetas_256_opt);
  /* END OLD PART*/

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = a0[i];
    c[2*i+1] = a01[i];
  }

  /* START NEW PART*/
  /* invntt_256_512_opt(c,inv_zetas_256_512_opt); */
  /* END NEW PART*/
}

/* /\*########################################## n = 1024 ############################################*\/ */
/* /\*########################################## q = 7681 ############################################*\/ */

void poly_product_1024_K0_P2_opt(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t b_tmp[N] __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    c[i] = a[i];
    b_tmp[i] = b[i];
  }

  // nttq256_1024(a_tmp);
  // nttq256_1024(b_tmp);
  ntt_256_1024_opt(c,zetas_256_1024_opt);
  poly_barrettq256_1024_opt(c);
  ntt_256_1024_opt(b_tmp,zetas_256_1024_opt);
  poly_barrettq256_1024_opt(b_tmp);

  // poly_mul_modq256(c,c,b_tmp,N,4,ntt_Y_256);
  poly_mul_4_modq256_1024_opt(c,b_tmp,ntt_Y_256_prod_pack4);

  // invnttq256_1024(c);
  invntt_256_1024_opt(c,inv_zetas_256_1024_opt);
}

void poly_product_1024_K1_P1_opt(int16_t *c, const int16_t *a, const int16_t *b){

  /* START OLD PART */
  int16_t a0[N/2] __attribute__((aligned(32))), a1[N/2] __attribute__((aligned(32))), a01[N/2] __attribute__((aligned(32)));
  int16_t b0[N/2] __attribute__((aligned(32))), b1[N/2] __attribute__((aligned(32))), b01[N/2] __attribute__((aligned(32)));

  for (size_t i = 0; i < N/2; i++) {
    a0[i] = a[2*i];a1[i]=a[2*i+1];
    b0[i] = b[2*i];b1[i]=b[2*i+1];
  }

  ntt_256_512_opt(a0,zetas_256_512_opt);
  ntt_256_512_opt(a1,zetas_256_512_opt);
  ntt_256_512_opt(b0,zetas_256_512_opt);
  ntt_256_512_opt(b1,zetas_256_512_opt);
  poly_barrettq256_512_opt(a0);
  poly_barrettq256_512_opt(b0);
  poly_barrettq256_512_opt(a1);
  poly_barrettq256_512_opt(b1);
  /* END OLD PART*/

  /* START NEW PART*/
  /* int16_t ab[6*N/2] __attribute__((aligned(32))); */
  /* int16_t* a0  = ab       ; int16_t* a1  = a0 +(N/2); */
  /* int16_t* b0  = a1 +(N/2); int16_t* b1  = b0 +(N/2); */
  /* int16_t* a01 = b1 +(N/2); int16_t* b01 = a01+(N/2); */

  /* for (size_t i = 0; i < N; i++) {b0[i] = a[i];a01[i] = b[i];} */
  /* ntt_256_1024_opt(a01,zetas_256_1024_opt); */
  /* ntt_256_1024_opt(b0,zetas_256_1024_opt); */
  /* for (size_t i = 0; i < N/2; i++) { */
  /*   a0[i]     = b0 [2*i  ]; */
  /*   a1[i]     = b0 [2*i+1]; */
  /* } */
  /* for (size_t i = 0; i < N/2; i++) { */
  /*   b0[i]     = a01[2*i  ]; */
  /*   b1[i]     = a01[2*i+1]; */
  /* } */
  /* poly_barrettq256_1024_opt(ab); */
  /* poly_barrettq256_1024_opt(ab+N); */
  /* END NEW PART*/

  /* Output of the ntt is in [0, q) */
  poly_add_512(a01,a0,a1);
  poly_add_512(b01,b0,b1);

  /* Polynomial products */
  poly_mul_modq256_512_opt(a0,b0,ntt_Y_256);
  poly_mul_modq256_512_opt(a01,b01,ntt_Y_256);
  poly_mul_modq256_512_opt(a1,b1,ntt_Y_256);

  /* Reconstruction a01 <- a01 - a0 - a1 in ]-3q,3q[ */
  poly_sub_512_short(a01,a0);poly_sub_512_short(a01,a1);
  /* Reconstruction a0 <- a0 + a1*ntt_Y  ]-2q,2q[*/
  poly_shift_2_modq256_512_opt(a1,ntt_Y_256); poly_add_512_short(a0,a1);

  /* Reduction before invntt */
  poly_barrettq256_512_opt(a0);
  poly_barrettq256_512_opt(a01);

  /* START OLD PART */
  invntt_256_512_opt(a0,inv_zetas_256_512_opt);
  invntt_256_512_opt(a01,inv_zetas_256_512_opt);
  /* END OLD PART*/

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = a0[i];
    c[2*i+1] = a01[i];
  }

  /* START NEW PART*/
  /* invntt_256_1024_opt(c,inv_zetas_256_1024_opt); */
  /* END NEW PART*/
}


void poly_product_1024_K2_P0_opt(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4] __attribute__((aligned(32))), a1[N/4] __attribute__((aligned(32))), a2[N/4] __attribute__((aligned(32))), a3[N/4] __attribute__((aligned(32))),\
  a01[N/4] __attribute__((aligned(32))), a23[N/4] __attribute__((aligned(32))), a02[N/4] __attribute__((aligned(32))), a13[N/4] __attribute__((aligned(32))),\
  a0123[N/4] __attribute__((aligned(32)));

  int16_t b0[N/4] __attribute__((aligned(32))), b1[N/4] __attribute__((aligned(32))), b2[N/4] __attribute__((aligned(32))), b3[N/4] __attribute__((aligned(32))),\
  b01[N/4] __attribute__((aligned(32))), b23[N/4] __attribute__((aligned(32))), b02[N/4] __attribute__((aligned(32))), b13[N/4] __attribute__((aligned(32))),\
  b0123[N/4] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  ntt_256_opt(a0,zetas_256_opt);
  ntt_256_opt(a1,zetas_256_opt);
  ntt_256_opt(a2,zetas_256_opt);
  ntt_256_opt(a3,zetas_256_opt);
  ntt_256_opt(b0,zetas_256_opt);
  ntt_256_opt(b1,zetas_256_opt);
  ntt_256_opt(b2,zetas_256_opt);
  ntt_256_opt(b3,zetas_256_opt);
  poly_barrettq256_256_opt(a0);
  poly_barrettq256_256_opt(a1);
  poly_barrettq256_256_opt(a2);
  poly_barrettq256_256_opt(a3);
  poly_barrettq256_256_opt(b0);
  poly_barrettq256_256_opt(b1);
  poly_barrettq256_256_opt(b2);
  poly_barrettq256_256_opt(b3);
  /* Output of ntt256 in [0,q) */

  /* Start computing the sums needed for Karatsuba */
  poly_add_256(a01,a0,a1);
  poly_add_256(a23,a2,a3);

  poly_add_256(a02,a0,a2);
  poly_add_256(a13,a1,a3);
  poly_add_256(a0123,a02,a13);

  poly_add_256(b01,b0,b1);
  poly_add_256(b23,b2,b3);

  poly_add_256(b02,b0,b2);
  poly_add_256(b13,b1,b3);
  poly_add_256(b0123,b02,b13);
  /* Output of the sums in [0,2q) or [0,4q)*/

  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  pointwise_mul_q256_256_opt_short(a0,b0);
  pointwise_mul_q256_256_opt_short(a1,b1);
  pointwise_mul_q256_256_opt_short(a01,b01);

  pointwise_mul_q256_256_opt_short(a2,b2);
  pointwise_mul_q256_256_opt_short(a3,b3);
  pointwise_mul_q256_256_opt_short(a23,b23);

  pointwise_mul_q256_256_opt_short(a02,b02);
  pointwise_mul_q256_256_opt_short(a13,b13);
  /* Need to reduce [0,4q) to ]-2q,2q[ otherwise Montgomery won't handle the product */
  poly_sub2q_cst_q256_256(a0123); poly_sub2q_cst_q256_256(b0123);
  pointwise_mul_q256_256_opt_short(a0123,b0123);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* a02 <- a02 - a0 - a2 ==> a02 in ]-3q,3q[ = ]-9987, 9987[ */
  poly_sub_256_short(a02,a0);poly_sub_256_short(a02,a2);
  /* a13 <- a13 - a1 - a3 ==> a13 in ]-3q, 3q[ */
  poly_sub_256_short(a13,a1);poly_sub_256_short(a13,a3);
  /* a0123 <- a0123 - a01 - a23 ==> a0123 in ]-3q, 3q[ */
  poly_sub_256_short(a0123,a01);poly_sub_256_short(a0123,a23);
  /* a0 <- a0 + a2*ntt_Y ==> a0 in ]-2q,2q[ = ]-2q,2q[ */
  pointwise_mul_q256_256_opt_short(a2,ntt_Y_256);
  poly_add_256_short(a0,a2);
  /* a1 <- a1 + a3*ntt_Y ==> a1 in ]-2q,2q[ */
  pointwise_mul_q256_256_opt_short(a3,ntt_Y_256);
  poly_add_256_short(a1,a3);
  /* a01 <- a01 + a23*ntt_Y ==> a01 in ]-2q,2q[ */
  pointwise_mul_q256_256_opt_short(a23,ntt_Y_256);
  poly_add_256_short(a01,a23);

  /* Reconstruct second level of Karatsuba */
  /* a01 <- a01 - a0 - a1 ==> in ]-6q, 6q[ thus reduce a01-a01 */
  poly_sub_256_short(a01,a0);
  poly_mersq256_256_opt(a01);
  poly_sub_256_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 ==> in ]-9q, 9q[ thus reduce each term */
  poly_mersq256_256_opt(a0123); poly_mersq256_256_opt(a02); poly_mersq256_256_opt(a13);
  poly_sub_256_short(a0123,a02);poly_sub_256_short(a0123,a13);
  /* a0 <- a0 + a13*ntt_Y ==> in ]-2q,2q[ */
  pointwise_mul_q256_256_opt_short(a13,ntt_Y_256);
  poly_add_256_short(a0,a13);
  /* a1 <- a1 + a02 ==> in ]-4q,4q[ */
  poly_add_256_short(a1,a02);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq256_256_opt(a0);
  poly_barrettq256_256_opt(a1);
  poly_barrettq256_256_opt(a01);
  poly_barrettq256_256_opt(a0123);

  invntt_256_opt(a0,inv_zetas_256_opt);
  invntt_256_opt(a1,inv_zetas_256_opt);
  invntt_256_opt(a01,inv_zetas_256_opt);
  invntt_256_opt(a0123,inv_zetas_256_opt);

  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = a0[i];
    c[4*i+1] = a01[i];
    c[4*i+2] = a1[i];
    c[4*i+3] = a0123[i];
  }
}

#endif
