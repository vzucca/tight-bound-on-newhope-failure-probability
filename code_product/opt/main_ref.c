#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <x86intrin.h>

#include "poly.h"
#include "product_q16.h"
#include "product_q32.h"
#include "product_q128.h"
#include "product_q256.h"
// #include "product_q1024_ref.h"
#include "product_q1024.h"

#if defined(__i386__)

static __inline__ unsigned long long rdtsc(void)
{
  unsigned long long int x;
  __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
  return x;
}

#elif defined(__x86_64__)

static __inline__ unsigned long long rdtsc(void)
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}
#endif

void print_size_invntt(unsigned int size_poly, unsigned int size_ntt){
  unsigned int l, s, i, j, t, k = 0;

  poly c;
  for(i = 0 ; i < N ; i++)
    c.coeffs[i] = 1;

  l = 1<<(size_poly-size_ntt);

  size_poly = 1<<size_poly;

  for(i = 0 ; i < size_ntt ; i++){
    for(s = 0 ; s < size_poly ; s = j+l){
      for(j = s ; j < s+l ; j++){
	if( ((i==1) && (j%4<2)) || ((i==3) && (j%16<8)) || ((i==5) && (j%64<32)) )
	  c.coeffs[j] = 1;
	else{
	  t = c.coeffs[j];
	  c.coeffs[j] = t + c.coeffs[j+l];
	}
	c.coeffs[j+l] = 1;
      }
      k++;
    }
    printf("%d",i+1);
    poly_print(&c,"",size_poly);
    l<<=1;
  }
}

int main(){

  srand(time(NULL));

  /* int IT = 1<<11; */

#if N == 512
  poly a, b, c0, c1, c2;

  unsigned long long t0 = 0, t1 = 0, t2 = 0, s, e;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q1024);
    poly_rand(&b,Q1024);

    s = rdtsc();
    poly_product_512_K0_P0(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;
  }

  printf("\nn = %u, q = %d : %d tests\n",N,Q1024,IT);

  printf("Number of cycles for 512-K0-P0: %llu\n",t0/IT);

  t0 = 0;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q256);
    poly_rand(&b,Q256);

    s = rdtsc();
    poly_product_512_K0_P1(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_512_K1_P0(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d : %d tests OK\n",N,Q256,IT);

  printf("Number of cycles for 512-K0-P1: %llu\n",t0/IT);
  printf("Number of cycles for 512-K1-P0: %llu\n",t1/IT);

  t0 = 0; t1 = 0;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q128);
    poly_rand(&b,Q128);

    s = rdtsc();
    poly_product_512_K0_P2(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_512_K1_P1(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    s = rdtsc();
    poly_product_512_K2_P0(c2.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t2 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c2) == 0){
      printf("Error for i = %d: c0 != c2\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c2.coeffs,N);
      poly_print(&diff,"c0-c2",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d : %d tests OK\n",N,Q128,IT);

  printf("Number of cycles for 512-K0-P2: %llu\n",t0/IT);
  printf("Number of cycles for 512-K1-P1: %llu\n",t1/IT);
  printf("Number of cycles for 512-K2-P0: %llu\n",t2/IT);

  t0 = 0; t1 = 0; t2 = 0;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q128);
    poly_rand(&b,Q128);

    s = rdtsc();
    poly_product_512_K0_P5(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_512_K1_P4(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    s = rdtsc();
    poly_product_512_K2_P3(c2.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t2 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c2) == 0){
      printf("Error for i = %d: c0 != c2\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c2.coeffs,N);
      poly_print(&diff,"c0-c2",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d : %d tests OK\n",N,Q16,IT);

  printf("Number of cycles for 512-K0-P5: %llu\n",t0/IT);
  printf("Number of cycles for 512-K1-P4: %llu\n",t1/IT);
  printf("Number of cycles for 512-K2-P3: %llu\n",t2/IT);

  t0 = 0; t1 = 0; t2 = 0;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q32);
    poly_rand(&b,Q32);

    s = rdtsc();
    poly_product_512_K0_P4(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_512_K1_P3(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    s = rdtsc();
    poly_product_512_K2_P2(c2.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t2 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c2) == 0){
      printf("Error for i = %d: c0 != c2\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c2.coeffs,N);
      poly_print(&diff,"c0-c2",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d : %d tests OK\n",N,Q32,IT);

  printf("Number of cycles for 512-K0-P4: %llu\n",t0/IT);
  printf("Number of cycles for 512-K1-P3: %llu\n",t1/IT);
  printf("Number of cycles for 512-K2-P2: %llu\n",t2/IT);


#elif N == 1024
  poly a, b, c0, c1, c2, c3;

  unsigned long long t0 = 0, t1 = 0, t2 = 0, t3 = 0, s, e;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q1024);
    poly_rand(&b,Q1024);

    s = rdtsc();
    poly_product_1024_K0_P0(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;
  }

  printf("n = %u, q = %d : %d tests\n",N,Q1024,IT);

  printf("Number of cycles for 1024-K0-P0: %llu\n",t0/IT);

  t0 = 0;

   for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q256);
    poly_rand(&b,Q256);

    s = rdtsc();
    poly_product_1024_K0_P2(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_1024_K1_P1(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    s = rdtsc();
    poly_product_1024_K2_P0(c2.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t2 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c2) == 0){
      printf("Error for i = %d: c0 != c2\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c2.coeffs,N);
      poly_print(&diff,"c0-c2",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d: %d tests OK\n",N,Q256,IT);

  printf("Number of cycles for 1024-K0-P2: %llu\n",t0/IT);
  printf("Number of cycles for 1024-K1-P1: %llu\n",t1/IT);
  printf("Number of cycles for 1024-K2-P0: %llu\n",t2/IT);

  t0 = 0, t1 = 0, t2 = 0;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q128);
    poly_rand(&b,Q128);

    s = rdtsc();
    poly_product_1024_K0_P3(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_1024_K1_P2(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    s = rdtsc();
    poly_product_1024_K2_P1(c2.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t2 += e-s;

    s = rdtsc();
    poly_product_1024_K3_P0(c3.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t3 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c2) == 0){
      printf("Error for i = %d: c0 != c2\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c2.coeffs,N);
      poly_print(&diff,"c0-c2",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c3) == 0){
      printf("Error for i = %d: c0 != c3\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c3.coeffs,N);
      poly_print(&diff,"c0-c3",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d: %d tests OK\n",N,Q128,IT);

  printf("Number of cycles for 1024-K0-P3: %llu\n",t0/IT);
  printf("Number of cycles for 1024-K1-P2: %llu\n",t1/IT);
  printf("Number of cycles for 1024-K2-P1: %llu\n",t2/IT);
  printf("Number of cycles for 1024-K3-P0: %llu\n",t3/IT);

  t0 = 0, t1 = 0, t2 = 0, t3 = 0;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q16);
    poly_rand(&b,Q16);

    s = rdtsc();
    poly_product_1024_K0_P6(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_1024_K1_P5(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    s = rdtsc();
    poly_product_1024_K2_P4(c2.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t2 += e-s;

    s = rdtsc();
    poly_product_1024_K3_P3(c3.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t3 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c2) == 0){
      printf("Error for i = %d: c0 != c2\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c2.coeffs,N);
      poly_print(&diff,"c0-c2",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c3) == 0){
      printf("Error for i = %d: c0 != c3\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c3.coeffs,N);
      poly_print(&diff,"c0-c3",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d: %d tests OK\n",N,Q16,IT);

  printf("Number of cycles for 1024-K0-P6: %llu\n",t0/IT);
  printf("Number of cycles for 1024-K1-P5: %llu\n",t1/IT);
  printf("Number of cycles for 1024-K2-P4: %llu\n",t2/IT);
  printf("Number of cycles for 1024-K3-P3: %llu\n",t3/IT);

  t0 = 0, t1 = 0, t2 = 0, t3 = 0;

  for(int i = 0 ; i < IT ; i++){
    poly_rand(&a,Q32);
    poly_rand(&b,Q32);

    s = rdtsc();
    poly_product_1024_K0_P5(c0.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t0 += e-s;

    s = rdtsc();
    poly_product_1024_K1_P4(c1.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t1 += e-s;

    s = rdtsc();
    poly_product_1024_K2_P3(c2.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t2 += e-s;

    s = rdtsc();
    poly_product_1024_K3_P2(c3.coeffs,a.coeffs,b.coeffs);
    e = rdtsc();
    t3 += e-s;

    if(poly_are_equals(&c0,&c1) == 0){
      printf("Error for i = %d: c0 != c1\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c1.coeffs,N);
      poly_print(&diff,"c0-c1",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c2) == 0){
      printf("Error for i = %d: c0 != c2\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c2.coeffs,N);
      poly_print(&diff,"c0-c2",N);
      return 1;
    }
    if(poly_are_equals(&c0,&c3) == 0){
      printf("Error for i = %d: c0 != c3\n",i);
      poly diff;
      poly_sub(diff.coeffs,c0.coeffs,c3.coeffs,N);
      poly_print(&diff,"c0-c3",N);
      return 1;
    }
  }

  printf("\nn = %u, q = %d: %d tests OK\n",N,Q32,IT);

  printf("Number of cycles for 1024-K0-P5: %llu\n",t0/IT);
  printf("Number of cycles for 1024-K1-P4: %llu\n",t1/IT);
  printf("Number of cycles for 1024-K2-P3: %llu\n",t2/IT);
  printf("Number of cycles for 1024-K3-P2: %llu\n",t3/IT);

#endif

  return 0;
}
