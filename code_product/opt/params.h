#ifndef PARAMS
#define PARAMS

#include <stdint.h>

#ifndef N
#define N 512
#endif

#ifndef IT
#define IT 1<<16
#endif

#define Q16 2017
#define Q16INV 31777    /* Q^(-1) mod M where M = 2^16 */
#define V16 16636       /* v128 = 2^25/q16 + 1*/
#define F16 994         /* F16  = (mont^2/n)%q*/

#define Q32 1601
#define Q32INV 2497
#define F32 1095        /* 1095 = (mont^2/n)%q*/
#define V32 20958

#define Q64 1409
#define Q64INV 14977
#define F64 1012
#define V64 23814

#define Q128 3329
#define Q128INV 62209 /* -3327 */
#define F128 1441
#define V128 ((1U << 26) / Q128 + 1) /* 20158 */

#define Q769 769
#define Q769INV 64769 /*-767*/
#define F769 655
#define V769 21817

#define Q256 7681
#define Q256INV 57857 /*-7679*/
#define F256 1912
#define V256 17474

#define Q1024 12289
#define Q1024INV 53249 /*-12287*/
#define F1024 3755
#define F512 7510
#define V1024 ((1U << 28) / Q1024 + 1) /* 21844 */

// dim 128
// #define V 20159

#endif
