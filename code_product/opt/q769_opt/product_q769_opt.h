#ifndef KARATSUBA769_OPT
#define KARATSUBA769_OPT

#include <stdint.h>

#include "reduce_q769_opt.h"
#include "ntt_q769_opt.h"

/*########################################### n = 512 ############################################*/
/*########################################## q = 769 ############################################*/

void poly_product_512_K0_P2_769_opt(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t b_tmp[N]  __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    c[i] = a[i];
    b_tmp[i] = b[i];
  }

  ntt_769_512_opt(c,zetas_769_512_opt);poly_barrettq769_512_opt(c);
  ntt_769_512_opt(b_tmp,zetas_769_512_opt);poly_barrettq769_512_opt(b_tmp);

  poly_mul_4_modq769_512_opt(c,b_tmp,ntt_Y_769_prod_pack4);

  invntt_769_512_opt(c,inv_zetas_769_512_opt);
}

void poly_product_512_K1_P1_769_opt(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2] __attribute__((aligned(32))), a1[N/2] __attribute__((aligned(32))), a01[N/2] __attribute__((aligned(32)));
  int16_t b0[N/2] __attribute__((aligned(32))), b1[N/2] __attribute__((aligned(32))), b01[N/2] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  ntt_769_256_opt(a0,zetas_769_256_opt);poly_barrettq769_256_opt(a0);
  ntt_769_256_opt(b0,zetas_769_256_opt);poly_barrettq769_256_opt(b0);
  ntt_769_256_opt(a1,zetas_769_256_opt);poly_barrettq769_256_opt(a1);
  ntt_769_256_opt(b1,zetas_769_256_opt);poly_barrettq769_256_opt(b1);

  /* Output of the ntt is in [0, q) */
  poly_add_256(a01,a0,a1);
  poly_add_256(b01,b0,b1);

  /* Polynomial products */
  poly_mul_modq769_256_opt(a0,b0,ntt_Y_769);
  poly_mul_modq769_256_opt(a01,b01,ntt_Y_769);
  poly_mul_modq769_256_opt(a1,b1,ntt_Y_769);

  /* Reconstruction a01 <- a01 - a0 - a1 in ]-3q,3q[ */
  poly_sub_256_short(a01,a0);poly_sub_256_short(a01,a1);
  /* Reconstruction a0 <- a0 + a1*ntt_Y  ]-2q,2q[*/
  poly_shift_2_modq769_256_opt(a1,ntt_Y_769); poly_add_256_short(a0,a1);

  /* Reduction before invntt */
  poly_barrettq769_256_opt(a0);
  poly_barrettq769_256_opt(a01);

  invntt_769_256_opt(a0,inv_zetas_769_256_opt);
  invntt_769_256_opt(a01,inv_zetas_769_256_opt);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = a0[i];
    c[2*i+1] = a01[i];
  }
  
  /* invntt_769_512_opt(c,inv_zetas_769_512_opt); */
}

void poly_product_512_K2_P0_769_opt(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4] __attribute__((aligned(32))), a1[N/4] __attribute__((aligned(32))), a2[N/4] __attribute__((aligned(32))), a3[N/4] __attribute__((aligned(32))), a01[N/4] __attribute__((aligned(32))), a23[N/4] __attribute__((aligned(32))), a02[N/4] __attribute__((aligned(32))), a13[N/4] __attribute__((aligned(32))), a0123[N/4] __attribute__((aligned(32)));

  int16_t b0[N/4] __attribute__((aligned(32))), b1[N/4] __attribute__((aligned(32))), b2[N/4] __attribute__((aligned(32))), b3[N/4] __attribute__((aligned(32))), b01[N/4] __attribute__((aligned(32))), b23[N/4] __attribute__((aligned(32))), b02[N/4] __attribute__((aligned(32))), b13[N/4] __attribute__((aligned(32))), b0123[N/4] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  ntt_769_128_opt(a0,zetas_769_opt);poly_barrettq769_128_opt(a0);
  ntt_769_128_opt(b0,zetas_769_opt);poly_barrettq769_128_opt(b0);
  ntt_769_128_opt(a1,zetas_769_opt);poly_barrettq769_128_opt(a1);
  ntt_769_128_opt(b1,zetas_769_opt);poly_barrettq769_128_opt(b1);
  ntt_769_128_opt(a2,zetas_769_opt);poly_barrettq769_128_opt(a2);
  ntt_769_128_opt(b2,zetas_769_opt);poly_barrettq769_128_opt(b2);
  ntt_769_128_opt(a3,zetas_769_opt);poly_barrettq769_128_opt(a3);
  ntt_769_128_opt(b3,zetas_769_opt);poly_barrettq769_128_opt(b3);
  /* Output of ntt128 in [0,q) */

  /* Start computing the sums needed for Karatsuba */
  poly_add_128(a01,a0,a1);
  poly_add_128(a23,a2,a3);

  poly_add_128(a02,a0,a2);
  poly_add_128(a13,a1,a3);
  poly_add_128(a0123,a02,a13);

  poly_add_128(b01,b0,b1);
  poly_add_128(b23,b2,b3);

  poly_add_128(b02,b0,b2);
  poly_add_128(b13,b1,b3);
  poly_add_128(b0123,b02,b13);
  /* Output of the sums in [0,2q) or [0,4q)*/

  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  pointwise_mul_q769_128_opt_short(a0,b0);
  pointwise_mul_q769_128_opt_short(a1,b1);
  pointwise_mul_q769_128_opt_short(a01,b01);

  pointwise_mul_q769_128_opt_short(a2,b2);
  pointwise_mul_q769_128_opt_short(a3,b3);
  pointwise_mul_q769_128_opt_short(a23,b23);

  pointwise_mul_q769_128_opt_short(a02,b02);
  pointwise_mul_q769_128_opt_short(a13,b13);
  pointwise_mul_q769_128_opt_short(a0123,b0123);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* a02 <- a02 - a0 - a2 ==> a02 in ]-3q,3q[ */
  poly_sub_128_short(a02,a0); poly_sub_128_short(a02,a2);
  /* a13 <- a13 - a1 - a3 ==> a13 in ]-3q,3q[ */
  poly_sub_128_short(a13,a1); poly_sub_128_short(a13,a3);
  /* a0123 <- a0123 - a01 - a23 ==> a0123 in ]-3q,3q[ */
  poly_sub_128_short(a0123,a01); poly_sub_128_short(a0123,a23);
  /* a0 <- a0 + a2*ntt_Y ==> a0 in ]-2q,2q[ */
  pointwise_mul_q769_128_opt_short(a2,ntt_Y_769); poly_add_128_short(a0,a2);
  /* a1 <- a1 + a3*ntt_Y ==> a1 in ]-2q,2q[ */
  pointwise_mul_q769_128_opt_short(a3,ntt_Y_769); poly_add_128_short(a1,a3);
  /* a01 <- a01 + a23*ntt_Y ==> a01 in ]-2q,2q[ */
  pointwise_mul_q769_128_opt_short(a23,ntt_Y_769); poly_add_128_short(a01,a23);

  /* Reconstruct second level of Karatsuba */
  /* a01 <- a01 - a0 - a1 ==> in ]-6q, 6q[ */
  poly_sub_128_short(a01,a0); poly_sub_128_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 ==> in ]-9q, 9q[ */
  poly_sub_128_short(a0123,a02); poly_sub_128_short(a0123,a13);
  /* a0 <- a0 + a13*ntt_Y ==> in ]-3q,3q[ */
  pointwise_mul_q769_128_opt_short(a13,ntt_Y_769); poly_add_128_short(a0,a13);
  /* a1 <- a1 + a02 ==> in ]-5q,5q[ */
  poly_add_128_short(a1,a02);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq769_128_opt(a01);
  poly_barrettq769_128_opt(a0123);
  poly_barrettq769_128_opt(a0);
  poly_barrettq769_128_opt(a1);

  /* invntt_769_128_opt(a0,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a1,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a01,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a0123,inv_zetas_769_opt); */

  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = a0[i];
    c[4*i+1] = a01[i];
    c[4*i+2] = a1[i];
    c[4*i+3] = a0123[i];
  }
  
  invntt_769_512_opt(c,inv_zetas_769_512_opt);
}


// /*########################################## n = 1024 ############################################*/
// /*########################################## q = 769 #############################################*/

void poly_product_1024_K0_P3_769_opt(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t b_tmp[N]  __attribute__((aligned(32)));

  for(int i = 0 ; i < N ; i++){
    c[i] = a[i];
    b_tmp[i] = b[i];
  }

  ntt_769_1024_opt(c,zetas_769_1024_opt);poly_barrettq769_1024_opt(c);
  ntt_769_1024_opt(b_tmp,zetas_769_1024_opt);poly_barrettq769_1024_opt(b_tmp);

  poly_mul_8_modq769_1024_opt(c,b_tmp,ntt_Y_769_prod_pack8);

  invntt_769_1024_opt(c,inv_zetas_769_1024_opt);
}

void poly_product_1024_K1_P2_769_opt(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2] __attribute__((aligned(32))), a1[N/2] __attribute__((aligned(32))), a01[N/2] __attribute__((aligned(32)));
  int16_t b0[N/2] __attribute__((aligned(32))), b1[N/2] __attribute__((aligned(32))), b01[N/2] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  ntt_769_512_opt(a0,zetas_769_512_opt);poly_barrettq769_512_opt(a0);
  ntt_769_512_opt(b0,zetas_769_512_opt);poly_barrettq769_512_opt(b0);
  ntt_769_512_opt(a1,zetas_769_512_opt);poly_barrettq769_512_opt(a1);
  ntt_769_512_opt(b1,zetas_769_512_opt);poly_barrettq769_512_opt(b1);

  /* Output of the ntt is in [0, q) */
  poly_add_512(a01,a0,a1);
  poly_add_512(b01,b0,b1);

  /* Polynomial products */
  poly_mul_4_modq769_512_opt(a0,b0,ntt_Y_769_prod_pack4);
  poly_mul_4_modq769_512_opt(a01,b01,ntt_Y_769_prod_pack4);
  poly_mul_4_modq769_512_opt(a1,b1,ntt_Y_769_prod_pack4);

  /* Reconstruction a01 <- a01 - a0 - a1 in ]-3q,3q[ */
  poly_sub_512_short(a01,a0); poly_sub_512_short(a01,a1);
  /* Reconstruction a0 <- a0 + a1*ntt_Y  ]-2q,2q[*/
  poly_shift_4_modq769_512_opt(a1,ntt_Y_769_pack4); poly_add_512_short(a0,a1);

  /* Reduction before invntt */
  poly_barrettq769_512_opt(a0);
  poly_barrettq769_512_opt(a01);

  invntt_769_512_opt(a0,inv_zetas_769_512_opt);
  invntt_769_512_opt(a01,inv_zetas_769_512_opt);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = a0[i];
    c[2*i+1] = a01[i];
  }
}

void poly_product_1024_K2_P1_769_opt(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4] __attribute__((aligned(32))), a1[N/4] __attribute__((aligned(32))), a2[N/4] __attribute__((aligned(32))), a3[N/4] __attribute__((aligned(32))), a01[N/4] __attribute__((aligned(32))), a23[N/4] __attribute__((aligned(32))), a02[N/4] __attribute__((aligned(32))), a13[N/4] __attribute__((aligned(32))), a0123[N/4] __attribute__((aligned(32)));

  int16_t b0[N/4] __attribute__((aligned(32))), b1[N/4] __attribute__((aligned(32))), b2[N/4] __attribute__((aligned(32))), b3[N/4] __attribute__((aligned(32))), b01[N/4] __attribute__((aligned(32))), b23[N/4] __attribute__((aligned(32))), b02[N/4] __attribute__((aligned(32))), b13[N/4] __attribute__((aligned(32))), b0123[N/4] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  ntt_769_256_opt(a0,zetas_769_256_opt);poly_barrettq769_256_opt(a0);
  ntt_769_256_opt(b0,zetas_769_256_opt);poly_barrettq769_256_opt(b0);
  ntt_769_256_opt(a1,zetas_769_256_opt);poly_barrettq769_256_opt(a1);
  ntt_769_256_opt(b1,zetas_769_256_opt);poly_barrettq769_256_opt(b1);
  ntt_769_256_opt(a2,zetas_769_256_opt);poly_barrettq769_256_opt(a2);
  ntt_769_256_opt(b2,zetas_769_256_opt);poly_barrettq769_256_opt(b2);
  ntt_769_256_opt(a3,zetas_769_256_opt);poly_barrettq769_256_opt(a3);
  ntt_769_256_opt(b3,zetas_769_256_opt);poly_barrettq769_256_opt(b3);
  /* Output of ntt128 in [0,q) */

  /* Start computing the sums needed for Karatsuba */
  poly_add_256(a01,a0,a1);
  poly_add_256(a23,a2,a3);

  poly_add_256(a02,a0,a2);
  poly_add_256(a13,a1,a3);
  poly_add_256(a0123,a02,a13);

  poly_add_256(b01,b0,b1);
  poly_add_256(b23,b2,b3);

  poly_add_256(b02,b0,b2);
  poly_add_256(b13,b1,b3);
  poly_add_256(b0123,b02,b13);
  /* Output of the sums in [0,2q) or [0,4q)*/

  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  poly_mul_modq769_256_opt(a0,b0,ntt_Y_769);
  poly_mul_modq769_256_opt(a1,b1,ntt_Y_769);
  poly_mul_modq769_256_opt(a01,b01,ntt_Y_769);

  poly_mul_modq769_256_opt(a2,b2,ntt_Y_769);
  poly_mul_modq769_256_opt(a3,b3,ntt_Y_769);
  poly_mul_modq769_256_opt(a23,b23,ntt_Y_769);

  poly_mul_modq769_256_opt(a02,b02,ntt_Y_769);
  poly_mul_modq769_256_opt(a13,b13,ntt_Y_769);
  poly_mul_modq769_256_opt(a0123,b0123,ntt_Y_769);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* a02 <- a02 - a0 - a2 ==> a02 in ]-3q,3q[ */
  poly_sub_256_short(a02,a0); poly_sub_256_short(a02,a2);
  /* a13 <- a13 - a1 - a3 ==> a13 in ]-3q, 3q[ */
  poly_sub_256_short(a13,a1); poly_sub_256_short(a13,a3);
  /* a0123 <- a0123 - a01 - a23 ==> a0123 in ]-3q, 3q[ */
  poly_sub_256_short(a0123,a01); poly_sub_256_short(a0123,a23);
  /* a0 <- a0 + a2*ntt_Y ==> a0 in ]-2q,2q[  */
  poly_shift_2_modq769_256_opt(a2,ntt_Y_769); poly_add_256_short(a0,a2);
  /* a1 <- a1 + a3*ntt_Y ==> a1 in ]-2q,2q[ */
  poly_shift_2_modq769_256_opt(a3,ntt_Y_769); poly_add_256_short(a1,a3);
  /* a01 <- a01 + a23*ntt_Y ==> a01 in ]-2q,2q[ */
  poly_shift_2_modq769_256_opt(a23,ntt_Y_769); poly_add_256_short(a01,a23);

  /* Reconstruct second level of Karatsuba */
  /* a01 <- a01 - a0 - a1 ==> in ]-6q, 6q[ */
  poly_sub_256_short(a01,a0); poly_sub_256_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 ==> in ]-9q, 9q[ */
  poly_sub_256_short(a0123,a02); poly_sub_256_short(a0123,a13);
  /* a0 <- a0 + a13*ntt_Y ==> in ]-5q,5q[ */
  poly_shift_2_modq769_256_opt(a13,ntt_Y_769);poly_add_256_short(a0,a13);
  /* a1 <- a1 + a02 ==> in ]-5q,5q[ */
  poly_add_256_short(a1,a02);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq769_256_opt(a0);
  poly_barrettq769_256_opt(a1);
  poly_barrettq769_256_opt(a01);
  poly_barrettq769_256_opt(a0123);

  invntt_769_256_opt(a0,inv_zetas_769_256_opt);
  invntt_769_256_opt(a1,inv_zetas_769_256_opt);
  invntt_769_256_opt(a01,inv_zetas_769_256_opt);
  invntt_769_256_opt(a0123,inv_zetas_769_256_opt);

  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = a0[i];
    c[4*i+1] = a01[i];
    c[4*i+2] = a1[i];
    c[4*i+3] = a0123[i];
  }
}

void poly_product_1024_K3_P0_769_opt(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N>>3] __attribute__((aligned(32))), a1[N>>3] __attribute__((aligned(32))), a2[N>>3] __attribute__((aligned(32))), a3[N>>3] __attribute__((aligned(32))), a4[N>>3] __attribute__((aligned(32))), a5[N>>3] __attribute__((aligned(32))), a6[N>>3] __attribute__((aligned(32))), a7[N>>3] __attribute__((aligned(32))), a04[N>>3] __attribute__((aligned(32))), a26[N>>3] __attribute__((aligned(32))), a02[N>>3] __attribute__((aligned(32))), a46[N>>3] __attribute__((aligned(32))), a0246[N>>3] __attribute__((aligned(32))), a15[N>>3] __attribute__((aligned(32))), a37[N>>3] __attribute__((aligned(32))), a13[N>>3] __attribute__((aligned(32))), a57[N>>3] __attribute__((aligned(32))), a1357[N>>3] __attribute__((aligned(32))), a01[N>>3] __attribute__((aligned(32))), a45[N>>3] __attribute__((aligned(32))), a0145[N>>3] __attribute__((aligned(32))), a23[N>>3] __attribute__((aligned(32))), a67[N>>3] __attribute__((aligned(32))), a2367[N>>3] __attribute__((aligned(32))), a0123[N>>3] __attribute__((aligned(32))), a4567[N>>3] __attribute__((aligned(32))), a01234567[N>>3] __attribute__((aligned(32)));

  int16_t b0[N>>3] __attribute__((aligned(32))), b1[N>>3] __attribute__((aligned(32))), b2[N>>3] __attribute__((aligned(32))), b3[N>>3] __attribute__((aligned(32))), b4[N>>3] __attribute__((aligned(32))), b5[N>>3] __attribute__((aligned(32))), b6[N>>3] __attribute__((aligned(32))), b7[N>>3] __attribute__((aligned(32))), b04[N>>3] __attribute__((aligned(32))), b26[N>>3] __attribute__((aligned(32))), b02[N>>3] __attribute__((aligned(32))), b46[N>>3] __attribute__((aligned(32))), b0246[N>>3] __attribute__((aligned(32))), b15[N>>3] __attribute__((aligned(32))), b37[N>>3] __attribute__((aligned(32))), b13[N>>3] __attribute__((aligned(32))), b57[N>>3] __attribute__((aligned(32))), b1357[N>>3] __attribute__((aligned(32))), b01[N>>3] __attribute__((aligned(32))), b45[N>>3] __attribute__((aligned(32))), b0145[N>>3] __attribute__((aligned(32))), b23[N>>3] __attribute__((aligned(32))), b67[N>>3] __attribute__((aligned(32))), b2367[N>>3] __attribute__((aligned(32))), b0123[N>>3] __attribute__((aligned(32))), b4567[N>>3] __attribute__((aligned(32))), b01234567[N>>3] __attribute__((aligned(32)));

  for(unsigned int i = 0 ; i < N/8 ; i++){
    a0[i] = a[8*i]; a1[i] = a[8*i+1]; a2[i] = a[8*i+2]; a3[i] = a[8*i+3];
    a4[i] = a[8*i+4]; a5[i] = a[8*i+5]; a6[i] = a[8*i+6]; a7[i] = a[8*i+7];

    b0[i] = b[8*i]; b1[i] = b[8*i+1]; b2[i] = b[8*i+2]; b3[i] = b[8*i+3];
    b4[i] = b[8*i+4]; b5[i] = b[8*i+5]; b6[i] = b[8*i+6]; b7[i] = b[8*i+7];
  }

  ntt_769_128_opt(a0,zetas_769_opt);poly_barrettq769_128_opt(a0);
  ntt_769_128_opt(a1,zetas_769_opt);poly_barrettq769_128_opt(a1);
  ntt_769_128_opt(a2,zetas_769_opt);poly_barrettq769_128_opt(a2);
  ntt_769_128_opt(a3,zetas_769_opt);poly_barrettq769_128_opt(a3);
  ntt_769_128_opt(a4,zetas_769_opt);poly_barrettq769_128_opt(a4);
  ntt_769_128_opt(a5,zetas_769_opt);poly_barrettq769_128_opt(a5);
  ntt_769_128_opt(a6,zetas_769_opt);poly_barrettq769_128_opt(a6);
  ntt_769_128_opt(a7,zetas_769_opt);poly_barrettq769_128_opt(a7);
  ntt_769_128_opt(b0,zetas_769_opt);poly_barrettq769_128_opt(b0);
  ntt_769_128_opt(b1,zetas_769_opt);poly_barrettq769_128_opt(b1);
  ntt_769_128_opt(b2,zetas_769_opt);poly_barrettq769_128_opt(b2);
  ntt_769_128_opt(b3,zetas_769_opt);poly_barrettq769_128_opt(b3);
  ntt_769_128_opt(b4,zetas_769_opt);poly_barrettq769_128_opt(b4);
  ntt_769_128_opt(b5,zetas_769_opt);poly_barrettq769_128_opt(b5);
  ntt_769_128_opt(b6,zetas_769_opt);poly_barrettq769_128_opt(b6);
  ntt_769_128_opt(b7,zetas_769_opt);poly_barrettq769_128_opt(b7);
  /* Output of nttq769 in ]-q,q[ */

  /* Compute the different sums and reduce them for the Montgomery multiplications to come */
  poly_add_128(a04,a0,a4); poly_add_128(a26,a2,a6);
  poly_add_128(a02,a0,a2); poly_add_128(a46,a4,a6);

  poly_add_128(a15,a1,a5); poly_add_128(a37,a3,a7);
  poly_add_128(a13,a1,a3); poly_add_128(a57,a5,a7);

  poly_add_128(a01,a0,a1); poly_add_128(a45,a4,a5);
  poly_add_128(a23,a2,a3); poly_add_128(a67,a6,a7);

  poly_add_128(a0246,a02,a46); poly_add_128(a1357,a13,a57);
  poly_add_128(a0145,a01,a45); poly_add_128(a2367,a23,a67);
  poly_add_128(a0123,a01,a23); poly_add_128(a4567,a45,a67);

  poly_add_128(a01234567,a0123,a4567);

  poly_add_128(b04,b0,b4); poly_add_128(b26,b2,b6);
  poly_add_128(b02,b0,b2); poly_add_128(b46,b4,b6);

  poly_add_128(b15,b1,b5); poly_add_128(b37,b3,b7);
  poly_add_128(b13,b1,b3); poly_add_128(b57,b5,b7);

  poly_add_128(b01,b0,b1); poly_add_128(b45,b4,b5);
  poly_add_128(b23,b2,b3); poly_add_128(b67,b6,b7);

  poly_add_128(b0246,b02,b46); poly_add_128(b1357,b13,b57);
  poly_add_128(b0145,b01,b45); poly_add_128(b2367,b23,b67);
  poly_add_128(b0123,b01,b23); poly_add_128(b4567,b45,b67);

  poly_add_128(b01234567,b0123,b4567);
  /* Output in [0,2q), [0,4q) or  [0,8q) one [0,8q) to ]-4q,4q[ otherwise the product won't fit in ]-q*2^15,q*2^15[*/

  /* Compute the products with Montgomery (Montgomery factor will be removed with invntt)*/
  pointwise_mul_q769_128_opt_short(a0,b0); pointwise_mul_q769_128_opt_short(a1,b1);
  pointwise_mul_q769_128_opt_short(a2,b2); pointwise_mul_q769_128_opt_short(a3,b3);
  pointwise_mul_q769_128_opt_short(a4,b4); pointwise_mul_q769_128_opt_short(a5,b5);
  pointwise_mul_q769_128_opt_short(a6,b6); pointwise_mul_q769_128_opt_short(a7,b7);

  pointwise_mul_q769_128_opt_short(a04,b04); pointwise_mul_q769_128_opt_short(a26,b26);
  pointwise_mul_q769_128_opt_short(a02,b02); pointwise_mul_q769_128_opt_short(a46,b46);
  pointwise_mul_q769_128_opt_short(a0246,b0246);

  pointwise_mul_q769_128_opt_short(a15,b15); pointwise_mul_q769_128_opt_short(a37,b37);
  pointwise_mul_q769_128_opt_short(a13,b13); pointwise_mul_q769_128_opt_short(a57,b57);
  pointwise_mul_q769_128_opt_short(a1357,b1357);

  pointwise_mul_q769_128_opt_short(a01,b01); pointwise_mul_q769_128_opt_short(a45,b45);
  pointwise_mul_q769_128_opt_short(a0145,b0145);

  pointwise_mul_q769_128_opt_short(a23,b23); pointwise_mul_q769_128_opt_short(a67,b67);
  pointwise_mul_q769_128_opt_short(a2367,b2367);

  pointwise_mul_q769_128_opt_short(a0123,b0123);pointwise_mul_q769_128_opt_short(a4567,b4567);

  /* One [0,8q) to [-4q,4q[ */
  poly_sub4q_cst_q769_128(a01234567); pointwise_mul_q769_128_opt_short(a01234567,b01234567);
  /*############################## Output in ]-q,q[ = ]-769,769[ ###############################*/

  /* Start the reconstruction of Karatsuba... */
  /* Begin 1st level */

  /* a04 <- a04 - a0 - a4 in ]-3q,3q[*/
  poly_sub_128_short(a04,a0); poly_sub_128_short(a04,a4);
  /* a26 <- a26 - a2 - a6 in ]-3q,3q[ */
  poly_sub_128_short(a26,a2); poly_sub_128_short(a26,a6);
  /* a0246 <- a0246 - a02 - a46 in ]-3q,3q[ */
  poly_sub_128_short(a0246,a02); poly_sub_128_short(a0246,a46);

  /* a0 <- a0 + a4*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a4,ntt_Y_769); poly_add_128_short(a0,a4);
  /* a2 <- a2 + a6*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a6,ntt_Y_769); poly_add_128_short(a2,a6);
  /* a02 <- a02 + a46*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a46,ntt_Y_769); poly_add_128_short(a02,a46);

  /* a15 <- a15 - a1 - a5 in ]-3q,3q[*/
  poly_sub_128_short(a15,a1); poly_sub_128_short(a15,a5);
  /* a37 <- a37 - a3 - a7 in ]-3q,3q[ */
  poly_sub_128_short(a37,a3); poly_sub_128_short(a37,a7);
  /* a1357 <- a1357 - a13 - a57 in ]-3q,3q[ */
  poly_sub_128_short(a1357,a13); poly_sub_128_short(a1357,a57);

  /* a1 <- a1 + a5*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a5,ntt_Y_769); poly_add_128_short(a1,a5);
  /* a3 <- a3 + a7*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a7,ntt_Y_769); poly_add_128_short(a3,a7);
  /* a13 <- a13 + a57*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a57,ntt_Y_769); poly_add_128_short(a13,a57);

  /* a0145 <- a0145 - a01 - a45 in ]-3q,3q[*/
  poly_sub_128_short(a0145,a01); poly_sub_128_short(a0145,a45);
  /* a2367 <- a2367 - a23 - a67 in ]-3q,3q[ */
  poly_sub_128_short(a2367,a23); poly_sub_128_short(a2367,a67);
  /* a01234567 <- a01234567 - a0123 - a4567 in ]-3q,3q[ */
  poly_sub_128_short(a01234567,a0123); poly_sub_128_short(a01234567,a4567);

  /* a01 <- a01 + a45*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a45,ntt_Y_769); poly_add_128_short(a01,a45);
  /* a23 <- a23 + a67*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a67,ntt_Y_769); poly_add_128_short(a23,a67);
  /* a0123 <- a0123 + a4567*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q769_128_opt_short(a4567,ntt_Y_769); poly_add_128_short(a0123,a4567);

  /* End 1st level */
  /*##############################################################################################*/
  /* Begin 2nd level */

  /* a02 <- a02 - a0 - a2 in ]-6q,6q[*/
  poly_sub_128_short(a02,a0); poly_sub_128_short(a02,a2);
  /* a0246 <- a0246 - a04 - a26 in ]-9q,9q[ */
  poly_sub_128_short(a0246,a04); poly_sub_128_short(a0246,a26);

  /* a13 <- a13 - a1 - a3 in ]-6q,6q[*/
  poly_sub_128_short(a13,a1); poly_sub_128_short(a13,a3);
  /* a1357 <- a1357 - a15 - a37 in ]-9q,9q[ */
  poly_sub_128_short(a1357,a15); poly_sub_128_short(a1357,a37);

  /* a0123 <- a0123 - a01 - a23 in ]-6q,6q[*/
  poly_sub_128_short(a0123,a01); poly_sub_128_short(a0123,a23);
  /* a01234567 <- a01234567 - a0145 - a2367 in ]-9q,9q[ */
  poly_sub_128_short(a01234567,a0145); poly_sub_128_short(a01234567,a2367);

  /* a0 <- a0 + a26*ntt_Y in ]-3q, 3q[ */
  pointwise_mul_q769_128_opt_short(a26,ntt_Y_769); poly_add_128_short(a0,a26);
  /* a1 <- a1 + a37*ntt_Y in ]-3q, 3q[ */
  pointwise_mul_q769_128_opt_short(a37,ntt_Y_769); poly_add_128_short(a1,a37);
  /* a01 <- a01 + a2367*ntt_Y in ]-3q, 3q[ */
  pointwise_mul_q769_128_opt_short(a2367,ntt_Y_769); poly_add_128_short(a01,a2367);

  /* a04 <- a04 + a2 in ]-5q,5q[ */
  poly_add_128_short(a04,a2);
  /* a15 <- a15 + a3 in ]-5q,5q[ */
  poly_add_128_short(a15,a3);
  /* a0145 <- a0145 + a23 in ]-5q,5q[ */
  poly_add_128_short(a0145,a23);

  /* a01 <- a01 - a0 - a1 in ]-9q,9q[ */
  poly_sub_128_short(a01,a0); poly_sub_128_short(a01,a1);
  /* a0123 <- a0123 - a02 - a13 in ]-18q,18q[ */
  poly_sub_128_short(a0123,a02); poly_sub_128_short(a0123,a13);
  /* a0145 <- a0145 - a04 - a15 in ]-15q,15q[ */
  poly_sub_128_short(a0145,a04); poly_sub_128_short(a0145,a15);
  /* a01234567 <- a01234567 - a1357 - a0246 in ]-27q,27q[ */
  poly_sub_128_short(a01234567,a1357); poly_sub_128_short(a01234567,a0246);

  /* End 2nd level */
  /*##############################################################################################*/
  /* Begin 3rd level */

  /* a02 <- a02 + a1 in ]-9q,9q[ */
  poly_add_128_short(a02,a1);
  /* a0246 <- a0246 + a15 in ]-14q,14q[ */
  poly_add_128_short(a0246,a15);
  /* a04 <- a04 + a13 in ]-11q,11q[ */
  poly_add_128_short(a04,a13);
  /* a0 <- a0 + a1357*ntt_Y in ]-4q,4q[ */
  pointwise_mul_q769_128_opt_short(a1357,ntt_Y_769); poly_add_128_short(a0,a1357);

  /* End 3rd and last level */
  /*##############################################################################################*/

  /* Reduce everyone before the inverse NTT ==> coeffs in ]-q,q[ */
  poly_barrettq769_128_opt(a0); poly_barrettq769_128_opt(a01);
  poly_barrettq769_128_opt(a02); poly_barrettq769_128_opt(a0123);
  poly_barrettq769_128_opt(a04); poly_barrettq769_128_opt(a0145);
  poly_barrettq769_128_opt(a0246); poly_barrettq769_128_opt(a01234567);

  /* invntt_769_128_opt(a0,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a01,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a02,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a0123,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a04,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a0145,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a0246,inv_zetas_769_opt); */
  /* invntt_769_128_opt(a01234567,inv_zetas_769_opt); */

  for(unsigned int i = 0 ; i < N/8 ; i++){
    c[8*i] = a0[i]; c[8*i+1] = a01[i];
    c[8*i+2] = a02[i]; c[8*i+3] = a0123[i];
    c[8*i+4] = a04[i]; c[8*i+5] = a0145[i];
    c[8*i+6] = a0246[i]; c[8*i+7] = a01234567[i];
  }

  invntt_769_1024_opt(c,inv_zetas_769_1024_opt);
}

#endif
