.include "asm_macros.h"

.global invntt_769_512_opt
invntt_769_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ769INV(%rip),%ymm0
vmovdqa		_16xQ769(%rip),%ymm1
vmovdqa		_16xv_769(%rip),%ymm2

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 1
#
# ---------------------------------------------------------------------------
#level 0

#load block 1 and 2
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder
# REORDER A
vmovdqa		_lowdword(%rip),%ymm3
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
# REORDER B (same pattern as A)
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

/*
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		(%rsi),%ymm7
vmovdqa		32(%rsi),%ymm9
vmovdqa		64(%rsi),%ymm11
vmovdqa		96(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------
*/

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		(%rsi),%ymm7
vmovdqa		32(%rsi),%ymm9
vmovdqa		64(%rsi),%ymm11
vmovdqa		96(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 1
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		512(%rsi),%ymm6
vmovdqa		544(%rsi),%ymm8
vmovdqa		576(%rsi),%ymm10
vmovdqa		608(%rsi),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1024(%rsi),%ymm12
vmovdqa		1056(%rsi),%ymm13
vmovdqa		1088(%rsi),%ymm14
vmovdqa		1120(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 3
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1536(%rsi),%ymm14
vmovdqa		1568(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

#level 4
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9


#zetas
vmovdqa		1792(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,128(%rdi)
vmovdqa		%ymm7,160(%rdi)
vmovdqa		%ymm8,192(%rdi)
vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE 1
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 2
#
# ---------------------------------------------------------------------------
#level 0
#load block 3 and 4
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder
vmovdqa		_lowdword(%rip),%ymm3
# REORDER A
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
# REORDER B (same pattern as A)
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

/*
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		128(%rsi),%ymm7
vmovdqa		160(%rsi),%ymm9
vmovdqa		192(%rsi),%ymm11
vmovdqa		224(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------
*/

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		128(%rsi),%ymm7
vmovdqa		160(%rsi),%ymm9
vmovdqa		192(%rsi),%ymm11
vmovdqa		224(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 1
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		640(%rsi),%ymm6
vmovdqa		672(%rsi),%ymm8
vmovdqa		704(%rsi),%ymm10
vmovdqa		736(%rsi),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1152(%rsi),%ymm12
vmovdqa		1184(%rsi),%ymm13
vmovdqa		1216(%rsi),%ymm14
vmovdqa		1248(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

#level 3
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1600(%rsi),%ymm14
vmovdqa		1632(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

#level 4
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9


#zetas
vmovdqa		1824(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
# vmovdqa		%ymm6,128(%rdi)
# vmovdqa		%ymm7,160(%rdi)
# vmovdqa		%ymm8,192(%rdi)
# vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE 2
#
# ---------------------------------------------------------------------------

# level 5 for TREE 1 and 2 can be done now

# shift to get the part (20-23), (28-31) is already loaded in ymm6,7,8,9
sub		$128,%rdi

#level 5

#load block 2, block 4 already loaded
vmovdqa		(%rdi),%ymm10
vmovdqa		32(%rdi),%ymm3
vmovdqa		64(%rdi),%ymm4
vmovdqa		96(%rdi),%ymm5

#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1920(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,256(%rdi)
vmovdqa		%ymm7,288(%rdi)
vmovdqa		%ymm8,320(%rdi)
vmovdqa		%ymm9,352(%rdi)

sub		$128,%rdi

#load block 1 and 3
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

gen_barrett_32_coeffs 4,8 2,1,8

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3

vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7

vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,256(%rdi)
vmovdqa		%ymm9,288(%rdi)
vmovdqa		%ymm10,320(%rdi)
vmovdqa		%ymm11,352(%rdi)

# shift to next trees 3,4
add		$512,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 3
#
# ---------------------------------------------------------------------------
#level 0
#load block 5 and 7
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder
vmovdqa		_lowdword(%rip),%ymm3
# REORDER A
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11

#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

/*
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		256(%rsi),%ymm7
vmovdqa		288(%rsi),%ymm9
vmovdqa		320(%rsi),%ymm11
vmovdqa		352(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------
*/

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		256(%rsi),%ymm7
vmovdqa		288(%rsi),%ymm9
vmovdqa		320(%rsi),%ymm11
vmovdqa		352(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 1
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		768(%rsi),%ymm6
vmovdqa		800(%rsi),%ymm8
vmovdqa		832(%rsi),%ymm10
vmovdqa		864(%rsi),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1280(%rsi),%ymm12
vmovdqa		1312(%rsi),%ymm13
vmovdqa		1344(%rsi),%ymm14
vmovdqa		1376(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

#level 3
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1664(%rsi),%ymm14
vmovdqa		1696(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

#level 4
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9


#zetas
vmovdqa		1856(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,128(%rdi)
vmovdqa		%ymm7,160(%rdi)
vmovdqa		%ymm8,192(%rdi)
vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE 3
#
# ---------------------------------------------------------------------------

# shift to next tree
add		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE 4
#
# ---------------------------------------------------------------------------
#level 0
#load block 6 and 8
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		128(%rdi),%ymm8
vmovdqa		160(%rdi),%ymm9
vmovdqa		192(%rdi),%ymm10
vmovdqa		224(%rdi),%ymm11

#reorder (4,5,6,7,8,9,10,11) -> same
vmovdqa		_lowdword(%rip),%ymm3
# REORDER A
vpand		%ymm3,%ymm4,%ymm12
vpand		%ymm3,%ymm5,%ymm13
vpand		%ymm3,%ymm6,%ymm14
vpand		%ymm3,%ymm7,%ymm15
vpsrld		$16,%ymm4,%ymm4
vpsrld		$16,%ymm5,%ymm5
vpsrld		$16,%ymm6,%ymm6
vpsrld		$16,%ymm7,%ymm7
vpackusdw	%ymm5,%ymm4,%ymm5
vpackusdw	%ymm13,%ymm12,%ymm4
vpackusdw	%ymm7,%ymm6,%ymm7
vpackusdw	%ymm15,%ymm14,%ymm6
vpermq		$0xd8,%ymm4,%ymm4
vpermq		$0xd8,%ymm5,%ymm5
vpermq		$0xd8,%ymm6,%ymm6
vpermq		$0xd8,%ymm7,%ymm7
# REORDER B (same pattern as A)
vpand		%ymm3,%ymm8,%ymm12
vpand		%ymm3,%ymm9,%ymm13
vpand		%ymm3,%ymm10,%ymm14
vpand		%ymm3,%ymm11,%ymm15
vpsrld		$16,%ymm8,%ymm8
vpsrld		$16,%ymm9,%ymm9
vpsrld		$16,%ymm10,%ymm10
vpsrld		$16,%ymm11,%ymm11
vpackusdw	%ymm9,%ymm8,%ymm9
vpackusdw	%ymm13,%ymm12,%ymm8
vpackusdw	%ymm11,%ymm10,%ymm11
vpackusdw	%ymm15,%ymm14,%ymm10
vpermq		$0xd8,%ymm8,%ymm8
vpermq		$0xd8,%ymm9,%ymm9
vpermq		$0xd8,%ymm10,%ymm10
vpermq		$0xd8,%ymm11,%ymm11


#shuffle
vmovdqa		_vpshufb_idx(%rip),%ymm3
vpshufb		%ymm3,%ymm4,%ymm12
vpshufb		%ymm3,%ymm5,%ymm13
vpshufb		%ymm3,%ymm6,%ymm14
vpshufb		%ymm3,%ymm7,%ymm15
vpblendw	$0x55,%ymm4,%ymm13,%ymm4
vpblendw	$0xAA,%ymm5,%ymm12,%ymm5
vpblendw	$0x55,%ymm6,%ymm15,%ymm6
vpblendw	$0xAA,%ymm7,%ymm14,%ymm7
vpshufb		%ymm3,%ymm8,%ymm12
vpshufb		%ymm3,%ymm9,%ymm13
vpshufb		%ymm3,%ymm10,%ymm14
vpshufb		%ymm3,%ymm11,%ymm15
vpblendw	$0x55,%ymm8,%ymm13,%ymm8
vpblendw	$0xAA,%ymm9,%ymm12,%ymm9
vpblendw	$0x55,%ymm10,%ymm15,%ymm10
vpblendw	$0xAA,%ymm11,%ymm14,%ymm11

/*
#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		384(%rsi),%ymm7
vmovdqa		416(%rsi),%ymm9
vmovdqa		448(%rsi),%ymm11
vmovdqa		480(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------
*/

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm4,%ymm12
vpshufd		$0xB1,%ymm5,%ymm13
vpshufd		$0xB1,%ymm6,%ymm14
vpshufd		$0xB1,%ymm7,%ymm15
vpblendd	$0x55,%ymm4,%ymm13,%ymm4
vpblendd	$0xAA,%ymm5,%ymm12,%ymm5
vpblendd	$0x55,%ymm6,%ymm15,%ymm6
vpblendd	$0xAA,%ymm7,%ymm14,%ymm7
vpshufd		$0xB1,%ymm8,%ymm12
vpshufd		$0xB1,%ymm9,%ymm13
vpshufd		$0xB1,%ymm10,%ymm14
vpshufd		$0xB1,%ymm11,%ymm15
vpblendd	$0x55,%ymm8,%ymm13,%ymm8
vpblendd	$0xAA,%ymm9,%ymm12,%ymm9
vpblendd	$0x55,%ymm10,%ymm15,%ymm10
vpblendd	$0xAA,%ymm11,%ymm14,%ymm11

#update
vpsubw		%ymm5,%ymm4,%ymm12
vpsubw		%ymm7,%ymm6,%ymm13
vpsubw		%ymm9,%ymm8,%ymm14
vpsubw		%ymm11,%ymm10,%ymm15
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpaddw		%ymm10,%ymm11,%ymm10

#zetas
vmovdqa		384(%rsi),%ymm7
vmovdqa		416(%rsi),%ymm9
vmovdqa		448(%rsi),%ymm11
vmovdqa		480(%rsi),%ymm3

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm7,%ymm12,%ymm5
vpmulhw		%ymm7,%ymm12,%ymm12
vpmullw		%ymm9,%ymm13,%ymm7
vpmulhw		%ymm9,%ymm13,%ymm13
vpmullw		%ymm11,%ymm14,%ymm9
vpmulhw		%ymm11,%ymm14,%ymm14
vpmullw		%ymm3,%ymm15,%ymm11
vpmulhw		%ymm3,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm5,%ymm5
vpmullw		%ymm0,%ymm7,%ymm7
vpmullw		%ymm0,%ymm9,%ymm9
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm5,%ymm5
vpmulhw		%ymm1,%ymm7,%ymm7
vpmulhw		%ymm1,%ymm9,%ymm9
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm5,%ymm12,%ymm5
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm9,%ymm14,%ymm9
vpsubw		%ymm11,%ymm15,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 1
#shuffle

# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm5,%ymm4,%ymm3
vshufpd		$0x0F,%ymm5,%ymm4,%ymm4
vshufpd		$0x00,%ymm7,%ymm6,%ymm5
vshufpd		$0x0F,%ymm7,%ymm6,%ymm6
vshufpd		$0x00,%ymm9,%ymm8,%ymm7
vshufpd		$0x0F,%ymm9,%ymm8,%ymm8
vshufpd		$0x00,%ymm11,%ymm10,%ymm9
vshufpd		$0x0F,%ymm11,%ymm10,%ymm10

#update
vpsubw		%ymm4,%ymm3,%ymm12
vpsubw		%ymm6,%ymm5,%ymm13
vpsubw		%ymm8,%ymm7,%ymm14
vpsubw		%ymm10,%ymm9,%ymm15
vpaddw		%ymm3,%ymm4,%ymm3
vpaddw		%ymm5,%ymm6,%ymm5
vpaddw		%ymm7,%ymm8,%ymm7
vpaddw		%ymm9,%ymm10,%ymm9

#zetas
vmovdqa		896(%rsi),%ymm6
vmovdqa		928(%rsi),%ymm8
vmovdqa		960(%rsi),%ymm10
vmovdqa		992(%rsi),%ymm11

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm6,%ymm12,%ymm4
vpmulhw		%ymm6,%ymm12,%ymm12
vpmullw		%ymm8,%ymm13,%ymm6
vpmulhw		%ymm8,%ymm13,%ymm13
vpmullw		%ymm10,%ymm14,%ymm8
vpmulhw		%ymm10,%ymm14,%ymm14
vpmullw		%ymm11,%ymm15,%ymm10
vpmulhw		%ymm11,%ymm15,%ymm15

#reduce
vpmullw		%ymm0,%ymm4,%ymm4
vpmullw		%ymm0,%ymm6,%ymm6
vpmullw		%ymm0,%ymm8,%ymm8
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm4,%ymm4
vpmulhw		%ymm1,%ymm6,%ymm6
vpmulhw		%ymm1,%ymm8,%ymm8
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm6,%ymm13,%ymm6
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm10,%ymm15,%ymm10

# ----------- SIGNED MONTGOMERY ENDS -----------

#level 2
#shuffle

# ----------- SWAP UPPER 8 AND LOW 8 -----------

vperm2i128	$0x02,%ymm3,%ymm4,%ymm11
vperm2i128	$0x13,%ymm3,%ymm4,%ymm3
vperm2i128	$0x02,%ymm5,%ymm6,%ymm4
vperm2i128	$0x13,%ymm5,%ymm6,%ymm5
vperm2i128	$0x02,%ymm7,%ymm8,%ymm6
vperm2i128	$0x13,%ymm7,%ymm8,%ymm7
vperm2i128	$0x02,%ymm9,%ymm10,%ymm8
vperm2i128	$0x13,%ymm9,%ymm10,%ymm9

#update
vmovdqa		%ymm11,%ymm12
vmovdqa		%ymm4,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm8,%ymm15
vpaddw		%ymm11,%ymm3,%ymm10
vpaddw		%ymm4,%ymm5,%ymm4
vpaddw		%ymm6,%ymm7,%ymm6
vpaddw		%ymm8,%ymm9,%ymm8
vpsubw		%ymm3,%ymm12,%ymm3
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm7,%ymm14,%ymm7
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1408(%rsi),%ymm12
vmovdqa		1440(%rsi),%ymm13
vmovdqa		1472(%rsi),%ymm14
vmovdqa		1504(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm12,%ymm3,%ymm11
vpmulhw		%ymm12,%ymm3,%ymm3
vpmullw		%ymm13,%ymm5,%ymm12
vpmulhw		%ymm13,%ymm5,%ymm5
vpmullw		%ymm14,%ymm7,%ymm13
vpmulhw		%ymm14,%ymm7,%ymm7
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm3,%ymm3
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm7,%ymm7
vpsubw		%ymm14,%ymm9,%ymm9

#level 3
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm15
vpaddw		%ymm10,%ymm4,%ymm10
vpaddw		%ymm3,%ymm5,%ymm3
vpaddw		%ymm6,%ymm8,%ymm6
vpaddw		%ymm7,%ymm9,%ymm7
vpsubw		%ymm4,%ymm12,%ymm4
vpsubw		%ymm5,%ymm13,%ymm5
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1728(%rsi),%ymm14
vmovdqa		1760(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS on w -----------

#mul
vpmullw		%ymm14,%ymm4,%ymm11
vpmullw		%ymm14,%ymm5,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmulhw		%ymm14,%ymm4,%ymm4
vpmulhw		%ymm14,%ymm5,%ymm5
vpmulhw		%ymm15,%ymm8,%ymm8

vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm4,%ymm4
vpsubw		%ymm12,%ymm5,%ymm5
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS on w -----------

#level 4
#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9


#zetas
vmovdqa		1888(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
# vmovdqa		%ymm6,128(%rdi)
# vmovdqa		%ymm7,160(%rdi)
# vmovdqa		%ymm8,192(%rdi)
# vmovdqa		%ymm9,224(%rdi)

# ---------------------------------------------------------------------------
#
#                              END TREE 4
#
# ---------------------------------------------------------------------------

# level 5 for TREE 3,4 can now be done

# shift to get the part (20-23), (28-31) is already loaded in ymm6,7,8,9
sub		$128,%rdi

#level 5

#load 6 (20-23), 8 (28-31) already loaded
vmovdqa		(%rdi),%ymm10
vmovdqa		32(%rdi),%ymm3
vmovdqa		64(%rdi),%ymm4
vmovdqa		96(%rdi),%ymm5

#update
vmovdqa		%ymm10,%ymm12
vmovdqa		%ymm3,%ymm13
vmovdqa		%ymm4,%ymm14
vmovdqa		%ymm5,%ymm15
vpaddw		%ymm10,%ymm6,%ymm10
vpaddw		%ymm3,%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpsubw		%ymm6,%ymm12,%ymm6
vpsubw		%ymm7,%ymm13,%ymm7
vpsubw		%ymm8,%ymm14,%ymm8
vpsubw		%ymm9,%ymm15,%ymm9

#zetas
vmovdqa		1952(%rsi),%ymm15

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm6,%ymm11
vpmullw		%ymm15,%ymm7,%ymm12
vpmullw		%ymm15,%ymm8,%ymm13
vpmullw		%ymm15,%ymm9,%ymm14
vpmulhw		%ymm15,%ymm6,%ymm6
vpmulhw		%ymm15,%ymm7,%ymm7
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9

#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm11,%ymm6,%ymm6
vpsubw		%ymm12,%ymm7,%ymm7
vpsubw		%ymm13,%ymm8,%ymm8
vpsubw		%ymm14,%ymm9,%ymm9

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm10,(%rdi)
vmovdqa		%ymm3,32(%rdi)
vmovdqa		%ymm4,64(%rdi)
vmovdqa		%ymm5,96(%rdi)
vmovdqa		%ymm6,256(%rdi)
vmovdqa		%ymm7,288(%rdi)
vmovdqa		%ymm8,320(%rdi)
vmovdqa		%ymm9,352(%rdi)

sub		$128,%rdi

#load block 5, 7
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		256(%rdi),%ymm8
vmovdqa		288(%rdi),%ymm9
vmovdqa		320(%rdi),%ymm10
vmovdqa		352(%rdi),%ymm11

gen_barrett_32_coeffs 4,8 2,1,8

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3

vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7

vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
# vmovdqa		%ymm8,256(%rdi)
# vmovdqa		%ymm9,288(%rdi)
# vmovdqa		%ymm10,320(%rdi)
# vmovdqa		%ymm11,352(%rdi)

sub		$256,%rdi

#level 6

#zetas
vmovdqa		1984(%rsi),%ymm15
vmovdqa		_F769(%rip),%ymm2

#load 3, 7 already loaded
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
# vmovdqa		512(%rdi),%ymm8
# vmovdqa		544(%rdi),%ymm9
# vmovdqa		576(%rdi),%ymm10
# vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add		$128,%rdi

#load 4, 8
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

sub		$256,%rdi

#load 2, 6
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

sub		$128,%rdi

#load 1, 5
vmovdqa		(%rdi),%ymm4
vmovdqa		32(%rdi),%ymm5
vmovdqa		64(%rdi),%ymm6
vmovdqa		96(%rdi),%ymm7
vmovdqa		512(%rdi),%ymm8
vmovdqa		544(%rdi),%ymm9
vmovdqa		576(%rdi),%ymm10
vmovdqa		608(%rdi),%ymm11

#update
vmovdqa		%ymm4,%ymm12
vmovdqa		%ymm5,%ymm13
vmovdqa		%ymm6,%ymm14
vmovdqa		%ymm7,%ymm3
vpaddw		%ymm4,%ymm8,%ymm4
vpaddw		%ymm5,%ymm9,%ymm5
vpaddw		%ymm6,%ymm10,%ymm6
vpaddw		%ymm7,%ymm11,%ymm7
vpsubw		%ymm8,%ymm12,%ymm8
vpsubw		%ymm9,%ymm13,%ymm9
vpsubw		%ymm10,%ymm14,%ymm10
vpsubw		%ymm11,%ymm3,%ymm11

# ----------- SIGNED MONTGOMERY STARTS on low part -----------

#mul
vpmullw		%ymm2,%ymm4,%ymm12
vpmullw		%ymm2,%ymm5,%ymm13
vpmullw		%ymm2,%ymm6,%ymm14
vpmullw		%ymm2,%ymm7,%ymm3
vpmulhw		%ymm2,%ymm4,%ymm4
vpmulhw		%ymm2,%ymm5,%ymm5
vpmulhw		%ymm2,%ymm6,%ymm6
vpmulhw		%ymm2,%ymm7,%ymm7

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm4,%ymm4
vpsubw		%ymm13,%ymm5,%ymm5
vpsubw		%ymm14,%ymm6,%ymm6
vpsubw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY ENDS on low part-----------

#correction +q if neg ou rien
vpsraw		$15,%ymm4,%ymm12
vpsraw		$15,%ymm5,%ymm13
vpsraw		$15,%ymm6,%ymm14
vpsraw		$15,%ymm7,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm4,%ymm4
vpaddw		%ymm13,%ymm5,%ymm5
vpaddw		%ymm14,%ymm6,%ymm6
vpaddw		%ymm3,%ymm7,%ymm7

# ----------- SIGNED MONTGOMERY STARTS -----------

#mul
vpmullw		%ymm15,%ymm8,%ymm12
vpmullw		%ymm15,%ymm9,%ymm13
vpmullw		%ymm15,%ymm10,%ymm14
vpmullw		%ymm15,%ymm11,%ymm3
vpmulhw		%ymm15,%ymm8,%ymm8
vpmulhw		%ymm15,%ymm9,%ymm9
vpmulhw		%ymm15,%ymm10,%ymm10
vpmulhw		%ymm15,%ymm11,%ymm11

#reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm13,%ymm13
vpmullw		%ymm0,%ymm14,%ymm14
vpmullw		%ymm0,%ymm3,%ymm3
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm13,%ymm13
vpmulhw		%ymm1,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm3,%ymm3
vpsubw		%ymm12,%ymm8,%ymm8
vpsubw		%ymm13,%ymm9,%ymm9
vpsubw		%ymm14,%ymm10,%ymm10
vpsubw		%ymm3,%ymm11,%ymm11

# ----------- SIGNED MONTGOMERY ENDS -----------

#correction +q if neg ou rien
vpsraw		$15,%ymm8,%ymm12
vpsraw		$15,%ymm9,%ymm13
vpsraw		$15,%ymm10,%ymm14
vpsraw		$15,%ymm11,%ymm3
vpand		%ymm1,%ymm12,%ymm12
vpand		%ymm1,%ymm13,%ymm13
vpand		%ymm1,%ymm14,%ymm14
vpand		%ymm1,%ymm3,%ymm3
vpaddw		%ymm12,%ymm8,%ymm8
vpaddw		%ymm13,%ymm9,%ymm9
vpaddw		%ymm14,%ymm10,%ymm10
vpaddw		%ymm3,%ymm11,%ymm11

#store
vmovdqa		%ymm4,(%rdi)
vmovdqa		%ymm5,32(%rdi)
vmovdqa		%ymm6,64(%rdi)
vmovdqa		%ymm7,96(%rdi)
vmovdqa		%ymm8,512(%rdi)
vmovdqa		%ymm9,544(%rdi)
vmovdqa		%ymm10,576(%rdi)
vmovdqa		%ymm11,608(%rdi)

add 	%r11,%rsp

ret
