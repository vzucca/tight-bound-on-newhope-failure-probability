#ifndef NTT769_OPT
#define NTT769_OPT

#include <stdio.h>

/* ################ NTT ################ */
/* ntt functions nttq16__(size of input) */
#include "const_769_opt.h"

void ntt_769_128_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_769_128_opt");
void invntt_769_128_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_769_128_opt");

void ntt_769_256_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_769_256_opt");
void invntt_769_256_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_769_256_opt");

void ntt_769_512_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_769_512_opt");
void invntt_769_512_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_769_512_opt");

void ntt_769_1024_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_769_1024_opt");
void invntt_769_1024_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_769_1024_opt");

#endif
