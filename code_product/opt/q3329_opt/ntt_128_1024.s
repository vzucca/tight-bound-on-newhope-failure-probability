.include "asm_macros.h"

.macro permut_one
vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
vperm2i128	$0x13,%ymm4,%ymm5,%ymm4
vperm2i128	$0x02,%ymm6,%ymm7,%ymm5
vperm2i128	$0x13,%ymm6,%ymm7,%ymm6
vperm2i128	$0x02,%ymm8,%ymm9,%ymm7
vperm2i128	$0x13,%ymm8,%ymm9,%ymm8
vperm2i128	$0x02,%ymm10,%ymm11,%ymm9
vperm2i128	$0x13,%ymm10,%ymm11,%ymm10
.endm

.macro end_permut
#shuffle
# ----------- SWAP UPPER 4 AND LOW 4 -----------

vshufpd		$0x00,%ymm4,%ymm3,%ymm11
vshufpd		$0x0F,%ymm4,%ymm3,%ymm3
vshufpd		$0x00,%ymm6,%ymm5,%ymm4
vshufpd		$0x0F,%ymm6,%ymm5,%ymm5
vshufpd		$0x00,%ymm8,%ymm7,%ymm6
vshufpd		$0x0F,%ymm8,%ymm7,%ymm7
vshufpd		$0x00,%ymm10,%ymm9,%ymm8
vshufpd		$0x0F,%ymm10,%ymm9,%ymm9
#vmovdqa	%ymm11,%ymm10

# ----------- END SWAP -----------

#shuffle

# ----------- SWAP UPPER 2s AND LOW 2s -----------

vpshufd		$0xB1,%ymm11,%ymm12
# vpshufd		$0xB1,%ymm10,%ymm12
vpshufd		$0xB1,%ymm3,%ymm13
vpshufd		$0xB1,%ymm4,%ymm14
vpshufd		$0xB1,%ymm5,%ymm15
vpblendd	$0x55,%ymm11,%ymm13,%ymm10
# vpblendd	$0x55,%ymm10,%ymm13,%ymm10
vpblendd	$0xAA,%ymm3,%ymm12,%ymm3
vpblendd	$0x55,%ymm4,%ymm15,%ymm4
vpblendd	$0xAA,%ymm5,%ymm14,%ymm5
vpshufd		$0xB1,%ymm6,%ymm12
vpshufd		$0xB1,%ymm7,%ymm13
vpshufd		$0xB1,%ymm8,%ymm14
vpshufd		$0xB1,%ymm9,%ymm15
vpblendd	$0x55,%ymm6,%ymm13,%ymm6
vpblendd	$0xAA,%ymm7,%ymm12,%ymm7
vpblendd	$0x55,%ymm8,%ymm15,%ymm8
vpblendd	$0xAA,%ymm9,%ymm14,%ymm9

#shuffle

# ----------- SWAP within couples (units, last level) -----------

vmovdqa		_vpshufb_idx(%rip),%ymm15
vpshufb		%ymm15,%ymm10,%ymm11
vpshufb		%ymm15,%ymm3,%ymm12
vpshufb		%ymm15,%ymm4,%ymm13
vpshufb		%ymm15,%ymm5,%ymm14
vpblendw	$0x55,%ymm10,%ymm12,%ymm10
vpblendw	$0xAA,%ymm3,%ymm11,%ymm3
vpblendw	$0x55,%ymm4,%ymm14,%ymm4
vpblendw	$0xAA,%ymm5,%ymm13,%ymm5
vpshufb		%ymm15,%ymm6,%ymm11
vpshufb		%ymm15,%ymm7,%ymm12
vpshufb		%ymm15,%ymm8,%ymm13
vpshufb		%ymm15,%ymm9,%ymm14
vpblendw	$0x55,%ymm6,%ymm12,%ymm6
vpblendw	$0xAA,%ymm7,%ymm11,%ymm7
vpblendw	$0x55,%ymm8,%ymm14,%ymm8
vpblendw	$0xAA,%ymm9,%ymm13,%ymm9

#reorder
vpunpcklwd	%ymm3,%ymm10,%ymm12
vpunpckhwd	%ymm3,%ymm10,%ymm13
vpunpcklwd	%ymm5,%ymm4,%ymm14
vpunpckhwd	%ymm5,%ymm4,%ymm15
vpunpcklwd	%ymm7,%ymm6,%ymm3
vpunpckhwd	%ymm7,%ymm6,%ymm4
vpunpcklwd	%ymm9,%ymm8,%ymm5
vpunpckhwd	%ymm9,%ymm8,%ymm6
vperm2i128	$0x20,%ymm13,%ymm12,%ymm11
vperm2i128	$0x31,%ymm13,%ymm12,%ymm12
vperm2i128	$0x20,%ymm15,%ymm14,%ymm13
vperm2i128	$0x31,%ymm15,%ymm14,%ymm14
vperm2i128	$0x20,%ymm4,%ymm3,%ymm15
vperm2i128	$0x31,%ymm4,%ymm3,%ymm3
vperm2i128	$0x20,%ymm6,%ymm5,%ymm4
vperm2i128	$0x31,%ymm6,%ymm5,%ymm5
.endm

.global ntt_128_1024_opt
ntt_128_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ128INV(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1
vmovdqa		_low_mask_128(%rip),%ymm2

#level 0

#zetas
vmovdqa		(%rsi),%ymm3

#Block 1 (0-3) with block 9 (32 - 35)
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 2 with block 10
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 3 with block 11
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 4 with block 12
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 5 with block 13
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 6 with block 14
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 7 with block 15
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
add		$128,%rdi

#Block 8 with block 16
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,1024,1056,1088,1120
# store (ymm4,5,6,7 will be reused in the immediate next level)
save_64_coeffs 8,9,10,11 rdi,1024,1056,1088,1120

#tree A and B ready to be started. Starts with A
sub		$512,%rdi

################################################################################
##
##
##                           BEGIN TREE A
##
##
################################################################################

#level 1

#zetas
vmovdqa		32(%rsi),%ymm3

#Block 4 with 8 (already loaded with ymm4,5,6,7)
#load
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
load_64_coeffs 8,9,10,11
ntt_butterfly_1 8,9,10,11,4,5,6,7,3
save_128_coeffs 8,9,10,11,4,5,6,7 rdi,0,32,64,96,512,544,576,608

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
sub		$128,%rdi

#Block 3 with 7
#load

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 2 with 6
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 1 with 5
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
#store (block 1 will be reused straight away)
save_64_coeffs 8,9,10,11 rdi,512,544,576,608

#level 2 for A1 and A2

#zetas
vmovdqa		96(%rsi),%ymm3

#Block 1 (already loaded) and Block 3
#load
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
load_64_coeffs 8,9,10,11 rdi,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 2 and Block 4
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees A1,A2 are ready to go. Start with tree A2
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A2
#
# ---------------------------------------------------------------------------

#Block 3 and 4 (4 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11

#level 3

vmovdqa		256(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4

load_32_coeffs 15,3 rsi,544,576
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3
# save_128_coeffs 4,5,6,7,8,9,10,11

load_64_coeffs 13,14,15,3 rsi,1120,1152,1184,1216
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
# save_128_coeffs 4,5,6,7,8,9,10,11

#level 6
permut_one

load_64_coeffs 12,13,14,15 rsi,2144,2176,2208,2240
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE A2
#
# ---------------------------------------------------------------------------

# tree A1 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A1
#
# ---------------------------------------------------------------------------


#Block 1 and 2
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3

vmovdqa		224(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,480,512
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,992,1024,1056,1088
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 6
permut_one

load_64_coeffs 12,13,14,15 rsi,2016,2048,2080,2112
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE A1
#
# ---------------------------------------------------------------------------


add		$512,%rdi

#level 2 for A3 and A4

#zetas
vmovdqa		128(%rsi),%ymm3

#Block 5 and Block 7
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 6 and Block 8
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store (Block 8 is goind to be used immediately)
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees A3,A4 are ready to go. Start with tree A4
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A4
#
# ---------------------------------------------------------------------------

#Block 7 and 8 (4 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		320(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,672,704
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,1376,1408,1440,1472
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 6
permut_one

#zetas
load_64_coeffs 12,13,14,15 rsi,2400,2432,2464,2496
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE A4
#
# ---------------------------------------------------------------------------

# tree A3 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE A3
#
# ---------------------------------------------------------------------------


#Block 5 and 6
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		288(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,608,640
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3


#level 5
load_64_coeffs 13,14,15,3 rsi,1248,1280,1312,1344
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 6
permut_one

load_64_coeffs 12,13,14,15 rsi,2272,2304,2336,2368
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE A3
#
# ---------------------------------------------------------------------------

# moves to tree B, from block 5 to block 12
add		$896,%rdi

###############################################################################
##
##
##                           BEGIN TREE B
##
##
################################################################################

#level 1

#zetas
vmovdqa		64(%rsi),%ymm3

#Block 12 with 16

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

# shift pointer by 4 blocks (128 * 8 = 4 * 256)
sub		$128,%rdi

#Block 11 with 15

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 10 with 14

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608

sub		$128,%rdi

#Block 9 with 13

load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,512,544,576,608
#store (block 9 will be reused straight away)
save_64_coeffs 8,9,10,11 rdi,512,544,576,608

#level 2 for B1 and B2

#zetas
vmovdqa		160(%rsi),%ymm3

#Block 9 (already loaded) and Block 11
# load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
load_64_coeffs 8,9,10,11 rdi,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 10 and Block 12
#load
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#store (Block 12 is goind to be used immediately)
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees B1,B2 are ready to go. Start with tree A2
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B2
#
# ---------------------------------------------------------------------------

#Block 11 and 12 (12 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11
#level 3

#zetas
vmovdqa		384(%rsi),%ymm3

ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,800,832
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3


#level 5
load_64_coeffs 13,14,15,3 rsi,1632,1664,1696,1728
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3
# save_128_coeffs 4,5,6,7,8,9,10,11

#level 6
permut_one
load_64_coeffs 12,13,14,15 rsi,2656,2688,2720,2752
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE B2
#
# ---------------------------------------------------------------------------

# tree B1 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B1
#
# ---------------------------------------------------------------------------


#Block 9 and 10
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		352(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,736,768
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,1504,1536,1568,1600
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 6
permut_one
load_64_coeffs 12,13,14,15 rsi,2528,2560,2592,2624
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE B1
#
# ---------------------------------------------------------------------------


add		$512,%rdi

#level 2 for B3 and B4

#zetas
vmovdqa		192(%rsi),%ymm3

#Block 13 and Block 15
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352

add		$128,%rdi

#Block 15 and Block 16
load_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
ntt_butterfly_1 4,5,6,7,8,9,10,11,3
#store (Block 16 is goind to be used immediately)
# save_128_coeffs 4,5,6,7,8,9,10,11 rdi,0,32,64,96,256,288,320,352
save_64_coeffs 4,5,6,7

# Trees A3,A4 are ready to go. Start with tree A4
add		$128,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B4
#
# ---------------------------------------------------------------------------

#Block 15 and 16 (16 already loaded in ymm8,9,10,11)
load_64_coeffs 4,5,6,7
# load_128_coeffs 4,5,6,7,8,9,10,11
#level 3

#zetas
vmovdqa		448(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,928,960
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3


#level 5
load_64_coeffs 13,14,15,3 rsi,1888,1920,1952,1984
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 6
permut_one

load_64_coeffs 12,13,14,15 rsi,2912,2944,2976,3008
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE B4
#
# ---------------------------------------------------------------------------

# tree B3 now
sub		$256,%rdi

# ---------------------------------------------------------------------------
#
#                              BEGIN TREE B3
#
# ---------------------------------------------------------------------------


#Block 13 and 14
load_128_coeffs 4,5,6,7,8,9,10,11

#level 3
vmovdqa		416(%rsi),%ymm3
ntt_butterfly_1 4,5,6,7,8,9,10,11,3

#level 4
load_32_coeffs 15,3 rsi,864,896
ntt_butterfly_2 4,5,8,9,6,7,10,11,15,3

#level 5
load_64_coeffs 13,14,15,3 rsi,1760,1792,1824,1856
ntt_butterfly_4 4,6,8,10,5,7,9,11,13,14,15,3

#level 6
permut_one
load_64_coeffs 12,13,14,15 rsi,2784,2816,2848,2880
ntt_butterfly_4 3,5,7,9,4,6,8,10,12,13,14,15 0,1,11,12,13,14
end_permut
save_128_coeffs 11,12,13,14,15,3,4,5

# ---------------------------------------------------------------------------
#
#                              END TREE B3
#
# ---------------------------------------------------------------------------

add 	%r11,%rsp

ret
