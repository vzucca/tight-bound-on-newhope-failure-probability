.include "asm_macros.h"
.include "asm_shuffle.h"
.include "product_macros.h"

.global poly_mul_modq128_256_opt
poly_mul_modq128_256_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ128INV(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_1_256

add 	%r11,%rsp
ret

.global poly_mul_modq128_1024_opt
poly_mul_modq128_1024_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ128INV(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_8_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_256

add 	%r11,%rsp
ret

.global poly_mul_modq128_512_opt
poly_mul_modq128_512_opt:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xQ128INV(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1
vmovdqa		_lowdword(%rip),%ymm2

product_deg_4_256
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_256

add 	%r11,%rsp
ret
