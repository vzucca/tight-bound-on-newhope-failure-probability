#ifndef NTT128_OPT
#define NTT128_OPT

#include <stdio.h>

/* ################ NTT ################ */
/* ntt functions nttq16__(size of input) */
#include "const_128_opt.h"

void ntt_128_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_128_opt");
void invntt_128_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_128_opt");

void ntt_128_256_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_128_256_opt");
void invntt_128_256_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_128_256_opt");

void ntt_128_512_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_128_512_opt");
void invntt_128_512_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_128_512_opt");

void ntt_128_1024_opt(int16_t *inout, const uint16_t *zetas) asm("ntt_128_1024_opt");
void invntt_128_1024_opt(int16_t *inout, const uint16_t *zetas) asm("invntt_128_1024_opt");

#endif
