.include "asm_macros.h"

.macro block_128
load_128_coeffs 2,3,4,5,6,7,8,9
gen_barrett_64_coeffs 2,3,4,5 0,1,10
gen_barrett_64_coeffs 6,7,8,9 0,1,10
save_128_coeffs 2,3,4,5,6,7,8,9
.endm

.global poly_barrett_q128_1024
poly_barrett_q128_1024:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_128(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1

/*repeat 8 times*/
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128

add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128

add 	%r11,%rsp

ret

.global poly_barrett_q128_512
poly_barrett_q128_512:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_128(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1

/*repeat 4 times*/
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128
add $256,%rdi
block_128

add 	%r11,%rsp

ret

.global poly_barrett_q128_256
poly_barrett_q128_256:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_128(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1

/*repeat 2 times*/
block_128
add $256,%rdi
block_128

add 	%r11,%rsp

ret

.global poly_barrett_q128_128
poly_barrett_q128_128:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_16xv_128(%rip),%ymm0
vmovdqa		_16xQ128(%rip),%ymm1

block_128

add 	%r11,%rsp

ret
