#ifndef REDUCE128_OPT
#define REDUCE128_OPT

// #include <stdio.h>
#include <stdint.h>
#include "params.h"

/* ############################################################################################## */
/* ############################### Functions for Q128 = 3329 ################################## */
/* ############################################################################################## */

/* coefficient-wise multiplication */
void pointwise_mul_q128_256_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q128_256_opt");
void pointwise_mul_q128_512_opt(int16_t *c, const int16_t *a, const int16_t* b) asm("pointwise_mul_q128_512_opt");

/* short version a <- a*b */
void pointwise_mul_q128_128_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q128_128_opt_short");
void pointwise_mul_q128_256_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q128_256_opt_short");
void pointwise_mul_q128_512_opt_short(int16_t *a, const int16_t* b) asm("pointwise_mul_q128_512_opt_short");

/* Barrett */
void poly_barrettq128_128_opt(int16_t *a) asm("poly_barrett_q128_128");
void poly_barrettq128_256_opt(int16_t *a) asm("poly_barrett_q128_256");
void poly_barrettq128_512_opt(int16_t *a) asm("poly_barrett_q128_512");
void poly_barrettq128_1024_opt(int16_t *a) asm("poly_barrett_q128_1024");

/* Mersenne */
void poly_mersq128_128_opt(int16_t* a) asm("poly_mers_q128_128");

/* classic multiplication a <- a * b */
void poly_mul_modq128_256_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq128_256_opt");
void poly_mul_4_modq128_512_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq128_512_opt");
void poly_mul_8_modq128_1024_opt(int16_t* a, const int16_t* b, const int16_t* ntt_Y) asm("poly_mul_modq128_1024_opt");


/* classic shift and multiply by ntt_Y*/
void poly_shift_2_modq128_256_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_2_modq128_256_opt");
void poly_shift_4_modq128_512_opt(int16_t* a, const int16_t* ntt_Y) asm ("poly_shift_4_modq128_512_opt");

/* sub cst */
void poly_sub2q_cst_q128_256(int16_t*a) asm ("sub_cst2q_q128_256");
void poly_sub2q_cst_q128_128(int16_t*a) asm ("sub_cst2q_q128_128");

#endif
