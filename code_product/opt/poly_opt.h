#ifndef POLY_OPT
#define POLY_OPT

#include <stdint.h>
#include "params.h"
#include "poly.h"

/* Tests for swap*/
void swap_pairs_16_32(int16_t* to_permut) asm("swap_pairs_16_32");
void split_evenodd_16_32(int16_t* to_permut) asm("split_evenodd_16_32");
void join_evenodd_16_32(int16_t* to_permut) asm("join_evenodd_16_32");

/* Arithmetic functions */
void poly_sub_64(int16_t *c, const int16_t *a, const int16_t* b) asm("poly_sub_64");
void poly_sub_128(int16_t *c, const int16_t *a, const int16_t* b) asm("poly_sub_128");
void poly_sub_256(int16_t *c, const int16_t *a, const int16_t* b) asm("poly_sub_256");

void poly_add_64(int16_t *c, const int16_t *a, const int16_t* b) asm("poly_add_64");
void poly_add_128(int16_t *c, const int16_t *a, const int16_t* b) asm("poly_add_128");
void poly_add_256(int16_t *c, const int16_t *a, const int16_t* b) asm("poly_add_256");
void poly_add_512(int16_t *c, const int16_t *a, const int16_t* b) asm("poly_add_512");

/* version a <- a + b*/
void poly_add_64_short(int16_t *a, const int16_t* b) asm("poly_add_64_short");
void poly_add_128_short(int16_t *a, const int16_t* b) asm("poly_add_128_short");
void poly_add_256_short(int16_t *a, const int16_t* b) asm("poly_add_256_short");
void poly_add_512_short(int16_t *a, const int16_t* b) asm("poly_add_512_short");

/* version a <- a - b*/
void poly_sub_64_short(int16_t *a, const int16_t* b) asm("poly_sub_64_short");
void poly_sub_128_short(int16_t *a, const int16_t* b) asm("poly_sub_128_short");
void poly_sub_256_short(int16_t *a, const int16_t* b) asm("poly_sub_256_short");
void poly_sub_512_short(int16_t *a, const int16_t* b) asm("poly_sub_512_short");

void poly_sub_cst_opt(int16_t *c, const int16_t *a, const int16_t b, unsigned int n){

  for(unsigned int i = 0 ; i < n ; i++)
    c[i] = a[i] - b;
}


void poly_subq_opt(int16_t *c, const int16_t *a, const int16_t* b, unsigned int n, unsigned int Q){

  for(unsigned int i = 0 ; i < n ; i++){
    c[i] = a[i] - b[i];
    c[i] += (c[i]>>15) & Q;
  }
}

#endif
