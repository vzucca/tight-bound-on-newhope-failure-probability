################################################################################

# macro to do a shift and multiply with ntt_Y per block of 2 on 128_coeffs
# ymm0 for Qinv, ymm1 for Q, ymm2 for lowdword
.macro shift_multiply_2_64
load_64_coeffs 3,4,5,6 rdi #load the coefficients to shift_and_multiply
load_32_coeffs 7,8 rsi #load all the necessary values of ntt_Y

# get the part to multiply
split_evenodd_16_64 3,4,5,6,3,4,5,6 2

# multiply the odd part by ntt_Y
signed_montgomery_32x32 7,8,4,6

#rejoin the parts of b
join_evenodd_16_64 3,4,5,6,3,4,5,6

#make the swap
swap_pairs_16_64 3,4,5,6,3,4,5,6

#save the results
save_64_coeffs 3,4,5,6 rdi
.endm

################################################################################

.macro shift_multiply_2_64_q32
load_64_coeffs 3,4,5,6 rdi #load the coefficients to shift_and_multiply
load_32_coeffs 7,8 rsi #load all the necessary values of ntt_Y

# get the part to multiply
split_evenodd_16_64 3,4,5,6,3,4,5,6 2

# multiply the odd part by ntt_Y
signed_montgomery_32x32 7,8,4,6

#rejoin the parts of b
join_evenodd_16_64 3,4,5,6,3,4,5,6

#make the swap
swap_pairs_16_64 3,4,5,6,3,4,5,6

#save the results
save_64_coeffs 3,4,5,6 rdi
.endm

################################################################################

.macro shift_multiply_4_64
# ymm0 for Qinv, ymm1 for Q, ymm2 for lowdword
load_64_coeffs 3,4,5,6 rdi #load the coefficients to shift_and_multiply
vmovdqa  (%rsi),%ymm7 #load all the necessary values of ntt_Y

#get all the shifts
vpsllq   $16,%ymm3,%ymm8
vpsllq   $16,%ymm4,%ymm9
vpsllq   $16,%ymm5,%ymm10
vpsllq   $16,%ymm6,%ymm11
vpsrlq   $48,%ymm3,%ymm3
vpsrlq   $48,%ymm4,%ymm4
vpsrlq   $48,%ymm5,%ymm5
vpsrlq   $48,%ymm6,%ymm6

#combine the coeffs to multiply
vpunpcklwd  %ymm4,%ymm3,%ymm12
vpunpckhwd  %ymm4,%ymm3,%ymm3
vpunpcklwd  %ymm5,%ymm6,%ymm13
vpunpckhwd  %ymm5,%ymm6,%ymm4
vpunpckldq  %ymm13,%ymm12,%ymm12
vpunpckldq  %ymm4,%ymm3,%ymm3
vpunpcklqdq %ymm3,%ymm12,%ymm3

#product
vpmullw		%ymm7,%ymm3,%ymm14
vpmulhw		%ymm7,%ymm3,%ymm15
#reduce
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm3

#put back the right values
vpblendw  $0x11,%ymm3,%ymm8,%ymm8
vpsrlq   $16,%ymm3,%ymm3
vpblendw  $0x11,%ymm3,%ymm9,%ymm9
vpsrlq   $16,%ymm3,%ymm3
vpblendw  $0x11,%ymm3,%ymm11,%ymm11
vpsrlq   $16,%ymm3,%ymm3
vpblendw  $0x11,%ymm3,%ymm10,%ymm10

#save_64_coeffs 3,4,5,6 rdi
vmovdqa     %ymm8,(%rdi)
vmovdqa     %ymm9,32(%rdi)
vmovdqa     %ymm10,64(%rdi)
vmovdqa     %ymm11,96(%rdi)

.endm

################################################################################

.macro shift_multiply_8_128
load_128_coeffs 2,3,4,5,6,7,8,9 rdi
vpslldq   $2,%ymm2,%ymm10
vpslldq   $2,%ymm3,%ymm11
vpslldq   $2,%ymm4,%ymm12
vpslldq   $2,%ymm5,%ymm13
vpsrldq   $14,%ymm2,%ymm2
vpsrldq   $14,%ymm3,%ymm3
vpsrldq   $14,%ymm4,%ymm4
vpsrldq   $14,%ymm5,%ymm5

vmovdqa     %ymm10,(%rdi)
vmovdqa     %ymm11,32(%rdi)
vmovdqa     %ymm12,64(%rdi)
vmovdqa     %ymm13,96(%rdi)

vpslldq   $2,%ymm6,%ymm10
vpslldq   $2,%ymm7,%ymm11
vpslldq   $2,%ymm8,%ymm12
vpslldq   $2,%ymm9,%ymm13
vpsrldq   $14,%ymm6,%ymm6
vpsrldq   $14,%ymm7,%ymm7
vpsrldq   $14,%ymm8,%ymm8
vpsrldq   $14,%ymm9,%ymm9

vmovdqa     %ymm10,128(%rdi)
vmovdqa     %ymm11,160(%rdi)
vmovdqa     %ymm12,192(%rdi)
vmovdqa     %ymm13,224(%rdi)

#combine the coeffs to multiply
vpunpcklwd  %ymm3,%ymm2,%ymm10
vpunpcklwd  %ymm5,%ymm4,%ymm12
vpunpcklwd  %ymm7,%ymm6,%ymm2
vpunpcklwd  %ymm9,%ymm8,%ymm4

#only take the low part from there
vpunpckldq  %ymm12,%ymm10,%ymm10
vpunpckldq  %ymm4,%ymm2,%ymm2

vpunpcklqdq %ymm2,%ymm10,%ymm2

# load the values of ntt_Y
vmovdqa  (%rsi),%ymm3

#start montgomery
#product
vpmullw		%ymm3,%ymm2,%ymm14
vpmulhw		%ymm3,%ymm2,%ymm15
#reduce
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm10

# reload the shifted values
load_128_coeffs 2,3,4,5,6,7,8,9 rdi

# reinsert the multiplied values
vpblendw  $0x01,%ymm10,%ymm2,%ymm2
vpsrldq   $2,%ymm10,%ymm10
vpblendw  $0x01,%ymm10,%ymm3,%ymm3
vpsrldq   $2,%ymm10,%ymm10
vpblendw  $0x01,%ymm10,%ymm4,%ymm4
vpsrldq   $2,%ymm10,%ymm10
vpblendw  $0x01,%ymm10,%ymm5,%ymm5
vpsrldq   $2,%ymm10,%ymm10
vpblendw  $0x01,%ymm10,%ymm6,%ymm6
vpsrldq   $2,%ymm10,%ymm10
vpblendw  $0x01,%ymm10,%ymm7,%ymm7
vpsrldq   $2,%ymm10,%ymm10
vpblendw  $0x01,%ymm10,%ymm8,%ymm8
vpsrldq   $2,%ymm10,%ymm10
vpblendw  $0x01,%ymm10,%ymm9,%ymm9

vmovdqa     %ymm2,(%rdi)
vmovdqa     %ymm3,32(%rdi)
vmovdqa     %ymm4,64(%rdi)
vmovdqa     %ymm5,96(%rdi)
vmovdqa     %ymm6,128(%rdi)
vmovdqa     %ymm7,160(%rdi)
vmovdqa     %ymm8,192(%rdi)
vmovdqa     %ymm9,224(%rdi)

.endm

################################################################################

.macro shift_multiply_16_256
load_128_coeffs 2,3,4,5,6,7,8,9 rdi

#group by 2
vpunpckhwd %ymm3,%ymm2,%ymm11
vpunpckhwd %ymm5,%ymm4,%ymm12
vpunpckhwd %ymm7,%ymm6,%ymm13
vpunpckhwd %ymm9,%ymm8,%ymm14
#group by 4
vpunpckhdq %ymm12,%ymm11,%ymm10
vpunpckhdq %ymm14,%ymm13,%ymm11
#group by 8
vpunpckhqdq %ymm11,%ymm10,%ymm10


#shift operation
vperm2i128 $0x08,%ymm2,%ymm2,%ymm11
vperm2i128 $0x08,%ymm3,%ymm3,%ymm12
vperm2i128 $0x08,%ymm4,%ymm4,%ymm13
vperm2i128 $0x08,%ymm5,%ymm5,%ymm14
vpalignr $14,%ymm11,%ymm2,%ymm2
vpalignr $14,%ymm12,%ymm3,%ymm3
vpalignr $14,%ymm13,%ymm4,%ymm4
vpalignr $14,%ymm14,%ymm5,%ymm5
vperm2i128 $0x08,%ymm6,%ymm6,%ymm11
vperm2i128 $0x08,%ymm7,%ymm7,%ymm12
vperm2i128 $0x08,%ymm8,%ymm8,%ymm13
vperm2i128 $0x08,%ymm9,%ymm9,%ymm14
vpalignr $14,%ymm11,%ymm6,%ymm6
vpalignr $14,%ymm12,%ymm7,%ymm7
vpalignr $14,%ymm13,%ymm8,%ymm8
vpalignr $14,%ymm14,%ymm9,%ymm9

save_128_coeffs 2,3,4,5,6,7,8,9 rdi

add  $256,%rdi

load_128_coeffs 2,3,4,5,6,7,8,9 rdi

#group by 2
vpunpckhwd %ymm3,%ymm2,%ymm11
vpunpckhwd %ymm5,%ymm4,%ymm12
vpunpckhwd %ymm7,%ymm6,%ymm13
vpunpckhwd %ymm9,%ymm8,%ymm14
#group by 4
vpunpckhdq %ymm12,%ymm11,%ymm12
vpunpckhdq %ymm14,%ymm13,%ymm11
#group by 8
vpunpckhqdq %ymm11,%ymm12,%ymm12

#group by 16
vperm2i128  $0x31,%ymm12,%ymm10,%ymm10

#shift operation
vperm2i128 $0x08,%ymm2,%ymm2,%ymm11
vperm2i128 $0x08,%ymm3,%ymm3,%ymm12
vperm2i128 $0x08,%ymm4,%ymm4,%ymm13
vperm2i128 $0x08,%ymm5,%ymm5,%ymm14
vpalignr $14,%ymm11,%ymm2,%ymm2
vpalignr $14,%ymm12,%ymm3,%ymm3
vpalignr $14,%ymm13,%ymm4,%ymm4
vpalignr $14,%ymm14,%ymm5,%ymm5
vperm2i128 $0x08,%ymm6,%ymm6,%ymm11
vperm2i128 $0x08,%ymm7,%ymm7,%ymm12
vperm2i128 $0x08,%ymm8,%ymm8,%ymm13
vperm2i128 $0x08,%ymm9,%ymm9,%ymm14
vpalignr $14,%ymm11,%ymm6,%ymm6
vpalignr $14,%ymm12,%ymm7,%ymm7
vpalignr $14,%ymm13,%ymm8,%ymm8
vpalignr $14,%ymm14,%ymm9,%ymm9

#start montgomery
#product
vpmulhw		%ymm15,%ymm10,%ymm12
vpmullw		%ymm15,%ymm10,%ymm11
#reduce
vpmullw		%ymm0,%ymm11,%ymm11
vpmulhw		%ymm1,%ymm11,%ymm11
vpsubw		%ymm11,%ymm12,%ymm10

#separate the two parts
vperm2i128  $0xF0,%ymm10,%ymm10,%ymm11
vperm2i128  $0xB1,%ymm10,%ymm10,%ymm10

#apply shifts and masks to insert elements
vpsrldq   $14,%ymm10,%ymm12
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm9,%ymm12,%ymm9
vpsrldq   $14,%ymm10,%ymm13
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm8,%ymm13,%ymm8
vpsrldq   $14,%ymm10,%ymm12
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm7,%ymm12,%ymm7
vpsrldq   $14,%ymm10,%ymm13
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm6,%ymm13,%ymm6
vpsrldq   $14,%ymm10,%ymm12
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm5,%ymm12,%ymm5
vpsrldq   $14,%ymm10,%ymm13
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm4,%ymm13,%ymm4
vpsrldq   $14,%ymm10,%ymm12
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm3,%ymm12,%ymm3
vpsrldq   $14,%ymm10,%ymm13
vpslldq   $2,%ymm10,%ymm10
vpor      %ymm2,%ymm13,%ymm2

save_128_coeffs 2,3,4,5,6,7,8,9 rdi

sub  $256,%rdi

load_128_coeffs 2,3,4,5,6,7,8,9 rdi

vpsrldq   $14,%ymm11,%ymm12
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm9,%ymm12,%ymm9
vpsrldq   $14,%ymm11,%ymm13
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm8,%ymm13,%ymm8
vpsrldq   $14,%ymm11,%ymm12
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm7,%ymm12,%ymm7
vpsrldq   $14,%ymm11,%ymm13
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm6,%ymm13,%ymm6
vpsrldq   $14,%ymm11,%ymm12
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm5,%ymm12,%ymm5
vpsrldq   $14,%ymm11,%ymm13
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm4,%ymm13,%ymm4
vpsrldq   $14,%ymm11,%ymm12
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm3,%ymm12,%ymm3
vpsrldq   $14,%ymm11,%ymm13
vpslldq   $2,%ymm11,%ymm11
vpor      %ymm2,%ymm13,%ymm2

save_128_coeffs 2,3,4,5,6,7,8,9 rdi

.endm

################################################################################
