################################################################################

# from vector a1,a2,a3,a4 and b1,b2,b3,b4 of 16-bit integers
# obtain a2,a1,a4,a3, and b2,b1,b4,b3
.macro swap_pairs_16_32 in1,in2,out1,out2
vpshuflw  $0xB1,%ymm\in1,%ymm\out1
vpshuflw  $0xB1,%ymm\in2,%ymm\out2
vpshufhw  $0xB1,%ymm\out1,%ymm\out1
vpshufhw  $0xB1,%ymm\out2,%ymm\out2
.endm

################################################################################

.macro swap_pairs_16_64 in1,in2,in3,in4,out1,out2,out3,out4
vpshuflw  $0xB1,%ymm\in1,%ymm\out1
vpshuflw  $0xB1,%ymm\in2,%ymm\out2
vpshuflw  $0xB1,%ymm\in3,%ymm\out3
vpshuflw  $0xB1,%ymm\in4,%ymm\out4
vpshufhw  $0xB1,%ymm\out1,%ymm\out1
vpshufhw  $0xB1,%ymm\out2,%ymm\out2
vpshufhw  $0xB1,%ymm\out3,%ymm\out3
vpshufhw  $0xB1,%ymm\out4,%ymm\out4
.endm

################################################################################

# from vectors a1,a2,a3,a4 and b1,b2,b3,b4 of 16-bit integers
# obtain a1,b1,a3,b3 and a2,b2,a4,b4 (must have a mask of _lowdword)
.macro split_evenodd_16_32 in1,in2,out1,out2 msk=0,t0=14,t1=15
vpand		%ymm\msk,%ymm\in1,%ymm\t0
vpand		%ymm\msk,%ymm\in2,%ymm\t1
vpsrld		$16,%ymm\in1,%ymm\out1
vpsrld		$16,%ymm\in2,%ymm\out2
vpackusdw	%ymm\out2,%ymm\out1,%ymm\out2
vpackusdw	%ymm\t1,%ymm\t0,%ymm\out1
vpermq		$0xd8,%ymm\out1,%ymm\out1
vpermq		$0xd8,%ymm\out2,%ymm\out2
.endm

################################################################################

#in1 with in2 and in3 with in4
.macro split_evenodd_16_64 in1,in2,in3,in4,out1,out2,out3,out4 msk=0,t0=12,t1=13,t2=14,t3=15
vpand		%ymm\msk,%ymm\in1,%ymm\t0
vpand		%ymm\msk,%ymm\in2,%ymm\t1
vpand		%ymm\msk,%ymm\in3,%ymm\t2
vpand		%ymm\msk,%ymm\in4,%ymm\t3
vpsrld		$16,%ymm\in1,%ymm\out1
vpsrld		$16,%ymm\in2,%ymm\out2
vpsrld		$16,%ymm\in3,%ymm\out3
vpsrld		$16,%ymm\in4,%ymm\out4
vpackusdw	%ymm\out2,%ymm\out1,%ymm\out2
vpackusdw	%ymm\t1,%ymm\t0,%ymm\out1
vpackusdw	%ymm\out4,%ymm\out3,%ymm\out4
vpackusdw	%ymm\t3,%ymm\t2,%ymm\out3
vpermq		$0xd8,%ymm\out1,%ymm\out1
vpermq		$0xd8,%ymm\out2,%ymm\out2
vpermq		$0xd8,%ymm\out3,%ymm\out3
vpermq		$0xd8,%ymm\out4,%ymm\out4
.endm

################################################################################

# from vectors a1,b1,a3,b3 and a2,b2,a4,b4 of 16-bit integers
# obtain a1,a2,a3,a4 and b1,b2,b3,b4
.macro join_evenodd_16_32 in1,in2,out1,out2 t0=14,t1=15
vpunpcklwd	%ymm\in2,%ymm\in1,%ymm\t0
vpunpckhwd	%ymm\in2,%ymm\in1,%ymm\t1
vperm2i128	$0x20,%ymm\t1,%ymm\t0,%ymm\out1
vperm2i128	$0x31,%ymm\t1,%ymm\t0,%ymm\out2
.endm

################################################################################

.macro join_evenodd_16_64 in1,in2,in3,in4,out1,out2,out3,out4 t0=12,t1=13,t2=14,t3=15
vpunpcklwd	%ymm\in2,%ymm\in1,%ymm\t0
vpunpcklwd	%ymm\in4,%ymm\in3,%ymm\t2
vpunpckhwd	%ymm\in2,%ymm\in1,%ymm\t1
vpunpckhwd	%ymm\in4,%ymm\in3,%ymm\t3
vperm2i128	$0x20,%ymm\t1,%ymm\t0,%ymm\out1
vperm2i128	$0x20,%ymm\t3,%ymm\t2,%ymm\out3
vperm2i128	$0x31,%ymm\t1,%ymm\t0,%ymm\out2
vperm2i128	$0x31,%ymm\t3,%ymm\t2,%ymm\out4
.endm

################################################################################

/*
.macro interleave
vpunpcklwd	%ymm\in2,%ymm\in1,%ymm\t0
vpunpcklwd	%ymm\in4,%ymm\in3,%ymm\t2
vpunpckhwd	%ymm\in2,%ymm\in1,%ymm\t1
vpunpckhwd	%ymm\in4,%ymm\in3,%ymm\t3

.endm
*/
