.include "asm_macros.h"
.include "asm_shuffle.h"
.include "product_macros.h"

.macro sft_bld_test sft,upd
vpslldq   $2,%ymm\upd,%ymm\upd
vpsrldq   $2,%ymm\sft,%ymm\sft
vpblendw  $0x80,%ymm\upd,%ymm\sft,%ymm\sft
.endm

.global test_function
test_function:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_32_coeffs 0,1 rsi
load_32_coeffs 2,3 rdx

// vpaddw   %ymm0,%ymm2,%ymm0
// vpaddw   %ymm3,%ymm5,%ymm5

/* set as before */
vmovdqa   %ymm0,%ymm3
vmovdqa   %ymm2,%ymm7

/* reversal */
vpermpd   $0x1B,%ymm3,%ymm5
vpshufhw  $0x1B,%ymm5,%ymm5
vpshuflw  $0x1B,%ymm5,%ymm5

/* no Montgomery product */
vmovdqa   %ymm7,%ymm4

vperm2i128 $0x12,%ymm3,%ymm4,%ymm7
vperm2i128 $0x01,%ymm4,%ymm4,%ymm4

/*step 1*/
// vpslldq   $2,%ymm0,%ymm0
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm7,%ymm5,%ymm5

/*Second step*/
// vpslldq   $2,%ymm0,%ymm0
// vpsrldq   $2,%ymm5,%ymm5
// vpblendw  $0x80,%ymm0,%ymm5,%ymm5
sft_bld_test 5,7 /*step 2*/
sft_bld_test 5,7 /*step 3*/
sft_bld_test 5,7 /*step 4*/
sft_bld_test 5,7 /*step 5*/
sft_bld_test 5,7 /*step 6*/
sft_bld_test 5,7 /*step 7*/
sft_bld_test 5,7 /*step 8*/

/*step 9*/
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm4,%ymm5,%ymm5
sft_bld_test 5,4 /*step 10*/
sft_bld_test 5,4 /*step 11*/
sft_bld_test 5,4 /*step 12*/
sft_bld_test 5,4 /*step 13*/
sft_bld_test 5,4 /*step 14*/
sft_bld_test 5,4 /*step 15*/

save_32_coeffs 5,0 rdi

add 	%r11,%rsp
ret
