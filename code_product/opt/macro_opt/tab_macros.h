#ifndef _OPT_TAB_MACROS_
#define _OPT_TAB_MACROS_

/* clang-format off */

#define _VAL_16x_(v) v,v,v,v,v,v,v,v,v,v,v,v,v,v,v,v
#define _VAL_2x8_(v1,v2) v1,v1,v1,v1,v1,v1,v1,v1,v2,v2,v2,v2,v2,v2,v2,v2
#define _VAL_4x4_(v1,v2,v3,v4) v1,v1,v1,v1,v2,v2,v2,v2,v3,v3,v3,v3,v4,v4,v4,v4
#define _VAL_8x2_(v1,v2,v3,v4,v5,v6,v7,v8) v1,v1,v2,v2,v3,v3,v4,v4,v5,v5,v6,v6,v7,v7,v8,v8

#define _TAB16_(v) {_VAL_16x_(v)};
#define _DEF_TAB16_(v) __attribute__((aligned(32))) = _TAB16_(v);

const uint16_t _lowdword[16] asm("_lowdword") __attribute__((aligned(32))) = {
  0xffff, 0x0, 0xffff, 0x0, 0xffff, 0x0, 0xffff, 0x0, 0xffff, 0x0, 0xffff, 0x0, 0xffff, 0x0, 0xffff, 0x0};
const uint8_t _vpshufb_idx[32] asm("_vpshufb_idx") __attribute__((aligned(32))) = {
  2, 3, 0, 1, 6, 7, 4, 5, 10, 11, 8, 9, 14, 15, 12, 13, 2, 3, 0, 1, 6, 7, 4, 5, 10, 11, 8, 9, 14, 15, 12, 13};

/* mapping from packs of 16 4*(1,2,4,3)*/
#define NTT_Y_PACK4_REORDER_16(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)\
 a1,a5,a13,a9,\
 a2,a6,a14,a10,\
 a3,a7,a15,a11,\
 a4,a8,a16,a12

#define NTT_Y_PROD_PACK4_REORDER_16(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)\
  _VAL_4x4_(a1,a2,a3,a4),\
  _VAL_4x4_(a5,a6,a7,a8),\
  _VAL_4x4_(a9,a10,a11,a12),\
  _VAL_4x4_(a13,a14,a15,a16)

#define NTT_Y_PROD_PACK8_REORDER_16(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)\
  _VAL_2x8_(a1,a2),_VAL_2x8_(a3,a4),\
  _VAL_2x8_(a5,a6),_VAL_2x8_(a7,a8),\
  _VAL_2x8_(a9,a10),_VAL_2x8_(a11,a12),\
  _VAL_2x8_(a13,a14),_VAL_2x8_(a15,a16)
/* clang-format on */

#endif
