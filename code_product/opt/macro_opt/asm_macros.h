# General Barrett reduction with xv=(montgomery coef)^2/N, xq=moduli, sft=shift
.macro gen_barrett_64_coeffs p0,p1,p2,p3 xv=0,xq=1,sft=10,tmp0=12,tmp1=13,tmp2=14,tmp3=15
vpmulhw		%ymm\xv,%ymm\p0,%ymm\tmp0
vpmulhw		%ymm\xv,%ymm\p1,%ymm\tmp1
vpmulhw		%ymm\xv,%ymm\p2,%ymm\tmp2
vpmulhw		%ymm\xv,%ymm\p3,%ymm\tmp3
vpsraw		$\sft,%ymm\tmp0,%ymm\tmp0
vpsraw		$\sft,%ymm\tmp1,%ymm\tmp1
vpsraw		$\sft,%ymm\tmp2,%ymm\tmp2
vpsraw		$\sft,%ymm\tmp3,%ymm\tmp3
vpmullw		%ymm\xq,%ymm\tmp0,%ymm\tmp0
vpmullw		%ymm\xq,%ymm\tmp1,%ymm\tmp1
vpmullw		%ymm\xq,%ymm\tmp2,%ymm\tmp2
vpmullw		%ymm\xq,%ymm\tmp3,%ymm\tmp3
vpsubw		%ymm\tmp0,%ymm\p0,%ymm\p0
vpsubw		%ymm\tmp1,%ymm\p1,%ymm\p1
vpsubw		%ymm\tmp2,%ymm\p2,%ymm\p2
vpsubw		%ymm\tmp3,%ymm\p3,%ymm\p3
.endm

.macro gen_barrett_32_coeffs p0,p1 xv=0,xq=1,sft=10,tmp0=12,tmp1=13
vpmulhw		%ymm\xv,%ymm\p0,%ymm\tmp0
vpmulhw		%ymm\xv,%ymm\p1,%ymm\tmp1
vpsraw		$\sft,%ymm\tmp0,%ymm\tmp0
vpsraw		$\sft,%ymm\tmp1,%ymm\tmp1
vpmullw		%ymm\xq,%ymm\tmp0,%ymm\tmp0
vpmullw		%ymm\xq,%ymm\tmp1,%ymm\tmp1
vpsubw		%ymm\tmp0,%ymm\p0,%ymm\p0
vpsubw		%ymm\tmp1,%ymm\p1,%ymm\p1
.endm

################################################################################

# Signed barrett, or pseudo-mersenne of form (2^pw1 - 2^pw2 + 1)
.macro signed_barrett_64_coeffs p0,p1,p2,p3 msk=0,pw1=11,pw2=5,tmp0=12,tmp1=13,tmp2=14,tmp3=15
vpsraw		$\pw1,%ymm\p0,%ymm\tmp0
vpsraw		$\pw1,%ymm\p1,%ymm\tmp1
vpsraw		$\pw1,%ymm\p2,%ymm\tmp2
vpsraw		$\pw1,%ymm\p3,%ymm\tmp3
vpand		%ymm\msk,%ymm\p0,%ymm\p0
vpand		%ymm\msk,%ymm\p1,%ymm\p1
vpand		%ymm\msk,%ymm\p2,%ymm\p2
vpand		%ymm\msk,%ymm\p3,%ymm\p3
vpsubw		%ymm\tmp0,%ymm\p0,%ymm\p0
vpsubw		%ymm\tmp1,%ymm\p1,%ymm\p1
vpsubw		%ymm\tmp2,%ymm\p2,%ymm\p2
vpsubw		%ymm\tmp3,%ymm\p3,%ymm\p3
vpsllw		$\pw2,%ymm\tmp0,%ymm\tmp0
vpsllw		$\pw2,%ymm\tmp1,%ymm\tmp1
vpsllw		$\pw2,%ymm\tmp2,%ymm\tmp2
vpsllw		$\pw2,%ymm\tmp3,%ymm\tmp3
vpaddw		%ymm\tmp0,%ymm\p0,%ymm\p0
vpaddw		%ymm\tmp1,%ymm\p1,%ymm\p1
vpaddw		%ymm\tmp2,%ymm\p2,%ymm\p2
vpaddw		%ymm\tmp3,%ymm\p3,%ymm\p3
.endm

#.macro or pseudo-mersenne of form (2^pw1 - 2^pw2 + 2^pw3 + 1)
.macro signed_barrett_64_coeffs_2 p0,p1,p2,p3 msk=0,pw1=12,pw2=10,pw3=8,t0=9,t1=10,t2=11,t3=12,t4=5,t5=6,t6=7,t7=8
vpsraw		$\pw1,%ymm\p0,%ymm\t0
vpsraw		$\pw1,%ymm\p1,%ymm\t1
vpsraw		$\pw1,%ymm\p2,%ymm\t2
vpsraw		$\pw1,%ymm\p3,%ymm\t3
vpand		%ymm\msk,%ymm\p0,%ymm\p0
vpand		%ymm\msk,%ymm\p1,%ymm\p1
vpand		%ymm\msk,%ymm\p2,%ymm\p2
vpand		%ymm\msk,%ymm\p3,%ymm\p3
vpsubw		%ymm\t0,%ymm\p0,%ymm\p0
vpsubw		%ymm\t1,%ymm\p1,%ymm\p1
vpsubw		%ymm\t2,%ymm\p2,%ymm\p2
vpsubw		%ymm\t3,%ymm\p3,%ymm\p3
vpsllw		$\pw2,%ymm\t0,%ymm\t4
vpsllw		$\pw3,%ymm\t0,%ymm\t0
vpsllw		$\pw2,%ymm\t1,%ymm\t5
vpsllw		$\pw3,%ymm\t1,%ymm\t1
vpsllw		$\pw2,%ymm\t2,%ymm\t6
vpsllw		$\pw3,%ymm\t2,%ymm\t2
vpsllw		$\pw2,%ymm\t3,%ymm\t7
vpsllw		$\pw3,%ymm\t3,%ymm\t3
vpsubw		%ymm\t0,%ymm\t4,%ymm\t0
vpsubw		%ymm\t1,%ymm\t5,%ymm\t1
vpsubw		%ymm\t2,%ymm\t6,%ymm\t2
vpsubw		%ymm\t3,%ymm\t7,%ymm\t3
vpaddw		%ymm\t0,%ymm\p0,%ymm\p0
vpaddw		%ymm\t1,%ymm\p1,%ymm\p1
vpaddw		%ymm\t2,%ymm\p2,%ymm\p2
vpaddw		%ymm\t3,%ymm\p3,%ymm\p3
.endm

#.macro or pseudo-mersenne of form (2^pw1 - 2^pw2 - 2^pw3 + 1)
.macro signed_barrett_64_coeffs_3 p0,p1,p2,p3 msk=0,pw1=12,pw2=10,pw3=8,t0=9,t1=10,t2=11,t3=12,t4=5,t5=6,t6=7,t7=8
vpsraw		$\pw1,%ymm\p0,%ymm\t0
vpsraw		$\pw1,%ymm\p1,%ymm\t1
vpsraw		$\pw1,%ymm\p2,%ymm\t2
vpsraw		$\pw1,%ymm\p3,%ymm\t3
vpand		%ymm\msk,%ymm\p0,%ymm\p0
vpand		%ymm\msk,%ymm\p1,%ymm\p1
vpand		%ymm\msk,%ymm\p2,%ymm\p2
vpand		%ymm\msk,%ymm\p3,%ymm\p3
vpsubw		%ymm\t0,%ymm\p0,%ymm\p0
vpsubw		%ymm\t1,%ymm\p1,%ymm\p1
vpsubw		%ymm\t2,%ymm\p2,%ymm\p2
vpsubw		%ymm\t3,%ymm\p3,%ymm\p3
vpsllw		$\pw2,%ymm\t0,%ymm\t4
vpsllw		$\pw3,%ymm\t0,%ymm\t0
vpsllw		$\pw2,%ymm\t1,%ymm\t5
vpsllw		$\pw3,%ymm\t1,%ymm\t1
vpsllw		$\pw2,%ymm\t2,%ymm\t6
vpsllw		$\pw3,%ymm\t2,%ymm\t2
vpsllw		$\pw2,%ymm\t3,%ymm\t7
vpsllw		$\pw3,%ymm\t3,%ymm\t3
vpaddw		%ymm\t0,%ymm\t4,%ymm\t0
vpaddw		%ymm\t1,%ymm\t5,%ymm\t1
vpaddw		%ymm\t2,%ymm\t6,%ymm\t2
vpaddw		%ymm\t3,%ymm\t7,%ymm\t3
vpaddw		%ymm\t0,%ymm\p0,%ymm\p0
vpaddw		%ymm\t1,%ymm\p1,%ymm\p1
vpaddw		%ymm\t2,%ymm\p2,%ymm\p2
vpaddw		%ymm\t3,%ymm\p3,%ymm\p3
.endm

################################################################################

#montgomery reduction, given low_part and high_parts
.macro montgomery_reduce_32 l0,l1,h0,h1,o0,o1 xq=1,xqinv=0
vpmullw		%ymm\xqinv,%ymm\l0,%ymm\l0
vpmullw		%ymm\xqinv,%ymm\l1,%ymm\l1
vpmulhw		%ymm\xq,%ymm\l0,%ymm\l0
vpmulhw		%ymm\xq,%ymm\l1,%ymm\l1
vpsubw		%ymm\l0,%ymm\h0,%ymm\o0
vpsubw		%ymm\l1,%ymm\h1,%ymm\o1
.endm

.macro montgomery_reduce_64 l0,l1,l2,l3,h0,h1,h2,h3,o0,o1,o2,o3 xq=1,xqinv=0
vpmullw		%ymm\xqinv,%ymm\l0,%ymm\l0
vpmullw		%ymm\xqinv,%ymm\l1,%ymm\l1
vpmullw		%ymm\xqinv,%ymm\l2,%ymm\l2
vpmullw		%ymm\xqinv,%ymm\l3,%ymm\l3
vpmulhw		%ymm\xq,%ymm\l0,%ymm\l0
vpmulhw		%ymm\xq,%ymm\l1,%ymm\l1
vpmulhw		%ymm\xq,%ymm\l2,%ymm\l2
vpmulhw		%ymm\xq,%ymm\l3,%ymm\l3
vpsubw		%ymm\l0,%ymm\h0,%ymm\o0
vpsubw		%ymm\l1,%ymm\h1,%ymm\o1
vpsubw		%ymm\l2,%ymm\h2,%ymm\o2
vpsubw		%ymm\l3,%ymm\h3,%ymm\o3
.endm

################################################################################

# Signed montgomery r <- (l * r)*qinv mod q
.macro signed_montgomery_32x32 l0,l1,r0,r1 xq=1,xqinv=0,tmp0=14,tmp1=15
#mul
vpmullw		%ymm\l0,%ymm\r0,%ymm\tmp0
vpmulhw		%ymm\l0,%ymm\r0,%ymm\r0
vpmullw		%ymm\l1,%ymm\r1,%ymm\tmp1
vpmulhw		%ymm\l1,%ymm\r1,%ymm\r1
#reduce
montgomery_reduce_32 \tmp0,\tmp1,\r0,\r1,\r0,\r1 \xq,\xqinv
.endm

################################################################################

.macro mul_lh_64 x1,x2,x3,x4,y1,y2,y3,y4,l1,l2,l3,l4,h1,h2,h3,h4
vpmullw		%ymm\x1,%ymm\y1,%ymm\l1
vpmulhw		%ymm\x1,%ymm\y1,%ymm\h1
vpmullw		%ymm\x2,%ymm\y2,%ymm\l2
vpmulhw		%ymm\x2,%ymm\y2,%ymm\h2
vpmullw		%ymm\x3,%ymm\y3,%ymm\l3
vpmulhw		%ymm\x3,%ymm\y3,%ymm\h3
vpmullw		%ymm\x4,%ymm\y4,%ymm\l4
vpmulhw		%ymm\x4,%ymm\y4,%ymm\h4
.endm

################################################################################

# Signed montgomery r <- (l * r)*qinv mod q
.macro signed_montgomery_64x64 l0,l1,l2,l3,r0,r1,r2,r3 xq=1,xqinv=0,tmp0=12,tmp1=13,tmp2=14,tmp3=15
#mul
mul_lh_64 \l0,\l1,\l2,\l3,\r0,\r1,\r2,\r3,\tmp0,\tmp1,\tmp2,\tmp3,\r0,\r1,\r2,\r3
#reduce
montgomery_reduce_64 \tmp0,\tmp1,\tmp2,\tmp3,\r0,\r1,\r2,\r3,\r0,\r1,\r2,\r3 \xq,\xqinv
.endm

################################################################################

# add 64 coeffs with 64 coeffs o <- l + r (o is l by default)
.macro add_64 l0,l1,l2,l3,r0,r1,r2,r3
vpaddw    %ymm\l0,%ymm\r0,%ymm\l0
vpaddw    %ymm\l1,%ymm\r1,%ymm\l1
vpaddw    %ymm\l2,%ymm\r2,%ymm\l2
vpaddw    %ymm\l3,%ymm\r3,%ymm\l3
.endm

# add 128 coeffs with 128 coeffs l <- l + r
.macro add_128 l0,l1,l2,l3,l4,l5,l6,l7,r0,r1,r2,r3,r4,r5,r6,r7
vpaddw    %ymm\l0,%ymm\r0,%ymm\l0
vpaddw    %ymm\l1,%ymm\r1,%ymm\l1
vpaddw    %ymm\l2,%ymm\r2,%ymm\l2
vpaddw    %ymm\l3,%ymm\r3,%ymm\l3
vpaddw    %ymm\l4,%ymm\r4,%ymm\l4
vpaddw    %ymm\l5,%ymm\r5,%ymm\l5
vpaddw    %ymm\l6,%ymm\r6,%ymm\l6
vpaddw    %ymm\l7,%ymm\r7,%ymm\l7
.endm

################################################################################

# sub 64 coeffs with 64 coeffs o <- r - l
.macro sub_64 l0,l1,l2,l3,r0,r1,r2,r3
vpsubw    %ymm\l0,%ymm\r0,%ymm\r0
vpsubw    %ymm\l1,%ymm\r1,%ymm\r1
vpsubw    %ymm\l2,%ymm\r2,%ymm\r2
vpsubw    %ymm\l3,%ymm\r3,%ymm\r3
.endm

# sub 128 coeffs with 128 coeffs r <- r - l
.macro sub_128 l0,l1,l2,l3,l4,l5,l6,l7,r0,r1,r2,r3,r4,r5,r6,r7
vpsubw    %ymm\l0,%ymm\r0,%ymm\r0
vpsubw    %ymm\l1,%ymm\r1,%ymm\r1
vpsubw    %ymm\l2,%ymm\r2,%ymm\r2
vpsubw    %ymm\l3,%ymm\r3,%ymm\r3
vpsubw    %ymm\l4,%ymm\r4,%ymm\r4
vpsubw    %ymm\l5,%ymm\r5,%ymm\r5
vpsubw    %ymm\l6,%ymm\r6,%ymm\r6
vpsubw    %ymm\l7,%ymm\r7,%ymm\r7
.endm

################################################################################

# sub 64 coeffs with 64 coeffs r <- r - c
.macro sub_64_cst r0,r1,r2,r3 c=0
vpsubw    %ymm\c,%ymm\r0,%ymm\r0
vpsubw    %ymm\c,%ymm\r1,%ymm\r1
vpsubw    %ymm\c,%ymm\r2,%ymm\r2
vpsubw    %ymm\c,%ymm\r3,%ymm\r3
.endm

# sub 128 coeffs with 128 coeffs r <- r - c
.macro sub_128_cst r0,r1,r2,r3,r4,r5,r6,r7 c=0
vpsubw    %ymm\c,%ymm\r0,%ymm\r0
vpsubw    %ymm\c,%ymm\r1,%ymm\r1
vpsubw    %ymm\c,%ymm\r2,%ymm\r2
vpsubw    %ymm\c,%ymm\r3,%ymm\r3
vpsubw    %ymm\c,%ymm\r4,%ymm\r4
vpsubw    %ymm\c,%ymm\r5,%ymm\r5
vpsubw    %ymm\c,%ymm\r6,%ymm\r6
vpsubw    %ymm\c,%ymm\r7,%ymm\r7
.endm

################################################################################

.macro load_32_coeffs p0,p1 ld=rdi,s0=0,s1=32
vmovdqa     \s0(%\ld),%ymm\p0
vmovdqa     \s1(%\ld),%ymm\p1
.endm


.macro load_64_coeffs p0,p1,p2,p3 ld=rdi,s0=0,s1=32,s2=64,s3=96
vmovdqa     \s0(%\ld),%ymm\p0
vmovdqa     \s1(%\ld),%ymm\p1
vmovdqa     \s2(%\ld),%ymm\p2
vmovdqa     \s3(%\ld),%ymm\p3
.endm

.macro load_128_coeffs p0,p1,p2,p3,p4,p5,p6,p7 ld=rdi,s0=0,s1=32,s2=64,s3=96,s4=128,s5=160,s6=192,s7=224
vmovdqa     \s0(%\ld),%ymm\p0
vmovdqa     \s1(%\ld),%ymm\p1
vmovdqa     \s2(%\ld),%ymm\p2
vmovdqa     \s3(%\ld),%ymm\p3
vmovdqa     \s4(%\ld),%ymm\p4
vmovdqa     \s5(%\ld),%ymm\p5
vmovdqa     \s6(%\ld),%ymm\p6
vmovdqa     \s7(%\ld),%ymm\p7
.endm

.macro save_32_coeffs p0,p1 ld=rdi
vmovdqa     %ymm\p0,(%\ld)
vmovdqa     %ymm\p1,32(%\ld)
.endm

.macro save_64_coeffs p0,p1,p2,p3 ld=rdi,s0=0,s1=32,s2=64,s3=96
vmovdqa     %ymm\p0,\s0(%\ld)
vmovdqa     %ymm\p1,\s1(%\ld)
vmovdqa     %ymm\p2,\s2(%\ld)
vmovdqa     %ymm\p3,\s3(%\ld)
.endm

.macro save_128_coeffs p0,p1,p2,p3,p4,p5,p6,p7 ld=rdi,s0=0,s1=32,s2=64,s3=96,s4=128,s5=160,s6=192,s7=224
vmovdqa     %ymm\p0,\s0(%\ld)
vmovdqa     %ymm\p1,\s1(%\ld)
vmovdqa     %ymm\p2,\s2(%\ld)
vmovdqa     %ymm\p3,\s3(%\ld)
vmovdqa     %ymm\p4,\s4(%\ld)
vmovdqa     %ymm\p5,\s5(%\ld)
vmovdqa     %ymm\p6,\s6(%\ld)
vmovdqa     %ymm\p7,\s7(%\ld)
.endm

################################################################################

# macro for ntt_butterfly
.macro reduce_update x1,x2,x3,x4,y1,y2,y3,y4 qinv=0,q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15
#reduce
montgomery_reduce_64 \tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4 \q,\qinv
#update
vpsubw		%ymm\tmp1,%ymm\x1,%ymm\y1
vpsubw		%ymm\tmp2,%ymm\x2,%ymm\y2
vpsubw		%ymm\tmp3,%ymm\x3,%ymm\y3
vpsubw		%ymm\tmp4,%ymm\x4,%ymm\y4
vpaddw		%ymm\tmp1,%ymm\x1,%ymm\x1
vpaddw		%ymm\tmp2,%ymm\x2,%ymm\x2
vpaddw		%ymm\tmp3,%ymm\x3,%ymm\x3
vpaddw		%ymm\tmp4,%ymm\x4,%ymm\x4
.endm

################################################################################

# ntt_butterfly 128-coeffs
.macro ntt_butterfly_4 x1,x2,x3,x4,y1,y2,y3,y4,z1,z2,z3,z4 qinv=0,q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15
#mul
mul_lh_64 \z1,\z2,\z3,\z4,\y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4
reduce_update \x1,\x2,\x3,\x4,\y1,\y2,\y3,\y4 \qinv,\q,\tmp1,\tmp2,\tmp3,\tmp4
.endm

.macro ntt_butterfly_1 x1,x2,x3,x4,y1,y2,y3,y4,z1 qinv=0,q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15
#mul
ntt_butterfly_4 \x1,\x2,\x3,\x4,\y1,\y2,\y3,\y4,\z1,\z1,\z1,\z1 \qinv,\q,\tmp1,\tmp2,\tmp3,\tmp4
.endm

.macro ntt_butterfly_2 x1,x2,x3,x4,y1,y2,y3,y4,z1,z2 qinv=0,q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15
#mul
ntt_butterfly_4 \x1,\x2,\x3,\x4,\y1,\y2,\y3,\y4,\z1,\z1,\z2,\z2 \qinv,\q,\tmp1,\tmp2,\tmp3,\tmp4
.endm

################################################################################

# macros for invntt_butterfly
.macro invntt_update x1,x2,x3,x4,y1,y2,y3,y4 o1=12,o2=13,o3=14,o4=15
vpsubw		%ymm\y1,%ymm\x1,%ymm\o1
vpsubw		%ymm\y2,%ymm\x2,%ymm\o2
vpsubw		%ymm\y3,%ymm\x3,%ymm\o3
vpsubw		%ymm\y4,%ymm\x4,%ymm\o4
vpaddw		%ymm\x1,%ymm\y1,%ymm\x1
vpaddw		%ymm\x2,%ymm\y2,%ymm\x2
vpaddw		%ymm\x3,%ymm\y3,%ymm\x3
vpaddw		%ymm\x4,%ymm\y4,%ymm\x4
.endm

################################################################################

.macro invntt_update_2 x1,x2,x3,x4,y1,y2,y3,y4 o1=12,o2=13,o3=14,o4=15
vmovdqa		%ymm\x1,%ymm\o1
vmovdqa		%ymm\x2,%ymm\o2
vmovdqa		%ymm\x3,%ymm\o3
vmovdqa		%ymm\x4,%ymm\o4
vpaddw		%ymm\x1,%ymm\y1,%ymm\x1
vpaddw		%ymm\x2,%ymm\y2,%ymm\x2
vpaddw		%ymm\x3,%ymm\y3,%ymm\x3
vpaddw		%ymm\x4,%ymm\y4,%ymm\x4
vpsubw		%ymm\y1,%ymm\o1,%ymm\y1
vpsubw		%ymm\y2,%ymm\o2,%ymm\y2
vpsubw		%ymm\y3,%ymm\o3,%ymm\y3
vpsubw		%ymm\y4,%ymm\o4,%ymm\y4
.endm

################################################################################

.macro invntt_update_2b x1,x2,x3,x4,y1,y2,y3,y4 o1=12,o2=13,o3=14,o4=15,s1=11
vmovdqa		%ymm\s1,%ymm\o1
vmovdqa		%ymm\x2,%ymm\o2
vmovdqa		%ymm\x3,%ymm\o3
vmovdqa		%ymm\x4,%ymm\o4
vpaddw		%ymm\s1,%ymm\y1,%ymm\x1
vpaddw		%ymm\x2,%ymm\y2,%ymm\x2
vpaddw		%ymm\x3,%ymm\y3,%ymm\x3
vpaddw		%ymm\x4,%ymm\y4,%ymm\x4
vpsubw		%ymm\y1,%ymm\o1,%ymm\y1
vpsubw		%ymm\y2,%ymm\o2,%ymm\y2
vpsubw		%ymm\y3,%ymm\o3,%ymm\y3
vpsubw		%ymm\y4,%ymm\o4,%ymm\y4
.endm

################################################################################

# invntt_butterfly 128-coeffs
.macro invntt_butterfly_4_z x1,x2,x3,x4,y1,y2,y3,y4,z1,z2,z3,z4 ld=rsi,qinv=0,q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15,tmp=11
invntt_update \x1,\x2,\x3,\x4,\y1,\y2,\y3,\y4 \tmp1,\tmp2,\tmp3,\tmp4
load_64_coeffs \y2,\y3,\y4,\tmp \ld,\z1,\z2,\z3,\z4
#mul
mul_lh_64 \y2,\y3,\y4,\tmp,\tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4
montgomery_reduce_64 \y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4 \q,\qinv
.endm

.macro invntt_butterfly_2_z x1,x2,x3,x4,y1,y2,y3,y4,z1,z2 ld=rsi,qinv=0,q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15,tmp=11
invntt_update \x1,\x2,\x3,\x4,\y1,\y2,\y3,\y4 \tmp1,\tmp2,\tmp3,\tmp4
load_32_coeffs \y4,\tmp \ld,\z1,\z2
mul_lh_64 \y4,\y4,\tmp,\tmp,\tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4
montgomery_reduce_64 \y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4 \q,\qinv
.endm

.macro invntt_butterfly_1 x1,x2,x3,x4,y1,y2,y3,y4,z1 ld=rsi,qinv=0,q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15
invntt_update \x1,\x2,\x3,\x4,\y1,\y2,\y3,\y4 \tmp1,\tmp2,\tmp3,\tmp4
mul_lh_64 \z1,\z1,\z1,\z1,\tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4
montgomery_reduce_64 \y1,\y2,\y3,\y4,\tmp1,\tmp2,\tmp3,\tmp4,\y1,\y2,\y3,\y4 \q,\qinv
.endm

################################################################################

# macro for corrections
.macro add_q_ifneg x1,x2,x3,x4 q=1,tmp1=12,tmp2=13,tmp3=14,tmp4=15
vpsraw		$15,%ymm\x1,%ymm\tmp1
vpsraw		$15,%ymm\x2,%ymm\tmp2
vpsraw		$15,%ymm\x3,%ymm\tmp3
vpsraw		$15,%ymm\x4,%ymm\tmp4
vpand		%ymm\q,%ymm\tmp1,%ymm\tmp1
vpand		%ymm\q,%ymm\tmp2,%ymm\tmp2
vpand		%ymm\q,%ymm\tmp3,%ymm\tmp3
vpand		%ymm\q,%ymm\tmp4,%ymm\tmp4
vpaddw		%ymm\tmp1,%ymm\x1,%ymm\x1
vpaddw		%ymm\tmp2,%ymm\x2,%ymm\x2
vpaddw		%ymm\tmp3,%ymm\x3,%ymm\x3
vpaddw		%ymm\tmp4,%ymm\x4,%ymm\x4
.endm

################################################################################
