.include "asm_macros.h"

# l <- l + r
.macro load_add_64
load_64_coeffs 0,1,2,3 rsi
load_64_coeffs 4,5,6,7 rdx
add_64 0,1,2,3,4,5,6,7
save_64_coeffs 0,1,2,3
.endm

.macro load_add_128
load_128_coeffs 0,1,2,3,4,5,6,7 rsi
load_128_coeffs 8,9,10,11,12,13,14,15 rdx
add_128 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
save_128_coeffs 0,1,2,3,4,5,6,7
.endm

.macro load_add_64_short
load_64_coeffs 0,1,2,3 rdi
load_64_coeffs 4,5,6,7 rsi
add_64 0,1,2,3,4,5,6,7
save_64_coeffs 0,1,2,3
.endm

.macro load_add_128_short
load_128_coeffs 0,1,2,3,4,5,6,7 rdi
load_128_coeffs 8,9,10,11,12,13,14,15 rsi
add_128 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
save_128_coeffs 0,1,2,3,4,5,6,7
.endm

# l <- l - r
.macro load_sub_64
load_64_coeffs 0,1,2,3 rdx
load_64_coeffs 4,5,6,7 rsi
sub_64 0,1,2,3,4,5,6,7
save_64_coeffs 4,5,6,7
.endm

.macro load_sub_128
load_128_coeffs 0,1,2,3,4,5,6,7 rdx
load_128_coeffs 8,9,10,11,12,13,14,15 rsi
sub_128 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
save_128_coeffs 8,9,10,11,12,13,14,15
.endm

# l <- l - r
.macro load_sub_64_short
load_64_coeffs 0,1,2,3 rsi
load_64_coeffs 4,5,6,7 rdi
sub_64 0,1,2,3,4,5,6,7
save_64_coeffs 4,5,6,7
.endm

.macro load_sub_128_short
load_128_coeffs 0,1,2,3,4,5,6,7 rsi
load_128_coeffs 8,9,10,11,12,13,14,15 rdi
sub_128 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
save_128_coeffs 8,9,10,11,12,13,14,15
.endm

.global poly_add_64
poly_add_64:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_64

add 	%r11,%rsp

ret

.global poly_add_128
poly_add_128:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_128

add 	%r11,%rsp

ret

.global poly_add_256
poly_add_256:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_128

add $256,%rdx
add $256,%rsi
add $256,%rdi

load_add_128

add 	%r11,%rsp

ret

.global poly_add_512
poly_add_512:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_128
add $256,%rdx
add $256,%rsi
add $256,%rdi
load_add_128
add $256,%rdx
add $256,%rsi
add $256,%rdi
load_add_128
add $256,%rdx
add $256,%rsi
add $256,%rdi
load_add_128

add 	%r11,%rsp

ret

.global poly_add_64_short
poly_add_64_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_64_short

add 	%r11,%rsp

ret

.global poly_add_128_short
poly_add_128_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_128_short

add 	%r11,%rsp

ret

.global poly_add_256_short
poly_add_256_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_128_short

add $256,%rsi
add $256,%rdi

load_add_128_short

add 	%r11,%rsp

ret

.global poly_add_512_short
poly_add_512_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_add_128_short
add $256,%rsi
add $256,%rdi
load_add_128_short
add $256,%rsi
add $256,%rdi
load_add_128_short
add $256,%rsi
add $256,%rdi
load_add_128_short

add 	%r11,%rsp

ret

.global poly_sub_64
poly_sub_64:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_sub_64

add 	%r11,%rsp

ret

.global poly_sub_128
poly_sub_128:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_sub_128

add 	%r11,%rsp

ret

.global poly_sub_256
poly_sub_256:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_sub_128

add $256,%rdx
add $256,%rsi
add $256,%rdi

load_sub_128

add 	%r11,%rsp

ret

.global poly_sub_64_short
poly_sub_64_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_sub_64_short

add 	%r11,%rsp

ret

.global poly_sub_128_short
poly_sub_128_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_sub_128_short

add 	%r11,%rsp

ret

.global poly_sub_256_short
poly_sub_256_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_sub_128_short

add $256,%rsi
add $256,%rdi

load_sub_128_short

add 	%r11,%rsp

ret

.global poly_sub_512_short
poly_sub_512_short:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

load_sub_128_short
add $256,%rsi
add $256,%rdi
load_sub_128_short
add $256,%rsi
add $256,%rdi
load_sub_128_short
add $256,%rsi
add $256,%rdi
load_sub_128_short

add 	%r11,%rsp

ret
