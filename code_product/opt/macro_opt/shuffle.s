.include "asm_shuffle.h"

################################################################################

.global swap_pairs_16_32
swap_pairs_16_32:

mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		(%rdi),%ymm0
vmovdqa   32(%rdi),%ymm1

swap_pairs_16_32 0,1,0,1

vmovdqa   %ymm0,(%rdi)
vmovdqa   %ymm1,32(%rdi)

add 	%r11,%rsp
ret

################################################################################

.global split_evenodd_16_32
split_evenodd_16_32:
mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		_lowdword(%rip),%ymm2

vmovdqa		(%rdi),%ymm0
vmovdqa   32(%rdi),%ymm1

split_evenodd_16_32 0,1,0,1 2

vmovdqa   %ymm0,(%rdi)
vmovdqa   %ymm1,32(%rdi)

add 	%r11,%rsp
ret

################################################################################

.global join_evenodd_16_32
join_evenodd_16_32:
mov		%rsp,%r11
and		$31,%r11
sub		%r11,%rsp

vmovdqa		(%rdi),%ymm0
vmovdqa   32(%rdi),%ymm1

join_evenodd_16_32 0,1,0,1

vmovdqa   %ymm0,(%rdi)
vmovdqa   %ymm1,32(%rdi)

add 	%r11,%rsp
ret

################################################################################
