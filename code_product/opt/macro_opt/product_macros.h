# multiply polynomials of degree 1 over 32 coefficients
# use all registers except 0,1,2 for constants : ymm0 for Qinv, ymm1 for Q, ymm2 for lowdword
# save 14,15 for temporary results in macro

################################################################################
#                             mod X^2 - Y                                      #
################################################################################

.macro product_deg_1_32
load_32_coeffs 3,4
load_32_coeffs 5,6 rsi
vmovdqa		(%rdx),%ymm7

swap_pairs_16_32 5,6,12,13
split_evenodd_16_32 5,6,5,6 2
vpmullw		%ymm6,%ymm7,%ymm14
vpmulhw		%ymm6,%ymm7,%ymm15
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm6
join_evenodd_16_32 5,6,14,15 8,9

vpmaddwd  %ymm12,%ymm3,%ymm5
vpmaddwd  %ymm14,%ymm3,%ymm3
vpmaddwd  %ymm13,%ymm4,%ymm6
vpmaddwd  %ymm15,%ymm4,%ymm4

split_evenodd_16_64 3,4,5,6,3,4,5,6 2
montgomery_reduce_32 5,3,6,4,6,4
join_evenodd_16_32 4,6,4,6

save_32_coeffs 4,6
.endm

################################################################################

.macro product_deg_1_64
load_32_coeffs 3,4
load_32_coeffs 5,6 rsi
vmovdqa		(%rdx),%ymm7

swap_pairs_16_32 5,6,12,13
split_evenodd_16_32 5,6,5,6 2
vpmullw		%ymm6,%ymm7,%ymm14
vpmulhw		%ymm6,%ymm7,%ymm15
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm6
join_evenodd_16_32 5,6,14,15 8,9
vpmaddwd  %ymm12,%ymm3,%ymm5
vpmaddwd  %ymm14,%ymm3,%ymm3
vpmaddwd  %ymm13,%ymm4,%ymm6
vpmaddwd  %ymm15,%ymm4,%ymm4

load_32_coeffs 8,9 rdi,64,96
load_32_coeffs 10,11 rsi,64,96
vmovdqa		32(%rdx),%ymm7

swap_pairs_16_32 10,11,12,13
split_evenodd_16_32 10,11,10,11 2
vpmullw		%ymm11,%ymm7,%ymm14
vpmulhw		%ymm11,%ymm7,%ymm15
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm11
join_evenodd_16_32 10,11,14,15 7,15
vpmaddwd  %ymm12,%ymm8,%ymm10
vpmaddwd  %ymm14,%ymm8,%ymm8
vpmaddwd  %ymm13,%ymm9,%ymm11
vpmaddwd  %ymm15,%ymm9,%ymm9

split_evenodd_16_64 3,4,5,6,3,4,5,6 2
split_evenodd_16_64 8,9,10,11,8,9,10,11 2
# montgomery_reduce_32 5,3,6,4,6,4
montgomery_reduce_64 5,3,10,8,6,4,11,9,6,4,11,9
# join_evenodd_16_32 4,6,4,6
join_evenodd_16_64 4,6,9,11,3,5,8,10

# save_32_coeffs 4,6
save_64_coeffs 3,5,8,10
.endm

################################################################################

#reuse previous macro but shift to the next 32 coefficients to multiply
.macro product_deg_1_32_extra
product_deg_1_32

add		$64,%rdi
add		$64,%rsi
add		$32,%rdx
.endm

#reuse previous macro but shift to the next 64 coefficients to multiply
.macro product_deg_1_64_extra
product_deg_1_64

add		$128,%rdi
add		$128,%rsi
add		$64,%rdx
.endm

.macro product_deg_1_128
product_deg_1_64
add		$128,%rdi
add		$128,%rsi
add		$64,%rdx
product_deg_1_64
.endm

.macro product_deg_1_256
product_deg_1_128
add		$128,%rdi
add		$128,%rsi
add		$64,%rdx
product_deg_1_128
.endm

################################################################################
#                              mod X^4 - Y                                     #
################################################################################

# this macro is a combination of shift-blend-update-multiply (same as above)
.macro sft_bld_upd_mul_4 sft,upd,mul,res
# operation shift-blend-update
vpslldq   $2,%ymm\upd,%ymm\upd
vpsrldq   $2,%ymm\sft,%ymm\sft
vpblendw  $0x88,%ymm\upd,%ymm\sft,%ymm\sft
# obtain the vector representing the sum terms
vpmaddwd  %ymm\mul,%ymm\sft,%ymm\res
.endm

################################################################################

# this is the product of 4*4 polynomials of degree 4
.macro product_deg_4_16

vmovdqa		(%rdi),%ymm3 #a
vmovdqa		(%rsi),%ymm4 #b
vmovdqa		(%rdx),%ymm5 #ntt_Y (x4,x4,x4,x4)

#create a symmetric version of b
vpshufhw  $0x1B,%ymm4,%ymm6
vpshuflw  $0x1B,%ymm6,%ymm6

#make the mongtomery product by ntt_Y
vpmullw		%ymm5,%ymm4,%ymm14
vpmulhw		%ymm5,%ymm4,%ymm15
# reduce
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm5

# start shift_multiply
vpmaddwd  %ymm3,%ymm6,%ymm13 # deg 3

vpsrldq   $2,%ymm6,%ymm6
vpblendw  $0x88,%ymm5,%ymm6,%ymm6
vpmaddwd  %ymm3,%ymm6,%ymm12 # deg 2

sft_bld_upd_mul_4 6,5,3,11 #deg 1
sft_bld_upd_mul_4 6,5,3,10 #deg 0

#horizontal sums
vphaddd %ymm11,%ymm10,%ymm10 # (a0,b0,a1,b1,c0,d0,c1,d1)
vphaddd %ymm13,%ymm12,%ymm12 # (a2,b2,a3,b3,c2,d2,c3,d3)

#reorder
vpunpckldq %ymm12,%ymm10,%ymm8 # (a0,a2,b0,b2,c0,d0,c2,d2)
vpunpckhdq %ymm12,%ymm10,%ymm9 # (a1,a3,b2,b3,c1,c3,d1,d3)
vpunpckldq %ymm9,%ymm8,%ymm10 # (a0,a1,a2,a3,c0,c1,c2,c3)
vpunpckhdq %ymm9,%ymm8,%ymm12 # (b0,b1,b2,b3,d0,d1,d2,d3)

#now split into high_part/low_part
split_evenodd_16_32 10,12,10,12 2,13,11

# reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm10,%ymm12,%ymm10

#permut first second and third parts of 64-bits
vpermq    $0xD8,%ymm10,%ymm10

#save final result
vmovdqa		%ymm10,(%rdi)

.endm

################################################################################

.macro product_deg_4_64
product_deg_4_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_16
.endm

.macro product_deg_4_256
product_deg_4_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_4_64
.endm

################################################################################

################################################################################
#                              mod X^8 - Y                                     #
################################################################################

# this macro is the two level horizontal sums between 4 terms of 32-bits
# overwrite a and c and store final result to a (128-registers order)
.macro two_hsum a,b,c,d
#first level
vphaddd   %ymm\b,%ymm\a,%ymm\a    #(a,b)
vphaddd   %ymm\d,%ymm\c,%ymm\c    #(c,d)
#second level
vphaddd   %ymm\c,%ymm\a,%ymm\a   #(a,b,c,d)
.endm

################################################################################

# this is the product of 2*2 polynomials of degree 8
# reserve ymm 0,1,2
.macro product_deg_8_16
vmovdqa		(%rdi),%ymm3 #a
vmovdqa		(%rsi),%ymm4 #b
vmovdqa		(%rdx),%ymm5 #ntt_Y (x8,x8)

#create a symmetric version of b
#swap packs of 8, then reverse them
vpermpd   $0xB1,%ymm4,%ymm6
vpshufhw  $0x1B,%ymm6,%ymm6
vpshuflw  $0x1B,%ymm6,%ymm6

#make the mongtomery product by ntt_Y
vpmullw		%ymm5,%ymm4,%ymm14
vpmulhw		%ymm5,%ymm4,%ymm15
# reduce
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm5

# start shift_multiply, high_degree
vpmaddwd  %ymm3,%ymm6,%ymm7 # deg 7

vpsrldq   $2,%ymm6,%ymm6
vpblendw  $0x80,%ymm5,%ymm6,%ymm6
vpmaddwd  %ymm3,%ymm6,%ymm9 # deg 6

sft_bld_upd_mul_8 6,5,3,15 # deg 5
sft_bld_upd_mul_8 6,5,3,14 # deg 4

two_hsum 14,15,9,7

# start shift_multiply, low_degree
sft_bld_upd_mul_8 6,5,3,13 # deg 3
sft_bld_upd_mul_8 6,5,3,12 # deg 2
sft_bld_upd_mul_8 6,5,3,11 # deg 1
sft_bld_upd_mul_8 6,5,3,10 # deg 0

two_hsum 10,11,12,13

#now split into high_part/low_part
split_evenodd_16_32 10,14,10,14 2,13,11

# reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm10,%ymm14,%ymm10

#permut first second and third parts of 64-bits
vpermq    $0xD8,%ymm10,%ymm10

#save final result
vmovdqa		%ymm10,(%rdi)

.endm

################################################################################

.macro product_deg_8_64
product_deg_8_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_16
.endm

.macro product_deg_8_256
product_deg_8_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_8_64
.endm

################################################################################
#                             mod X^16 - Y                                     #
################################################################################

# this macro is a combination of shift-blend-update-multiply
.macro sft_bld_upd_mul_8 sft,upd,mul,res
# operation shift-blend-update
vpslldq   $2,%ymm\upd,%ymm\upd /* (hY,gY,fY,eY|dY,cY,bY,aY) -> (gY,fY,eY,0|cY,bY,aY,0) */
vpsrldq   $2,%ymm\sft,%ymm\sft /* (a,b,c,d|e,f,g,h) -> (0,a,b,c|0,e,f,g) */
vpblendw  $0x80,%ymm\upd,%ymm\sft,%ymm\sft /* (a,b,c,d|e,f,g,h) -> (hY,a,b,c|d,e,f,g) */
# obtain the vector representing the sum terms
vpmaddwd  %ymm\mul,%ymm\sft,%ymm\res
.endm

################################################################################

# this macro is the three level horizontal sums between 8 terms of 32-bits
# overwrite a to z and store final result to a
.macro three_hsum a,b,c,d,e,f,g,h
# compute horizontal sums of 32-bits in a tree way
#first level
vphaddd   %ymm\e,%ymm\a,%ymm\a    #(a,e)
vphaddd   %ymm\f,%ymm\b,%ymm\b    #(b,f)
vphaddd   %ymm\g,%ymm\c,%ymm\c    #(g,c)
vphaddd   %ymm\h,%ymm\d,%ymm\d    #(h,d)
#intermediate shuffle (4a,4a,4b,4b,4a,4a,4b,4b) -> (4a,4a,4a,4a,4b,4b,4b,4b)
vpermq    $0xD8,%ymm\a,%ymm\a
vpermq    $0xD8,%ymm\b,%ymm\b
vpermq    $0xD8,%ymm\c,%ymm\c
vpermq    $0xD8,%ymm\d,%ymm\d
#second level
vphaddd   %ymm\b,%ymm\a,%ymm\a   #(a,b,e,f)
vphaddd   %ymm\d,%ymm\c,%ymm\c   #(c,d,g,h)
#third  level
vphaddd   %ymm\c,%ymm\a,%ymm\a   #(a,b,c,d,e,f,g,h)
.endm

################################################################################

# this is the product of 2 polynomials of degree 16
# reserve ymm 0,1,6
.macro product_deg_16_16
vmovdqa		(%rdi),%ymm2 #a
vmovdqa		(%rsi),%ymm3 #b
vmovdqa		(%rdx),%ymm4 #ntt_Y (x16)

#create a symmetric version of b
#swap packs of 4, then reverse them
vpermpd   $0x1B,%ymm3,%ymm5
vpshufhw  $0x1B,%ymm5,%ymm5
vpshuflw  $0x1B,%ymm5,%ymm5

#make the mongtomery product by ntt_Y
vpmullw		%ymm4,%ymm3,%ymm14
vpmulhw		%ymm4,%ymm3,%ymm15
# reduce
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm4

# reorganize the two halves of the shifts
# one vector for the first 8 shifts, the second one for the last 7 shifts.
vperm2i128 $0x12,%ymm3,%ymm4,%ymm7
vperm2i128 $0x01,%ymm4,%ymm4,%ymm4

# start first half (15-8)
vpmaddwd  %ymm2,%ymm5,%ymm15 # deg 15

# operation shift-blend-update / possible 8 times with the same vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm7,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm14 # deg 14

sft_bld_upd_mul_8 5,7,2,13 #deg 13
sft_bld_upd_mul_8 5,7,2,12 #deg 12
sft_bld_upd_mul_8 5,7,2,11 #deg 11
sft_bld_upd_mul_8 5,7,2,10 #deg 10
sft_bld_upd_mul_8 5,7,2,9  #deg 19
sft_bld_upd_mul_8 5,7,2,8  #deg 8

#(8,9,10,11,12,13,14,15)
three_hsum 8,9,10,11,12,13,14,15

sft_bld_upd_mul_8 5,7,2,7  #deg 7

# operation shift-blend-update / apply 7 times with the new vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm4,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm9 #deg 6 (ymm6 is taken by the mask)

sft_bld_upd_mul_8 5,4,2,15  #deg 5
sft_bld_upd_mul_8 5,4,2,14  #deg 4
sft_bld_upd_mul_8 5,4,2,13  #deg 3
sft_bld_upd_mul_8 5,4,2,12  #deg 2
sft_bld_upd_mul_8 5,4,2,11  #deg 1
sft_bld_upd_mul_8 5,4,2,10  #deg 0


#(0,1,2,3,4,5,6,7)
three_hsum 10,11,12,13,14,15,9,7

#now split into high_part/low_part
split_evenodd_16_32 10,8,10,8 6

# reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm10,%ymm8,%ymm10

vmovdqa		%ymm10,(%rdi)

.endm

################################################################################

.macro prod_deg_16_64
product_deg_16_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_16_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_16_16
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
product_deg_16_16
.endm

.macro prod_deg_16_256
prod_deg_16_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
prod_deg_16_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
prod_deg_16_64
add  $32,%rsi
add  $32,%rdi
add  $32,%rdx
prod_deg_16_64
.endm

################################################################################
#                             mod X^32 - Y                                     #
################################################################################
/******************************************************************************/
/*        Honestly got lazy, I am reusing the part of x^16 - Y                */
/*
Use this "graphical" representation to understand
4x4
c0 = a0b0 + a1b3Y + a2b2Y + a3b1Y
c1 = a0b1 + a1b0  + a2b3Y + a3b2Y
c2 = a0b2 + a1b1  + a2b0  + a3b3Y
c3 = a0b3 + a1b2  + a2b1  + a3b0

8x8
c0 = a0b0 + a1b7Y + a2b6Y + a3b5Y |+| a4b4Y + a5b3Y + a6b2Y + a7b1Y
c1 = a0b1 + a1b0  + a2b7Y + a3b6Y |+| a4b5Y + a5b4Y + a6b3Y + a7b2Y
c2 = a0b2 + a1b1  + a2b0  + a3b7Y |+| a4b6Y + a5b5Y + a6b4Y + a7b3Y
c3 = a0b3 + a1b2  + a2b1  + a3b0  |+| a4b7Y + a5b6Y + a6b5Y + a7b4Y
---------------------------------------------------------------------
c4 = a0b4 + a1b3  + a2b2  + a3b1  |+| a4b0  + a5b7Y + a6b6Y + a7b5Y
c5 = a0b5 + a1b4  + a2b3  + a3b2  |+| a4b1  + a5b0  + a6b7Y + a7b6Y
c6 = a0b6 + a1b5  + a2b4  + a3b3  |+| a4b2  + a5b1  + a6b0  + a7b7Y
c7 = a0b7 + a1b6  + a2b5  + a3b4  |+| a4b3  + a5b2  + a6b1  + a7b0
*/
/******************************************************************************/

# this is the product of 2 polynomials of degree 32 (a1,a2)*(b1,b2)=(c1,c2)
# reserve ymm 0,1,6
.macro product_deg_32_32
/******************************************************************************/
/*            TOP-LEFT PART : part of a1*(b1-reversed shift b2Y)              */
/******************************************************************************/

vmovdqa		(%rdi),%ymm2 #a
vmovdqa		(%rsi),%ymm3 #b1
vmovdqa		32(%rsi),%ymm7 #b2
vmovdqa		(%rdx),%ymm4 #ntt_Y (x16)

#create a symmetric version of b1
#swap packs of 4, then reverse them
vpermpd   $0x1B,%ymm3,%ymm5
vpshufhw  $0x1B,%ymm5,%ymm5
vpshuflw  $0x1B,%ymm5,%ymm5

#make the mongtomery product by b2*ntt_Y
vpmullw		%ymm4,%ymm7,%ymm14
vpmulhw		%ymm4,%ymm7,%ymm15
# reduce
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm4

# reorganize the two halves of the shifts
# one vector for the first 8 shifts, the second one for the last 7 shifts.
vperm2i128 $0x12,%ymm3,%ymm4,%ymm7
vperm2i128 $0x01,%ymm4,%ymm4,%ymm4

# start first half (15-8)
vpmaddwd  %ymm2,%ymm5,%ymm15 # deg 15

# operation shift-blend-update / possible 8 times with the same vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm7,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm14 # deg 14

sft_bld_upd_mul_8 5,7,2,13 #deg 13
sft_bld_upd_mul_8 5,7,2,12 #deg 12
sft_bld_upd_mul_8 5,7,2,11 #deg 11
sft_bld_upd_mul_8 5,7,2,10 #deg 10
sft_bld_upd_mul_8 5,7,2,9  #deg 19
sft_bld_upd_mul_8 5,7,2,8  #deg 8

#(8,9,10,11,12,13,14,15)
three_hsum 8,9,10,11,12,13,14,15

sft_bld_upd_mul_8 5,7,2,7  #deg 7

# operation shift-blend-update / apply 7 times with the new vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm4,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm9 #deg 6 (ymm6 is taken by the mask)

sft_bld_upd_mul_8 5,4,2,15  #deg 5
sft_bld_upd_mul_8 5,4,2,14  #deg 4
sft_bld_upd_mul_8 5,4,2,13  #deg 3
sft_bld_upd_mul_8 5,4,2,12  #deg 2
sft_bld_upd_mul_8 5,4,2,11  #deg 1
sft_bld_upd_mul_8 5,4,2,10  #deg 0


#(0,1,2,3,4,5,6,7)
three_hsum 10,11,12,13,14,15,9,7

#now split into high_part/low_part
split_evenodd_16_32 10,8,10,8 6

# reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm10,%ymm8,%ymm10

vmovdqa		%ymm10,(%rdi)

/******************************************************************************/
/*            BOT-LEFT PART :  a1*(b2-reversed shift b1)                     */
/******************************************************************************/
/* register ymm2 still contains the original a1, use it for a1,b1 */
vmovdqa		(%rsi),%ymm7 #b1
vmovdqa		32(%rsi),%ymm3 #b2
/* vmovdqa		(%rdx),%ymm4 #ntt_Y (x16) */ /* ntt_Y is not needed */

#create a symmetric version of b2
#swap packs of 4, then reverse them
vpermpd   $0x1B,%ymm3,%ymm5
vpshufhw  $0x1B,%ymm5,%ymm5
vpshuflw  $0x1B,%ymm5,%ymm5

// #make the mongtomery product by b1*ntt_Y
// vpmullw		%ymm4,%ymm7,%ymm14
// vpmulhw		%ymm4,%ymm7,%ymm15
// # reduce
// vpmullw		%ymm0,%ymm14,%ymm14
// vpmulhw		%ymm1,%ymm14,%ymm14
// vpsubw		%ymm14,%ymm15,%ymm4
/* no montgomery product on this part */
vmovdqa  %ymm7,%ymm4

# reorganize the two halves of the shifts
# one vector for the first 8 shifts, the second one for the last 7 shifts.
vperm2i128 $0x12,%ymm3,%ymm4,%ymm7
vperm2i128 $0x01,%ymm4,%ymm4,%ymm4

# start first half (15-8)
vpmaddwd  %ymm2,%ymm5,%ymm15 # deg 15

# operation shift-blend-update / possible 8 times with the same vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm7,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm14 # deg 14

sft_bld_upd_mul_8 5,7,2,13 #deg 13
sft_bld_upd_mul_8 5,7,2,12 #deg 12
sft_bld_upd_mul_8 5,7,2,11 #deg 11
sft_bld_upd_mul_8 5,7,2,10 #deg 10
sft_bld_upd_mul_8 5,7,2,9  #deg 19
sft_bld_upd_mul_8 5,7,2,8  #deg 8

#(8,9,10,11,12,13,14,15)
three_hsum 8,9,10,11,12,13,14,15

sft_bld_upd_mul_8 5,7,2,7  #deg 7

# operation shift-blend-update / apply 7 times with the new vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm4,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm9 #deg 6 (ymm6 is taken by the mask)

sft_bld_upd_mul_8 5,4,2,15  #deg 5
sft_bld_upd_mul_8 5,4,2,14  #deg 4
sft_bld_upd_mul_8 5,4,2,13  #deg 3
sft_bld_upd_mul_8 5,4,2,12  #deg 2
sft_bld_upd_mul_8 5,4,2,11  #deg 1
sft_bld_upd_mul_8 5,4,2,10  #deg 0


#(0,1,2,3,4,5,6,7)
three_hsum 10,11,12,13,14,15,9,7

#now split into high_part/low_part
split_evenodd_16_32 10,8,10,8 6

# reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm10,%ymm8,%ymm10

/* Before we overwrite a2, recover it as the new fixed ymm2 */
vmovdqa		32(%rdi),%ymm2
/*Now overwrite*/
vmovdqa		%ymm10,32(%rdi) /* BOT-LEFT PART is part of c2*/

/******************************************************************************/
/*          TOP-RIGHT PART :  a2*(b2Y-reversed shift b1Y)                     */
/******************************************************************************/
vmovdqa		(%rsi),%ymm3 #b1
vmovdqa		32(%rsi),%ymm7 #b2
vmovdqa		(%rdx),%ymm4 #ntt_Y (x16)

#make the mongtomery product by b2*ntt_Y
#make the mongtomery product by b1*ntt_Y
vpmullw		%ymm4,%ymm7,%ymm12
vpmulhw		%ymm4,%ymm7,%ymm13
vpmullw		%ymm4,%ymm3,%ymm14
vpmulhw		%ymm4,%ymm3,%ymm15
# reduce
vpmullw		%ymm0,%ymm12,%ymm12
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm12,%ymm12
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm12,%ymm13,%ymm7
vpsubw		%ymm14,%ymm15,%ymm4

#create a symmetric version of b2*ntt_Y
#swap packs of 4, then reverse them
vpermpd   $0x1B,%ymm7,%ymm5
vpshufhw  $0x1B,%ymm5,%ymm5
vpshuflw  $0x1B,%ymm5,%ymm5

# reorganize the two halves of the shifts
# one vector for the first 8 shifts, the second one for the last 7 shifts.
vperm2i128 $0x12,%ymm7,%ymm4,%ymm7
// vperm2i128 $0x12,%ymm4,%ymm4,%ymm7
vperm2i128 $0x01,%ymm4,%ymm4,%ymm4

# start first half (15-8)
vpmaddwd  %ymm2,%ymm5,%ymm15 # deg 15

# operation shift-blend-update / possible 8 times with the same vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm7,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm14 # deg 14

sft_bld_upd_mul_8 5,7,2,13 #deg 13
sft_bld_upd_mul_8 5,7,2,12 #deg 12
sft_bld_upd_mul_8 5,7,2,11 #deg 11
sft_bld_upd_mul_8 5,7,2,10 #deg 10
sft_bld_upd_mul_8 5,7,2,9  #deg 19
sft_bld_upd_mul_8 5,7,2,8  #deg 8

#(8,9,10,11,12,13,14,15)
three_hsum 8,9,10,11,12,13,14,15

sft_bld_upd_mul_8 5,7,2,7  #deg 7

# operation shift-blend-update / apply 7 times with the new vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm4,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm9 #deg 6 (ymm6 is taken by the mask)

sft_bld_upd_mul_8 5,4,2,15  #deg 5
sft_bld_upd_mul_8 5,4,2,14  #deg 4
sft_bld_upd_mul_8 5,4,2,13  #deg 3
sft_bld_upd_mul_8 5,4,2,12  #deg 2
sft_bld_upd_mul_8 5,4,2,11  #deg 1
sft_bld_upd_mul_8 5,4,2,10  #deg 0


#(0,1,2,3,4,5,6,7)
three_hsum 10,11,12,13,14,15,9,7

#now split into high_part/low_part
split_evenodd_16_32 10,8,10,8 6

# reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm10,%ymm8,%ymm10

/* Need to do a sum before saving */
vmovdqa   (%rdi),%ymm7
vpaddw    %ymm7,%ymm10,%ymm10

/* warning the output is between 0 and 2q*/
vmovdqa		%ymm10,(%rdi)

/******************************************************************************/
/*          BOT-RIGHT PART :  a2*(b1-reversed shift b2Y)                     */
/******************************************************************************/
vmovdqa		(%rsi),%ymm3 #b1
vmovdqa		32(%rsi),%ymm7 #b2
vmovdqa		(%rdx),%ymm4 #ntt_Y (x16)

#create a symmetric version of b1
#swap packs of 4, then reverse them
vpermpd   $0x1B,%ymm3,%ymm5
vpshufhw  $0x1B,%ymm5,%ymm5
vpshuflw  $0x1B,%ymm5,%ymm5

#make the mongtomery product by b2*ntt_Y
vpmullw		%ymm4,%ymm7,%ymm14
vpmulhw		%ymm4,%ymm7,%ymm15
# reduce
vpmullw		%ymm0,%ymm14,%ymm14
vpmulhw		%ymm1,%ymm14,%ymm14
vpsubw		%ymm14,%ymm15,%ymm4

# reorganize the two halves of the shifts
# one vector for the first 8 shifts, the second one for the last 7 shifts.
vperm2i128 $0x12,%ymm3,%ymm4,%ymm7
vperm2i128 $0x01,%ymm4,%ymm4,%ymm4

# start first half (15-8)
vpmaddwd  %ymm2,%ymm5,%ymm15 # deg 15

# operation shift-blend-update / possible 8 times with the same vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm7,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm14 # deg 14

sft_bld_upd_mul_8 5,7,2,13 #deg 13
sft_bld_upd_mul_8 5,7,2,12 #deg 12
sft_bld_upd_mul_8 5,7,2,11 #deg 11
sft_bld_upd_mul_8 5,7,2,10 #deg 10
sft_bld_upd_mul_8 5,7,2,9  #deg 19
sft_bld_upd_mul_8 5,7,2,8  #deg 8

#(8,9,10,11,12,13,14,15)
three_hsum 8,9,10,11,12,13,14,15

sft_bld_upd_mul_8 5,7,2,7  #deg 7

# operation shift-blend-update / apply 7 times with the new vector
vpsrldq   $2,%ymm5,%ymm5
vpblendw  $0x80,%ymm4,%ymm5,%ymm5
vpmaddwd  %ymm2,%ymm5,%ymm9 #deg 6 (ymm6 is taken by the mask)

sft_bld_upd_mul_8 5,4,2,15  #deg 5
sft_bld_upd_mul_8 5,4,2,14  #deg 4
sft_bld_upd_mul_8 5,4,2,13  #deg 3
sft_bld_upd_mul_8 5,4,2,12  #deg 2
sft_bld_upd_mul_8 5,4,2,11  #deg 1
sft_bld_upd_mul_8 5,4,2,10  #deg 0


#(0,1,2,3,4,5,6,7)
three_hsum 10,11,12,13,14,15,9,7

#now split into high_part/low_part
split_evenodd_16_32 10,8,10,8 6

# reduce
vpmullw		%ymm0,%ymm10,%ymm10
vpmulhw		%ymm1,%ymm10,%ymm10
vpsubw		%ymm10,%ymm8,%ymm10

/* Need to do a sum before saving */
vmovdqa   32(%rdi),%ymm7
vpaddw    %ymm7,%ymm10,%ymm10

/* warning the output is between 0 and 2q*/
vmovdqa		%ymm10,32(%rdi)

.endm

################################################################################

.macro prod_deg_32_64
product_deg_32_32
add  $64,%rsi
add  $64,%rdi
add  $32,%rdx
product_deg_32_32
.endm

.macro prod_deg_32_128
prod_deg_32_64
add  $64,%rsi
add  $64,%rdi
add  $32,%rdx
prod_deg_32_64
.endm

.macro prod_deg_32_256
prod_deg_32_128
add  $64,%rsi
add  $64,%rdi
add  $32,%rdx
prod_deg_32_128
.endm

.macro prod_deg_32_512
prod_deg_32_256
add  $64,%rsi
add  $64,%rdi
add  $32,%rdx
prod_deg_32_256
.endm

################################################################################
