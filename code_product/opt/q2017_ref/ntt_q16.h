#ifndef NTT16
#define NTT16

#include <stdio.h>

#include "reduce_q16.h"

/* static const uint16_t tree[16] = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 }; */


/* zetas[i] = M*g^tree[i] with g = 227 */
const int16_t zetas_16[16] ={
  992, 753, 1043, 1176, 1954, 308, 1782, 1373, 1297, 1503, 772, 708, 1835, 1338, 1114, 1053 };

/* inv_zetas[i] = M*g^(-tree[i]-1)%q */
const int16_t inv_zetas_16[16] = {
  964, 903, 679, 182, 1309, 1245, 514, 720, 644, 235, 1709, 63, 841, 974, 1264, 1025 };

/* ntt_Y stored in Montgonery form (useful for Karatsuba reconstruction)*/
const int16_t ntt_Y_16[16] __attribute__((aligned(32))) = { 1809, 1608, 284, 749, 1632, 1692, 465, 1630, 929, 426, 147, 925, 644, 1983, 1134, 79 };

/* ################ NTT ################ */
/* ntt functions ntt_q16__(size of input) */

/* input array of size 64 */
void ntt_q16_64(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 64/2;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 64 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q16((int32_t)c[j+l]*zetas_16[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 64 ; s++)
    c[s] = barrett_q16(c[s]);
}

/* input array of size 128 */
void ntt_q16_128(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 128/2;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 128 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q16((int32_t)c[j+l]*zetas_16[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 128 ; s++)
    c[s] = barrett_q16(c[s]);
}


/* input array of size 256 */
void ntt_q16_256(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 256/2;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 256 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q16((int32_t)c[j+l]*zetas_16[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 256 ; s++)
    c[s] = barrett_q16(c[s]);
}

/* input array of size 512 */
void ntt_q16_512(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 512/2;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 512 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q16((int32_t)c[j+l]*zetas_16[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 512 ; s++)
    c[s] = barrett_q16(c[s]);
}

/* input array of size 1024 */
void ntt_q16_1024(int16_t *c){

  unsigned int l, s, i, j, k = 1;

  int16_t t, u;

  l = 1024/2;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 1024 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = montgomery_reduce_q16((int32_t)c[j+l]*zetas_16[k]);

	/* No reductions -> be lazy */
	u = c[j];
	c[j+l] = u - t;
	c[j] = u + t;
      }
      k++;
    }
    l >>=1;
  }

  for(s = 0 ; s < 1024 ; s++)
    c[s] = barrett_q16(c[s]);
}

/* ################ INVNTT ################ */

void invntt_q16_64(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<2;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 64 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];
	c[j] = t + c[j+l];
	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q16((int32_t)c[j+l]*inv_zetas_16[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 64 ; j++){
    c[j] = montgomery_reduce_q16((int32_t) c[j]*F16); /* F16 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q16;
  }
}


void invntt_q16_128(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<3;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 128 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];
	c[j] = t + c[j+l];
	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q16((int32_t)c[j+l]*inv_zetas_16[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 128 ; j++){
    c[j] = montgomery_reduce_q16((int32_t) c[j]*F16); /* F16 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q16;
  }
}

void invntt_q16_256(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<4;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 256 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];
	c[j] = t + c[j+l];
	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q16((int32_t)c[j+l]*inv_zetas_16[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 256 ; j++){
    c[j] = montgomery_reduce_q16((int32_t) c[j]*F16); /* F16 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q16;
  }
}

void invntt_q16_512(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<5;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 512 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];
	c[j] = t + c[j+l];
	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q16((int32_t)c[j+l]*inv_zetas_16[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 512 ; j++){
    c[j] = montgomery_reduce_q16((int32_t) c[j]*F16); /* F16 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q16;
  }
}

void invntt_q16_1024(int16_t *c){

  unsigned int l, s, i, j, t, k = 0;

  l = 1<<6;
  for(i = 0 ; i < 4 ; i++){
    for(s = 0 ; s < 1024 ; s = j+l){
      for(j = s ; j < s+l ; j++){
	t = c[j];
	c[j] = t + c[j+l];
	c[j+l] = t - c[j+l];
	c[j+l] = montgomery_reduce_q16((int32_t)c[j+l]*inv_zetas_16[k]);
      }
      k++;
    }
    l<<=1;
  }

  for(j = 0 ; j < 1024 ; j++){
    c[j] = montgomery_reduce_q16((int32_t) c[j]*F16); /* F16 = (mont^2/n)%q*/
    c[j] += (c[j]>>15) & Q16;
  }
}

#endif
