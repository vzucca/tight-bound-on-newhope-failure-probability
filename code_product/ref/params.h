#ifndef PARAMS
#define PARAMS

#include <stdint.h>

#define N 512

#define Q769 769

#define Q16 2017

#define Q32 1601

#define Q64 1409

#define Q128 3329

#define Q256 7681

#define Q1024 12289

#endif
