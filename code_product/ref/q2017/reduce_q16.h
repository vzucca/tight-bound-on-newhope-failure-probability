#ifndef REDUCE16
#define REDUCE16

#include <stdio.h>

/* ############################################################################################## */
/* ################################ Functions for Q16 = 2017 ################################### */
/* ################################ ]-16q,16q[ C ]-2^15,2^15[ ################################### */
/* ############################################################################################## */
/* Works only for 2017 = 2^11 - 2^5 + 1 */
/* Input in [-2^15, 2^15), output in [-496,2513) = [-2^15-16q, 2^15-15q)*/
int16_t signed_mers_q16(int16_t a){

  int16_t t, u;

  t = a>>11;

  u = a & ((1<<11)-1);

  u -= t;

  return u + (t<<5);
}


/* v128 = 2^25/q16 + 1*/
const int16_t v16 = 16636;

/* Input in [-2^15, 2^15), output in [0,q) */
int16_t barrett_q16(int16_t a){
  
  int16_t t;

  t = ((int32_t) v16*a)>>25;

  t *= Q16;

  return a - t;   
}

/* Q^(-1) mod M where M = 2^16 */
const int16_t Q16INV = 31777;

int16_t montgomery_reduce_q16(int32_t a)
{
  int32_t t;
  int16_t u;

  u = a * Q16INV;
  
  t = (int32_t)u * Q16;
  t = a - t;
  t >>= 16;
  
  return t;
}

void poly_barrettq16(int16_t *r, int16_t *a, unsigned int n){
  for(unsigned int i = 0 ; i < n ; i++)
    r[i] = barrett_q16(a[i]);
}

void poly_mersq16(int16_t *r, int16_t *a, unsigned int n){
  for(unsigned int i = 0 ; i < n ; i++)
    r[i] = signed_mers_q16(a[i]);
}

/* coefficient-wise multiplication */
void pointwise_mul_q16(int16_t *c, const int16_t *a, const int16_t* b, unsigned int n){

  for(unsigned int i = 0 ; i < n ; i++)
    c[i] = montgomery_reduce_q16((int32_t)a[i]*b[i]); /* Output ab*M^(-1) */
  
}

/* mutiplication of size_input coefficients as if they were polynomials of size size_poly modulo X^deg_poly-1 - zeta*/
void poly_mul_modq16(int16_t *c, const int16_t *a, const int16_t* b, const unsigned int size_input, const unsigned int size_poly, const int16_t *zeta){

  int32_t tmp;
  int16_t c_tmp[size_poly];
  
  for(unsigned int i = 0 ; i < size_input/size_poly ; i++){
    for(unsigned int j = 0 ; j < size_poly ; j++){
      tmp = 0;
      for(unsigned int k = j+1 ; k < size_poly ; k++)
	tmp += a[i*size_poly + k]*b[i*size_poly + size_poly - k + j];
      
      c_tmp[j] = montgomery_reduce_q16(tmp);
      tmp = c_tmp[j]*zeta[i];
      
      for(unsigned int k = 0 ; k <= j ; k++)
	tmp += a[i*size_poly + k]*b[i*size_poly + j-k];
      
      c_tmp[j] = montgomery_reduce_q16(tmp);
    }
    for(unsigned int j = 0 ; j < size_poly ; j++)
      c[i*size_poly + j] = c_tmp[j];
  } 
}

/* Compute a*X mod X^size_poly - zeta^i on a block of size_input coefficients */
void poly_shift_modq16(int16_t *c, const int16_t *a, const unsigned int size_input, const unsigned int size_poly, const int16_t *zeta){

  int16_t tmp;
  
  for(unsigned int i = 0 ; i < size_input/size_poly ; i++){
    tmp = montgomery_reduce_q16( (int32_t)a[i*size_poly + size_poly-1]*zeta[i]);
    for(unsigned int j = size_poly-1 ; j >=1 ; j--)
      c[i*size_poly + j] = a[i*size_poly + j-1];
    c[i*size_poly] = tmp;
  }
}

#endif
