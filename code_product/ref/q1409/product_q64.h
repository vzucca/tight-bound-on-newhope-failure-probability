#ifndef KARATSUBA64
#define KARATSUBA64

#include <stdint.h>

#include "reduce_q64.h"
#include "ntt_q64.h"

/*########################################### n = 512 ############################################*/
/*########################################## q = 1409 ############################################*/

void poly_product_512_K0_P3(int16_t *c, const int16_t *a, const int16_t *b){
  int16_t a_tmp[N], b_tmp[N];

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  nttq64_512(a_tmp);
  nttq64_512(b_tmp);

  poly_mul_modq64(c, a_tmp, b_tmp, N, 1<<3, ntt_Y_64);

  invnttq64_512(c);
}

void poly_product_512_K1_P2(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2], a1[N/2], a01[N/2];
  int16_t b0[N/2], b1[N/2], b01[N/2];
  int16_t c0[N/2], c1[N/2], c2[N/2];

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  nttq64_256(a0);
  nttq64_256(a1);
  nttq64_256(b0);
  nttq64_256(b1);
  
  /* Output of the ntt is in [0, q) */
  poly_add(a01,a0,a1,N/2);
  poly_add(b01,b0,b1,N/2);

  /* Polynomial products */
  poly_mul_modq64(c0, a0, b0, N/2, 1<<2, ntt_Y_64);
  poly_mul_modq64(c1, a01, b01, N/2, 1<<2, ntt_Y_64);
  poly_mul_modq64(c2, a1, b1, N/2, 1<<2, ntt_Y_64);
  
  /* Reconstruction c1 <- c1 - c0 - c2 in ]-3q,3q[ */
  poly_sub(c1,c1,c0,N/2); poly_sub(c1,c1,c2,N/2);
  /* Reconstruction c0 <- c0 + c2*ntt_Y  ]-2q,2q[*/
  poly_shift_modq64(c2,c2,N/2,1<<2,ntt_Y_64); poly_add(c0,c0,c2,N/2);

  /* Reduction before invntt */
  poly_barrettq64(c0,c0,N/2);
  poly_barrettq64(c1,c1,N/2);
  
  invnttq64_256(c0);
  invnttq64_256(c1);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = c0[i];
    c[2*i+1] = c1[i];
  }
}

void poly_product_512_K2_P1(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4], a1[N/4], a2[N/4], a3[N/4], a01[N/4], a23[N/4], a02[N/4], a13[N/4], a0123[N/4];

  int16_t b0[N/4], b1[N/4], b2[N/4], b3[N/4], b01[N/4], b23[N/4], b02[N/4], b13[N/4], b0123[N/4];

  int16_t c0[N/4], c1[N/4], c2[N/4], c3[N/4], c01[N/4], c23[N/4], c02[N/4], c13[N/4], c0123[N/4];

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  nttq64_128(a0); nttq64_128(a1); nttq64_128(a2); nttq64_128(a3);
  nttq64_128(b0); nttq64_128(b1); nttq64_128(b2); nttq64_128(b3);
  /* Output of ntt128 in [0,q) */
  
  /* Start computing the sums needed for Karatsuba */
  poly_add(a01,a0,a1,N/4);
  poly_add(a23,a2,a3,N/4);

  poly_add(a02,a0,a2,N/4);
  poly_add(a13,a1,a3,N/4);
  poly_add(a0123,a02,a13,N/4);

  poly_add(b01,b0,b1,N/4);
  poly_add(b23,b2,b3,N/4);

  poly_add(b02,b0,b2,N/4);
  poly_add(b13,b1,b3,N/4);
  poly_add(b0123,b02,b13,N/4);
  /* Output of the sums in [0,2q) or [0,4q)*/
  
  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  poly_mul_modq64(c0,a0,b0,N/4,1<<1,ntt_Y_64);
  poly_mul_modq64(c1,a1,b1,N/4,1<<1,ntt_Y_64);
  poly_mul_modq64(c01,a01,b01,N/4,1<<1,ntt_Y_64);

  poly_mul_modq64(c2,a2,b2,N/4,1<<1,ntt_Y_64);
  poly_mul_modq64(c3,a3,b3,N/4,1<<1,ntt_Y_64);
  poly_mul_modq64(c23,a23,b23,N/4,1<<1,ntt_Y_64);

  poly_mul_modq64(c02,a02,b02,N/4,1<<1,ntt_Y_64);
  poly_mul_modq64(c13,a13,b13,N/4,1<<1,ntt_Y_64);

  poly_mul_modq64(c0123,a0123,b0123,N/4,1<<1,ntt_Y_64);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* c02 <- c02 - c0 - c2 ==> c02 in ]-3q,3q[ */
  poly_sub(c02,c02,c0,N/4); poly_sub(c02,c02,c2,N/4);
  /* c13 <- c13 - c1 - c3 ==> c13 in ]-3q,3q[ */
  poly_sub(c13,c13,c1,N/4); poly_sub(c13,c13,c3,N/4);
  /* c0123 <- c0123 - c01 - c23 ==> c0123 in ]-3q,3q[ */
  poly_sub(c0123,c0123,c01,N/4); poly_sub(c0123,c0123,c23,N/4);
  /* c0 <- c0 + c2*ntt_Y ==> c0 in ]-2q,2q[ = ]-3q,3q[ */
  poly_shift_modq64(c2,c2,N/4,1<<1,ntt_Y_64); poly_add(c0,c0,c2,N/4);
  /* c1 <- c1 + c3*ntt_Y ==> c1 in ]-3q,3q[ */
  poly_shift_modq64(c3,c3,N/4,1<<1,ntt_Y_64); poly_add(c1,c1,c3,N/4);
  /* c01 <- c01 + c23*ntt_Y ==> c01 in ]-3q,3q[ */
  poly_shift_modq64(c23,c23,N/4,1<<1,ntt_Y_64); poly_add(c01,c01,c23,N/4);

  /* Reconstruct second level of Karatsuba */
  /* c01 <- c01 - c0 - c1 ==> in ]-6q, 6q[ */
  poly_sub(c01,c01,c0,N/4); poly_sub(c01,c01,c1,N/4);
  /* c0123 <- c0123 - c02 - c13 ==> in ]-9q, 9q[ */
  poly_sub(c0123,c0123,c02,N/4); poly_sub(c0123,c0123,c13,N/4);
  /* c0 <- c0 + c13*ntt_Y ==> in ]-3q,3q[ */
  poly_shift_modq64(c13,c13,N/4,1<<1,ntt_Y_64); poly_add(c0,c0,c13,N/4);
  /* c1 <- c1 + c02 ==> in ]-5q,5q[ */
  poly_add(c1,c1,c02,N/4);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq64(c01,c01,N/4);
  poly_barrettq64(c0123,c0123,N/4);
  poly_barrettq64(c0,c0,N/4);
  poly_barrettq64(c1,c1,N/4);

  invnttq64_128(c0);
  invnttq64_128(c1);
  invnttq64_128(c01);
  invnttq64_128(c0123);
  
  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = c0[i];
    c[4*i+1] = c01[i];
    c[4*i+2] = c1[i];
    c[4*i+3] = c0123[i];
  }
}



void poly_product_512_K3_P0(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N>>3], a1[N>>3], a2[N>>3], a3[N>>3], a4[N>>3], a5[N>>3], a6[N>>3], a7[N>>3], a04[N>>3], a26[N>>3], a02[N>>3], a46[N>>3], a0246[N>>3], a15[N>>3], a37[N>>3], a13[N>>3], a57[N>>3], a1357[N>>3], a01[N>>3], a45[N>>3], a0145[N>>3], a23[N>>3], a67[N>>3], a2367[N>>3], a0123[N>>3], a4567[N>>3], a01234567[N>>3];

  int16_t b0[N>>3], b1[N>>3], b2[N>>3], b3[N>>3], b4[N>>3], b5[N>>3], b6[N>>3], b7[N>>3], b04[N>>3], b26[N>>3], b02[N>>3], b46[N>>3], b0246[N>>3], b15[N>>3], b37[N>>3], b13[N>>3], b57[N>>3], b1357[N>>3], b01[N>>3], b45[N>>3], b0145[N>>3], b23[N>>3], b67[N>>3], b2367[N>>3], b0123[N>>3], b4567[N>>3], b01234567[N>>3];

  int16_t c0[N>>3], c1[N>>3], c2[N>>3], c3[N>>3], c4[N>>3], c5[N>>3], c6[N>>3], c7[N>>3], c04[N>>3], c26[N>>3], c02[N>>3], c46[N>>3], c0246[N>>3], c15[N>>3], c37[N>>3], c13[N>>3], c57[N>>3], c1357[N>>3], c01[N>>3], c45[N>>3], c0145[N>>3], c23[N>>3], c67[N>>3], c2367[N>>3], c0123[N>>3], c4567[N>>3], c01234567[N>>3];

  for(unsigned int i = 0 ; i < N/8 ; i++){
    a0[i] = a[8*i]; a1[i] = a[8*i+1]; a2[i] = a[8*i+2]; a3[i] = a[8*i+3];
    a4[i] = a[8*i+4]; a5[i] = a[8*i+5]; a6[i] = a[8*i+6]; a7[i] = a[8*i+7];
    
    b0[i] = b[8*i]; b1[i] = b[8*i+1]; b2[i] = b[8*i+2]; b3[i] = b[8*i+3];
    b4[i] = b[8*i+4]; b5[i] = b[8*i+5]; b6[i] = b[8*i+6]; b7[i] = b[8*i+7];
  }

  nttq64_64(a0); nttq64_64(a1); nttq64_64(a2); nttq64_64(a3);
  nttq64_64(a4); nttq64_64(a5); nttq64_64(a6); nttq64_64(a7);
  
  nttq64_64(b0); nttq64_64(b1); nttq64_64(b2); nttq64_64(b3);
  nttq64_64(b4); nttq64_64(b5); nttq64_64(b6); nttq64_64(b7);
  /* Output of nttq16 in ]-q,q[ */

  /* Compute the different sums and reduce them for the Montgomery multiplications to come */
  poly_add(a04,a0,a4,N/8); poly_add(a26,a2,a6,N/8);
  poly_add(a02,a0,a2,N/8); poly_add(a46,a4,a6,N/8);

  poly_add(a15,a1,a5,N/8); poly_add(a37,a3,a7,N/8);
  poly_add(a13,a1,a3,N/8); poly_add(a57,a5,a7,N/8);

  poly_add(a01,a0,a1,N/8); poly_add(a45,a4,a5,N/8);
  poly_add(a23,a2,a3,N/8); poly_add(a67,a6,a7,N/8);

  poly_add(a0246,a02,a46,N/8); poly_add(a1357,a13,a57,N/8);
  poly_add(a0145,a01,a45,N/8); poly_add(a2367,a23,a67,N/8);
  poly_add(a0123,a01,a23,N/8); poly_add(a4567,a45,a67,N/8);

  poly_add(a01234567,a0123,a4567,N/8);

  poly_add(b04,b0,b4,N/8); poly_add(b26,b2,b6,N/8);
  poly_add(b02,b0,b2,N/8); poly_add(b46,b4,b6,N/8);

  poly_add(b15,b1,b5,N/8); poly_add(b37,b3,b7,N/8);
  poly_add(b13,b1,b3,N/8); poly_add(b57,b5,b7,N/8);

  poly_add(b01,b0,b1,N/8); poly_add(b45,b4,b5,N/8);
  poly_add(b23,b2,b3,N/8); poly_add(b67,b6,b7,N/8);

  poly_add(b0246,b02,b46,N/8); poly_add(b1357,b13,b57,N/8);
  poly_add(b0145,b01,b45,N/8); poly_add(b2367,b23,b67,N/8);
  poly_add(b0123,b01,b23,N/8); poly_add(b4567,b45,b67,N/8);

  poly_add(b01234567,b0123,b4567,N/8);
  /* Output in [0,2q), [0,4q) or  [0,8q) need to reduce [0,8q) to [-4q,4q) otherwise the product won't fit in ]-q*2^15,q*2^15[*/

  /* Compute the products with Montgomery (Montgomery factor will be removed with invntt)*/
  pointwise_mul_q64(c0,a0,b0,N/8); pointwise_mul_q64(c1,a1,b1,N/8);
  pointwise_mul_q64(c2,a2,b2,N/8); pointwise_mul_q64(c3,a3,b3,N/8);
  pointwise_mul_q64(c4,a4,b4,N/8); pointwise_mul_q64(c5,a5,b5,N/8);
  pointwise_mul_q64(c6,a6,b6,N/8); pointwise_mul_q64(c7,a7,b7,N/8);

  pointwise_mul_q64(c04,a04,b04,N/8); pointwise_mul_q64(c26,a26,b26,N/8);
  pointwise_mul_q64(c02,a02,b02,N/8); pointwise_mul_q64(c46,a46,b46,N/8);
  pointwise_mul_q64(c0246,a0246,b0246,N/8);

  pointwise_mul_q64(c15,a15,b15,N/8); pointwise_mul_q64(c37,a37,b37,N/8);
  pointwise_mul_q64(c13,a13,b13,N/8); pointwise_mul_q64(c57,a57,b57,N/8);
  pointwise_mul_q64(c1357,a1357,b1357,N/8);

  pointwise_mul_q64(c01,a01,b01,N/8); pointwise_mul_q64(c45,a45,b45,N/8);
  pointwise_mul_q64(c0145,a0145,b0145,N/8);

  pointwise_mul_q64(c23,a23,b23,N/8); pointwise_mul_q64(c67,a67,b67,N/8);
  pointwise_mul_q64(c2367,a2367,b2367,N/8);

  pointwise_mul_q64(c0123,a0123,b0123,N/8); pointwise_mul_q64(c4567,a4567,b4567,N/8);

  /* [0,8q) to [-4q,4q) */
  poly_sub_cst(a01234567,a01234567,4*Q64,N/8); poly_sub_cst(b01234567,b01234567,4*Q64,N/8);
  pointwise_mul_q64(c01234567,a01234567,b01234567,N/8);
  /*############################## Output in ]-q,q[ = ]-1409,1409[ ###############################*/

  /* Start the reconstruction of Karatsuba... */
  /* Begin 1st level */
  
  /* c04 <- c04 - c0 - c4 in ]-3q,3q[*/
  poly_sub(c04,c04,c0,N/8); poly_sub(c04,c04,c4,N/8);
  /* c26 <- c26 - c2 - c6 in ]-3q,3q[ */
  poly_sub(c26,c26,c2,N/8); poly_sub(c26,c26,c6,N/8);
  /* c0246 <- c0246 - c02 - c46 in ]-3q,3q[ */
  poly_sub(c0246,c0246,c02,N/8); poly_sub(c0246,c0246,c46,N/8);

  /* c0 <- c0 + c4*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c4,c4,ntt_Y_64,N/8); poly_add(c0,c0,c4,N/8);
  /* c2 <- c2 + c6*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c6,c6,ntt_Y_64,N/8); poly_add(c2,c2,c6,N/8);
  /* c02 <- c02 + c46*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c46,c46,ntt_Y_64,N/8); poly_add(c02,c02,c46,N/8);

  /* c15 <- c15 - c1 - c5 in ]-3q,3q[*/
  poly_sub(c15,c15,c1,N/8); poly_sub(c15,c15,c5,N/8);
  /* c37 <- c37 - c3 - c7 in ]-3q,3q[ */
  poly_sub(c37,c37,c3,N/8); poly_sub(c37,c37,c7,N/8);
  /* c1357 <- c1357 - c13 - c57 in ]-3q,3q[ */
  poly_sub(c1357,c1357,c13,N/8); poly_sub(c1357,c1357,c57,N/8);

  /* c1 <- c1 + c5*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c5,c5,ntt_Y_64,N/8); poly_add(c1,c1,c5,N/8);
  /* c3 <- c3 + c7*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c7,c7,ntt_Y_64,N/8); poly_add(c3,c3,c7,N/8);
  /* c13 <- c13 + c57*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c57,c57,ntt_Y_64,N/8); poly_add(c13,c13,c57,N/8);

  /* c0145 <- c0145 - c01 - c45 in ]-3q,3q[*/
  poly_sub(c0145,c0145,c01,N/8); poly_sub(c0145,c0145,c45,N/8);
  /* c2367 <- c2367 - c23 - c67 in ]-3q,3q[ */
  poly_sub(c2367,c2367,c23,N/8); poly_sub(c2367,c2367,c67,N/8);
  /* c01234567 <- c01234567 - c0123 - c4567 in ]-3q,3q[ */
  poly_sub(c01234567,c01234567,c0123,N/8); poly_sub(c01234567,c01234567,c4567,N/8);
  
  /* c01 <- c01 + c45*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c45,c45,ntt_Y_64,N/8); poly_add(c01,c01,c45,N/8);
  /* c23 <- c23 + c67*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c67,c67,ntt_Y_64,N/8); poly_add(c23,c23,c67,N/8);
  /* c0123 <- c0123 + c4567*ntt_Y in ]-2q, 2q[ */
  pointwise_mul_q64(c4567,c4567,ntt_Y_64,N/8); poly_add(c0123,c0123,c4567,N/8);
  
  /* End 1st level */
  /*##############################################################################################*/
  /* Begin 2nd level */

  /* c02 <- c02 - c0 - c2 in ]-6q,6q[*/
  poly_sub(c02,c02,c0,N/8); poly_sub(c02,c02,c2,N/8);
  /* c0246 <- c0246 - c04 - c26 in ]-9q,9q[ */
  poly_sub(c0246,c0246,c04,N/8); poly_sub(c0246,c0246,c26,N/8);
  
  /* c13 <- c13 - c1 - c3 in ]-6q,6q[*/
  poly_sub(c13,c13,c1,N/8); poly_sub(c13,c13,c3,N/8);
  /* c1357 <- c1357 - c15 - c37 in ]-9q,9q[ */
  poly_sub(c1357,c1357,c15,N/8); poly_sub(c1357,c1357,c37,N/8);

  /* c0123 <- c0123 - c01 - c23 in ]-6q,6q[*/
  poly_sub(c0123,c0123,c01,N/8); poly_sub(c0123,c0123,c23,N/8);
  /* c01234567 <- c01234567 - c0145 - c2367 in ]-9q,9q[ */
  poly_sub(c01234567,c01234567,c0145,N/8); poly_sub(c01234567,c01234567,c2367,N/8);
  
  /* c0 <- c0 + c26*ntt_Y in ]-3q, 3q[ */
  pointwise_mul_q64(c26,c26,ntt_Y_64,N/8); poly_add(c0,c0,c26,N/8);
  /* c1 <- c1 + c37*ntt_Y in ]-3q, 3q[ */
  pointwise_mul_q64(c37,c37,ntt_Y_64,N/8); poly_add(c1,c1,c37,N/8);
  /* c01 <- c01 + c2367*ntt_Y in ]-3q, 3q[ */
  pointwise_mul_q64(c2367,c2367,ntt_Y_64,N/8); poly_add(c01,c01,c2367,N/8);

  /* c04 <- c04 + c2 in ]-5q,5q[ */
  poly_add(c04,c04,c2,N/8);
  /* c15 <- c15 + c3 in ]-5q,5q[ */
  poly_add(c15,c15,c3,N/8);
  /* c0145 <- c0145 + c23 in ]-5q,5q[ */
  poly_add(c0145,c0145,c23,N/8);

  /* c01 <- c01 - c0 - c1 in ]-9q,9q[ */
  poly_sub(c01,c01,c0,N/8); poly_sub(c01,c01,c1,N/8);
  /* c0123 <- c0123 - c02 - c13 in ]-18q,18q[ */
  poly_sub(c0123,c0123,c02,N/8); poly_sub(c0123,c0123,c13,N/8);
  /* c0145 <- c0145 - c04 - c15 in ]-15q,15q[ */
  poly_sub(c0145,c0145,c04,N/8); poly_sub(c0145,c0145,c15,N/8);
  /* c01234567 <- c01234567 - c1357 - c0246 in ]-27q,27q[ --> Reduce c01234567 - c1357 --> Output in [-2^15+6q,2^15-5q[*/
  poly_sub(c01234567,c01234567,c1357,N/8); poly_mersq64(c01234567,c01234567,N/8);
  poly_sub(c01234567,c01234567,c0246,N/8);

  /* End 2nd level */
  /*##############################################################################################*/
  /* Begin 3rd level */

  /* c02 <- c02 + c1 in ]-3q,4q[ */
  poly_add(c02,c02,c1,N/8);
  /* c0246 <- c0246 + c15 in ]-14q,14q[ */
  poly_add(c0246,c0246,c15,N/8);
  /* c04 <- c04 + c13 in ]-11q,11q[ */
  poly_add(c04,c04,c13,N/8);
  /* c0 <- c0 + c1357*ntt_Y in ]-4q,4q[ */
  pointwise_mul_q64(c1357,c1357,ntt_Y_64,N/8); poly_add(c0,c0,c1357,N/8);
  
  /* End 3rd and last level */
  /*##############################################################################################*/
  
  /* Reduce everyone before the inverse NTT ==> coeffs in ]-q,q[ */
  poly_barrettq64(c0,c0,N/8); poly_barrettq64(c01,c01,N/8);
  poly_barrettq64(c02,c02,N/8); poly_barrettq64(c0123,c0123,N/8);
  poly_barrettq64(c04,c04,N/8); poly_barrettq64(c0145,c0145,N/8);
  poly_barrettq64(c0246,c0246,N/8); poly_barrettq64(c01234567,c01234567,N/8);

  invnttq64_64(c0); invnttq64_64(c01);
  invnttq64_64(c02); invnttq64_64(c0123);
  invnttq64_64(c04); invnttq64_64(c0145);
  invnttq64_64(c0246); invnttq64_64(c01234567);
  
  for(unsigned int i = 0 ; i < N/8 ; i++){
    c[8*i] = c0[i]; c[8*i+1] = c01[i];
    c[8*i+2] = c02[i]; c[8*i+3] = c0123[i];
    c[8*i+4] = c04[i]; c[8*i+5] = c0145[i];
    c[8*i+6] = c0246[i]; c[8*i+7] = c01234567[i];
  }
}





/* /\* /\\*########################################### n = 1024 ############################################*\\/ *\/ */
/* /\* /\\*########################################## q = 1409 ############################################*\\/ *\/ */

void poly_product_1024_K0_P4(int16_t *c, const int16_t *a, const int16_t *b){
 int16_t a_tmp[N], b_tmp[N];

  for(int i = 0 ; i < N ; i++){
    a_tmp[i] = a[i];
    b_tmp[i] = b[i];
  }

  nttq64_1024(a_tmp);
  nttq64_1024(b_tmp);

  poly_mul_modq64(c,a_tmp,b_tmp,N,1<<4,ntt_Y_64);

  invnttq64_1024(c);
}

void poly_product_1024_K1_P3(int16_t *c, const int16_t *a, const int16_t *b){

  int16_t a0[N/2], a1[N/2], a01[N/2];
  int16_t b0[N/2], b1[N/2], b01[N/2];
  int16_t c0[N/2], c1[N/2], c2[N/2];

  for(unsigned int i = 0 ; i < N/2 ; i++){
    a0[i] = a[2*i]; a1[i] = a[2*i+1];
    b0[i] = b[2*i]; b1[i] = b[2*i+1];
  }

  nttq64_512(a0); nttq64_512(a1);
  nttq64_512(b0); nttq64_512(b1);
  
  /* Output of the ntt is in [0, q) */
  poly_add(a01,a0,a1,N/2);
  poly_add(b01,b0,b1,N/2);
  
  /* Polynomial products */
  poly_mul_modq64(c0,a0,b0,N/2,1<<3,ntt_Y_64);
  poly_mul_modq64(c1,a01,b01,N/2,1<<3,ntt_Y_64);
  poly_mul_modq64(c2,a1,b1,N/2,1<<3,ntt_Y_64);
  
  /* Reconstruction c1 <- c1 - c0 - c2 in ]-3q,3q[ */
  poly_sub(c1,c1,c0,N/2); poly_sub(c1,c1,c2,N/2);
  /* Reconstruction c0 <- c0 + c2*ntt_Y  ]-2q,2q[*/
  poly_shift_modq64(c2,c2,N/2,1<<3,ntt_Y_64); poly_add(c0,c0,c2,N/2);

  /* Reduction before invntt */
  poly_barrettq64(c0,c0,N/2);
  poly_barrettq64(c1,c1,N/2);
  
  invnttq64_512(c0);
  invnttq64_512(c1);

  for(unsigned int i = 0 ; i < N/2 ; i++){
    c[2*i] = c0[i];
    c[2*i+1] = c1[i];
  }
}

void poly_product_1024_K2_P2(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N/4], a1[N/4], a2[N/4], a3[N/4], a01[N/4], a23[N/4], a02[N/4], a13[N/4], a0123[N/4];

  int16_t b0[N/4], b1[N/4], b2[N/4], b3[N/4], b01[N/4], b23[N/4], b02[N/4], b13[N/4], b0123[N/4];

  int16_t c0[N/4], c1[N/4], c2[N/4], c3[N/4], c01[N/4], c23[N/4], c02[N/4], c13[N/4], c0123[N/4];

  for(unsigned int i = 0 ; i < N/4 ; i++){
    a0[i] = a[4*i]; a1[i] = a[4*i+1]; a2[i] = a[4*i+2]; a3[i] = a[4*i+3];
    b0[i] = b[4*i]; b1[i] = b[4*i+1]; b2[i] = b[4*i+2]; b3[i] = b[4*i+3];
  }

  nttq64_256(a0); nttq64_256(a1); nttq64_256(a2); nttq64_256(a3);
  nttq64_256(b0); nttq64_256(b1); nttq64_256(b2); nttq64_256(b3);
  /* Output of ntt16 in [0,q) */
  
  /* Start computing the sums needed for Karatsuba */
  poly_add(a01,a0,a1,N/4);
  poly_add(a23,a2,a3,N/4);

  poly_add(a02,a0,a2,N/4);
  poly_add(a13,a1,a3,N/4);
  poly_add(a0123,a02,a13,N/4);

  poly_add(b01,b0,b1,N/4);
  poly_add(b23,b2,b3,N/4);

  poly_add(b02,b0,b2,N/4);
  poly_add(b13,b1,b3,N/4);
  poly_add(b0123,b02,b13,N/4);
  /* Output of the sums in [0,2q) or [0,4q)*/
  
  /* Compute the products with Montgomery (extra Montgomery factor will be removed with invntt) */
  poly_mul_modq64(c0,a0,b0,N/4,1<<2,ntt_Y_64);
  poly_mul_modq64(c1,a1,b1,N/4,1<<2,ntt_Y_64);
  poly_mul_modq64(c01,a01,b01,N/4,1<<2,ntt_Y_64);

  poly_mul_modq64(c2,a2,b2,N/4,1<<2,ntt_Y_64);
  poly_mul_modq64(c3,a3,b3,N/4,1<<2,ntt_Y_64);
  poly_mul_modq64(c23,a23,b23,N/4,1<<2,ntt_Y_64);

  poly_mul_modq64(c02,a02,b02,N/4,1<<2,ntt_Y_64);
  poly_mul_modq64(c13,a13,b13,N/4,1<<2,ntt_Y_64);

  poly_mul_modq64(c0123,a0123,b0123,N/4,1<<2,ntt_Y_64);
  /* Output in ]-q,q[ */

  /* Reconstruct first level of Karatsuba */
  /* c02 <- c02 - c0 - c2 ==> c02 in ]-3q,3q[ */
  poly_sub(c02,c02,c0,N/4); poly_sub(c02,c02,c2,N/4);
  /* c13 <- c13 - c1 - c3 ==> c13 in ]-3q, 3q[ */
  poly_sub(c13,c13,c1,N/4); poly_sub(c13,c13,c3,N/4);
  /* c0123 <- c0123 - c01 - c23 ==> c0123 in ]-3q, 3q[ */
  poly_sub(c0123,c0123,c01,N/4); poly_sub(c0123,c0123,c23,N/4);
  /* c0 <- c0 + c2*ntt_Y ==> c0 in ]-2q,2q[ = ]-2q,2q[ */
  poly_shift_modq64(c2,c2,N/4,1<<2,ntt_Y_64); poly_add(c0,c0,c2,N/4);
  /* c1 <- c1 + c3*ntt_Y ==> c1 in ]-2q,2q[ */
  poly_shift_modq64(c3,c3,N/4,1<<2,ntt_Y_64); poly_add(c1,c1,c3,N/4);
  /* c01 <- c01 + c23*ntt_Y ==> c01 in ]-2q,2q[ */
  poly_shift_modq64(c23,c23,N/4,1<<2,ntt_Y_64); poly_add(c01,c01,c23,N/4);

  /* Reconstruct second level of Karatsuba */
  /* c01 <- c01 - c0 - c1 ==> in ]-6q, 6q[ */
  poly_sub(c01,c01,c0,N/4); poly_sub(c01,c01,c1,N/4);
  /* c0123 <- c0123 - c02 - c13 ==> in ]-9q, 9q[ */
  poly_sub(c0123,c0123,c02,N/4); poly_sub(c0123,c0123,c13,N/4);
  /* c0 <- c0 + c13*ntt_Y ==> in ]-5q,5q[ */
  poly_shift_modq64(c13,c13,N/4,1<<2,ntt_Y_64); poly_add(c0,c0,c13,N/4);
  /* c1 <- c1 + c02 ==> in ]-5q,5q[ */
  poly_add(c1,c1,c02,N/4);

  /* Reduce before invntt -> coeffs need to be in [-q,q] */
  poly_barrettq64(c0,c0,N/4);
  poly_barrettq64(c1,c1,N/4);
  poly_barrettq64(c01,c01,N/4);
  poly_barrettq64(c0123,c0123,N/4);
  
  invnttq64_256(c0);
  invnttq64_256(c1);
  invnttq64_256(c01);
  invnttq64_256(c0123);
  
  for(unsigned int i = 0 ; i < N/4 ; i++){
    c[4*i] = c0[i];
    c[4*i+1] = c01[i];
    c[4*i+2] = c1[i];
    c[4*i+3] = c0123[i];
  }
}


void poly_product_1024_K3_P1(int16_t* c, const int16_t* a, const int16_t* b){

  int16_t a0[N>>3], a1[N>>3], a2[N>>3], a3[N>>3], a4[N>>3], a5[N>>3], a6[N>>3], a7[N>>3], a04[N>>3], a26[N>>3], a02[N>>3], a46[N>>3], a0246[N>>3], a15[N>>3], a37[N>>3], a13[N>>3], a57[N>>3], a1357[N>>3], a01[N>>3], a45[N>>3], a0145[N>>3], a23[N>>3], a67[N>>3], a2367[N>>3], a0123[N>>3], a4567[N>>3], a01234567[N>>3];

  int16_t b0[N>>3], b1[N>>3], b2[N>>3], b3[N>>3], b4[N>>3], b5[N>>3], b6[N>>3], b7[N>>3], b04[N>>3], b26[N>>3], b02[N>>3], b46[N>>3], b0246[N>>3], b15[N>>3], b37[N>>3], b13[N>>3], b57[N>>3], b1357[N>>3], b01[N>>3], b45[N>>3], b0145[N>>3], b23[N>>3], b67[N>>3], b2367[N>>3], b0123[N>>3], b4567[N>>3], b01234567[N>>3];

  int16_t c0[N>>3], c1[N>>3], c2[N>>3], c3[N>>3], c4[N>>3], c5[N>>3], c6[N>>3], c7[N>>3], c04[N>>3], c26[N>>3], c02[N>>3], c46[N>>3], c0246[N>>3], c15[N>>3], c37[N>>3], c13[N>>3], c57[N>>3], c1357[N>>3], c01[N>>3], c45[N>>3], c0145[N>>3], c23[N>>3], c67[N>>3], c2367[N>>3], c0123[N>>3], c4567[N>>3], c01234567[N>>3];

  for(unsigned int i = 0 ; i < N/8 ; i++){
    a0[i] = a[8*i]; a1[i] = a[8*i+1]; a2[i] = a[8*i+2]; a3[i] = a[8*i+3];
    a4[i] = a[8*i+4]; a5[i] = a[8*i+5]; a6[i] = a[8*i+6]; a7[i] = a[8*i+7];
    
    b0[i] = b[8*i]; b1[i] = b[8*i+1]; b2[i] = b[8*i+2]; b3[i] = b[8*i+3];
    b4[i] = b[8*i+4]; b5[i] = b[8*i+5]; b6[i] = b[8*i+6]; b7[i] = b[8*i+7];
  }

  nttq64_128(a0); nttq64_128(a1); nttq64_128(a2); nttq64_128(a3);
  nttq64_128(a4); nttq64_128(a5); nttq64_128(a6); nttq64_128(a7);
  
  nttq64_128(b0); nttq64_128(b1); nttq64_128(b2); nttq64_128(b3);
  nttq64_128(b4); nttq64_128(b5); nttq64_128(b6); nttq64_128(b7);
  /* Output of nttq64 in ]-q,q[ */

  /* Compute the different sums and reduce them for the Montgomery multiplications to come */
  poly_add(a04,a0,a4,N/8); poly_add(a26,a2,a6,N/8);
  poly_add(a02,a0,a2,N/8); poly_add(a46,a4,a6,N/8);

  poly_add(a15,a1,a5,N/8); poly_add(a37,a3,a7,N/8);
  poly_add(a13,a1,a3,N/8); poly_add(a57,a5,a7,N/8);

  poly_add(a01,a0,a1,N/8); poly_add(a45,a4,a5,N/8);
  poly_add(a23,a2,a3,N/8); poly_add(a67,a6,a7,N/8);

  poly_add(a0246,a02,a46,N/8); poly_add(a1357,a13,a57,N/8);
  poly_add(a0145,a01,a45,N/8); poly_add(a2367,a23,a67,N/8);
  poly_add(a0123,a01,a23,N/8); poly_add(a4567,a45,a67,N/8);

  poly_add(a01234567,a0123,a4567,N/8);

  poly_add(b04,b0,b4,N/8); poly_add(b26,b2,b6,N/8);
  poly_add(b02,b0,b2,N/8); poly_add(b46,b4,b6,N/8);

  poly_add(b15,b1,b5,N/8); poly_add(b37,b3,b7,N/8);
  poly_add(b13,b1,b3,N/8); poly_add(b57,b5,b7,N/8);

  poly_add(b01,b0,b1,N/8); poly_add(b45,b4,b5,N/8);
  poly_add(b23,b2,b3,N/8); poly_add(b67,b6,b7,N/8);

  poly_add(b0246,b02,b46,N/8); poly_add(b1357,b13,b57,N/8);
  poly_add(b0145,b01,b45,N/8); poly_add(b2367,b23,b67,N/8);
  poly_add(b0123,b01,b23,N/8); poly_add(b4567,b45,b67,N/8);

  poly_add(b01234567,b0123,b4567,N/8);
  /* Output in [0,2q), [0,4q) or  [0,8q) need to reduce [0,8q) to [-4q,4q) otherwise the product won't fit in ]-q*2^15,q*2^15[*/

  /* Compute the products with Montgomery (Montgomery factor will be removed with invntt)*/
  poly_mul_modq64(c0,a0,b0,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c1,a1,b1,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c2,a2,b2,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c3,a3,b3,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c4,a4,b4,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c5,a5,b5,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c6,a6,b6,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c7,a7,b7,N/8,1<<1,ntt_Y_64);

  poly_mul_modq64(c04,a04,b04,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c26,a26,b26,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c02,a02,b02,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c46,a46,b46,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c0246,a0246,b0246,N/8,1<<1,ntt_Y_64);

  poly_mul_modq64(c15,a15,b15,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c37,a37,b37,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c13,a13,b13,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c57,a57,b57,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c1357,a1357,b1357,N/8,1<<1,ntt_Y_64);

  poly_mul_modq64(c01,a01,b01,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c45,a45,b45,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c0145,a0145,b0145,N/8,1<<1,ntt_Y_64);

  poly_mul_modq64(c23,a23,b23,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c67,a67,b67,N/8,1<<1,ntt_Y_64);
  poly_mul_modq64(c2367,a2367,b2367,N/8,1<<1,ntt_Y_64);

  poly_mul_modq64(c0123,a0123,b0123,N/8,1<<1,ntt_Y_64); poly_mul_modq64(c4567,a4567,b4567,N/8,1<<1,ntt_Y_64);

  /* [0,8q) to [-4q,4q) */
  poly_sub_cst(a01234567,a01234567,4*Q64,N/8); poly_sub_cst(b01234567,b01234567,4*Q64,N/8);
  poly_mul_modq64(c01234567,a01234567,b01234567,N/8,1<<1,ntt_Y_64);
  /*############################## Output in ]-q,q[ = ]-3649,3649[ ###############################*/

  /* Start the reconstruction of Karatsuba... */
  /* Begin 1st level */
  
  /* c04 <- c04 - c0 - c4 in ]-3q,3q[*/
  poly_sub(c04,c04,c0,N/8); poly_sub(c04,c04,c4,N/8);
  /* c26 <- c26 - c2 - c6 in ]-3q,3q[ */
  poly_sub(c26,c26,c2,N/8); poly_sub(c26,c26,c6,N/8);
  /* c0246 <- c0246 - c02 - c46 in ]-3q,3q[ */
  poly_sub(c0246,c0246,c02,N/8); poly_sub(c0246,c0246,c46,N/8);

  /* c0 <- c0 + c4*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c4,c4,N/8,1<<1,ntt_Y_64); poly_add(c0,c0,c4,N/8);
  /* c2 <- c2 + c6*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c6,c6,N/8,1<<1,ntt_Y_64); poly_add(c2,c2,c6,N/8);
  /* c02 <- c02 + c46*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c46,c46,N/8,1<<1,ntt_Y_64); poly_add(c02,c02,c46,N/8);

  /* c15 <- c15 - c1 - c5 in ]-3q,3q[*/
  poly_sub(c15,c15,c1,N/8); poly_sub(c15,c15,c5,N/8);
  /* c37 <- c37 - c3 - c7 in ]-3q,3q[ */
  poly_sub(c37,c37,c3,N/8); poly_sub(c37,c37,c7,N/8);
  /* c1357 <- c1357 - c13 - c57 in ]-3q,3q[ */
  poly_sub(c1357,c1357,c13,N/8); poly_sub(c1357,c1357,c57,N/8);

  /* c1 <- c1 + c5*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c5,c5,N/8,1<<1,ntt_Y_64); poly_add(c1,c1,c5,N/8);
  /* c3 <- c3 + c7*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c7,c7,N/8,1<<1,ntt_Y_64); poly_add(c3,c3,c7,N/8);
  /* c13 <- c13 + c57*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c57,c57,N/8,1<<1,ntt_Y_64); poly_add(c13,c13,c57,N/8);

  /* c0145 <- c0145 - c01 - c45 in ]-3q,3q[*/
  poly_sub(c0145,c0145,c01,N/8); poly_sub(c0145,c0145,c45,N/8);
  /* c2367 <- c2367 - c23 - c67 in ]-3q,3q[ */
  poly_sub(c2367,c2367,c23,N/8); poly_sub(c2367,c2367,c67,N/8);
  /* c01234567 <- c01234567 - c0123 - c4567 in ]-3q,3q[ */
  poly_sub(c01234567,c01234567,c0123,N/8); poly_sub(c01234567,c01234567,c4567,N/8);
  
  /* c01 <- c01 + c45*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c45,c45,N/8,1<<1,ntt_Y_64); poly_add(c01,c01,c45,N/8);
  /* c23 <- c23 + c67*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c67,c67,N/8,1<<1,ntt_Y_64); poly_add(c23,c23,c67,N/8);
  /* c0123 <- c0123 + c4567*ntt_Y in ]-2q, 2q[ */
  poly_shift_modq64(c4567,c4567,N/8,1<<1,ntt_Y_64); poly_add(c0123,c0123,c4567,N/8);
  
  /* End 1st level */
  /*##############################################################################################*/
  /* Begin 2nd level */

  /* c02 <- c02 - c0 - c2 in ]-6q,6q[*/
  poly_sub(c02,c02,c0,N/8); poly_sub(c02,c02,c2,N/8);
  /* c0246 <- c0246 - c04 - c26 in ]-9q,9q[ */
  poly_sub(c0246,c0246,c04,N/8); poly_sub(c0246,c0246,c26,N/8);
  
  /* c13 <- c13 - c1 - c3 in ]-6q,6q[*/
  poly_sub(c13,c13,c1,N/8); poly_sub(c13,c13,c3,N/8);
  /* c1357 <- c1357 - c15 - c37 in ]-9q,9q[ */
  poly_sub(c1357,c1357,c15,N/8); poly_sub(c1357,c1357,c37,N/8);

  /* c0123 <- c0123 - c01 - c23 in ]-6q,6q[*/
  poly_sub(c0123,c0123,c01,N/8); poly_sub(c0123,c0123,c23,N/8);
  /* c01234567 <- c01234567 - c0145 - c2367 in ]-9q,9q[ */
  poly_sub(c01234567,c01234567,c0145,N/8); poly_sub(c01234567,c01234567,c2367,N/8);
  
  /* c0 <- c0 + c26*ntt_Y in ]-3q, 3q[ */
  poly_shift_modq64(c26,c26,N/8,1<<1,ntt_Y_64); poly_add(c0,c0,c26,N/8);
  /* c1 <- c1 + c37*ntt_Y in ]-3q, 3q[ */
  poly_shift_modq64(c37,c37,N/8,1<<1,ntt_Y_64); poly_add(c1,c1,c37,N/8);
  /* c01 <- c01 + c2367*ntt_Y in ]-3q, 3q[ */
  poly_shift_modq64(c2367,c2367,N/8,1<<1,ntt_Y_64); poly_add(c01,c01,c2367,N/8);

  /* c04 <- c04 + c2 in ]-5q,5q[ */
  poly_add(c04,c04,c2,N/8);
  /* c15 <- c15 + c3 in ]-5q,5q[ */
  poly_add(c15,c15,c3,N/8);
  /* c0145 <- c0145 + c23 in ]-5q,5q[ */
  poly_add(c0145,c0145,c23,N/8);

  /* c01 <- c01 - c0 - c1 in ]-9q,9q[ */
  poly_sub(c01,c01,c0,N/8); poly_sub(c01,c01,c1,N/8);
  /* c0123 <- c0123 - c02 - c13 in ]-18q,18q[  */
  poly_sub(c0123,c0123,c02,N/8); poly_sub(c0123,c0123,c13,N/8);
  /* c0145 <- c0145 - c04 - c15 in ]-15q,15q[ */
  poly_sub(c0145,c0145,c04,N/8); poly_sub(c0145,c0145,c15,N/8);
  /* c01234567 <- c01234567 - c1357 - c0246 in ]-27q,27q[ --> Reduce c01234567-c1357 to [-2^15+15q,2^15-14q) --> in ]-2^15+6q,2^15-5q[ */
  poly_sub(c01234567,c01234567,c1357,N/8);  poly_mersq64(c01234567,c01234567,N/8);
  poly_sub(c01234567,c01234567,c0246,N/8);

  /* End 2nd level */
  /*##############################################################################################*/
  /* Begin 3rd level */

  /* c02 <- c02 + c1 in ]-9q,9q[ */
  poly_add(c02,c02,c1,N/8);
  /* c0246 <- c0246 + c15 in ]-14q,14q[ */
  poly_add(c0246,c0246,c15,N/8);
  /* c04 <- c04 + c13 in ]-11q,11q[ */
  poly_add(c04,c04,c13,N/8);
  /* c0 <- c0 + c1357*ntt_Y in ]-4q,4q[ */
  poly_shift_modq64(c1357,c1357,N/8,1<<1,ntt_Y_64); poly_add(c0,c0,c1357,N/8);
  
  /* End 3rd and last level */
  /*##############################################################################################*/
  
  /* Reduce everyone before the inverse NTT ==> coeffs in ]-q,q[ */
  poly_barrettq64(c0,c0,N/8); poly_barrettq64(c01,c01,N/8);
  poly_barrettq64(c02,c02,N/8); poly_barrettq64(c0123,c0123,N/8);
  poly_barrettq64(c04,c04,N/8); poly_barrettq64(c0145,c0145,N/8);
  poly_barrettq64(c0246,c0246,N/8); poly_barrettq64(c01234567,c01234567,N/8);

  invnttq64_128(c0); invnttq64_128(c01);
  invnttq64_128(c02); invnttq64_128(c0123);
  invnttq64_128(c04); invnttq64_128(c0145);
  invnttq64_128(c0246); invnttq64_128(c01234567);

  for(unsigned int i = 0 ; i < N/8 ; i++){
    c[8*i] = c0[i]; c[8*i+1] = c01[i];
    c[8*i+2] = c02[i]; c[8*i+3] = c0123[i];
    c[8*i+4] = c04[i]; c[8*i+5] = c0145[i];
    c[8*i+6] = c0246[i]; c[8*i+7] = c01234567[i];
  }
}

#endif
